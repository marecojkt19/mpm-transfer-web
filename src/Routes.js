import { Dashboard, Money, Settings, Transaction, Users, Verification, Wallet } from "./assets/icons";
import {
  ROUTE_ACCOUNTS,
  ROUTE_DASHBOARD,
  ROUTE_DEPOSIT,
  ROUTE_TRANSACTION,
  // ROUTE_TRANSACTION_BY_BULKING,
  // ROUTE_TRANSACTION_BY_API,
  ROUTE_CREATE_TRANSACTION,
  ROUTE_VERIFICATION,
  ROUTE_SETTINGS
} from "./utils/helpers/roleHelper";

const routes = [
  {
    type: 'MAIN',
    title: 'Verifikasi Akun',
    icon: <Verification />,
    url: '/verification',
    routeKey: ROUTE_VERIFICATION,
    isNew: false
  },
  {
    type: 'MAIN',
    title: 'Beranda',
    icon: <Dashboard />,
    url: '/dashboard',
    routeKey: ROUTE_DASHBOARD,
    isNew: false
  },
  // {
  //   type: 'MAIN',
  //   title: 'Verifikasi',
  //   icon: <Verification />,
  //   url: '/verification',
  //   routeKey: ROUTE_VERIFICATION
  // },
  // {
  //   type: 'MAIN',
  //   title: 'Daftar Transaksi',
  //   icon: <Transaction />,
  //   url: '/transaction',
  //   routeKey: ROUTE_TRANSACTION
  // },
  {
    type: 'MAIN',
    title: 'Buat Transaksi',
    icon: <Money />,
    url: '/create-transaction',
    routeKey: ROUTE_CREATE_TRANSACTION,
    isNew: false
  },
  {
    type: 'MAIN',
    title: 'Daftar Transaksi',
    icon: <Transaction />,
    url: '/transaction',
    routeKey: ROUTE_TRANSACTION
  },
  // {
  //   type: 'PARENT',
  //   title: 'Daftar Transaksi',
  //   icon: <Transaction />,
  //   routeKey: ROUTE_TRANSACTION,
  //   url: '/transaction',
  //   child: [
  //     {
  //       type: 'CHILDREN',
  //       title: 'By Bulking',
  //       icon: <Transaction />,
  //       url: '/transaction/bulking',
  //       routeKey: ROUTE_TRANSACTION_BY_BULKING
  //     },
  //     {
  //       type: 'CHILDREN',
  //       title: 'By API',
  //       icon: <Transaction />,
  //       url: '/transaction/api',
  //       routeKey: ROUTE_TRANSACTION_BY_API
  //     },
  //   ]
  // },
  {
    type: 'MAIN',
    title: 'Deposit',
    icon: <Wallet />,
    url: '/deposit',
    routeKey: ROUTE_DEPOSIT,
    isNew: false
  }
]

const adminRoutes = [
  {
    type: 'MAIN',
    title: 'Kelola Multi Akun',
    icon: <Users />,
    url: '/accounts',
    routeKey: ROUTE_ACCOUNTS,
    isNew: false
  },
  {
    type: 'MAIN',
    title: 'Pengaturan',
    icon: <Settings />,
    url: '/settings',
    routeKey: ROUTE_SETTINGS,
    isNew: true
  },
]

export { routes, adminRoutes }
