export async function getFileFromUrl(url, name, defaultType = 'image/jpeg') {
  fetch(url)
    .then((response) => {
      response.blob()
    })
    .then((data) => {
      return new File([data], name, {
        type: data.type || defaultType,
      });
    })
    .catch((err) => console.log(err))
}
