export const removeWhitespace = (str) => {
  if (!str) return str;
  return str.replace(/^\s+|\s+$/g, "")
}

export const censoredName = (str) => {
  if (!str) return str;

  const result = [];
  const arrOfName = str.split(" ");

  for (var item of arrOfName) {
    if (item) {
      const firstChar = item[0];
      const lastChar = item.length < 5 ? item.slice(-1) : item.slice(-2);
      const middleChars = item.length < 4
        ? "*"
        : item.length < 5
          ? "**"
          : "*".repeat(item.length - 3);

      result.push(firstChar + middleChars + lastChar);
    }
  }

  return result.join(" ")
}
