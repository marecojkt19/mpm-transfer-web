import { UserRoleTypes } from "../enums/userRoleTypes";

export const ROUTE_DASHBOARD = 'ROUTE_DASHBOARD';
export const ROUTE_LOGOUT = 'ROUTE_LOGOUT';
export const ROUTE_VERIFICATION = 'ROUTE_VERIFICATION';
export const ROUTE_DEPOSIT = 'ROUTE_DEPOSIT';
export const ROUTE_ACCOUNTS = 'ROUTE_ACCOUNTS';
export const ROUTE_SETTINGS = 'ROUTE_SETTINGS';
export const ROUTE_TRANSACTION = 'ROUTE_TRANSACTION';
export const ROUTE_TRANSACTION_BY_BULKING = 'ROUTE_TRANSACTION_BY_BULKING';
export const ROUTE_TRANSACTION_BY_API = 'ROUTE_TRANSACTION_BY_API';
export const ROUTE_CREATE_TRANSACTION = 'ROUTE_CREATE_TRANSACTION';

class roleHelper {
  static getRoles(routeKey) {
    let allAccess, verifiedStatus, roles, hideForThisCompany;

    switch (routeKey) {
      case ROUTE_DASHBOARD:
        roles = [UserRoleTypes.SUPERVISOR, UserRoleTypes.OPERATOR];
        hideForThisCompany = [];
        verifiedStatus = true;
        break;
      case ROUTE_VERIFICATION:
        roles = [UserRoleTypes.SUPERVISOR, UserRoleTypes.OPERATOR];
        hideForThisCompany = [];
        verifiedStatus = false
        break;
      case ROUTE_TRANSACTION:
        roles = [UserRoleTypes.SUPERVISOR, UserRoleTypes.OPERATOR];
        hideForThisCompany = [];
        verifiedStatus = true;
        break;
      case ROUTE_TRANSACTION_BY_BULKING:
        roles = [UserRoleTypes.SUPERVISOR, UserRoleTypes.OPERATOR];
        hideForThisCompany = [];
        verifiedStatus = true;
        break;
      case ROUTE_TRANSACTION_BY_API:
        roles = [UserRoleTypes.SUPERVISOR, UserRoleTypes.OPERATOR];
        hideForThisCompany = [];
        verifiedStatus = true;
        break;
      case ROUTE_CREATE_TRANSACTION:
        roles = [UserRoleTypes.SUPERVISOR, UserRoleTypes.OPERATOR];
        hideForThisCompany = ['TOPINDOKU'];
        verifiedStatus = true;
        break;
      case ROUTE_DEPOSIT:
        roles = [UserRoleTypes.SUPERVISOR, UserRoleTypes.OPERATOR];
        hideForThisCompany = ['TOPINDOKU'];
        verifiedStatus = true;
        break;
      case ROUTE_ACCOUNTS:
        roles = [UserRoleTypes.SUPERVISOR];
        hideForThisCompany = [];
        verifiedStatus = true;
        break;
      case ROUTE_SETTINGS:
        roles = [UserRoleTypes.SUPERVISOR];
        hideForThisCompany = [];
        verifiedStatus = true;
        break;
      case ROUTE_LOGOUT:
        allAccess = true;
        break;
      default:
        allAccess = false;
        roles = [];
        hideForThisCompany = [];
        break;
    }
    return { allAccess, roles, verifiedStatus, hideForThisCompany };
  }

  static hasAccess(role, verifiedStatusParam, routeKey, companyCode) {
    if (!routeKey) return false
    const { allAccess, roles, verifiedStatus, hideForThisCompany } = this.getRoles(routeKey)
    if (hideForThisCompany.length && verifiedStatus === verifiedStatusParam) return !Boolean(hideForThisCompany.includes(companyCode))
    if (allAccess) return true
    return roles.includes(role) && verifiedStatus === verifiedStatusParam;
  }
}

export default roleHelper;
