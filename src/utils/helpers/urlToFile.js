export async function urlToFile(
  imageSrc,
  filename
) {
  imageSrc = `https://api.allorigins.win/raw?url=${encodeURIComponent(imageSrc)}`
  const fetchImage = await customFetch(imageSrc)
  const blob = await convertToBlob(fetchImage.blob())

  var imageUrl = URL.createObjectURL(blob);

  const file = await toDataURL(imageUrl).then(dataUrl => dataURLtoFile(dataUrl, filename));

  return file
}

function customFetch(url) {
  return new Promise((resolve, reject) => {
    fetch(url).then(response => {
      if (response.ok) {
        resolve(response)
      } else {
        reject(new Error('error'))
      }
    }, error => {
      reject(new Error(error.message))
    })
  })
}
function convertToBlob(promisePayload) {
  return new Promise((resolve, reject) => {
    promisePayload.then(data => {
      resolve(data)
    })
  })
}
const toDataURL = url => fetch(url)
  .then(response => response.blob())
  .then(blob => new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onloadend = () => resolve(reader.result)
    reader.onerror = reject
    reader.readAsDataURL(blob)
  }))
const dataURLtoFile = (dataurl, filename) => {
  var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], filename, { type: mime });
}
