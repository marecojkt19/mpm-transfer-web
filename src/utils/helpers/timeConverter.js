export default function timeConvert(num) {
  const minutes = String(Math.floor(num / 60));
  const second = String(num % 60);
  return `${minutes.length > 1 ? minutes : "0" + minutes}:${second.length > 1 ? second : "0" + second}`
}
