export function combineBy(str = [], separator = ' ') {
	return str.filter(Boolean).join(separator);
}