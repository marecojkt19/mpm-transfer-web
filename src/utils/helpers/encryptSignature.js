import CryptoJS from 'crypto-js';

export const encryptSignature = (scope) => {
  const data = {
    scope,
  };
  const payload = JSON.stringify(data);

  // The secret key and IV should be Hex encoded strings.
  const secretKey = CryptoJS.enc.Hex.parse(process.env.REACT_APP_USER_KEY_SECURITY);
  const iv = CryptoJS.enc.Hex.parse(process.env.REACT_APP_USER_IV_SECURITY);

  const encrypted = CryptoJS.AES.encrypt(payload, secretKey, {
    iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7,
  });

  return encrypted.toString();
};
