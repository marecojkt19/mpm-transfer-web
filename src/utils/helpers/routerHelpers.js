export function getCurrentUrl(location) {
    return location.pathname.split(/[?#]/)[0].split('/')[1];
}

export function checkIsActive(location, url) {
    const current = getCurrentUrl(location);
    const splittedUrl = url.split('/')[1];
    if (!current || !splittedUrl) {
        return false;
    }
    if (current === splittedUrl) {
        return true;
    }

    return false;
}
