const paginate = (limit = 0, page, sort) => `limit=${limit}&skip=${page ? (page - 1) * limit : 0}${sort ? `&sort=${sort}` : ''}`;
export default paginate
