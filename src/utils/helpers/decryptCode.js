import { AES, enc } from 'crypto-js';

export const decryptCode = (encryptedText, secretKey) => {
  const [ivText, encryptedData] = encryptedText.split(':');
  const iv = enc.Base64.parse(ivText);

  const decrypted = AES.decrypt(encryptedData, secretKey, {
    iv,
  });

  return decrypted.toString(enc.Utf8);
};
