import { statusTypes } from "../enums/statusTypes"

export const getStatus = ({ status }) => {
  switch (status) {
    case "Menunggu Pembayaran":
      return (
        <StatusItem
          bgColor="#E3ECFD"
          color="#276EF1"
          status="Menunggu Pembayaran"
          iconName="receipt_long"
        />
      )
    case statusTypes.PENDING:
    case "Menunggu Persetujuan":
      return (
        <StatusItem
          bgColor="#E3ECFD"
          color="#276EF1"
          status="Menunggu Persetujuan"
          iconName="receipt_long"
        />
      )
    case "Menunggu Konfirmasi":
      return (
        <StatusItem
          bgColor="#FFFAF0"
          color="#FF9500"
          status="Menunggu Konfirmasi"
          iconName="av_timer"
        />
      )
    case statusTypes.PROCESSING:
    case "Diproses":
      return (
        <StatusItem
          bgColor="#FFEED8"
          color="#FFC043"
          status="Diproses"
          iconName="autorenew"
        />
      )
    case "Kedaluwarsa":
      return (
        <StatusItem
          bgColor="#EEEEEE"
          color="#757575"
          status="Kedaluwarsa"
          iconName="gpp_maybe"
        />
      )
    case statusTypes.COMPLETED:
      return (
        <StatusItem
          bgColor="#E6F2ED"
          color="#05944F"
          status="Selesai"
          iconName="check_circle"
        />
      )
    case "Berhasil":
      return (
        <StatusItem
          bgColor="#E6F2ED"
          color="#05944F"
          status="Berhasil"
          iconName="check_circle"
        />
      )
    case statusTypes.CANCELED:
    case "Dibatalkan":
      return (
        <StatusItem
          bgColor="#FFEFED"
          color="#E11900"
          status="Dibatalkan"
          iconName="cancel"
        />
      )
    case statusTypes.FAILED:
      return (
        <StatusItem
          bgColor="#FFEFED"
          color="#E11900"
          status="Gagal"
          iconName="cancel"
        />
      )
    default:
      return (
        <div>{status}</div>
      );
  }
}

export const StatusItem = ({ color, bgColor, iconName, status }) => (
  <div style={{ backgroundColor: bgColor, maxWidth: "fit-content" }} className="d-flex align-items-center gap-3 p-10 border-radius-10">
    {
      iconName
      &&
      <span style={{ color }} className="material-symbols-rounded">
        {iconName}
      </span>
    }
    <span style={{ color }} className="font-extrabold">{status}</span>
  </div>
)
