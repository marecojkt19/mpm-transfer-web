class PaymentMethodTypes {
  static DEPOSIT = 'DEPOSIT';
  static BANK_TRANSFER = 'BANK_TRANSFER';
  static PAYLATER = 'PAY_LATER';

  static getStr(en) {
    switch (en) {
      case PaymentMethodTypes.DEPOSIT:
        return 'Deposit';
      case PaymentMethodTypes.BANK_TRANSFER:
        return 'Transfer Bank';
      case PaymentMethodTypes.PAYLATER:
        return 'Paylater';
      default:
        return 'Unknown';
    }
  }
}

const PaymentMethodOpts = [
  {
    value: PaymentMethodTypes.DEPOSIT,
    label: PaymentMethodTypes.getStr(PaymentMethodTypes.DEPOSIT),
  },
  {
    value: PaymentMethodTypes.BANK_TRANSFER,
    label: PaymentMethodTypes.getStr(PaymentMethodTypes.BANK_TRANSFER),
  },
  {
    value: PaymentMethodTypes.PAYLATER,
    label: PaymentMethodTypes.getStr(PaymentMethodTypes.PAYLATER),
  }
];

export { PaymentMethodTypes, PaymentMethodOpts };
