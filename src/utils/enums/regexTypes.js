export const numberRegExp = /^[0-9]*$/;
export const noWhiteSpace = /^\S*$/;
export const phoneNumberRegex = /^\+[0-9]*$/;
export const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!#%?])[A-Za-z\d!#%?]{8,}$/;
