class statusTypes {
  static WAITING_CONFIRMATION = 'WAITING_CONFIRMATION';
  static WAITING_PAYMENT = 'WAITING_PAYMENT';
  static EXPIRED = 'EXPIRED';
  static SUCCESS = 'SUCCESS';
  static COMPLETED = 'COMPLETED';
  static PROCESSING = 'PROCESSING';
  static FAILED = 'FAILED';
  static CANCELED = 'CANCELED';
  static PENDING = 'PENDING';

  static getStr(en) {
    switch (en) {
      case "WAITING_CONFIRMATION":
        return 'Menunggu Konfirmasi';
      case "WAITING_PAYMENT":
        return 'Menunggu Pembayaran';
      case "PENDING":
        return 'Menunggu persetujuan';
      case "EXPIRED":
        return 'Kadaluarsa';
      case "COMPLETED":
      case "SUCCESS":
        return 'Berhasil';
      case "CANCELED":
        return 'Dibatalkan';
      case "FAILED":
        return 'Failed';
      case "PROCESSING":
        return 'Diproses';
      default:
        return '';
    }
  }
}
const statusTypesOpts = [
  { value: statusTypes.WAITING_CONFIRMATION, label: statusTypes.getStr(statusTypes.WAITING_CONFIRMATION) },
  { value: statusTypes.WAITING_PAYMENT, label: statusTypes.getStr(statusTypes.WAITING_PAYMENT) },
  { value: statusTypes.EXPIRED, label: statusTypes.getStr(statusTypes.EXPIRED) },
  { value: statusTypes.SUCCESS, label: statusTypes.getStr(statusTypes.SUCCESS) },
  { value: statusTypes.COMPLETED, label: statusTypes.getStr(statusTypes.COMPLETED) },
  { value: statusTypes.CANCELED, label: statusTypes.getStr(statusTypes.CANCELED) },
  { value: statusTypes.PENDING, label: statusTypes.getStr(statusTypes.PENDING) },
  { value: statusTypes.FAILED, label: statusTypes.getStr(statusTypes.FAILED) },
  { value: statusTypes.PROCESSING, label: statusTypes.getStr(statusTypes.PROCESSING) },
];
export { statusTypes, statusTypesOpts };
