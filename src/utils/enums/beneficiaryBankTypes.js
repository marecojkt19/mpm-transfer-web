class BeneficiaryBankTypes {
  static BNI = 'BNI';
  static MANDIRI = 'MANDIRI';
  static BCA = 'BCA';

  static getStr(en) {
    switch (en) {
      case "BNI":
        return 'BNI';
      case "MANDIRI":
        return 'MANDIRI';
      case "BCA":
        return 'BCA';
      default:
        return '';
    }
  }
}
const BeneficiaryBankOpts = [
  { value: BeneficiaryBankTypes.BNI, label: BeneficiaryBankTypes.getStr(BeneficiaryBankTypes.BNI) },
  { value: BeneficiaryBankTypes.MANDIRI, label: BeneficiaryBankTypes.getStr(BeneficiaryBankTypes.MANDIRI) },
  { value: BeneficiaryBankTypes.BCA, label: BeneficiaryBankTypes.getStr(BeneficiaryBankTypes.BCA) },
];
export { BeneficiaryBankTypes, BeneficiaryBankOpts };
