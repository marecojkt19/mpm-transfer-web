class UserRoleTypes {
  static SUPERVISOR = 'SUPERVISOR';
  static OPERATOR = 'OPERATOR';
  static RETAIL = 'RETAIL';
  static ADMIN = 'ADMIN';
  static DEVELOPER = 'DEVELOPER';

  static getStr(en) {
    switch (en) {
      case UserRoleTypes.SUPERVISOR:
        return 'Admin';
      case UserRoleTypes.OPERATOR:
        return 'Maker';
      case UserRoleTypes.RETAIL:
        return 'RETAIL';
      case UserRoleTypes.ADMIN:
        return 'Admin';
      case UserRoleTypes.DEVELOPER:
        return 'Developer';
      default:
        return 'Unknown';
    }
  }

  static getModel(en) {
    switch (en) {
      case UserRoleTypes.SUPERVISOR:
        return 'ADMIN';
      case UserRoleTypes.OPERATOR:
        return 'MAKER';
      case UserRoleTypes.RETAIL:
        return 'RETAIL';
      case UserRoleTypes.ADMIN:
        return 'ADMIN';
      case UserRoleTypes.DEVELOPER:
        return 'DEVELOPER';
      default:
        return '';
    }
  }
}

export { UserRoleTypes };
