class BankTypes {
  static BCA = 'BCA';
  static MANDIRI = 'MANDIRI';

  static getStr(en) {
    switch (en) {
      case "BCA":
        return 'BCA';
      case "MANDIRI":
        return 'Bank Mandiri';
      default:
        return '';
    }
  }
}
const BankOpts = [
  { value: BankTypes.BCA, label: BankTypes.getStr(BankTypes.BCA), logo: "/assets/media/banks/BCA.png" },
  { value: BankTypes.MANDIRI, label: BankTypes.getStr(BankTypes.MANDIRI), logo: "/assets/media/banks/Mandiri.png" },
];
export { BankTypes, BankOpts };
