class TransactionStatusTypes {
  static PENDING = 'PENDING';
  static COMPLETED = 'COMPLETED';
  static FAILED = 'FAILED';
  static getStr(en) {
    switch (en) {
      case "PENDING":
        return 'Sedang di proses';
      case "COMPLETED":
        return 'Selesai';
      case "FAILED":
        return 'Gagal';
      default:
        return '';
    }
  }
}
const TransactionStatusOpts = [
  { value: TransactionStatusTypes.PENDING, label: TransactionStatusTypes.getStr(TransactionStatusTypes.PENDING) },
  { value: TransactionStatusTypes.COMPLETED, label: TransactionStatusTypes.getStr(TransactionStatusTypes.COMPLETED) },
  { value: TransactionStatusTypes.FAILED, label: TransactionStatusTypes.getStr(TransactionStatusTypes.FAILED) }
];
export { TransactionStatusTypes, TransactionStatusOpts };
