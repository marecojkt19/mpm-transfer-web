class HistoryDepositStatusTypes {
  static WAITING_CONFIRMATION = 'WAITING_CONFIRMATION';
  static WAITING_PAYMENT = 'WAITING_PAYMENT';
  static EXPIRED = 'EXPIRED';
  static SUCCESS = 'SUCCESS';
  static CANCELED = 'CANCELED';

  static getStr(en) {
    switch (en) {
      case "WAITING_CONFIRMATION":
        return 'Menunggu Konfirmasi';
      case "WAITING_PAYMENT":
        return 'Menunggu Pembayaran';
      case "EXPIRED":
        return 'Kedaluwarsa';
      case "SUCCESS":
        return 'Berhasil';
      case "CANCELED":
        return 'Dibatalkan';
      default:
        return '';
    }
  }
}
const HistoryDepositStatusOpts = [
  { value: HistoryDepositStatusTypes.WAITING_PAYMENT, label: HistoryDepositStatusTypes.getStr(HistoryDepositStatusTypes.WAITING_PAYMENT) },
  { value: HistoryDepositStatusTypes.WAITING_CONFIRMATION, label: HistoryDepositStatusTypes.getStr(HistoryDepositStatusTypes.WAITING_CONFIRMATION) },
  { value: HistoryDepositStatusTypes.EXPIRED, label: HistoryDepositStatusTypes.getStr(HistoryDepositStatusTypes.EXPIRED) },
  { value: HistoryDepositStatusTypes.SUCCESS, label: HistoryDepositStatusTypes.getStr(HistoryDepositStatusTypes.SUCCESS) },
  { value: HistoryDepositStatusTypes.CANCELED, label: HistoryDepositStatusTypes.getStr(HistoryDepositStatusTypes.CANCELED) },
];
export { HistoryDepositStatusTypes, HistoryDepositStatusOpts };
