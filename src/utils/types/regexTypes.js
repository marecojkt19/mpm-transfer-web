export const numberRegExp = /^[0-9]*$/;
export const noWhiteSpace = /^\S*$/;
export const phoneNumberRegex = /^\+[0-9]*$/;
export const passwordRegex = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$!%*?&^~#(){}[\]|/\\=+<>:_;,"'])\S{8,}$/;
export const emailRegex = /^$|^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
