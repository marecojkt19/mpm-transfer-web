// eslint-disable-next-line import/no-anonymous-default-export
export default [
  {
    email: 'novi@mareco.id',
    role: 'Admin',
    status: 'Aktif',
    createdAt: '5 Apr 2020, 14:45'
  },
  {
    email: 'tamara@mareco.id',
    role: 'Maker',
    status: 'Aktif',
    createdAt: '5 Apr 2020, 14:45'
  }
];
