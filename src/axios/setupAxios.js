import { authActions } from "../redux/actions/authActions";
import { ME_URL } from "./services/authService";

export default function setupAxios(axios, store) {
  axios.defaults.baseURL = process.env.REACT_APP_BASE_API;
  axios.interceptors.request.use(
    config => {
      const {
        auth: { authToken }
      } = store.getState();

      if (authToken) {
        config.headers.Authorization = `Bearer ${authToken}`;
      }

      return config;
    },
    err => Promise.reject(err)
  );
  axios.interceptors.response.use(
    response => response.data,
    async err => {
      // Set message if no response returned from Request
      if (!err?.response) err.response = {
        body: {
          message: "Connection Timed Out"
        }
      }

      if (err.response?.config?.url === ME_URL) {
        store.dispatch(authActions.logout())
      }

      // Return Error
      return Promise.reject(err)
    }
  );
}
