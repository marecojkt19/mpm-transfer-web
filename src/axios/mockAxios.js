import MockAdapter from "axios-mock-adapter";
import mockAccounts from "./mocks/mockAccounts";
import mockAuth from "./mocks/mockAuth";
import mockTransactions from "./mocks/mockTransaction";

const mockAxios = (axios) => {
  // Code to track Every Request been called
  axios.interceptors.request.use(
    config => {
      console.log('[Axios Request]', config)
      return config;
    }
  );
  axios.interceptors.response.use(
    response => {
      console.log('[Axios Response Success]', response)
      return response
    },
    err => {
      console.log('[Axios Response Error]', err?.response)
      return Promise.reject(err)
    },
  );

  const mock = new MockAdapter(axios, { delayResponse: 300 });

  mockAuth(mock);
  mockTransactions(mock);
  mockAccounts(mock);

  return mock;
}

export default mockAxios
