import axios from "axios";

export const GENERATE_CREDENTIALS_URL = "/companies/generate-credentials";
export const UPDATE_PUBLIC_KEY = (id) => `/companies/${id}/update`

const credentialService = {
  generateCredentials: () => axios.get(GENERATE_CREDENTIALS_URL),
  updatePublicKey: (id, body) => axios.patch(UPDATE_PUBLIC_KEY(id), body),
}

export default credentialService
