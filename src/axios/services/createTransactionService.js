import axios from "axios";
import jsonToFormData from "../../utils/helpers/jsonToFormData";
import paginate from "../../utils/helpers/paginate";

export const UPLOAD_XLSX_URL = "transactions/makers/upload-transaction";
export const CREATE_TRANSACTION = "transactions/makers/create-transaction";
export const INQUIRY_SINGLE = "transactions/makers/inquiry-single";

export const GET_LIST_TRANSACTION = (id) => `transactions/makers/${id}/list-transaction`
export const DETAIL_TRANSACTION = (id) => `transactions/makers/${id}/detail-transaction`;
export const DELETE = (id) => `transactions/makers/${id}/delete`;
export const CANCEL_CREATE = (id) => `transactions/makers/${id}/cancel`;
export const CANCEL_UPLOAD_URL = "transactions/makers/cancel-upload";
export const ADJUSTMENT_NAME_BULK = (orderId) => `/transactions/makers/${orderId}/customize-name`;
export const UPDATE_TRANSACTION = (id) => `/transactions/makers/${id}/update`;
export const VALIDATE_ACCOUNT_NUMBER = "transactions/makers/validate-account-number";

const createTransactionService = {
  uploadClsx: (data) => {
    return axios.post(UPLOAD_XLSX_URL, jsonToFormData(data))
  },
  list: ({ id, isNameMatch, validateAccount, page, limit }) => {
    let url = `${GET_LIST_TRANSACTION(id)}?${paginate(limit, page)}`;
    if (isNameMatch) {
      url += `&isNameMatch=${isNameMatch}`;
    }
    if (validateAccount) {
      url += `&validateAccount=${validateAccount}`;
    }
    return axios.get(url)
  },
  detail: (id) => {
    return axios.get(DETAIL_TRANSACTION(id))
  },
  create: (id) => {
    return axios.post(CREATE_TRANSACTION, { transactionId: id })
  },
  inquirySingle: (data) => {
    return axios.post(INQUIRY_SINGLE, data)
  },
  adjustmentNameBulk: (id) => {
    return axios.patch(ADJUSTMENT_NAME_BULK(id))
  },
  updateTransaction: (id, data) => {
    return axios.patch(UPDATE_TRANSACTION(id), data)
  },
  cancelCreate: (id) => {
    return axios.post(CANCEL_CREATE(id))
  },
  cancelUpload: () => {
    return axios.post(CANCEL_UPLOAD_URL)
  },
  validateAccountNumber: (data) => {
    return axios.patch(VALIDATE_ACCOUNT_NUMBER, data)
  },
  delete: (id) => {
    return axios.delete(DELETE(id))
  }
}

export default createTransactionService
