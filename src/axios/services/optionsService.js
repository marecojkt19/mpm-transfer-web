import axios from "axios";

export const TRX_VOLUMES_LIST_URL = "/monthly-trx-volumes";
export const BUSINESS_ENTITY_LIST_URL = "/business-entity-forms";
export const BUSINESS_INDUSTRIES_LIST_URL = "/business-industries";
export const JOB_DEPARTMENTS_LIST_URL = "/job-departments";
export const JOB_POSITIONS_LIST_URL = "/job-positions";

export const OCCUPATIONS_LIST_URL = "/occupations";
export const USAGE_PURPOSES_LIST_URL = "/usage-purposes";

const optionsService = {
  monthlyTransactionVolumeList: () => {
    return axios.get(TRX_VOLUMES_LIST_URL)
  },
  businessEntityList: () => {
    return axios.get(BUSINESS_ENTITY_LIST_URL)
  },
  businessIndustryList: () => {
    return axios.get(BUSINESS_INDUSTRIES_LIST_URL)
  },
  jobDepartmentList: () => {
    return axios.get(JOB_DEPARTMENTS_LIST_URL)
  },
  jobPositionList: () => {
    return axios.get(JOB_POSITIONS_LIST_URL)
  },
  occupationList: () => {
    return axios.get(OCCUPATIONS_LIST_URL)
  },
  usagePurposeList: () => {
    return axios.get(USAGE_PURPOSES_LIST_URL)
  }
}

export default optionsService
