import axios from "axios";
import jsonToFormData from "../../utils/helpers/jsonToFormData";
import paginate from "../../utils/helpers/paginate";

export const CREATE_DEPOSIT_URL = "deposits/create";
export const HISTORY_LIST_URL = "deposits/list";
export const CHECK_WAITING_DEPOSIT = "deposits/check-waiting-deposit"
export const DEPOSIT_DETAIL_URL = (id) => `deposits/${id}/detail`;
export const CANCEL_DEPOSIT_URL = (id) => `deposits/${id}/cancel`;
export const UPLOAD_PROOF_URL = (id) => `deposits/${id}/upload-proof`;

const depositService = {
  create: (data) => {
    return axios.post(CREATE_DEPOSIT_URL, data)
  },
  historyList: ({ page, limit, sort, search, startDate, endDate, filter, bankName }) => {
    let url = `${HISTORY_LIST_URL}?${paginate(limit, page, sort)}`;
    if (search) {
      url += `&search=${search}`;
    }
    if (startDate) {
      url += `&fromDate=${encodeURIComponent(startDate)}`;
    }
    if (endDate) {
      url += `&toDate=${encodeURIComponent(endDate)}`;
    }
    if (bankName) {
      url += `&bankName=${bankName}`;
    }
    if (filter) {
      url += `&filter=${filter}`;
    }
    return axios.get(url)
  },
  detail: (id) => axios.get(DEPOSIT_DETAIL_URL(id)),
  cancel: (id) => axios.patch(CANCEL_DEPOSIT_URL(id)),
  checkWaitingDeposit: () => axios.get(CHECK_WAITING_DEPOSIT),
  uploadProof: (id, file) => axios.patch(UPLOAD_PROOF_URL(id), jsonToFormData({ paymentProof: file }))
}

export default depositService;
