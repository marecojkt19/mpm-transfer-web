import axios from "axios";

export const CITIES_URL = "/cities";
export const PROVINCE_URL = "/provinces";

const commonService = {
  cities: (companyProvinceCode) => {
    let url = CITIES_URL;
    if (companyProvinceCode) {
      url += `?provinceCode=${companyProvinceCode}`;
    }
    return axios.get(url)
  },
  province: () => axios.get(PROVINCE_URL)
}

export default commonService
