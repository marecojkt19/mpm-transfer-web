import axios from "axios";
export const LIST_URL = "banks/list-enum";
export const LIST_BANK_URL = "banks/bank-list"
export const CHECK_ACCOUNT_NUMBER_URL = "banks/check-account-number"

const bankService = {
  list: ({ filter }) => {
    let url = `${LIST_URL}?limit=0&skip=0`;
    if (filter) {
      url += `&filter=${filter}`;
    }
    return axios.get(url)
  },
  listBank: () => {
    return axios.get(LIST_BANK_URL)
  },
  checkAccountNumber: (data) => {
    return axios.post(CHECK_ACCOUNT_NUMBER_URL, data)
  }
}

export default bankService
