import axios from "axios";
import { encryptSignature } from "../../utils/helpers/encryptSignature";

export const REQUEST_OTP_URL = "authentication-gateways/request-otp/verif";
export const VALIDATE_OTP_URL = "authentication-gateways/validate-otp/verif";

const userVerificationService = {
  requestOtp: (data) =>
    axios.post(REQUEST_OTP_URL, data, {
      headers: {
        "x-signature": encryptSignature(["/authentication-gateways/request-otp/verif"]),
      }
    }),
  validateOtp: (data, signature) =>
    axios.post(VALIDATE_OTP_URL, data, {
      headers: {
        "x-signature": signature,
      }
    })
}

export default userVerificationService
