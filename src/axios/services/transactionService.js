import axios from "axios";
import jsonToFormData from "../../utils/helpers/jsonToFormData";
import paginate from "../../utils/helpers/paginate";

export const PAYMENT_URL = (id) => `orders/${id}/approve `;
export const INVOICE_URL = (id) => `transactions/${id}/detail-invoice`;
export const CANCEL_URL = (id) => `transactions/${id}/cancel`;
export const TRANSACTION_LIST_URL = (id) => `transactions/${id}/list`;
export const SEND_FUND = (orderId) => `transactions/${orderId}/send-fund`;
export const DETAIL_URL = (id) => `transactions/${id}/detail-transaction`;
export const TRANSFER_FEE_DETAILS_URL = "transactions/payment";
export const ORDER_LIST_URL = "transactions/all-transaction";
export const ORDER_BY_API_URL = "orders/list";
export const VALIDATE_OTP_URL = "transactions/validate-otp";
export const REQUEST_OTP_URL = "transactions/request-otp";
export const UPLOAD_XLSX_URL = "orders/upload-xlsx";
export const LIST_BANK_URL = "orders/list-bank";
export const CREATE_TRANSACTION_BY_ADMIN = (type) => `transactions/makers/create/${type}`;

const transactionService = {
  transactionList: ({ page, limit, sort, id }) => {
    let url = `${TRANSACTION_LIST_URL(id)}?${paginate(limit, page, sort)}`;
    return axios.get(url)
  },
  createByAdmin: (data, type) => {
    return axios.post(CREATE_TRANSACTION_BY_ADMIN(type), data)
  },
  payment: (id, data) => {
    return axios.post(PAYMENT_URL(id), data)
  },
  invoice: (id) => {
    return axios.get(INVOICE_URL(id))
  },

  // New API
  orderList: ({ page, limit, sort, search, startDate, endDate, filter }) => {
    let url = `${ORDER_LIST_URL}?${paginate(limit, page, sort)}`;
    if (search) {
      url += `&search=${search}`;
    }
    if (startDate) {
      url += `&fromDate=${encodeURIComponent(startDate)}`;
    }
    if (endDate) {
      url += `&toDate=${encodeURIComponent(endDate)}`;
    }
    if (filter) {
      url += `&filter=${filter}`;
    }
    return axios.get(url)
  },
  orderByAPI: ({ page, limit, sort, search, startDate, endDate, status, bank }) => {
    let url = `${ORDER_BY_API_URL}?${paginate(limit, page, sort)}`;
    if (search) {
      url += `&search=${search}`;
    }
    if (startDate) {
      url += `&fromDate=${encodeURIComponent(startDate)}`;
    }
    if (endDate) {
      url += `&toDate=${encodeURIComponent(endDate)}`;
    }
    if (status) {
      url += `&filter=${status}`;
    }
    if (bank) {
      url += `&bank=${bank}`;
    }
    return axios.get(url);
  },
  orderListByType: ({ page, limit, sort, search, startDate, endDate, status, bank, type }) => {
    let url = `${ORDER_BY_API_URL}?type=${type}&${paginate(limit, page, sort)}`;
    if (search) {
      url += `&search=${search}`;
    }
    if (startDate) {
      url += `&fromDate=${encodeURIComponent(startDate)}`;
    }
    if (endDate) {
      url += `&toDate=${encodeURIComponent(endDate)}`;
    }
    if (status) {
      url += `&filter=${status}`;
    }
    if (bank) {
      url += `&bank=${bank}`;
    }
    return axios.get(url);
  },
  sendFund: ({ orderId, morph, type, otp, approveOrderDto }) => {
    let url = SEND_FUND(orderId);
    return axios.post(url, {
      approveOrderDto: approveOrderDto,
      otp: {
        morph: morph,
        type: type,
        token: otp
      }
    })
  },
  cancel: (orderId) => {
    return axios.get(CANCEL_URL(orderId))
  },
  getListBank: () => {
    return axios.get(LIST_BANK_URL)
  },
  uploadClsx: (data) => {
    axios.post(UPLOAD_XLSX_URL, jsonToFormData(data))
  },
  validateOtp: (data) => {
    axios.post(VALIDATE_OTP_URL, jsonToFormData(data))
  },
  requestOTP: ({ morph, type }) => {
    return axios.post(REQUEST_OTP_URL, {
      morph: morph,
      type: type
    })
  },
  transferFeeDetails: (id) => {
    let url = TRANSFER_FEE_DETAILS_URL;
    if (id) {
      url += `?transactionCode=${id}`
    }
    return axios.get(url);
  },
  detail: ({ trxCode, limit, page, sort, status, bank, search }) => {
    let url = `${DETAIL_URL(trxCode)}?${paginate(limit, page, sort)}`;
    if (status) {
      url += `&filter=${status}`;
    }
    if (search) {
      url += `&search=${search}`;
    }
    if (bank) {
      url += `&bank=${bank}`;
    }
    return axios.get(url);
  }
}

export default transactionService
