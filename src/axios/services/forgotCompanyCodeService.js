import axios from "axios";

export const FORGOT_COMPANY_CODE_URL = "users/forgot-company-code";

const forgotCompanyCodeService = {
  forgotCompanyCode: (email) => {
    return axios.post(FORGOT_COMPANY_CODE_URL, email)
  }
}

export default forgotCompanyCodeService
