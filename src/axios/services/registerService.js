import axios from "axios";
import jsonToFormData from "../../utils/helpers/jsonToFormData";
import { encryptSignature } from "../../utils/helpers/encryptSignature";

export const REGISTER_URL = "authentication-gateways/register";
export const REQUEST_OTP_URL = "authentication-gateways/request-otp";
export const VALIDATE_OTP_URL = "authentication-gateways/validate-otp";

const registerService = {
  register: (data, signature) =>
    axios.post(REGISTER_URL, jsonToFormData(data), {
      headers: {
        "x-signature": signature,
      }
    }),
  requestOtp: (data) =>
    axios.post(REQUEST_OTP_URL, data, {
      headers: {
        "x-signature": encryptSignature(["/authentication-gateways/request-otp"]),
      }
    }),
  validateOtp: (data, signature) =>
    axios.post(VALIDATE_OTP_URL, data, {
      headers: {
        "x-signature": signature,
      }
    })
}

export default registerService
