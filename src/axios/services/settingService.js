import axios from "axios";
import jsonToFormData from "../../utils/helpers/jsonToFormData";

export const UPDATE_PROFILE_URL = (id) => `settings/${id}/update-company-information`
export const REQUEST_OTP_URL = (id) => `settings/${id}/request-otp`;
export const VALIDATE_OTP_URL = (id) => `settings/${id}/validate-otp`;
export const CHECK_PHONE_NUMBER = (id) => `settings/${id}/check-phone`;
export const NOTIFICATION_DEPOSIT_URL = companyId => `settings/${companyId}/update-notification-minimum-deposit`;
export const DETAIL_PROFILE_URL = companyId => `settings/${companyId}/detail`;

const settingService = {
  updateProfile: ({ id, data }) => axios.post(UPDATE_PROFILE_URL(id), jsonToFormData(data)),
  detailProfile: ({ id }) => axios.get(DETAIL_PROFILE_URL(id)),
  checkPhoneNumber: (data, id) => axios.post(CHECK_PHONE_NUMBER(id), data),
  validateOtp: (data, id) => axios.post(VALIDATE_OTP_URL(id), data),
  requestOtp: (data, id) => axios.post(REQUEST_OTP_URL(id), data),
  notification: (data, id) => axios.post(NOTIFICATION_DEPOSIT_URL(id), data)
}

export default settingService
