

import axios from "axios";
export const NOTIFICATION_TRANSACTION_URL = "notifications/transactions";

const notificationTransactionService = {
  notification: () => axios.get(NOTIFICATION_TRANSACTION_URL),
}

export default notificationTransactionService
