import axios from "axios";

export const HOME_REPORT_URL = "orders/home-report";
export const ANALYTIC_URL = "orders/statistic";

const dashboardService = {
  homeReport: () => axios.get(HOME_REPORT_URL),
  analytic: ({
    timeRange,
    filter
  }) => {
    let url = ANALYTIC_URL;
    if (timeRange) {
      url += `?timeRange=${timeRange}`;
    }
    if (filter) {
      url += `&filter=${filter}`;
    }
    return axios.get(url)
  }
}

export default dashboardService
