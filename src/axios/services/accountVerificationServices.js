import axios from "axios";
import jsonToFormData from "../../utils/helpers/jsonToFormData";

export const DATA_VERIFICATION = id => `users/${id}/add-verification-data`;
export const UPLOAD_DOCUMENT = id => `users/${id}/upload-verification-doc`;
export const CHANGE_STEP = id => `users/${id}/change-verif-step`;
export const GET_SUMMARY = id => `users/${id}/verification-summary`;

const accountVerificationServices = {
  dataVerification: (data, id, signature) => {
    return axios.post(DATA_VERIFICATION(id), data, {
      headers: {
        "x-signature": signature,
      }
    })
  },
  uploadDocument: (data, id, signature) => {
    return axios.post(UPLOAD_DOCUMENT(id), jsonToFormData(data), {
      headers: {
        "x-signature": signature,
      }
    })
  },
  changeStep: (toStep, id, signature) => {
    return axios.patch(CHANGE_STEP(id), toStep, {
      headers: {
        "x-signature": signature,
      }
    })
  },
  getSummary: ({ id, signature }) => {
    return axios.get(GET_SUMMARY(id), {
      headers: {
        "x-signature": signature,
      }
    })
  }
}

export default accountVerificationServices;
