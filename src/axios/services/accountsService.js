import axios from "axios";
import paginate from "../../utils/helpers/paginate";
export const LIST_URL = "users/list";
export const CREATE_URL = "users/register-multi-account";
export const DETAIL_URL = (id) => `users/${id}/detail`;
export const UPDATE_URL = (id) => `user-roles/${id}/update`;
export const DELETE_URL = (id) => `user-roles/${id}/delete`;

export const CHECK_EMAIL_URL = (email) => `users/${email}/check-email`;
export const INVITE_EMAIL_URL = 'users/invite-email'

const accountsService = {
  list: ({ page, limit, sort, search, startDate, endDate, filter }) => {
    let url = `${LIST_URL}?${paginate(limit, page, sort)}`;
    if (search) {
      url += `&search=${search}`;
    }
    if (startDate) {
      url += `&fromDate=${encodeURIComponent(startDate)}`;
    }
    if (endDate) {
      url += `&toDate=${encodeURIComponent(endDate)}`;
    }
    if (filter) {
      url += `&filter=${filter}`;
    }
    return axios.get(url)
  },
  create: (data) => axios.post(CREATE_URL, data),
  update: ({ userId, data }) => axios.patch(UPDATE_URL(userId), data),
  delete: ({ userId, data }) => axios.patch(DELETE_URL(userId), data),
  detail: ({ userId, companyCode }) => {
    let url = DETAIL_URL(userId);
    if (companyCode) {
      url += `?companyCode=${companyCode}`;
    }
    return axios.get(url, { companyCode })
  },

  checkEmail: (email) => axios.get(CHECK_EMAIL_URL(email)),
  inviteEmail: (data) => axios.post(INVITE_EMAIL_URL, data)
}

export default accountsService
