import {
  ORDER_LIST_URL
} from "../services/transactionService";
import transactionsTable from "../tables/transactionTable";

export default function mockTransactions(mock) {
  mock.onGet(ORDER_LIST_URL).reply(() => {
    return [200, {
      data: {
        transactions: transactionsTable,
        count: 4
      }
    }];
  });
}
