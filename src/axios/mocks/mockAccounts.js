import {
  LIST_URL
} from "../services/accountsService";
import accountsTable from "../tables/accountsTable";

export default function mockAccounts(mock) {
  mock.onGet(LIST_URL).reply(() => {
    return [200, {
      data: {
        accounts: accountsTable,
        count: 2
      }
    }];
  });
}
