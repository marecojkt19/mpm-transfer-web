import {
  LOGIN_URL,
  ME_URL
} from "../services/authService";
import userTableMock from "../tables/userTable";

export default function mockAuth(mock) {
  mock.onPost(LOGIN_URL).reply(({ data }) => {
    const { code, email, password } = JSON.parse(data);

    if (code && email && password) {
      const user = userTableMock.find(
        x =>
          x.company.code === code &&
          x.user.password === password &&
          x.user.email === email
      );

      if (user) {
        return [200, {
          data: {
            user: user.user,
            accessToken: user.accessToken,
            refreshToken: user.refreshToken,
            company: user.company
          }
        }];
      }
    }

    return [400, {
      message: 'Kredensial tidak valid'
    }];
  });

  mock.onGet(ME_URL).reply(({ headers: { Authorization } }) => {
    const accessToken =
      Authorization &&
      Authorization.startsWith("Bearer ") &&
      Authorization.slice("Bearer ".length);

    if (accessToken) {
      const user = userTableMock.find(x => x.accessToken === accessToken);

      if (user) {
        return [200, {
          data: {
            user: user.user,
            accessToken: user.accessToken,
            refreshToken: user.refreshToken,
            company: user.company
          }
        }];
      }
    }

    return [401];
  });
}
