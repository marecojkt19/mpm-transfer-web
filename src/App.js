import { MantineProvider } from '@mantine/core';
import React from "react";
import { Helmet } from "react-helmet";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import { ToastProvider } from 'react-toast-notifications';
import { ToastContainer } from 'react-toastify';
import { PersistGate } from "redux-persist/integration/react";
import BaseRoutes from "./BaseRoutes";
import Toast from "./components/Toast";
import "./sass/vendors.scss"

const App = ({ store, persistor, basename }) => {
  return (
    <Provider store={store}>
      <Helmet defaultTitle="Dipay Disbursement" titleTemplate="%s • Dipay Disbursement" />
      <PersistGate persistor={persistor} loading={'loading'}>
        <Router basename={basename}>
          <MantineProvider
            theme={{
              fontFamily: 'Inter, sans-serif',
            }}
          >
            <ToastProvider
              autoDismiss
              autoDismissTimeout={2000}
              components={{ Toast: Toast }}
              placement="top-center"
            >
              <BaseRoutes />
              <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                pauseOnHover
                theme="colored"
              />
            </ToastProvider>
          </MantineProvider>
        </Router >
      </PersistGate>
    </Provider>
  );
}

export default App;
