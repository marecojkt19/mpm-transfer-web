import React from "react";

function SortDownIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="20"
      fill="none"
      viewBox="0 0 20 20"
    >
      <mask
        id="mask0_516_66971"
        style={{ maskType: "alpha" }}
        width="20"
        height="20"
        x="0"
        y="0"
        maskUnits="userSpaceOnUse"
      >
        <path fill="#D9D9D9" d="M0 0H20V20H0z"></path>
      </mask>
      <g mask="url(#mask0_516_66971)">
        <path
          fill="#276EF1"
          d="M9.417 11.583L7.25 9.417c-.264-.264-.323-.566-.177-.907.146-.34.406-.51.781-.51h4.292c.375 0 .635.17.781.51.146.34.087.643-.177.907l-2.167 2.166a.863.863 0 01-.27.188.785.785 0 01-.313.062.784.784 0 01-.312-.062.863.863 0 01-.271-.188z"
        ></path>
      </g>
    </svg>
  );
}

export default SortDownIcon;
