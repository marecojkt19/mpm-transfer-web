import React from "react";

function CloseIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <mask
        id="mask0_669_32386"
        style={{ maskType: "alpha" }}
        width="24"
        height="24"
        x="0"
        y="0"
        maskUnits="userSpaceOnUse"
      >
        <path fill="#D9D9D9" d="M0 0H24V24H0z"></path>
      </mask>
      <g mask="url(#mask0_669_32386)">
        <path
          fill="#000"
          d="M12 13.4l-4.9 4.9a.948.948 0 01-.7.275.948.948 0 01-.7-.275.948.948 0 01-.275-.7c0-.284.092-.517.275-.7l4.9-4.9-4.9-4.9a.948.948 0 01-.275-.7c0-.284.092-.517.275-.7a.948.948 0 01.7-.275c.283 0 .517.091.7.275l4.9 4.9 4.9-4.9a.948.948 0 01.7-.275c.283 0 .517.091.7.275a.948.948 0 01.275.7.948.948 0 01-.275.7L13.4 12l4.9 4.9a.948.948 0 01.275.7.948.948 0 01-.275.7.948.948 0 01-.7.275.949.949 0 01-.7-.275L12 13.4z"
        ></path>
      </g>
    </svg>
  );
}

export default CloseIcon;
