import React from "react";

function ArrowBackIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="20"
      fill="none"
      viewBox="0 0 20 20"
    >
      <mask
        id="mask0_1002_43820"
        style={{ maskType: "alpha" }}
        width="20"
        height="20"
        x="0"
        y="0"
        maskUnits="userSpaceOnUse"
      >
        <path fill="#D9D9D9" d="M0 0H20V20H0z"></path>
      </mask>
      <g mask="url(#mask0_1002_43820)">
        <path
          fill="#276EF1"
          d="M6.518 10.834l4.084 4.084a.763.763 0 01.24.583.838.838 0 01-.261.583.88.88 0 01-.583.24.763.763 0 01-.584-.24l-5.5-5.5a.73.73 0 01-.177-.27.922.922 0 01-.052-.313c0-.111.017-.215.052-.313a.731.731 0 01.177-.27l5.5-5.5a.777.777 0 01.573-.23.85.85 0 01.594.23c.167.166.25.364.25.593 0 .23-.083.427-.25.594L6.518 9.168h9.313c.236 0 .434.08.594.24.16.159.24.357.24.593s-.08.434-.24.594a.806.806 0 01-.594.24H6.518z"
        ></path>
      </g>
    </svg>
  );
}

export default ArrowBackIcon;
