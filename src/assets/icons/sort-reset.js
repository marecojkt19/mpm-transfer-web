import React from "react";

function SortResetIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="20"
      fill="none"
      viewBox="0 0 20 20"
    >
      <mask
        id="mask0_516_66964"
        style={{ maskType: "alpha" }}
        width="20"
        height="20"
        x="0"
        y="0"
        maskUnits="userSpaceOnUse"
      >
        <path fill="#D9D9D9" d="M0 0H20V20H0z"></path>
      </mask>
      <g fill="#545454" mask="url(#mask0_516_66964)">
        <path d="M9.417 14.417L7.25 12.25c-.264-.264-.323-.566-.177-.906.146-.34.406-.51.781-.51h4.292c.375 0 .635.17.781.51.146.34.087.642-.177.906l-2.167 2.167a.863.863 0 01-.27.187.785.785 0 01-.313.063.784.784 0 01-.312-.063.863.863 0 01-.271-.187zM9.417 5.583L7.25 7.75c-.264.264-.323.566-.177.906.146.34.406.51.781.51h4.292c.375 0 .635-.17.781-.51.146-.34.087-.642-.177-.906l-2.167-2.167a.785.785 0 00-.583-.25.785.785 0 00-.583.25z"></path>
      </g>
    </svg>
  );
}

export default SortResetIcon;
