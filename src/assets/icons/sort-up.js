import React from "react";

function SortUpIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="20"
      fill="none"
      viewBox="0 0 20 20"
    >
      <mask
        id="mask0_516_66968"
        style={{ maskType: "alpha" }}
        width="20"
        height="20"
        x="0"
        y="0"
        maskUnits="userSpaceOnUse"
      >
        <path fill="#D9D9D9" d="M0 0H20V20H0z"></path>
      </mask>
      <g mask="url(#mask0_516_66968)">
        <path
          fill="#276EF1"
          d="M9.417 8.417L7.25 10.583c-.264.264-.323.566-.177.907.146.34.406.51.781.51h4.292c.375 0 .635-.17.781-.51.146-.34.087-.643-.177-.907l-2.167-2.166a.785.785 0 00-.583-.25.785.785 0 00-.583.25z"
        ></path>
      </g>
    </svg>
  );
}

export default SortUpIcon;
