import React from 'react'
import styles from './onboardLayout.module.scss'

const OnboardLayout = ({
  children
}) => {
  return (
    <div className={styles.onboardLayout}>
      {children}
    </div>
  )
}

export default OnboardLayout
