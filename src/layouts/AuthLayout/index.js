import React from 'react'
import styles from './authLayout.module.scss'

const AuthLayout = ({
  children
}) => {
  return (
    <div className={styles.authLayout}>
      <div className={styles.wrapper}>
        <div className={styles.banner}>
          <img
            src="/assets/media/auth/auth-banner.png"
            alt=""
          />
        </div>
        <div className={styles.form}>
          <div className={styles.logo}>
            <img src="/assets/dipay-logo.png" alt="" />
          </div>
          <div className={styles.heading}>
            <h1>Selamat datang di Dipay Disbursement</h1>
            <p>Nikmati kemudahan manajemen transfer untuk bisnis Anda</p>
          </div>
          <div className={styles.formWrapper}>
            {children}
          </div>
        </div>
      </div>
    </div>
  )
}

export default AuthLayout
