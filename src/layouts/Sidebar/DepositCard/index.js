import React from 'react'
import { useSelector } from 'react-redux';
import toIDR from '../../../utils/helpers/toIDR'
import styles from './depositCard.module.scss'

const DepositCard = () => {
  const { company } = useSelector(state => state.auth);

  // const hideForThisCompany = ['TOPINDOKU']

  return (
    <div className={styles.depositCard}>
      <div>
        <span>Deposit Saat Ini</span>
        <h6>Rp{toIDR(company.balance.deposit, false)}</h6>
      </div>
      {/* {Boolean(hideForThisCompany.includes(company.code)) ?
        <div>
          <span>Total Akumulasi</span>
          <h6>Rp{toIDR(company.balance.paylater, false)}</h6>
        </div>
        : null
      } */}
    </div >
  )
}

export default DepositCard
