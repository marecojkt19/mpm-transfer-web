import React, { useRef } from 'react'
import styles from './sidebar.module.scss'
import DepositCard from './DepositCard'
import { Li, MenuList, MultiLevel } from './MenuList'
import { useLocation } from 'react-router-dom'
import { routes, adminRoutes } from '../../Routes'
import pakaiClass from 'pakai-class'
import { useSelector } from 'react-redux'
import roleHelper, { ROUTE_LOGOUT } from '../../utils/helpers/roleHelper'
import { Exit } from '../../assets/icons'
import UserCard from './UserCard'

const Sidebar = ({ open, setOpen }) => {
  const { user, company } = useSelector(state => state.auth);
  const menuRef = useRef();
  const location = useLocation()

  return (
    <div className={pakaiClass(
      styles.sidebar,
      open && styles.open
    )}>
      <div className={styles.header}>
        <UserCard />
        <DepositCard balance={company.balance.deposit} />
      </div>
      <div className={styles.menu} ref={menuRef}>
        {routes.map((route, key) => {
          if (route.type === 'PARENT') {
            return roleHelper.hasAccess(user.role, Boolean(company.status.verifiedAt), route?.routeKey, company.code)
              &&
              <MultiLevel
                key={key}
                child={route.child}
                location={location}
                route={route}
              />
          }
          return roleHelper.hasAccess(user.role, Boolean(company.status.verifiedAt), route?.routeKey, company.code)
            &&
            <div key={key}>
              <MenuList>
                <Li
                  location={location}
                  route={route}
                  setOpen={setOpen}
                  isNew={route.isNew}
                />
              </MenuList>
            </div>
        })}
        {company.status.verifiedAt ?
          <div>
            <div className={styles.line}></div>
            <MenuList>
              {adminRoutes.map((route, key) => {
                return roleHelper.hasAccess(user.role, Boolean(company.status.verifiedAt), route?.routeKey, company.code)
                  &&
                  <div key={key}>
                    <Li
                      location={location}
                      route={route}
                      key={key}
                      setOpen={setOpen}
                      isNew={route.isNew}
                    />
                  </div>
              })}
            </MenuList>
          </div>
          : null
        }
      </div>
      <div className={styles.logout}>
        <MenuList>
          <Li
            location={location}
            route={{
              type: 'BOTTOM',
              title: 'Keluar',
              icon: <Exit />,
              url: '/logout',
              routeKey: ROUTE_LOGOUT
            }}
            setOpen={setOpen}
          />
        </MenuList>
      </div>
    </div>
  )
}

export default Sidebar
