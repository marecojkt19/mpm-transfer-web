import React, { useMemo, useState } from 'react'
import { useSelector } from "react-redux";
import { toAbsoluteUrl } from '../../../utils/helpers/pathHelper';
import styles from './userCard.module.scss';

const defaultImage = toAbsoluteUrl('/assets/media/imageField/company.png')

const UserCard = () => {
  const { user, company } = useSelector(state => state.auth);
  const [isError, setIsError] = useState(false);

  const logo = useMemo(() => {
    if (company.logo?.url) {
      if (!isError) {
        return `${process.env.REACT_APP_BASE_API}/upload${company.logo?.url.split("upload")[1]}`
      } else {
        return defaultImage
      }
    }

    return defaultImage
  }, [company, isError])

  return (
    <div className={styles.userCard}>
      <div className={styles.avatar}>
        <img
          src={logo}
          alt=""
          onError={() => setIsError(true)}
        />
      </div>
      <div className={styles.user}>
        <div className={styles.name}>
          <span>{user?.fullName}</span>
        </div>
        <div className={styles.company}>
          <span>{company?.name}</span>
        </div>
        <div
          style={{ color: `${user?.roleData?.fontColor}`, backgroundColor: `${user?.roleData?.bgColor}`, border: `1px solid ${user?.roleData?.fontColor}24` }}
          className={styles.role}
        >
          <span>{user?.roleData?.name}</span>
        </div>
      </div>
    </div>
  )
}

export default UserCard
