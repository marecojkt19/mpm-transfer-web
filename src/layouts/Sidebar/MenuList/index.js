import pakaiClass from 'pakai-class';
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { checkIsActive } from '../../../utils/helpers/routerHelpers';
import styles from './menuList.module.scss'
import { ChevronDown } from '../../../assets/icons';
import { useHistory } from 'react-router-dom';

export const MenuList = ({ children }) => {
  return (
    <ul className={styles.menuNav}>
      {children}
    </ul>
  );
}

export const Li = ({ location, route, setOpen, isNew }) => {
  return (
    <li
      className={styles.menuItem}
    >
      <Link
        onClick={() => {
          setOpen(false)
        }}
        className={pakaiClass(
          styles.menuLink,
          checkIsActive(location, route.url) && styles.active
        )}
        to={route.url}
      >
        <div className={styles.menuTitle}>
          {route.icon}
          <div className={styles.menuText}>{route.title}</div>
        </div>
        {/* {isNew ? <div className={styles.newBadge}><span>Baru!</span></div> : null} */}
      </Link>
    </li >
  )
}


export const MultiLevel = ({ child, location, route }) => {
  const [open, setOpen] = useState(false)
  const urlMatch = location.pathname.includes(route.url)
  const { push } = useHistory()

  useEffect(() => {
    if (urlMatch) {
      setOpen(true)
    }
  }, [urlMatch])

  return (
    <div className={styles.multiLevel}>
      <MenuList>
        <div className={styles.menuItem}>
          <div
            onClick={() => {
              setOpen(!open)
              push("/transaction")
            }}
            className={pakaiClass(styles.menuLink, urlMatch && styles.active)}
          >
            <div className={styles.menuTitle}>
              {route.icon}
              <div className={styles.menuText}>{route.title}</div>
            </div>
            <div className={pakaiClass(styles.arrowUp, open && styles.open)}>
              <ChevronDown />
            </div>
          </div>
        </div>
        <div
          className={pakaiClass(
            styles.levelContent,
            open && styles.open
          )}
        >
          {
            child.map((route) => (
              <li key={route.routeKey}>
                <Link to={route.url} className={pakaiClass(location.pathname.includes(route.url) && styles.active)}>{route.title}</Link>
              </li>
            ))
          }
        </div>
      </MenuList>
    </div>
  )
}
