import clsx from 'clsx';
import React from 'react';
import styles from './th.module.scss'
import SortDownIcon from '../../../assets/icons/sort-down';
import SortResetIcon from '../../../assets/icons/sort-reset';
import SortUpIcon from '../../../assets/icons/sort-up';

const Th = ({ col, sort = "", onSort = () => { }, className, total }) => {
  const sortable = col?.sortable;
  const [key, direction] = sort.split('|');

  const getClass = () => {
    let classList;
    if (sortable) {
      if (col.key === key) {
        classList = clsx(className, styles.sortable, styles[direction], styles.active);
      }
    }

    return classList;
  }

  const doSort = () => {
    if (!sortable) return;
    if (!total) return;

    let newSortValue = '';

    if (col.key === key) {
      if (direction === 'asc') {
        newSortValue = 'desc';
      }
    } else {
      newSortValue = 'asc';
    }

    if (newSortValue) {
      newSortValue = `${col.key}|${newSortValue}`;
    }

    onSort(newSortValue);
  }

  return (
    <th
      name={col.name}
      onClick={doSort}
    >
      <div className={clsx(styles.th, getClass(), col?.align && styles[col.align])}>
        <span>{col.title}</span>
        {sortable ?
          col.key === key ?
            direction === 'desc' ?
              <SortDownIcon />
              :
              <SortUpIcon />
            :
            <SortResetIcon />
          : null
        }
      </div>
    </th>
  )
}

export default Th
