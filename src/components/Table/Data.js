import React, { memo } from 'react'
import objectPath from 'object-path';
import styles from './table.module.scss';
import pakaiClass from 'pakai-class';

const Data = memo(({ data = [], columns, withIndex }) => {
  const view = () => {
    if (data?.length > 0) {
      return data.map((row, key) => (
        <DataRow
          key={key}
          columns={columns}
          row={row}
          index={key + 1}
          withIndex={withIndex}
        />
      ))
    } else {
      return (
        <tr>
          <td colSpan={withIndex ? columns.length + 1 : columns.length}>
            <div className={styles.noData}>
              <img src="/assets/empty-state.png" alt="" />
              <h1>Belum ada transaksi</h1>
              <p>Kamu belum pernah melakukan transaksi.</p>
            </div>
          </td>
        </tr>
      )
    }
  }

  return view()
})

const DataRow = ({ columns, row, withIndex, index }) => {
  return (
    <>
      <tr>
        {withIndex && <td>{index}.</td>}
        {columns.map((col, key) =>
          <td
            key={key}
            className={pakaiClass(col.align && styles[col.align])}
          >
            <Render row={row} col={col} />
          </td>
        )}
      </tr>
    </>
  )
}

const Render = ({ row, col }) => {
  const keyValue = objectPath.get(row, col.key) ?? undefined;

  if (typeof col.render === "function") {
    return col.render(keyValue, row)
  }

  return keyValue ?? null
}

export default Data
