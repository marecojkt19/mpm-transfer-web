import React, { useMemo, memo } from 'react';
import styles from './show.module.scss';
import Select, { components } from 'react-select';
import { ChevronDown } from '../../../assets/icons';

const customStyles = {
  singleValue: (styles) => ({
    ...styles,
    fontWeight: 400,
    fontSize: 12,
    lineHeight: "16px",
    textAlign: "center",
    color: "#7E8299"
  }),
  valueContainer: (styles) => ({
    ...styles,
    position: "static"
  }),
}

const options = [5, 10, 50, 100].map(v => ({ value: v, label: v }))

const Show = memo(({
  limit,
  onLimit,
  total,
  currentPage,
  showRender,
  getOptionValue = opt => opt.value,
}) => {
  const value = getValue(options, limit, getOptionValue);
  const from = useMemo(() => {
    let res = ((currentPage - 1) * limit) + 1
    if (res > total) return total;
    return res
  }, [currentPage, limit, total])

  const to = useMemo(() => {
    let res = currentPage * limit;
    if (res > total) return total;
    return res
  }, [currentPage, limit, total])

  const handleChange = e => {
    onLimit(Number(e?.value))
  }

  return (
    <div className={styles.show}>
      <span className={styles.showText}>
        {
          typeof showRender === "function" ?
            showRender(from, to, total)
            :
            `Showing ${from} to ${to} of ${total}`
        }
      </span>
      {
        onLimit ?
          <Select
            value={value}
            onChange={handleChange}
            styles={customStyles}
            className={styles.select}
            components={{
              IndicatorsContainer: () => null,
              Placeholder: () => null,
              Control: (props) => {
                const { children } = props;
                return (
                  <components.Control
                    {...props}
                    className={styles.selectControl}
                  >
                    <>
                      {children}
                      <div className={styles.arrowDown}>
                        <ChevronDown size={20} />
                      </div>
                    </>
                  </components.Control>
                )
              }
            }}
            options={options}
          /> : null
      }
    </div>
  )
})

// Method for Custom Select
function flatten(arr) {
  return arr.reduce((acc, val) => (Array.isArray(val.options)
    ? acc.concat(flatten(val.options))
    : acc.concat(val)
  ), []);
}

function getValue(opts, val, getOptVal) {
  if (val === undefined) return undefined;

  const options = flatten(opts);
  const value = options.find(o => getOptVal(o) === val);

  return value;
}

export default Show;
