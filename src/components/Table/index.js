import pakaiClass from 'pakai-class';
import React, { memo, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import ReactDOMServer from 'react-dom/server';
import { useDispatch } from 'react-redux';
import { EXPORT_EXCEL_ALL } from '../../utils/enums/actionTypes';
import LoadingBar from '../Loadings/LoadingBar';
import Modal, { ModalHead, ModalWrapper, ModalBody } from '../ModalNew';
import Data from './Data';
import { ExcelTable } from './ExcelTable';
import Pagination from './Pagination';
import Show from './Show';
import styles from './table.module.scss';
import Th from './Th';
import Download from '../../assets/icons/download';
import Refresh from '../../assets/icons/refresh';
import MainButton from '../Buttons/MainButton';
import TextField from '../Form/TextFieldNew';
import SearchIcon from '../../assets/icons/search';
import CircularProgress2 from '../Loadings/CircularProgress2';

const base64 = (s) => window.btoa(unescape(encodeURIComponent(s)))
const format = (s, c) => s.replace(/{(\w+)}/g, (m, p) => c[p])

const Table = memo(({
  data,
  load,
  excel,
  search,
  searchPlaceholder,
  config: {
    columns,
    withIndex = false,
    total,
    limit,
    currentPage,
    showRender,
    pagination = true,
    loading,
    sort
  },
  onSort = () => { },
  onChangePage = () => { },
  onSearch = () => { },
  onLimit = () => { },
  children,
  activeFilters,
  customTableFooter
}) => {
  const dispatch = useDispatch()
  const tableBodyRef = useRef();
  const [showExcelAction, setShowExcelAction] = useState(false);
  const [isRendered, setIsRendered] = useState(false);

  const exportExcel = useCallback(({ exportType, index } = {}) => {
    let generatedTable;
    if (typeof excel?.CurrentPageCustomTable === 'function') {
      generatedTable = excel.CurrentPageCustomTable({
        columns: excel.columns,
        items: data,
        lastLength: data.length
      });
    } else {
      generatedTable = ExcelTable({ columns: excel.columns, items: data });
    }

    const html = ReactDOMServer.renderToStaticMarkup(generatedTable);
    const excelHref = 'data:application/vnd.ms-excel;base64,';
    const filename = ((Math.ceil(total / limit) > 1) ? `${excel?.filename}(${currentPage} of ${Math.ceil(total / limit)})` : excel?.filename) + '.xls'
    const template =
      '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-mic' +
      'rosoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><meta cha' +
      'rset="UTF-8"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:Exce' +
      'lWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/>' +
      '</x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></' +
      'xml><![endif]--></head><body>{html}</body></html>';
    const context = {
      worksheet: 'Worksheet',
      html,
    };

    if (exportType === 'all' && excel?.allPage) {
      const totalPage = Math.ceil(total / limit);
      dispatch({
        type: EXPORT_EXCEL_ALL,
        columns: excel.columns,
        totalPage: totalPage,
        filename: excel.filename,
        customTable: excel?.AllPageCustomTable,
        limit: limit,
        api: {
          apiResponseKey: excel.api.apiResponseKey,
          service: excel.api.service,
          APIParams: excel.api.APIParams
        },
      })
      setShowExcelAction(false)
    } else if (exportType === "current") {
      let a = document.createElement('a')
      a.href = excelHref + base64(format(template, context));
      a.download = filename
      if (a.download.split(".").filter(el => el.includes("xls")).length > 1) {
        a.download = a.download.replace(".xls", "")
      }
      a.click()
      setShowExcelAction(false)
    }
  }, [data, excel, currentPage, limit, total, dispatch]);

  const customTFoot = useMemo(() => {
    if (isRendered && customTableFooter) {
      return (
        <div
          style={{
            height: tableBodyRef?.current?.scrollHeight,
            width: tableBodyRef?.current?.clientWidth
          }}
          className={styles.bgTable}>
        </div>
      )
    }

    return null
  }, [isRendered, customTableFooter])

  useEffect(() => {
    setTimeout(() => {
      setIsRendered(true)
    }, 100)
  }, [])

  return (
    <div>
      {search || excel || load ?
        <div className={styles.toolsControl}>
          {search ?
            <div className={styles.left}>
              <div className={styles.search}>
                <TextField
                  placeholder={searchPlaceholder ?? "Cari ..."}
                  icon={<SearchIcon />}
                  onChange={(e) => onSearch(e.target.value)}
                />
              </div>
            </div>
            : null
          }
          {excel || load ?
            <div className={styles.right}>
              {
                excel
                &&
                <div>
                  <MainButton
                    width={190}
                    height={40}
                    onClick={() => setShowExcelAction(true)}
                    className={styles.excelBtn}
                    icon={<Download />}
                  >
                    Unduh Laporan
                  </MainButton>
                  <Modal
                    in={showExcelAction}
                    onClose={() => setShowExcelAction(false)}
                  >
                    <ModalWrapper>
                      <ModalHead
                        title={
                          <div className={styles.downloadLabel}>
                            <div className={styles.downloadIcon}>
                              <Download size={24} />
                            </div>
                            <p>Unduh Laporan</p>
                          </div>
                        }
                        onClose={() => setShowExcelAction(false)}
                      />
                      <ModalBody>
                        <div className={styles.downloadExcelButtons}>
                          {
                            excel?.allPage
                            &&
                            <MainButton onClick={() => exportExcel({ exportType: 'all' })}>Seluruh Halaman</MainButton>
                          }
                          <MainButton variant='primary-light' onClick={() => exportExcel({ exportType: 'current' })}>Halaman Saat Ini</MainButton>
                        </div>
                      </ModalBody>
                    </ModalWrapper>
                  </Modal>
                </div>
              }
              {load ?
                <MainButton
                  variant='primary-light'
                  height={40}
                  icon={<Refresh />}
                  onClick={load}
                >
                </MainButton>
                : null
              }
            </div>
            : null
          }
        </div>
        : null
      }
      {
        children &&
        <div className={styles.topControl}>
          <div className={styles.filterWrapper}>
            {children}
          </div>
        </div>
      }
      {activeFilters}
      <div className={pakaiClass(styles.tableWrapper, !customTableFooter && styles.tableBorder)}>
        <table className={styles.table}>
          <thead>
            <tr>
              {withIndex ? <th>#</th> : null}
              {
                columns.map((col, key) =>
                  <Th
                    key={key}
                    col={col}
                    sort={sort}
                    onSort={onSort}
                    total={total}
                  />
                )
              }
            </tr>
            {
              loading &&
              <tr className={styles.loadingRow}>
                <td colSpan={columns.length}>
                  <LoadingBar position="bottom" />
                </td>
              </tr>
            }
          </thead>
          <tbody ref={tableBodyRef}>
            <Data
              data={data}
              columns={columns}
              withIndex={withIndex}
            />
            {customTableFooter}
            {
              loading ?
                <tr>
                  <td style={{ padding: 0 }} colSpan={columns?.length}>
                    <div className={styles.loading}>
                      <div className={pakaiClass(styles.loadingCard, !(data?.length > 1) && styles.onlyOne)}>
                        <CircularProgress2 className="mb-16" />
                        <span>Harap Tunggu Sebentar</span>
                      </div>
                    </div>
                  </td>
                </tr>
                : null
            }
          </tbody>
        </table>
        {customTFoot}
      </div>
      {
        pagination &&
        <div className={styles.bottomControl}>
          <Show
            total={total}
            limit={limit}
            onLimit={onLimit}
            currentPage={currentPage}
            showRender={showRender}
          />
          <Pagination
            total={total}
            limit={limit}
            currentPage={currentPage}
            onChangePage={onChangePage}
          />
        </div>
      }
    </div>
  )
})

export default Table
