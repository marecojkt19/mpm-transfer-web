import { XIcon } from '../../../assets/icons';
import styles from './activeFilters.module.scss';
import { Swiper, SwiperSlide } from "swiper/react";

function ActiveFilters({ filters, reset }) {
  if (!filters.length) return <></>;
  return (
    <div className={styles.activeFilters}>
      <div>
        <button onClick={reset} className={styles.reset}>
          Reset filter
        </button>
      </div>
      <div className={styles.filters}>
        <Swiper
          className={styles.swiper}
          slidesPerView="auto"
          spaceBetween={8}
        >
          {filters.map((v, i) => {
            return (
              <SwiperSlide className={styles.swiperSlide} key={i}>
                <FilterItem key={v?.name} title={v?.title} onClear={v?.onClear} />
              </SwiperSlide>
            )
          })}
        </Swiper>
      </div>
    </div>
  )
}

function FilterItem({ title, onClear }) {

  return (
    <div className={styles.btnFilter}>
      <button onClick={onClear}>
        <XIcon />
      </button>
      {title}
    </div>
  )
}

export default ActiveFilters;
