import pakaiClass from 'pakai-class';
import React, { Children, cloneElement, forwardRef, useState } from 'react'
import styles from './radioGroup.module.scss'
import { Swiper, SwiperSlide } from "swiper/react";

const RadioGroup = forwardRef((
  {
    label,
    className,
    name,
    onChange,
    value,
    defaultValue,
    children,
    row
  },
  ref
) => {
  const [currentValue, setCurrentValue] = useState(defaultValue);

  const handleChange = (e) => {
    setCurrentValue(e.target.value)
    if (typeof onChange === 'function') onChange(e)
  }

  return (
    <fieldset
      className={pakaiClass(
        styles.radioGroup,
        row ? styles.row : styles.col,
        className
      )}
    >
      {label && <legend className={styles.radioGroupLabel}>{label}</legend>}
      <Swiper
        className={styles.swiper}
        slidesPerView="auto"
        spaceBetween={8}
      >
        {Children.map(children, (child, i) =>
          <SwiperSlide className={styles.swiperSlide} key={i}>
            {
              child ?
                cloneElement(child, {
                  className: pakaiClass(children.props?.className, styles.radio),
                  name: name,
                  checked: value ? value === child?.props?.value : currentValue === child?.props?.value,
                  ref: ref,
                  onChange: handleChange
                })
                :
                null
            }
          </SwiperSlide>
        )}
      </Swiper>
    </fieldset>
  )
})

export default RadioGroup
