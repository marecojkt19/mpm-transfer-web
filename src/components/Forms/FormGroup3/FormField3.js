import pakaiClass from 'pakai-class'
import React from 'react'
import styles from './formGroup.module.scss'

const FormField3 = ({
  className,
  children
}) => {
  return (
    <div
      className={pakaiClass(
        styles.formField,
        className
      )}
    >
      <div
        className={styles.formGroup}
      >
        {children}
      </div>
    </div>
  )
}

export default FormField3
