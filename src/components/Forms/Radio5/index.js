import React from 'react'
import pakaiClass from 'pakai-class'
import styles from './radio.module.scss'
import { CheckedCircle2, UnCheckedCircle2 } from '../../../assets/icons'

const Radio5 = ({
  id,
  className,
  name,
  label,
  onChange,
  value,
  checked,
  imageLabel,
  inputProps
}) => {
  return (
    <label
      className={pakaiClass(
        styles.radioRoot,
        className
      )}
    >
      <span className={styles.radioWrapper}>
        <input
          {...inputProps}
          type="radio"
          id={id || name}
          name={name}
          checked={checked}
          value={value}
          onChange={onChange}
        />
        {checked ?
          <CheckedCircle2 />
          :
          <UnCheckedCircle2 />
        }
      </span>
      {
        label &&
        <span className={styles.radioLabel}>{label}</span>
      }
      {
        imageLabel &&
        <div className={styles.imageLabel}>
          <img src={imageLabel} alt="" />
        </div>
      }
    </label>
  )
}

export default Radio5
