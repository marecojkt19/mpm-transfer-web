import React from 'react'
import pakaiClass from 'pakai-class'
import styles from './radio.module.scss'

const Radio = ({
  id,
  className,
  name,
  label,
  onChange,
  value,
  checked,
  inputProps,
  variant
}) => {
  return (
    <label
      className={pakaiClass(
        styles.radioRoot,
        variant && styles[variant],
        className
      )}
    >
      <span className={styles.radioWrapper}>
        <input
          {...inputProps}
          type="radio"
          id={id || name}
          name={name}
          checked={checked}
          value={value}
          onChange={onChange}
        />
        <span className={styles.radio} />
      </span>
      <span className={styles.radioLabel}>{label}</span>
    </label>
  )
}

export default Radio
