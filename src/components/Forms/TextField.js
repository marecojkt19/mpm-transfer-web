import React, { forwardRef, memo, useEffect, useMemo, useRef, useState } from 'react';
import FormField from './FormField';
import styles from './forms.module.scss'
// import Button from '../Button';
import { ClosedEye, Eye } from '../../assets/icons';
import FormField2 from './FormField2';
import pakaiClass from 'pakai-class';
import NumberFormat from 'react-number-format';
import FormField3 from './FormGroup3/FormField3';

const TextField = memo(forwardRef((
  {
    id,
    name,
    value,
    label,
    onChange,
    onFocus,
    onBlur,
    error,
    helperText,
    placeholder,
    multiline,
    autoComplete = "new-password",
    spellCheck = false,
    readOnly,
    disabled,
    autoFocus,
    row,
    className,
    inputProps,
    textareaProps,
    animate = true,
    password,
    format,
    imaskprops,
    defaultValue,
    variant,
    loading,
    tooltip,
    showTooltip,
    popperPlacement,
    moneyInput,
    phoneInput,
    allowLeadingZero,
    allowDot,
    maxLength
  },
  ref
) => {
  const inputRef = useRef()
  const [focus, setFocus] = useState(false)
  const [isFilled, setIsFilled] = useState(false)
  const [showPassword, setShowPassword] = useState(false);
  const componentInput = useMemo(() => {
    if (multiline) return <textarea
      {...textareaProps}
      id={id || name}
      name={name}
      value={value}
      onChange={onChange}
      onBlur={onBlur}
      placeholder={placeholder}
      disabled={disabled}
      readOnly={readOnly}
      autoComplete={autoComplete}
      spellCheck={spellCheck}
      ref={(el) => {
        inputRef.current = el;
        if (typeof ref === 'function') ref(el);
      }}
    />
    if (format || moneyInput || phoneInput)
      return <NumberFormat
        {...imaskprops}
        value={value}
        defaultValue={defaultValue}
        mask={format}
        allowNegative={false}
        getInputRef={(el) => {
          inputRef.current = el;
          if (typeof ref === "function") ref(el);
        }}
        onValueChange={({ value }) => {
          setIsFilled(Boolean(value))
          if (typeof onChange === "function") onChange(value)
        }}
        onBlur={(e) => {
          setFocus(false);
          if (typeof onBlur === "function") onBlur(e);
        }}
        onFocus={(e) => {
          setFocus(true);
          if (typeof onFocus === "function") onFocus(e);
        }}
        thousandSeparator={Boolean(moneyInput) ? "." : ""}
        decimalSeparator={Boolean(moneyInput) ? "," : false}
        isAllowed={(values) => {
          const { value, floatValue } = values;


          if (typeof floatValue === 'undefined' || typeof value === 'undefined') {
            return true
          }
          if (allowDot) {
            if (allowLeadingZero) {
              if (maxLength) {
                if (value.length <= maxLength) {
                  return true
                }
              } else {
                return true
              }
            } else {
              if (value.charAt(0) !== '0') {
                if (maxLength) {
                  if (value.length <= maxLength) {
                    return true
                  }
                } else {
                  return true
                }
              }
            }
          } else {
            if (value.charAt(0) !== '.') {
              if (allowLeadingZero) {
                if (maxLength) {
                  if (value.length <= maxLength) {
                    return true
                  }
                } else {
                  return true
                }
              } else {
                if (value.charAt(0) !== '0') {
                  if (maxLength) {
                    if (value.length <= maxLength) {
                      return true
                    }
                  } else {
                    return true
                  }
                }
              }
            }
          }
        }}
      />

    return <input
      {...inputProps}
      onBlur={(e) => {
        setFocus(false)
        if (typeof onBlur === 'function') onBlur(e)
      }}
      onFocus={e => {
        setFocus(true)
        if (typeof onFocus === 'function') onFocus(e)
      }}
      id={id || name}
      name={name}
      value={value}
      onChange={onChange}
      placeholder={placeholder}
      disabled={disabled}
      readOnly={readOnly}
      autoComplete={autoComplete}
      spellCheck={spellCheck}
      type={password && !showPassword ? 'password' : 'text'}
      ref={(el) => {
        inputRef.current = el;
        if (typeof ref === 'function') ref(el);
      }}
    />
  }, [allowLeadingZero, allowDot, maxLength, multiline, defaultValue, phoneInput, moneyInput, value, disabled, autoComplete, id, name, onBlur, onFocus, onChange, placeholder, readOnly, ref, spellCheck, textareaProps, inputProps, password, showPassword, format, imaskprops])

  const EyeIcon = useMemo(() => showPassword ? ClosedEye : Eye, [showPassword])

  const FormFieldTag = useMemo(() => {
    if (variant === "outlined") return FormField2
    else if (variant === "normal") return FormField3

    return FormField
  }, [variant])

  useEffect(() => {
    if (inputRef.current?.value) setIsFilled(true)
    else setIsFilled(false)
  }, [inputRef.current?.value])

  useEffect(() => {
    if (autoFocus) {
      inputRef.current.focus()
    }
  }, [autoFocus])

  return (
    <FormFieldTag
      disabled={disabled}
      className={className}
      id={id || name}
      row={row}
      label={label}
      error={error}
      helperText={helperText}
      shrink={isFilled || value ? false : !focus}
      animate={animate}
      isPassword={password}
      onClickLabel={() => inputRef?.current?.focus()}
      loading={loading}
      tooltip={tooltip}
      showTooltip={typeof showTooltip === undefined ? focus : showTooltip}
      popperPlacement={popperPlacement}
      phoneInput={phoneInput}
    >
      <div className={styles.inputWrapper}>
        <div className={pakaiClass(styles.formControl, loading && styles.formControlLoading)}>
          {componentInput}
          {
            password &&
            <div className={styles.eyeBtn} onClick={() => setShowPassword(!showPassword)}>
              <EyeIcon />
            </div>
          }
        </div>
      </div>
    </FormFieldTag>
  )
}));

export default TextField;
