import React from 'react'
import styles from './tnc.module.scss'

const TncTemplate = () => {
  return (
    <div className={styles.modalWrapper}>
      <div className={styles.modalCard}>
        <div className={styles.tncContainer}>
          <div className={styles.textWrapper}>
            <h3>SYARAT DAN KETENTUAN</h3>
            <p>
              MOHON MEMBACA DAN MEMAHAMI SYARAT DAN KETENTUAN KAMI DENGAN SEKSAMA SEBELUM
              MENGGUNAKAN LAYANAN KAMI UNTUK PERTAMA KALI-NYA.
            </p>
            <p>Selamat Datang di Dipay <i>Disbursement</i>.</p>
            <p className={styles.withIndent}>
              Terima kasih Anda telah menggunakan maupun mengakses Dipay <i>Disbursement</i>. Syarat dan Ketentuan ini ditetapkan untuk mengatur penggunaan layanan dan platform yang disediakan oleh Dipay. Dengan menggunakan Dipay <i>Disbursement</i>, Anda mengakui dan menyetujui bahwa Anda telah membaca dengan teliti, memahami, menerima dan menyetujui Syarat dan Ketentuan tanpa terkecuali, dimana akan berlaku sebagai perjanjian antara Anda dengan PT Mareco Prima Mandiri, beserta rekanan PT Mareco Prima Mandiri yang tergabung dalam Layanan Dipay <i>Disbursement</i>.
            </p>
            <p className={styles.withIndent}>
              Jika diperlukan Kami akan mencantumkan atau memberlakukan Syarat dan Ketentuan tambahan
              untuk keperluan Layanan tertentu pada Dipay <i>Disbursement</i>, dimana persyaratan tambahan tersebut merupakan satu kesatuan dari Syarat dan Ketentuan ini.
            </p>
            <div className={styles.content}>
              <ol type="A">
                <li>
                  <b>DEFINISI</b>
                  <ol>
                    <li>
                      <b>DIPAY</b> adalah layanan sistem pembayaran yang telah memperoleh izin dari Bank Indonesia berdasarkan Surat Bank Indonesia No. 22/630/DKSP/Srt/B dan No.22/631/DKSP/Srt/B tanggal 8 Desember 2020 yang diselenggarakan oleh PT Mareco Prima Mandiri (<a href="https://www.dipay.id">www.dipay.id</a>).
                    </li>
                    <li>
                      <b>AKUN</b> atau <b>AKUN PENGGUNA</b> adalah rekening uang
                      elektronik yang dibuka oleh Pengguna berdasarkan permintaan
                      pendaftaran Pengguna.
                    </li>
                    <li>
                      <b>KAMI</b> adalah PT Mareco Prima Mandiri (Dipay).
                    </li>
                    <li>
                      <b>ANDA</b> atau <b>PENGGUNA</b> adalah badan usaha dan/atau badan hukum yang terdaftar pada Platform Kami dan bertindak sebagai pengirim dengan memberikan perintah Transfer Dana melalui Platform Dipay <i>Disbursement</i>.
                    </li>
                    <li>
                      <b>PENERIMA</b> adalah perorangan, badan usaha atau badan hukum yang disebutkan dalam Perintah Transfer Dana untuk menerima Dana.
                    </li>
                    <li>
                      <b>AKUN</b> adalah setiap akun yang Anda buat pada Platform, untuk mengakses, menggunakan dan/atau menikmati Dipay <i>Disbursement</i> serta untuk mencatat, memonitor dan/atau melihat seluruh transaksi dan/atau aktivitas yang Anda lakukan melalui atau menggunakan Platform termasuk jumlah Deposit.
                    </li>
                    <li>
                      <b>PLATFORM</b> adalah aplikasi berbasis web (aplikasi yang dapat diakses melalui website/situs) <a href="https://enterprise.evy.id/" target>https://enterprise.evy.id/</a> yang dikelola oleh Kami dan dapat diperbarui dari waktu ke waktu.
                    </li>
                    <li>
                      <b>DATA</b> adalah setiap data, informasi dan/atau keterangan dalam bentuk apapun dari waktu ke waktu yang Anda sampaikan atau yang Anda cantumkan dalam, pada atau melalui Platform.
                    </li>
                    <li>
                      <b>SYARAT DAN KETENTUAN</b> adalah Syarat dan Ketentuan ini berikut setiap perubahan, penambahan, penggantian, penyesuaian dan/atau modifikasinya yang dibuat dari waktu ke waktu.
                    </li>
                    <li>
                      <b>LAYANAN DIPAY ENTERPRISE</b> adalah setiap layanan, program, jasa, produk, fitur, sistem, fasilitas dan/atau jasa yang disediakan dan/atau ditawarkan Kami yang dapat diakses melalui Platform yang mungkin diubah dan/atau ditambah dari waktu ke waktu.
                    </li>
                    <li>
                      <b>DEPOSIT</b> adalah sejumlah uang yang disetorkan oleh Anda ke rekening penampungan atas nama Kami di bank yang ditetapkan oleh Kami, dimana dana tersebut tidak menghasilkan bunga dan digunakan oleh Kami untuk melaksanakan Transfer atas perintah dari Anda.
                    </li>
                    <li>
                      <b>PERINTAH</b> adalah instruksi yang diberikan kepada Kami untuk memindahkan sejumlah dana berupa deposit ke rekening Bank di suatu institusi perbankan milik penerima yang tertera dalam perintah transfer dana dan/atau yang mungkin diubah dari waktu ke waktu.
                    </li>
                    <li>
                      <b>REKENING BANK ANDA</b> atau <b>PENGGUNA</b> adalah rekening atas nama Anda pada institusi perbankan yang telah didaftarkan pada Platform.
                    </li>
                    <li>
                      <b>ONE-TIME TRANSACTION PASSWORD (OTP)</b> adalah password dinamis yang dikirimkan ke nomor ponsel Anda yang bertujuan untuk mengamankan Akun dari penyalahgunaan tanpa sepengetahuan Anda dengan cara mengirimkan notifikasi ke ponsel Anda untuk meminta persetujuan.
                    </li>
                    <li>
                      <b>PASSWORD</b> adalah kombinasi dari beberapa karakter berupa huruf, angka dan simbol.
                    </li>
                    <li>
                      <b>TRANSAKSI</b> adalah transaksi transfer dana dalam Platform.
                    </li>
                    <li>
                      <b>PENUTUPAN AKUN</b> adalah proses penutupan akun Anda berdasarkan permintaan Anda.
                    </li>
                  </ol>
                </li>
                <br />
                <li className="my-24">
                  <b>KETENTUAN AKUN DAN VERIFIKASI</b>
                  <ol>
                    <li>Informasi Umum<br />
                      Untuk dapat mengakses dan menggunakan Platform, Anda harus mendaftarkan diri pada sistem yang terdapat di Platform dengan membuat Akun terlebih dahulu. Dalam proses pendaftaran diri, Anda wajib mengisi data pribadi secara lengkap dan jujur di halaman Akun, termasuk pada nama lengkap, nomor ponsel, alamat e-mail yang aktif, dan informasi lainnya yang dibutuhkan. Kami tidak bertanggung jawab atas segala akibat apabila terdapat informasi yang tidak benar, tidak akurat, maupun menyesatkan mengenai data yang Anda cantumkan pada Akun yang menyebabkan kerugian pada Anda maupun pihak ketiga.
                    </li>
                    <li>Informasi Tambahan<br />
                      Dengan tujuan verifikasi, kami akan meminta Anda untuk memberikan kepada kami terkait data Anda, antara lain:
                      <ol>
                        <li>
                          Individu yang bertanggung jawab atas akun perusahaan (<i>Person In Charge</i>), yaitu NIK, Tempat dan tanggal lahir, Kota Tinggal, Alamat tempat tinggal terkini, KTP dan Swafoto KTP pada Platform.
                        </li>
                        <li>
                          Akun Perusahaan, yaitu mengisi alamat Perusahaan, bentuk badan usaha, bidang usaha, frekuensi transaksi serta mengunggah NPWP, SIUP, SK Kemenkumham, Akta Perusahaan pada Platform.
                        </li>
                      </ol>
                    </li>
                    <li>
                      Apabila Anda tidak bersedia untuk memberikan/melengkapi semua informasi/persyaratan
                      yang kami butuhkan, kami berhak untuk menolak pendaftaran Akun Anda termasuk namun
                      tidak terbatas pada perintah-perintah berdasarkan kebijakan Kami.
                    </li>
                    <li>
                      Apabila Anda telah bersedia untuk memberikan/melengkapi semua informasi/persyaratan yang kami butuhkan, jika terdapat perubahan atas setiap informasi dan data pada Akun Anda, maka Anda wajib segera memperbarui informasi mengenai hal tersebut kepada Kami, serta Anda menjamin bahwa seluruh informasi dan data yang tersimpan pada sistem Kami adalah informasi dan data yang paling terkini.
                    </li>
                    <li>
                      Anda tidak diperbolehkan untuk melakukan tindakan apapun secara langsung maupun tidak langsung (melalui Platform) yang dapat merusak dan mengganggu reputasi Kami.
                    </li>
                    <li>
                      Anda tidak diperbolehkan untuk melakukan pengalihan dan/atau menjual Akun Anda yang telah terdaftar kepada pihak lain tanpa sepengetahuan dan persetujuan Kami.
                    </li>
                    <li>
                      Anda bertanggung jawab atas semua keamanan dan kerahasiaan informasi dan data pada Akun Anda. Maka dari itu, Anda dengan ini menyatakan bahwa Kami tidak bertanggung jawab atas kerugian ataupun kendala yang timbul atas penyalahgunaan Akun Anda yang diakibatkan oleh kelalaian atau kesalahan Anda, terkecuali apabila terdapat indikasi penyalahgunaan atas Akun Anda, Kami hanya dapat membantu Anda untuk memeriksa atau menghentikan penyalahgunaan akses terhadap Akun setelah Anda menginformasikan kepada Kami mengenai hal tersebut melalui fitur Hubungi Kami pada Platform atau sarana komunikasi lainnya yang Kami sediakan dari waktu ke waktu.
                    </li>
                    <li>
                      Kami akan menganggap dan menjalankan setiap Perintah atau Transaksi yang dilakukan melalui Akun Anda sebagai permintaan yang sah dari Anda.
                    </li>
                    <li>
                      Apabila Anda dengan ini menyatakan bahwa Anda cakap secara hukum maupun legalitas, mampu untuk mengikatkan diri dalam sebuah perjanjian yang sah menurut hukum dan legalitas, maka Anda berhak sepenuhnya untuk menggunakan layanan yang tersedia pada Platform Kami.
                    </li>
                  </ol>
                </li>
                <li className="my-24">
                  <b>KETENTUAN PERINTAH DAN TRANSAKSI TRANSFER DANA</b>
                  <ol>
                    <li>
                      Melakukan Transaksi Transfer Dana dengan Layanan Dipay <i>Disbursement</i>, Anda harus terlebih dahulu mengisi Deposit kepada Akun dengan cara transfer dana kepada rekening kami yang telah kami sediakan. Jumlah deposit yang telah disetorkan, dapat dilihat dalam Platform.
                    </li>
                    <li>
                      Melakukan Transaksi Transfer Dana dengan Layanan Dipay <i>Disbursement</i>, Anda harus terlebih dahulu mengisi Deposit kepada Akun dengan cara transfer dana kepada rekening kami yang telah kami sediakan. Jumlah deposit yang telah disetorkan, dapat dilihat dalam Platform.
                    </li>
                    <li>
                      Sebelum Transaksi Transfer Dana dijalankan, Kami akan meminta konfirmasi dari Anda untuk memastikan bahwa Anda bermaksud melanjutkan pelaksanaan Transaksi Transfer Dana.
                    </li>
                    <li>
                      Untuk mengautentikasi bahwa Perintah Transfer Dana berasal dari Anda, Kami mewajibkan Anda untuk mengisi One Time Password (OTP) yang akan dikirimkan oleh sistem ke e-mail dan/atau nomor ponsel terdaftar. Setiap autentikasi yang Anda lakukan akan Kami anggap sebagai bentuk konfirmasi dari Anda, dimana Kami berhak melakukan pemotongan secara langsung sejumlah dana pada Deposit yang telah disetorkan untuk melakukan transaksi Transfer Dana Anda.
                    </li>
                    <li>
                      Jika segala autentikasi belum Anda konfirmasi, maka Perintah transaksi Transfer Dana belum bisa dijalankan oleh seorang Admin dan/atau setiap Perintah Transfer Dana yang telah Anda konfirmasi, tidak dapat dibatalkan.
                    </li>
                    <li>
                      Batas pengiriman dana per rekening adalah sebesar Rp 50.000.000,- (Lima Puluh Juta Rupiah).
                    </li>
                    <li>
                      Untuk keperluan mitigasi terjadinya penyalahgunaan Akun, penipuan, dan tindakan-tindakan yang bertentangan dengan ketentuan Hukum Yang Berlaku, pemindahan Dana yang dilakukan baik dalam satu atau beberapa Perintah Transfer Dana dengan total maksimum sebesar Rp100.000.000,- (Seratus Juta Rupiah) akan memerlukan pemeriksaan dan pengecekan lebih lanjut oleh kami. Anda memahami dan menyetujui bahwa dokumen dan/atau informasi tertentu mungkin disyaratkan untuk kepentingan ini. Kami berhak untuk mengubah batasan jumlah Dana tersebut, yang mana perubahan tersebut akan diberitahukan kepada Anda melalui Syarat dan Ketentuan ini atau melalui media lainnya.
                    </li>
                    <li>
                      Deposit milik Anda yang telah diterima hanya Kami gunakan untuk melaksanakan Perintah Transfer Dana dari Anda, dimana Deposit tersebut tidak akan Kami gunakan untuk membiayai kegiatan diluar kewajiban Kami terhadap Anda dan Penerima.
                    </li>
                    <li>
                      Anda memahami bahwa dalam mengeksekusi Transaksi Transfer Dana berdasarkan Perintah Transfer Dana dari Anda, Kami dibantu oleh pihak ketiga, antara lain: Perbankan dan Lembaga Swicthing, maka dari itu Anda secara tidak langsung melepaskan Kami dari tanggung jawab atas setiap terjadinya kesalahan atau keterlambatan proses transaksi pindah dana kepada rekening bank Penerima yang dituju apabila kesalahan tersebut bukan disebabkan oleh sistem yang Kami kelola.
                    </li>
                    <li>
                      Kami berhak untuk tidak melaksanakan Transaksi Transfer Dana berdasarkan Perintah Transfer Dana apabila:
                      <ol>
                        <li>
                          Jumlah Deposit tidak mencukupi untuk melakukan Transaksi Transfer Dana sesuai Perintah Transfer Dana;
                        </li>
                        <li>
                          Berdasarkan hasil pemeriksaan Kami, instruksi Anda terindikasi, mengandung dan/atau terdapat unsur penipuan dan/atau <i>Money Laundering</i> dan/atau tindakan pidana lainnya;
                        </li>
                        <li>
                          Transaksi Transfer Dana tersebut merupakan suatu perbuatan yang melanggar ketentuan Hukum Yang Berlaku.
                        </li>
                      </ol>
                    </li>
                    <li>
                      Anda memahami bahwa sesuai ketentuan Hukum Yang Berlaku terdapat transaksi-transaksi tertentu yang wajib Kami laporkan kepada regulator terkait.
                    </li>
                    <li>
                      Setelah Transaksi Transfer Dana berhasil dilakukan, Kami akan menerbitkan bukti Transaksi Transfer Dana yang telah berhasil dilaksanakan.
                    </li>
                    <li>
                      Biaya yang dikenakan atas setiap transaksi transfer yang berhasil adalah sebesar Rp 1,000,- (Seribu Rupiah) hingga Rp 5,000,- (Lima Ribu Rupiah), tergantung tujuan bank dimana akan didebet secara langsung pada Saldo Deposit Anda.
                    </li>
                  </ol>
                </li>
                <li className="my-24">
                  <b>PERNYATAAN dan JAMINAN</b>
                  <p>Anda dengan tanpa syarat dan tidak dapat ditarik kembali menyatakan dan menjamin bahwa:</p>
                  <ol className="list-lower-alpha">
                    <li>
                      Jika Anda melakukan pendaftaran atau mengunduh Platform atas nama suatu badan hukum, atau entitas lainnya, Anda dengan ini menyatakan dan menjamin bahwa Anda memiliki kapasitas, hak dan wewenang yang sah untuk bertindak untuk dan atas nama badan hukum, entitas lainnya dan tunduk pada seluruh isi Syarat dan Ketentuan ini. Anda juga menjamin bahwa Layanan Dipay <i>Disbursement</i>, Platform maupun Akun akan digunakan untuk kepentingan Anda sendiri atau untuk kepentingan badan hukum atau pihak lain yang secara sah Anda wakili.
                    </li>
                    <li>
                      Anda merupakan pihak yang cakap secara hukum (tidak cacat hukum), tidak dalam kondisi dipaksa atau diancam (tidak cacat kehendak), mempunyai kuasa dan kewenangan untuk sepakat, tunduk pada, mengikatkan diri serta melaksanakan seluruh tugas dan tanggung jawabnya yang diatur dalam Syarat dan Ketentuan ini.
                    </li>
                    <li>
                      Dana yang digunakan tidak diperoleh secara melawan hukum atau berhubungan dengan tindak pidana termasuk namun tidak terbatas pada tindakan pencucian uang, korupsi, dan tindak pidana lainnya berdasarkan Hukum Yang Berlaku, serta Anda memiliki hak atau kewenangan untuk memberikan Perintah Transfer Dana. Anda dengan ini bertanggung jawab sepenuhnya atas kebenaran atas setiap informasi yang Anda berikan kepada Kami, serta wajib bertanggung jawab secara perdata maupun pidana, apabila terdapat pernyataan yang tidak sesuai dengan keadaan sebenarnya.
                    </li>
                    <li>
                      Anda dengan ini melepaskan Kami dari segala bentuk tanggung jawab dan wajib mengganti seluruh kerugian yang Kami alami atas penggunaan Platform untuk tindakan melanggar hukum, termasuk namun tidak terbatas pada tindakan pencucian uang, pendanaan terorisme, korupsi, dan tindak pidana lainnya berdasarkan Hukum Yang Berlaku. Seluruh Data baik yang telah Anda sampaikan atau cantumkan maupun yang akan Anda sampaikan atau cantumkan di kemudian hari atau dari waktu ke waktu adalah benar, lengkap, akurat terkini dan tidak menyesatkan serta tidak melanggar hak (termasuk tetapi tidak terbatas pada hak kekayaan intelektual) atau kepentingan pihak manapun. Penyampaian Data oleh Anda kepada Kami atau melalui Platform tidak bertentangan dengan Hukum Yang Berlaku serta tidak melanggar akta, perjanjian, kontrak, kesepakatan atau dokumen lain dimana Anda merupakan pihak atau dimana Anda atau aset Anda terikat.
                    </li>
                    <li>
                      Dengan menggunakan Layanan Dipay <b>Disbursement</b> melalui <b>Platform</b>, Anda memahami bahwa seluruh komunikasi dan instruksi dari Anda yang diterima oleh Kami akan diperlakukan sebagai suatu pembuktian yang valid, dan dengan demikian, Anda setuju untuk mengganti rugi dan melepaskan Kami dari segala kerugian, tanggung jawab, tuntutan dan pengeluaran (termasuk biaya litigasi) yang dapat timbul sehubungan dengan pelaksanaan instruksi Anda.
                    </li>
                    <li>
                      Dengan Anda menggunakan Layanan Dipay <b>Disbursement</b> melalui <b>Platform</b>, terdapat manfaat
                      yang didapat, ialah:
                      <ol type="a">
                        <li>Tarif lebih murah</li>
                        <li>Notifikasi status transaksi</li>
                        <li>Cek rekening tujuan otomatis</li>
                        <li>Kirim uang 7x24 jam secara real-time</li>
                        <li>Transfer ke banyak tujuan sekaligus</li>
                      </ol>
                    </li>
                  </ol>
                </li>
                <li className="my-24">
                  <b>DATA PRIBADI ANDA</b>
                  <ol>
                    <li>
                      Kami akan selalu menjaga data pribadi Anda dengan mengacu pada Syarat dan Ketentuan ini dan Hukum yang berlaku. Ketentuan lebih lanjut mengenai keamanan data pribadi Anda dapat diakses melalui Kebijakan Privasi kami yang merupakan bagian yang tidak terpisahkan dari Syarat dan Ketentuan ini.
                    </li>
                  </ol>
                </li>
                <li className="my-24">
                  <b>PENUTUPAN AKUN DAN PENGEMBALIAN DEPOSIT</b>
                  <ol>
                    <li>
                      Permintaan resmi dari Anda untuk melakukan penutupan Akun dan/atau pengembalian Deposit melalui jalur pengaduan pada Platform Kami/komunikasi lainnya dengan memenuhi persyaratan dan ketentuan yang diberikan oleh Kami, serta Kami berhak untuk melakukan Penutupan Akun Anda secara paksa untuk jangka waktu tertentu dan/atau seterusnya berdasarkan analisis, pertimbangan, serta keputusan yang Kami dan pihak yang ikut serta anggap tepat dan masuk akal.
                    </li>
                  </ol>
                </li>
                <li className="my-24">
                  <b>HAK KEKAYAAN INTELEKTUAL</b>
                  <ol>
                    <li>
                      Platform, Fitur, nama, nama dagang, logo, nuansa, tampilan, tulisan, gambar, video, konten, kode pemrograman, layanan dan materi lainnya yang disediakan oleh Kami (“Materi”) dilindungi oleh hak kekayaan intelektual berdasarkan Hukum Yang Berlaku. Seluruh hak, kepemilikan dan kepentingan dalam Materi adalah milik Kami seluruhnya dan Kami memberikan Anda lisensi non-eksklusif yang terbatas, tidak dapat dijual dan tidak dapat dialihkan yang mana dapat dicabut atau ditarik kembali atas kewenangan Kami sendiri. Anda dengan ini memahami bahwa Anda tidak akan memiliki hak, kepemilikan atau kepentingan terhadap Materi kecuali ditentukan lain oleh Syarat dan Ketentuan ini.
                    </li>
                    <li>
                      Anda dilarang untuk menyalin, mengubah, mencetak, mengadaptasi, menerjemahkan, menciptakan karya tiruan dari, mendistribusikan, memberi lisensi, menjual, memindahkan, menggandakan, membuat karya turunan dari Materi, menyiarkan lewat media online maupun offline, membongkar, atau mengeksploitasi bagian mana pun dari Platform Kami.
                    </li>
                    <li>
                      Jika Kami menemukan adanya indikasi/dugaan pelanggaran Syarat dan Ketentuan ini khususnya perihal hak kekayaan intelektual, Kami berhak untuk melakukan investigasi lebih lanjut, mengakhiri akses Anda terhadap Platform beserta Fitur di dalamnya, serta melakukan upaya hukum lainnya untuk menindaklanjuti indikasi/dugaan pelanggaran tersebut.
                    </li>
                  </ol>
                </li>
                <li className="my-24">
                  <b>LARANGAN</b>
                  <p>
                    Anda hanya dapat mengakses dan menggunakan Layanan Dipay <i>Disbursement</i> pada Platform untuk kepentingan usaha Anda tanpa melanggar Hukum. Pada saat mengakses Platform dan/atau menggunakan Layanan Dipay <i>Disbursement</i> Anda dilarang untuk:
                  </p>
                  <ol>
                    <li>
                      Dengan sengaja dan tanpa hak atau melawan hukum dengan cara apapun melakukan penyadapan, peretasan, mengubah, menambah, mengurangi, merusak, menghilangkan, memindahkan, menyembunyikan atas data dan informasi yang berupa elektronik milik Penerima;
                    </li>
                    <li>
                      Dengan sengaja dan tanpa hak atau melawan hukum dengan cara apapun melakukan
                      tindakan yang berakibat terganggu nya akses ke Platform;
                    </li>
                    <li>
                      Dengan sengaja dan tanpa hak atau melawan hukum melakukan sublisensi, memproduksi, menjual, mengadakan untuk digunakan oleh pihak lain, mendistribusikan, menyediakan atau mengakui kepemilikan Platform, Fitur, atau hak kekayaan intelektual lainnya milik Kami;
                    </li>
                    <li>
                      Dengan sengaja atau melawan hukum melakukan manipulasi, perubahan, penghilangan, pengrusakan sebagian atau seluruh Platform.
                    </li>
                    <li>
                      Kami memiliki hak untuk menggugat secara perdata maupun melakukan proses hukum secara pidana atas seluruh perbuatan yang dilarang sebagaimana dimaksud dalam Pasal ini maupun Syarat dan Ketentuan ini secara keseluruhan.
                    </li>
                  </ol>
                </li>
                <li className="my-24">
                  <b>BATASAN TANGGUNG JAWAB DAN GANTI RUGI</b>
                  <ol>
                    <li>
                      Kami selalu berupaya untuk menjaga Platform Kami aman, nyaman, dan berfungsi dengan baik. Namun, Kami tidak dapat menjamin bahwa Platform ini akan beroperasi secara terus-menerus atau akses ke Platform Kami dapat selalu sempurna.
                    </li>
                    <li>
                      Kami tidak dapat memungkiri kemungkinan Platform Kami sewaktu-waktu tidak dapat diakses, dalam perbaikan, atau mengalami kendala yang akan Kami perbaiki secepatnya. Penggunaan Anda terhadap Platform, Fitur, Layanan Dipay <i>Disbursement</i>, dan konten di dalamnya merupakan risiko Anda sendiri. Platform ini Kami sediakan bagi Anda dalam keadaan <b>"sebagaimana adanya"</b> dan <b>"sebagaimana tersedia"</b>.
                    </li>
                    <li>
                      Segala kerusakan yang terjadi pada jaringan komputer, telepon seluler, aplikasi, atau perangkat lainnya milik Anda akibat penggunaan Platform merupakan tanggung jawab Anda sepenuhnya.
                    </li>
                    <li>
                      Sepanjang diizinkan Hukum yang berlaku, Kami dengan ini tidak bertanggung jawab atas:
                      <ol type="a">
                        <li>
                          (i) hilangnya penggunaan; (ii) hilangnya keuntungan; (iii) hilangnya pendapatan; (iv) hilangnya data; (iv) hilangnya keuntungan yang diperkirakan, untuk setiap kasus baik secara langsung maupun tidak langsung;
                        </li>
                        <li>
                          Kerugian tidak langsung, kerugian immaterial, khusus atau konsekuensial, yang timbul dari atau sehubungan dengan penggunaan atau ketidakmampuan untuk menggunakan Platform, Fitur, Layanan Dipay <i>Disbursement</i> , termasuk, dengan tidak terbatas pada, setiap kerugian yang diakibatkan olehnya, baik jika telah diberi tahu atau tidak tentang kemungkinan kerugian tersebut;
                        </li>
                        <li>
                          Kesalahan Anda dalam memberikan Perintah Transfer Dana, termasuk namun tidak
                          terbatas pada kesalahan data Penerima, kesalahan jumlah dana, dan Perintah Transfer
                          Dana ganda karena kesalahan Anda yang masuk ke sistem Kami sehingga menyebabkan
                          Transaksi Transfer Dana ganda;
                        </li>
                        <li>
                          Kelalaian Anda dalam menjaga keamanan dan
                          kerahasiaan <i>password Akun, password Transaksi</i>, OTP dan/atau kode sistem keamanan
                          lainnya yang berfungsi untuk mengautentifikasi suatu tindakan yang dilakukan oleh Anda;
                        </li>
                        <li>
                          Transaksi yang mendasari dilakukannya transfer Dana antara Pengguna dengan Penerima;
                        </li>
                        <li>
                          Transaksi yang mendasari dilakukannya transfer Dana antara Pengguna dengan Penerima;
                        </li>
                        <li>
                          Transaksi yang mendasari dilakukannya transfer Dana antara Pengguna dengan Penerima;
                        </li>
                        <li>
                          Transaksi yang mendasari dilakukannya transfer Dana antara Pengguna dengan Penerima;
                        </li>
                        <li>
                          Pemblokiran Rekening berdasarkan perintah dari Lembaga pemerintah yang berwenang
                          atau diwajibkan untuk dilakukan oleh Kami berdasarkan suatu ketentuan Hukum yang
                          berlaku;
                        </li>
                        <li>
                          Ketidakakuratan atau tidak benarnya informasi yang disampaikan oleh Pengguna;
                        </li>
                        <li>
                          Kelalaian atau kesalahan yang Anda lakukan selama mengakses dan/atau menggunakan
                          Platform;
                        </li>
                        <li>
                          Penggunaan Platform yang tidak sesuai dengan Syarat dan Ketentuan;
                        </li>
                        <li>
                          Peretasan yang dilakukan pihak ketiga kepada Akun Anda;
                        </li>
                        <li>
                          Keadaan Kahar/Force Majeure.
                        </li>
                      </ol>
                    </li>
                    <li>
                      Kami akan melakukan tindakan dalam batas yang wajar untuk membantu menyelesaikan
                      permasalahan yang Anda hadapi dalam hal terdapat kesalahan transaksi melalui Platform
                      yang timbul akibat kelalaian Anda.
                    </li>
                    <li>
                      Anda dengan ini setuju bahwa akan melepaskan Kami dan seluruh karyawan Kami dari segala tuntutan hukum yang akan ada di kemudian hari.
                    </li>
                  </ol>
                </li>
                <li className="my-24">
                  <b>PENYELESAIAN SENGKETA</b>
                  <ol>
                    <li>
                      Seluruh perselisihan yang muncul antara Anda dan Kami sehubungan dengan pelaksanaan
                      Syarat dan Ketentuan ini akan diselesaikan secara musyawarah dan mufakat.
                    </li>
                    <li>
                      Apabila tidak tercapai kesepakatan dari musyawarah dan mufakat maka akan dirujuk dan
                      diselesaikan pada Pengadilan Negeri Jakarta Selatan.
                    </li>
                  </ol>
                </li>
                <li className="my-24">
                  <b>KONTAK KAMI</b>
                  <ol>
                    <li>
                      Dalam hal Anda mengalami kendala atau memiliki pertanyaan, permintaan, atau keluhan terkait Akun dan/atau Layanan Dipay <i>Disbursement</i> selama Anda menggunakan Platform, Anda dapat menghubungi kami melalui email <a href="mailto:enterprise@dipay.id">enterprise@dipay.id</a> atau telepon ke nomor 021-252-7003.
                    </li>
                    <li>
                      Untuk merespon pertanyaan atau pengaduan Anda, Kami akan terlebih dahulu melakukan verifikasi atas data Anda. Kami berhak untuk menolak pemrosesan pertanyaan atau pengaduan Anda jika data yang Anda berikan tidak sesuai dengan data yang tertera pada sistem Kami.
                    </li>
                    <li>
                      Kami akan memberikan upaya terbaik Kami untuk membantu Anda menyelesaikan setiap kendala yang Anda alami. Agar Kami dapat meningkatkan layanan Kami, setiap korespondensi antara Anda dengan Kami dapat kami catat atau kami rekam.
                    </li>
                  </ol>
                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default TncTemplate
