import pakaiClass from 'pakai-class';
import { useMemo } from 'react';
import { useDropzone } from 'react-dropzone';
import { CheckedCircleOutlined, Dangerous, UploadDocument } from '../../../assets/icons';
import { IMAGE_FORMATS } from '../../../utils/helpers/fileFormats';
import LoadingDots from '../../Loadings/LoadingDots';
import './accordionUpload.scss'

const Upload = ({
  onChange,
  format = [...IMAGE_FORMATS],
  variant,
  customText,
  className,
  uploading,
  disabled,
  onFocus,
  maxSize,
  minSize,
  existingFile = [],
  fileName
}) => {
  const {
    acceptedFiles,
    getRootProps,
    getInputProps,
    fileRejections
  } = useDropzone({
    accept: Object.assign(...format.map(v => { return { [v]: [] } })),
    maxSize,
    minSize,
    disabled: disabled || uploading,
    onFileDialogOpen: (e) => {
      if (typeof onFocus === 'function') onFocus();
    },
    onDrop: (e) => {
      const file = e[0];
      if (!file) return
      if (typeof onChange === 'function') onChange(file);
    }
  });

  const uploadState = useMemo(() => {
    if (!uploading) {
      if (fileRejections[0]?.errors[0]?.code === "file-invalid-type") {
        return (
          <>
            <Dangerous />
            <div className="label">
              <span>{fileRejections[0].file.name}</span>
            </div>
            <div className="label">
              <h3 className="failed">File gagal diunggah</h3>
            </div>
            <div className="label">
              <button>
                <UploadDocument size="28" />
                Pilih File
              </button>
            </div>
            <div className="info">
              <span className="failed">Format file tidak didukung</span>
            </div>
          </>
        )
      } else if (fileRejections[0]?.errors[0]?.code === "file-too-large") {
        return (
          <>
            <Dangerous />
            <div className="label">
              <span>{fileRejections[0].file.name}</span>
            </div>
            <div className="label">
              <h3 className="failed">File gagal diunggah</h3>
            </div>
            <div className="label">
              <button>
                <UploadDocument size="28" />
                Pilih File
              </button>
            </div>
            <div className="info">
              <span className="failed">Ukuran file terlalu besar</span>
            </div>
          </>
        )
      } else if (fileName || acceptedFiles.length) {
        return (
          <>
            <CheckedCircleOutlined color="#05944F" />
            <div className="label">
              <span>{acceptedFiles[0]?.name ?? fileName}</span>
            </div>
            <div className="label">
              <h3 className="success">File berhasil diunggah </h3>
            </div>
            <div className="label">
              <button className="primary-light">
                <UploadDocument size="28" />
                Pilih File
              </button>
            </div>
            <div className="info">
              <span>Ukuran maksimal yang boleh diunggah: 10MB</span>
            </div>
          </>
        )
      } else {
        return (
          <>
            <div className="label">
              <h3>{customText ?? "Taruh file JPG, JPEG atau PNG di sini"}</h3>
            </div>
            <div className="label">
              <span>atau</span>
            </div>
            <div className="label">
              <button>
                <UploadDocument size="28" />
                Pilih File
              </button>
            </div>
            <div className="info">
              <span>Ukuran maksimal yang boleh diunggah: 10MB</span>
            </div>
          </>
        )
      }
    } else {
      return (
        <div className="loading">
          <div className="loadingWrapper">
            <LoadingDots />
          </div>
        </div>
      )
    }
  }, [acceptedFiles, customText, fileName, fileRejections, uploading])

  const wrapper = useMemo(() => {
    return (
      <div className="cardWrapper">
        <div className="wrapper">
          {uploadState}
        </div>
      </div>
    )
  }, [uploadState])

  return (
    <div
      {...getRootProps({ className: pakaiClass("uploadFile", [variant], className) })}
    >
      <div className="uploadCard">
        {wrapper}
        <input {...getInputProps()} />
      </div>
    </div>
  )
}

export default Upload;
