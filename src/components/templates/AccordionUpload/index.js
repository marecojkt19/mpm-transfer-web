import { Checkbox } from '@mantine/core';
import { useEffect } from 'react';
import { useCallback } from 'react';
import { useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import Accordion from "../../Accordion";
import Upload from "./Upload";

const AccordionUpload = ({ Items }) => {
  return (
    <Accordion
      variant="separated"
      multiple={true}
      Items={Items}
      open={true}
    />
  )
}

const AdditionalCheckbox = ({ Items, infoLabel, stateName, ...rest }) => {
  const { checked } = useSelector(state => state[stateName]);

  return (
    <div className="infoSection">
      {
        infoLabel && <h2>{infoLabel}</h2>
      }
      <div className="checkboxSection">
        {
          Items ? Items.map((item) => (
            <Checkbox
              key={item.name}
              label={item.label}
              name={item.name}
              checked={checked.some(e => e.name === item.name) ?? item.uploadExistingFile}
              {...rest}
            />
          )) : "Something went Wrong~"
        }
      </div>
    </div>
  )
}

const CheckboxMultiple = ({ Items, infoLabel, actions, stateKey, existingChecked, ...rest }) => {
  const [selectedName, setSelectedName] = useState(null)
  const dispatch = useDispatch();
  const handleChange = (e) => {
    const { name, value } = e.target;
    setSelectedName(name)
    dispatch(actions[stateKey](value))
  }

  return (
    <div className="infoSection">
      {
        infoLabel && <h2>{infoLabel}</h2>
      }
      <div className="checkboxSection">
        {
          Items ? Items.map(item => (
            <Checkbox
              key={item.name}
              label={item.label}
              name={item.name}
              value={item.value}
              checked={item.name === existingChecked || item.name === selectedName}
              onChange={handleChange}
              {...rest}
            />
          )) : "Something went Wrong~"
        }
      </div>
    </div>
  )
}

const dataURLtoFile = (dataurl, filename) => {
  var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], filename, { type: mime });
}

const UploadItem = ({ notes, label, fileName, name, customText, format, uploadExistingFile, actions, topElement, bottomElement }) => {
  // const { upload } = useSelector(state => state[stateName]);
  const [file, setFile] = useState(null);
  const dispatch = useDispatch();

  const handleChange = (v) => {
    setFile(v)
    if (v && name) {
      dispatch(actions.upload(v, name))
    }
  }

  useEffect(() => {
    if (uploadExistingFile && !file) {
      const files = dataURLtoFile(uploadExistingFile, fileName)
      dispatch(actions.upload(files, name));
    }
  }, [actions, dispatch, file, fileName, name, uploadExistingFile])

  return (
    <div className="uploadItem">
      {
        label && <h2>{label}</h2>
      }
      {
        topElement
        &&
        <div className="addtional">
          {topElement()}
        </div>
      }
      <div className="dropZone">
        <Upload
          customText={customText}
          format={format}
          onChange={handleChange}
          onFocus={() => setFile()}
          maxSize={10000000}
          existingFile={file ?? uploadExistingFile}
          fileName={fileName}
        />
      </div>
      {
        notes
        &&
        notes()
      }
      {
        bottomElement
        &&
        bottomElement()
      }
    </div>
  )
}

const AddtionalUploadItem = ({ Items, infoLabel, format, customText, actions, stateName }) => {
  const { checked } = useSelector(state => state[stateName]);
  const dispatch = useDispatch();
  const arr = Items.filter(item => item.uploadExistingFile)

  const handleCheckboxChange = (e) => {
    if (e.target.checked) {
      const arrShow = Items.filter(item => item.name === e.target.name);
      dispatch(actions.setChecked([...checked, ...arrShow]))
    } else {
      const arrShow = checked.filter(item => item.name !== e.target.name);
      dispatch(actions.removeFile(e.target.name))
      dispatch(actions.setChecked([...arrShow]))
    }
  }

  const load = useCallback(() => {
    if (!checked.length && arr.length) {
      dispatch(actions.setChecked([...arr]))
    }
  }, [actions, arr, checked.length, dispatch])

  useEffect(() => {
    load()
  }, [load])

  return (
    <div className="addtional">
      {
        Items
        &&
        <AdditionalCheckbox
          Items={Items}
          infoLabel={infoLabel}
          stateName={stateName}
          onChange={handleCheckboxChange}
        />
      }
      {
        checked
          ? checked.map(item => <UploadItem
            key={item.label}
            name={item?.name}
            stateName={stateName}
            actions={actions}
            fileName={item.fileName}
            format={format}
            customText={customText}
            uploadExistingFile={item.uploadExistingFile}
          />)
          : "Something went Wrong~"
      }
    </div>
  )
}

const DefaultUploadItem = ({ Items, format, customText, stateName, actions }) => {
  return (
    <div>
      {
        Items ? Items.map(item => (
          <UploadItem
            key={item.label}
            name={item.name}
            stateName={stateName}
            actions={actions}
            fileName={item.fileName}
            format={format}
            customText={customText}
            uploadExistingFile={item.uploadExistingFile}
          />
        )) : "Something went Wrong~"
      }
      <br />
    </div>
  )
}

const UploadItemMultiple = ({ notes, Items, format, customText, AdditionalItems, actions, stateName }) => {
  return (
    <div className="uploadItem">
      {
        Items
          ?
          <DefaultUploadItem
            Items={Items}
            actions={actions}
            stateName={stateName}
            customText={customText}
            format={format}
          />
          : "Something went Wrong~"
      }
      {
        AdditionalItems
          ?
          <AddtionalUploadItem
            Items={AdditionalItems}
            actions={actions}
            stateName={stateName}
            customText={customText}
            format={format}
            infoLabel="Centang apabila Anda memiliki Akta Perubahan Anggaran Dasar dan/atau Akta Perubahan Pengurus"
          />
          : "Something went Wrong~"
      }
      {
        notes
        &&
        notes()
      }
    </div>
  )
}

export { AccordionUpload, UploadItem, UploadItemMultiple, AddtionalUploadItem, CheckboxMultiple };
