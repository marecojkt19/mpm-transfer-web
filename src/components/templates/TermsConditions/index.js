import React from 'react'
import MainButton from '../../Form/MainButton'
import styles from './termsConditions.module.scss'

const TermsConditions = ({
  onClose,
  onSubmit,
  title,
  description,
  loading,
  customCancelTitle,
  customNextTitle
}) => {
  return (
    <div className={styles.modalWrapper}>
      <div className={styles.modalCard}>
        <div className={styles.heading}>
          <h1>{title}</h1>
        </div>
        <div className={styles.modalLine}></div>
        <div className={styles.infoWrapper}>
          <p>{description}</p>
        </div>
        <div className={styles.buttons}>
          <MainButton
            variant="secondary"
            onClick={onClose}
          >
            {customCancelTitle || "Batalkan"}
          </MainButton>
          <MainButton
            onClick={onSubmit}
            loading={loading}
          >
            {customNextTitle || "Ya, Lanjutkan"}
          </MainButton>
        </div>
      </div>
    </div>
  )
}

export default TermsConditions
