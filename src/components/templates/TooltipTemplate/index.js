import React, { useState } from 'react'
import { CloseCircle, Tooltip } from '../../../assets/icons';
import Modal from '../../Modal'
import styles from './tooltip.module.scss'

const TooltipTemplate = ({
  label,
  heading,
  content
}) => {
  const [showTooltip, setShowTooltip] = useState(false);

  return (
    <>
      <button
        type="button"
        className={styles.tooltip}
        onClick={() => setShowTooltip(true)}
      >
        <Tooltip />
        <span>{label}</span>
      </button>
      <Modal in={showTooltip} onClose={() => setShowTooltip(false)}>
        <div className={styles.modalCard}>
          <div className={styles.header}>
            <button onClick={() => setShowTooltip(false)}>
              <CloseCircle />
            </button>
          </div>
          <div className={styles.imgWrapper}>
            <img
              src="/assets/media/others/info.png"
              alt=""
            />
          </div>
          <div className={styles.tooltipWrapper}>
            <h5>{heading}</h5>
            <p>{content}</p>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default TooltipTemplate
