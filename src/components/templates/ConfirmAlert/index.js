import React from 'react'
import MainButton from '../../Form/MainButton'
import styles from './confirmAlert.module.scss'
import { ModalBody, ModalHead, ModalWrapper } from '../../ModalNew'

const ConfirmAlert = ({
  onClose,
  onSubmit,
  title,
  description,
  subTitle,
  loading,
  customCancelTitle,
  customNextTitle
}) => {
  return (
    <ModalWrapper width={560}>
      <ModalHead
        title={title}
        onClose={onClose}
      />
      <ModalBody>
        <div className={styles.infoWrapper}>
          {description ? <p>{description}</p> : null}
          {subTitle ? <h4>{subTitle}</h4> : null}
        </div>
        <div className={styles.buttons}>
          <MainButton
            variant="secondary"
            onClick={onClose}
          >
            {customCancelTitle || "Batalkan"}
          </MainButton>
          <MainButton
            onClick={() => {
              onSubmit()
              onClose()
            }}
            loading={loading}
          >
            {customNextTitle || "Ya, Lanjutkan"}
          </MainButton>
        </div>
      </ModalBody>
    </ModalWrapper>
  )
}

export default ConfirmAlert
