import React from 'react'
import styles from './backButton.module.scss'
import ArrowBackIcon from '../../../assets/icons/create/arrow-back'

const BackButton = ({
  onClick
}) => {
  return (
    <div className={styles.backButton}>
      <button
        onClick={onClick}
      >
        <ArrowBackIcon />
        <span>Kembali</span>
      </button>
    </div>
  )
}

export default BackButton
