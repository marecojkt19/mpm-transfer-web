import React from 'react'
import { CloseCircle } from '../../../assets/icons';
import Modal from '../../Modal'
import styles from './tooltip.module.scss'

const TooltipTemplate = ({
  heading,
  content,
  show,
  onClose,
  onClick
}) => {
  return (
    <Modal in={show} onClose={onClose}>
      <div className={styles.modalCard}>
        <div className={styles.header}>
          <button onClick={onClose}>
            <CloseCircle />
          </button>
        </div>
        <div className={styles.imgWrapper}>
          <img
            src="/assets/media/others/info.png"
            alt=""
          />
        </div>
        <div className={styles.tooltipWrapper}>
          <h5>{heading}</h5>
          <p>{content}</p>
        </div>
      </div>
    </Modal>
  )
}

export default TooltipTemplate
