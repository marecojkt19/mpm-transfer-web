import pakaiClass from 'pakai-class'
import React from 'react'
import Button from '../Button'
import styles from './templates.module.scss'

const MainButton = ({
  children,
  className,
  variant = 'primary',
  loading,
  ...props
}) => {
  return (
    <Button
      {...props}
      className={pakaiClass(styles.mainButton, variant && styles[variant], className)}
    >
      {loading ? "loading.." : children}
    </Button>
  )
}

export default MainButton
