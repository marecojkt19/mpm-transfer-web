import React from 'react'
import { PushPin } from '../../../../assets/icons'
import styles from './notes.module.scss'

const Notes = ({ withoutPin, text }) => (
  <div className={styles.notes}>
    {
      !withoutPin
      &&
      <PushPin />
    }
    {text ?? <p>Dokumen harus <b>jelas (tidak kabur), lengkap, dan tidak terpotong.</b></p>}
  </div>
)

export default Notes
