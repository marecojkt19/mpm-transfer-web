import React, { useState } from 'react'
import { Eye } from '../../../../assets/icons';
import MainButton from '../../../Form/MainButton';
import Modal from '../../../ModalNew';
import SVG from '../../../SVG';
import styles from './fileField.module.scss'

const FileField = ({
  file,
  fileName,
  onEdit
}) => {
  const [fileViewOpen, setFileViewOpen] = useState(false);

  return (
    <>
      <div className={styles.fileField}>
        <div className={styles.wrapper}>
          {fileName && fileName.split('.').pop().toUpperCase() === "PDF" ?
            // <div className={styles.pdf}>
            //   <SVG src="/assets/icons/picture_as_pdf.svg" />
            // </div>
            <div className={styles.pdf}>
              <SVG src="/assets/icons/picture_as_pdf.svg" />
            </div>
            :
            <button
              onClick={() => setFileViewOpen(true)}
            >
              <div className={styles.image}>
                <img src={file} alt="" />
                <Eye />
              </div>
            </button>
          }
          <div className={styles.filename}>
            <h3>{fileName}</h3>
          </div>
        </div>
        <div className={styles.buttonWrapper}>
          <MainButton
            variant='secondary'
            onClick={onEdit}
          >
            Ubah
          </MainButton>
        </div>
      </div>
      <Modal
        in={fileViewOpen}
        onClose={() => setFileViewOpen(false)}
      >
        <div className={styles.modalWrapper}>
          <div className={styles.image}>
            <img src={file} alt="" />
          </div>
        </div>
      </Modal>
    </>
  )
}

export default FileField
