import pakaiClass from 'pakai-class';
import React, { useMemo, useRef, useState } from 'react'
import { BrandingWatermark, ChevronDown } from '../../../../assets/icons'
import MainButton from '../../../Form/MainButton';
import styles from './accordion.module.scss'

const Accordion = ({
  title,
  subTitle,
  children,
  isOpen,
  editConfig
}) => {
  const [open, setOpen] = useState(isOpen);
  const contentRef = useRef();

  const customHeight = useMemo(() => {
    if (open) {
      return "auto"
    }

    return 0
  }, [open])

  return (
    <div className={styles.accordion}>
      <button type="button" onClick={() => setOpen(prev => !prev)} className={styles.label}>
        <div className={styles.wrapper}>
          <BrandingWatermark />
          <div className={pakaiClass(styles.title, subTitle && styles.subTitle)}>
            <h1>{title}</h1>
            <p>{subTitle}</p>
          </div>
        </div>
        <div className={pakaiClass(styles.dropdown, open && styles.open)}>
          <ChevronDown size={48} />
        </div>
      </button>

      <div
        className={pakaiClass(styles.children, open && styles.open)}
        style={{ height: customHeight }}
      >
        <div ref={contentRef}>
          {children}
        </div>
      </div>
      {editConfig?.isEdit &&
        <div className={styles.buttonWrapper}>
          <MainButton
            variant='plain'
            onClick={editConfig?.onclose}
            type="button"
          >
            Batal
          </MainButton>
          <MainButton
            variant='secondary'
            loading={editConfig?.loading}
            disabled={editConfig?.disabled}
          >
            Simpan
          </MainButton>
        </div>
      }
    </div>
  )
}

export default Accordion
