import React, { useState } from 'react'
import { CheckedCircle2, XIcon } from '../../../../assets/icons';
import MainButton from '../../../Form/MainButton';
import Modal from '../../../ModalNew';
import Notes from '../Notes';
import styles from './uploadExample.module.scss'

const UploadExample = () => {
  const [showExample, setShowExample] = useState(false);

  return (
    <>
      <button
        type="button"
        className={styles.uploadExample}
        onClick={() => setShowExample(true)}
      >
        <span>Lihat contoh</span>
      </button>
      <Modal in={showExample} onClose={() => setShowExample(false)}>
        <div className={styles.modalUploadExampleCard}>
          <div className={styles.header}>
            <h1>Contoh Foto Dokumen Identitas</h1>
          </div>
          <div className={styles.line}></div>
          <div className={styles.wrapper}>
            <div className={styles.correctCard}>
              <div className={styles.label}>
                <CheckedCircle2 />
                <span>Benar</span>
              </div>
              <img src="/assets/example/correct.png" alt="" />
            </div>
            <div className={styles.wrongCard}>
              <div className={styles.label}>
                <XIcon size="22" color="#E11900" />
                <span>Salah</span>
              </div>
              <img src="/assets/example/wrong.png" alt="" />
            </div>
          </div>

          <Notes />

          <div className={styles.button}>
            <MainButton
              type="button"
              onClick={() => setShowExample(false)}
            >
              Oke, Mengerti
            </MainButton>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default UploadExample
