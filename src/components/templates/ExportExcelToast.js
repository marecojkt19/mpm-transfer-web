import React from 'react';
import styles from './templates.module.scss'

const ExportExcelToast = ({ filename, progress, isNotValid }) => {
  if (isNotValid) {
    return (
      <div className={styles.exportExcel}>
        <h5>Export Excel</h5>
        <p>Data tidak tersedia</p>
      </div>
    )
  } else {
    return (
      <div className={styles.exportExcel}>
        <h5>Export Excel</h5>
        <p>{filename}</p>
        <p>{Math.round(progress * 100)}%</p>
      </div>
    )
  }
}

export default ExportExcelToast
