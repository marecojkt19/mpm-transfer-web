import pakaiClass from 'pakai-class';
import { useMemo } from 'react';
import { useDropzone } from 'react-dropzone';
import { CheckedCircleOutlined, Dangerous, DriveFolderUpload } from '../../assets/icons';
import { IMAGE_FORMATS, PDF_FORMATS } from '../../utils/helpers/fileFormats';
import LoadingDots from '../Loadings/LoadingDots';
import MainButton from '../Form/MainButton';
import styles from './uploadFile.module.scss'

const UploadFile = ({
  onChange,
  format = [...IMAGE_FORMATS, ...PDF_FORMATS],
  variant,
  className,
  uploading,
  disabled,
  onFocus,
  maxSize,
  minSize
}) => {
  const {
    acceptedFiles,
    getRootProps,
    getInputProps,
    fileRejections
  } = useDropzone({
    accept: Object.assign(...format.map(v => { return { [v]: [] } })),
    maxSize,
    minSize,
    disabled: disabled || uploading,
    onFileDialogOpen: (e) => {
      if (typeof onFocus === 'function') onFocus();
    },
    onDrop: (e) => {
      const file = e[0];
      if (!file) return
      if (typeof onChange === 'function') onChange(file);
    }
  });

  const uploadState = useMemo(() => {
    if (!uploading) {
      if (acceptedFiles.length) {
        return (
          <>
            <CheckedCircleOutlined />
            <div className={styles.info}>
              <span>{acceptedFiles[0]?.name}</span>
            </div>
            <div className={styles.label}>
              <h5>Bukti Pembayaran berhasil diupload, </h5>
              <h6>Status transaksi dapat di cek pada menu riwayat deposit</h6>
              <MainButton
                variant="primary-light"
              >
                Pilih Ulang Gambar
              </MainButton>
            </div>
          </>
        )
      } else if (fileRejections[0]?.errors[0]?.code === "file-invalid-type") {
        return (
          <>
            <Dangerous />
            <div className={styles.info}>
              <span>{fileRejections[0].file.name}</span>
            </div>
            <div className={styles.label}>
              <h5>Format Bukti Pembayaran Tidak Sesuai</h5>
              <h6>Pastikan Format JPG, JPEG, PNG & PDF</h6>
              <MainButton
                variant="primary-light"
              >
                Pilih Ulang Gambar
              </MainButton>
            </div>
          </>
        )
      } else if (fileRejections[0]?.errors[0]?.code === "file-too-large") {
        return (
          <>
            <Dangerous />
            <div className={styles.info}>
              <span>{fileRejections[0].file.name}</span>
            </div>
            <div className={styles.label}>
              <h5>Bukti Pembayaran Lebih dari 2 MB</h5>
              <h6>Pastikan Bukti Pembayaran kurang dari 2 MB</h6>
              <MainButton
                variant="primary-light"
              >
                Pilih Ulang Gambar
              </MainButton>
            </div>
          </>
        )
      } else {
        return (
          <>
            <DriveFolderUpload />
            <div className={styles.info}>
              <span>Format Gambar : JPG, JPEG, PNG & PDF Max 2 MB</span>
            </div>
            <div className={styles.label}>
              <h5>Taruh Gambar Disini</h5>
              <h4>atau</h4>
              <MainButton
                variant="primary-light"
              >
                Pilih Bukti
              </MainButton>
            </div>
          </>
        )
      }
    } else {
      return (
        <div className={styles.loading}>
          <div className={styles.loadingWrapper}>
            <LoadingDots />
          </div>
        </div>
      )
    }
  }, [acceptedFiles, fileRejections, uploading])

  const wrapper = useMemo(() => {
    return (
      <div
        className={
          pakaiClass(
            styles.cardWrapper,
            fileRejections.length && styles.error,
            acceptedFiles.length && styles.success,
          )
        }
      >
        <div className={styles.wrapper}>
          {uploadState}
        </div>
      </div>
    )
  }, [fileRejections, acceptedFiles, uploadState])

  return (
    <div
      {...getRootProps({ className: pakaiClass(styles.uploadFile, styles[variant], className) })}
    >
      <div className={styles.uploadCard}>
        {wrapper}
        <input {...getInputProps()} />
      </div>
    </div>
  )
}

export default UploadFile
