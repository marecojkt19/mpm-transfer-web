import { useCallback, useState } from "react";
import styles from "./dates.module.scss";
import { DateRangePicker as MantineDateRangePicker } from '@mantine/dates';
import { Calendar } from "../../assets/icons";

function DateRangePicker(props) {
  const [defaultValue, setDefaultValue] = useState([
    props.startDate ? new Date(props.startDate) : null,
    props.endDate ? new Date(props.endDate) : null,
  ]);

  const handleChange = useCallback(
    (date) => {
      props.handleChange(date[0], date[1])
      setDefaultValue([date[0], date[1]])
    },
    [props]
  )

  return (
    <MantineDateRangePicker
      firstDayOfWeek="sunday"
      inputFormat={'D MMM YYYY'}
      icon={
        <div style={{ margin: '5px 0 0 14px' }}>
          <Calendar />
        </div>
      }
      weekendDays={[0]}
      allowSingleDateInRange
      className={styles.wrapperDateRangePicker}
      defaultValue={defaultValue}
      clearable
      onChange={handleChange}
      {...props}
    />
  )
}

export default DateRangePicker;