import React, { useMemo } from 'react'
import clsx from 'clsx';
import styles from './button.module.scss'
import CircularProgress from '../../Loadings/CircularProgress';
import { Link } from 'react-router-dom';

const MainButton = ({
  children,
  variant = "primary",
  className,
  icon,
  onClick,
  width,
  height,
  form,
  link,
  size,
  loading,
  disabled,
  type = "submit",
  href,
  target,
  title
}) => {
  const ActionTag = useMemo(() => href || link ? Link : "button", [href, link])

  return (
    <ActionTag
      style={{ height, width }}
      form={form}
      onClick={onClick}
      disabled={disabled}
      type={type}
      to={href}
      href={link}
      target={target}
      title={title}
      className={clsx(
        styles.mainButton,
        variant && styles[variant],
        size && styles[size],
        disabled && styles.disabled,
        className
      )}
    >
      {(!loading && icon) && icon}
      {children && <span>{children}</span>}
      {loading && <CircularProgress />}
    </ActionTag>
  )
}

export default MainButton
