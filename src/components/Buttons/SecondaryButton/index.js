import React from 'react'
import SVG from '../../SVG'
import clsx from 'clsx';
import styles from './button.module.scss'

const SecondaryButton = ({
  title,
  variant,
  className,
  icon
}) => {
  return (
    <button
      className={clsx(
        styles.secondaryButton,
        variant && styles[variant],
        className
      )}>
      <span>{title}</span>
      <SVG src={icon} />
    </button>
  )
}

export default SecondaryButton
