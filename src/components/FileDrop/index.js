import pakaiClass from 'pakai-class';
import React, { useCallback, useRef, useState } from 'react'
import { IMAGE_FORMATS, PDF_FORMATS } from '../../utils/helpers/fileFormats';
import MainButton from '../templates/MainButton';
import styles from './fileDrop.module.scss'

const eventHasFiles = (event) => {
  let hasFiles = false;
  if (event.dataTransfer) {
    const types = event.dataTransfer.types;
    for (const keyOrIndex in types) {
      if (types[keyOrIndex] === 'Files') {
        hasFiles = true;
        break;
      }
    }
  }
  return hasFiles;
}

const FileDrop = ({
  className,
  onDrop,
  onDragOver,
  onDragLeave,
  onChange,
  accept = [
    ...IMAGE_FORMATS,
    ...PDF_FORMATS
  ]
}) => {
  const [draggingOver, setDraggingOver] = useState(false)
  const inputRef = useRef()

  const handleChange = useCallback((e) => {
    const file = e.target.files[0];
    if (!file || !accept.includes(file.type)) {
      inputRef.current.value = "";
      return
    }
    if (typeof onChange === 'function') onChange(file);
  }, [onChange, accept])

  const handleDragOver = (e) => {
    e.preventDefault()
    if (eventHasFiles(e)) {
      setDraggingOver(true)
      e.dataTransfer.dropEffect = 'copy'
      if (typeof onDragOver === 'function') onDragOver(e)
    }
  }

  const handleDragLeave = (e) => {
    e.preventDefault()
    setDraggingOver(false)
    if (typeof onDragLeave === 'function') onDragLeave(e)
  }

  const handleDrop = (e) => {
    e.preventDefault()
    setDraggingOver(false)
    if (eventHasFiles(e) && typeof onDrop === 'function' && e.dataTransfer?.files?.length === 1) onDrop(e.dataTransfer?.files[0], e)
  }

  return (
    <div
      className={pakaiClass(
        styles.fileDrop,
        draggingOver && styles.draggingOver,
        className
      )}
      onDragOver={handleDragOver}
      onDragLeave={handleDragLeave}
      onDrop={handleDrop}
    >
      <p className={`${styles.textPrimary} mb-16`}>Taruh file XLSX disini</p>
      <p className={`${styles.textSecondary} mb-16`}>atau</p>
      <MainButton
        onClick={_ => inputRef?.current?.click()}
        className={styles.uploadButton}
      >
        Pilih File
      </MainButton>
      <p className={styles.textSecondary}>Ukuran maksimal file: <span className="font-500">2.0 MB</span></p>
      <input
        type="file"
        onChange={handleChange}
        ref={inputRef}
        accept={accept.join(', ')}
      />
    </div>
  )
}

export default FileDrop
