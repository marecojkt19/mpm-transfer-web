import pakaiClass from 'pakai-class'
import React, { Children, cloneElement, useLayoutEffect, useRef } from 'react'
import { useState } from 'react'
import { Transition } from 'react-transition-group'
import { Checked } from '../../assets/icons'
import styles from './steps.module.scss'

const duration = 100;

const defaultStyle = {
  transition: `transform ${duration}ms ease-in-out, opacity ${duration}ms ease-in-out`,
  opacity: 0,
}

const transitionStyles = {
  entering: { opacity: 0, transform: 'translateY(-5px)' },
  entered: { opacity: 1 },
  exiting: { opacity: 0, transform: 'translateY(5px)', position: 'absolute', top: 0, left: 0, width: '100%', height: 0 },
}

const Steps = ({
  children,
  className,
  activeStep
}) => {
  const nodeRef = useRef();

  return (
    <>
      <div className={pakaiClass(styles.steps, className)}>
        {Children.map(children, (child, i) =>
          child ?
            cloneElement(child, {
              index: i,
              active: child.props?.active ?? activeStep === i,
              activeStep: child.props?.activeStep ?? activeStep
            })
            :
            null
        )}
      </div>
      <div className='relative'>
        {Children.map(children, (child, i) => {
          return child ?
            <Transition
              nodeRef={nodeRef}
              in={child.props?.active ?? (activeStep >= children.length ? children.length === (i + 1) : activeStep === i)}
              timeout={duration}
              appear={true}
              unmountOnExit
            >
              {state =>
                <div
                  ref={nodeRef}
                  style={{
                    ...defaultStyle,
                    ...transitionStyles[state],
                  }}>
                  {child.props.children}
                </div>
              }
            </Transition>
            :
            null
        }
        )}
      </div>
    </>
  )
}

export const Step = ({
  index,
  label,
  active,
  activeStep
}) => {
  const labelRef = useRef();
  const ellipseRef = useRef();
  const [gapWidth, setGapWidth] = useState(0);

  useLayoutEffect(() => {
    if (labelRef.current && ellipseRef.current) {
      setGapWidth((56 - ellipseRef.current.clientWidth));
    }
  }, [labelRef, ellipseRef, index]);

  return (
    <div className="relative">
      <div
        className={styles.step}
        style={{ marginLeft: index === 0 ? 0 : (gapWidth + 8) }}
      >
        <div
          key={index}
          ref={ellipseRef}
          className={pakaiClass(styles.ellipse, activeStep >= index ? "bg-primary" : "")}
        >
          {(index + 1) <= activeStep ? <Checked /> : <h3 className={styles.number}>{index + 1}</h3>}
        </div>
        <p ref={labelRef} className={pakaiClass(styles.label, (active || activeStep >= index) && styles.active)}>{label}</p>
        <div
          style={{ width: gapWidth }}
          className={pakaiClass(styles.line, index <= activeStep && styles.active, index === 0 && "d-none")}
        ></div>
      </div>
    </div>
  )
}

export default Steps
