import pakaiClass from 'pakai-class'
import React from 'react'
import { Link } from 'react-router-dom'
import CircularProgress from '../../Loadings/CircularProgress'
import styles from './mainButton.module.scss'

const MainButton = ({
  children,
  variant = "primary",
  icon,
  className,
  loading,
  disabled,
  href,
  ...props
}) => {
  const ActionTag = href ? Link : 'button';

  return (
    <ActionTag
      {...props}
      disabled={disabled || loading}
      to={href}
      className={pakaiClass(
        styles.mainButton,
        styles[variant],
        className
      )}
    >
      {icon}
      <span>{children}</span>
      {loading && <CircularProgress />}
    </ActionTag>
  )
}

export default MainButton
