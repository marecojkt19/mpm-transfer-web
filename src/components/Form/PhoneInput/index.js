import pakaiClass from 'pakai-class';
import React, { useState } from 'react'
import NumberFormat from 'react-number-format';
import styles from './phoneInput.module.scss'

const PhoneInput = ({
  value,
  onChange,
  onBlur,
  onFocus,
  format,
  defaultValue,
  label,
  helperText,
  error,
  placeholder,
  className,
  disabled,
  allowLeadingZero = true,
  allowDot = true,
  maxLength,
  name
}) => {
  const [isFilled, setIsFilled] = useState(false);
  const [focus, setFocus] = useState(false);

  return (
    <div
      className={pakaiClass(
        styles.phoneInput,
        error && styles.error,
        (focus || isFilled) && styles.focus,
        className
      )}
    >
      <div className={styles.label}>
        {label && <p>{label}</p>}
      </div>
      <div className={styles.wrapper}>
        <div className={styles.phoneCode}>
          <img src="/assets/media/others/flag.png" alt="" width={16} height={16} />
          <h5>+62</h5>
        </div>
        <div className={styles.inputField}>
          <NumberFormat
            value={value}
            defaultValue={defaultValue}
            allowNegative={false}
            format={format}
            name={name}
            decimalSeparator={false}
            placeholder={placeholder}
            disabled={disabled}
            onValueChange={({ value }) => {
              setIsFilled(Boolean(value))
              if (typeof onChange === "function") onChange(value)
            }}
            onBlur={(e) => {
              setFocus(false);
              if (typeof onBlur === "function") onBlur(e);
            }}
            onFocus={(e) => {
              setFocus(true);
              if (typeof onFocus === "function") onFocus(e);
            }}
            isAllowed={(values) => {
              const { value, floatValue } = values;

              if (typeof floatValue === 'undefined' || typeof value === 'undefined') {
                return true
              }

              if (allowDot) {
                if (allowLeadingZero) {
                  if (maxLength) {
                    if (value.length <= maxLength) {
                      return true
                    }
                  } else {
                    return true
                  }
                } else {
                  if (value.charAt(0) !== '0') {
                    if (maxLength) {
                      if (value.length <= maxLength) {
                        return true
                      }
                    } else {
                      return true
                    }
                  }
                }
              } else {
                if (value.charAt(0) !== '.') {
                  if (allowLeadingZero) {
                    if (maxLength) {
                      if (value.length <= maxLength) {
                        return true
                      }
                    } else {
                      return true
                    }
                  } else {
                    if (value.charAt(0) !== '0') {
                      if (maxLength) {
                        if (value.length <= maxLength) {
                          return true
                        }
                      } else {
                        return true
                      }
                    }
                  }
                }
              }
            }}
          />
          {
            (helperText || error) &&
            <div className={styles.helperText}>
              <p>
                {helperText}
              </p>
            </div>
          }
        </div>
      </div>
    </div>
  )
}

export default PhoneInput
