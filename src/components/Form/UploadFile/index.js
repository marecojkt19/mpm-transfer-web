import pakaiClass from 'pakai-class';
import { useMemo } from 'react';
import { useDropzone } from 'react-dropzone';
import { CheckedCircleOutlined, Dangerous, UploadDocument } from '../../../assets/icons';
import { IMAGE_FORMATS } from '../../../utils/helpers/fileFormats';
import MainButton from '../MainButton';
import styles from './uploadFile.module.scss'

const spesificFormatValidator = (file, format) => {
  const supportedSpecificFormat = format.map(v => v.split('/')[1]);
  const isFormatSpecific = supportedSpecificFormat.includes(file?.name.split('.')[file?.name.split('.').length - 1].toLowerCase());

  if (!isFormatSpecific) {
    return {
      code: "file-invalid-type",
      message: "File gagal diunggah"
    };
  }

  return null;
};

const UploadFile = ({
  onChange,
  format = [...IMAGE_FORMATS],
  variant,
  className,
  disabled,
  onFocus,
  maxSize,
  minSize,
  defaultLabel
}) => {
  const {
    acceptedFiles,
    getRootProps,
    getInputProps,
    fileRejections
  } = useDropzone({
    accept: Object.assign(...format.map(v => { return { [v]: [] } })),
    maxSize,
    minSize,
    disabled,
    validator: (file) => spesificFormatValidator(file, format),
    onFileDialogOpen: (e) => {
      if (typeof onFocus === 'function') onFocus();
    },
    onDrop: (e) => {
      const file = e[0];
      if (!file) return
      if (typeof onChange === 'function') onChange(file);
    }
  });

  const uploadState = useMemo(() => {
    if (acceptedFiles.length || (!fileRejections.length && defaultLabel)) {
      return (
        <>
          <div className={styles.statusIcon}>
            <CheckedCircleOutlined color="#05944F" />
          </div>
          <div className={styles.label}>
            <h6>{acceptedFiles[0]?.name || defaultLabel}</h6>
            <h1>File berhasil diunggah</h1>
            <MainButton
              icon={<UploadDocument color="#276EF1" size="28" />}
              variant="secondary"
              type="button"
            >
              Pilih Ulang
            </MainButton>
          </div>
          <div className={styles.info}>
            <span>Ukuran maksimal yang boleh diunggah: 10MB</span>
          </div>
        </>
      )
    } else if (fileRejections[0]?.errors[0]?.code === "file-invalid-type") {
      return (
        <>
          <div className={styles.statusIcon}>
            <Dangerous />
          </div>
          <div className={styles.label}>
            <h6>{fileRejections[0].file.name}</h6>
            <h1>File gagal diunggah</h1>
            <MainButton
              icon={<UploadDocument color="#276EF1" size="28" />}
              variant="secondary"
              type="button"
            >
              Pilih Ulang
            </MainButton>
          </div>
          <div className={styles.info}>
            <span>Format file tidak didukung</span>
          </div>
        </>
      )
    } else if (fileRejections[0]?.errors[0]?.code === "file-too-large") {
      return (
        <>
          <div className={styles.statusIcon}>
            <Dangerous />
          </div>
          <div className={styles.label}>
            <h6>{fileRejections[0].file.name}</h6>
            <h1>File gagal diunggah</h1>
            <MainButton
              icon={<UploadDocument color="#276EF1" size="28" />}
              variant="secondary"
              type="button"
            >
              Pilih Ulang
            </MainButton>
          </div>
          <div className={styles.info}>
            <span className="failed">Ukuran file terlalu besar</span>
          </div>
        </>
      )
    }

    return (
      <>
        <div className={styles.label}>
          <h1>{`Taruh file ${format.includes('application/pdf') ? 'PDF,' : ''} JPG, JPEG atau PNG di sini`}</h1>
          <p>atau</p>
          <MainButton
            type="button"
            icon={<UploadDocument size="28" />}
          >
            Pilih File
          </MainButton>
        </div>
        <div className={styles.info}>
          <span>Ukuran maksimal yang boleh diunggah: 10MB</span>
        </div>
      </>
    )
  }, [acceptedFiles, fileRejections, format, defaultLabel])

  const wrapper = useMemo(() => {
    return (
      <div
        className={
          pakaiClass(
            styles.cardWrapper,
            fileRejections.length && styles.error,
            acceptedFiles.length && styles.success,
          )
        }
      >
        <div className={styles.wrapper}>
          {uploadState}
        </div>
      </div>
    )
  }, [fileRejections, acceptedFiles, uploadState])

  return (
    <div
      {...getRootProps({ className: pakaiClass(styles.uploadFile, styles[variant], className) })}
    >
      <div className={styles.uploadCard}>
        {wrapper}
        <input {...getInputProps()} />
      </div>
    </div>
  )
}

export default UploadFile
