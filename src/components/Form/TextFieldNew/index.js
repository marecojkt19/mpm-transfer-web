import pakaiClass from 'pakai-class';
import React, { forwardRef, memo, useMemo, useRef, useState } from 'react'
import NumberFormat from 'react-number-format';
import { ClosedEye, Eye } from '../../../assets/icons';
import FormField from '../FormField';
import styles from './textField.module.scss'

const TextField = ({
  multiline,
  textareaProps,
  id,
  name,
  value,
  onChange,
  onBlur,
  onFocus,
  placeholder,
  disabled,
  readOnly,
  autoComplete = "nope",
  spellCheck,
  password,
  inputProps,
  label,
  error,
  helperText,
  loading,
  className,
  defaultValue,
  format,
  moneyInput,
  allowLeadingZero = true,
  maxLength,
  autoFocus,
  allowDot = true,
  allowNumeric = true,
  additionalLabel,
  icon
},
  ref
) => {
  const inputRef = useRef();
  const [showPassword, setShowPassword] = useState(false);
  const [focus, setFocus] = useState(false);
  const [textareaValue, setTextareaValue] = useState("");

  const componentInput = useMemo(() => {
    if (multiline) {
      return (
        <textarea
          {...textareaProps}
          id={id || name}
          name={name}
          value={value}
          onChange={onChange}
          onBlur={onBlur}
          onFocus={onFocus}
          onKeyDown={(e) => {
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (!allowNumeric) {
              if ((keycode > 47 && keycode < 58) || (keycode > 95 && keycode < 107)) e.preventDefault()
            }

            setTextareaValue(e?.target?.value)
          }}
          placeholder={placeholder}
          disabled={disabled}
          maxLength={maxLength}
          readOnly={readOnly}
          autoComplete={autoComplete}
          spellCheck={spellCheck}
          autoFocus={autoFocus}
          ref={(el) => {
            inputRef.current = el;
            if (typeof ref === 'function') ref(el);
          }}
        />
      )
    }
    if (format || moneyInput)
      return (
        <NumberFormat
          name={name}
          value={value}
          defaultValue={defaultValue}
          mask={format}
          allowNegative={false}
          allowLeadingZeros={allowLeadingZero}
          getInputRef={(el) => {
            inputRef.current = el;
            if (typeof ref === "function") ref(el);
          }}
          onValueChange={({ value }) => {
            if (typeof onChange === "function") onChange(value)
          }}
          onBlur={(e) => {
            setFocus(false);
            if (typeof onBlur === "function") onBlur(e);
          }}
          onFocus={(e) => {
            setFocus(true);
            if (typeof onFocus === "function") onFocus(e);
          }}
          thousandSeparator={Boolean(moneyInput) ? "." : null}
          decimalSeparator={Boolean(moneyInput) ? "," : false}
          placeholder={placeholder}
          autoComplete={autoComplete}
          disabled={disabled}
          isAllowed={(values) => {
            const { value, floatValue } = values;

            if (typeof floatValue === 'undefined' || typeof value === 'undefined') {
              return true
            }

            if (allowDot) {
              if (allowLeadingZero) {
                if (maxLength) {
                  if (value.length <= maxLength) {
                    return true
                  }
                } else {
                  return true
                }
              } else {
                if (value.charAt(0) !== '0') {
                  if (maxLength) {
                    if (value.length <= maxLength) {
                      return true
                    }
                  } else {
                    return true
                  }
                }
              }
            } else {
              if (value.charAt(0) !== '.') {
                if (allowLeadingZero) {
                  if (maxLength) {
                    if (value.length <= maxLength) {
                      return true
                    }
                  } else {
                    return true
                  }
                } else {
                  if (value.charAt(0) !== '0') {
                    if (maxLength) {
                      if (value.length <= maxLength) {
                        return true
                      }
                    } else {
                      return true
                    }
                  }
                }
              }
            }
          }}
        />
      );

    return (
      <input
        {...inputProps}
        onBlur={(e) => {
          setFocus(false)
          if (typeof onBlur === 'function') onBlur(e)
        }}
        autoFocus={autoFocus}
        onFocus={e => {
          setFocus(true)
          if (typeof onFocus === 'function') onFocus(e)
        }}
        id={id || name}
        name={name}
        maxLength={maxLength}
        value={value}
        onChange={onChange}
        onKeyDown={(e) => {
          var keycode = (e.keyCode ? e.keyCode : e.which);
          if (!allowNumeric) {
            if ((keycode > 47 && keycode < 58) || (keycode > 95 && keycode < 107)) e.preventDefault()
          }
        }}
        placeholder={placeholder}
        disabled={disabled}
        readOnly={readOnly}
        autoComplete={autoComplete}
        spellCheck={spellCheck}
        type={password && !showPassword ? 'password' : 'text'}
        ref={(el) => {
          inputRef.current = el;
          if (typeof ref === 'function') ref(el);
        }}
      />
    )
  }, [
    autoFocus,
    multiline,
    textareaProps,
    id,
    name,
    value,
    onChange,
    onBlur,
    onFocus,
    placeholder,
    disabled,
    readOnly,
    spellCheck,
    password,
    autoComplete,
    showPassword,
    ref,
    inputProps,
    moneyInput,
    allowLeadingZero,
    format,
    defaultValue,
    maxLength,
    allowDot,
    allowNumeric
  ])

  const EyeIcon = useMemo(() => showPassword ? ClosedEye : Eye, [showPassword]);

  const textCounter = useMemo(() => {
    if (textareaValue?.length) {
      return textareaValue?.length
    }

    return 0
  }, [textareaValue])

  return (
    <>
      <FormField
        label={label}
        error={error}
        helperText={helperText}
        className={className}
        additionalLabel={additionalLabel}
      >
        <div className={
          pakaiClass(
            styles.inputWrapper,
            focus && styles.focus,
            error && styles.error,
            disabled && styles.disabled
          )}
        >
          <div className={pakaiClass(styles.formControl, loading && styles.formControlLoading)}>
            {icon}
            {componentInput}
            {
              password &&
              <div className={styles.eyeBtn} onClick={() => setShowPassword(!showPassword)}>
                <EyeIcon />
              </div>
            }
          </div>
          {multiline &&
            <div className={styles.inputCounter}>
              <span>{textCounter}/{maxLength}</span>
            </div>
          }
        </div>
      </FormField>
    </>
  )
}

export default memo(forwardRef(TextField))
