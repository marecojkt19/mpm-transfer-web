import React, { useEffect, useRef, useState, useMemo } from 'react';
import Select, { components } from 'react-select';
import FormField from '../FormField';
import clsx from 'clsx';
import styles from './selectControl.module.scss'
import { Close } from '../../../assets/icons';
import FormField2 from '../FormField2';

const customStyles = {
  control: (provided) => {
    return ({
      ...provided,
      "border": "1px solid #e5e5ea",
      "boxShadow": "none",
      "borderRadius": "10px",
      "padding": "12px"
    })
  },
  singleValue: (provided) => ({
    ...provided,
    "max-width": "calc(100% - 16px)"
  }),
  placeholder: (provided) => ({
    ...provided,
    "width": "100%",
    "overflow": "hidden",
    "text-overflow": "ellipsis",
    "white-space": "nowrap"
  }),
  input: (provided) => ({
    ...provided,
    "padding": 0,
    "margin": 0
  }),
  valueContainer: (provided) => ({
    ...provided,
    "padding": 0,
    "margin": 0
  }),
  menuList: (provided) => ({
    ...provided,
    "padding": "0",
  }),
  menu: (provided) => ({
    ...provided,
    "zIndex": "99",
    "border-radius": "10px",
    "overflow": "hidden",
    "padding": "10px 0"
  }),
  multiValue: (provided) => ({
    ...provided,
    "margin": 0,
    "margin-right": "4px"
  }),
  option: (styles, { isDisabled }) => {
    return {
      ...styles,
      padding: "12px",
      fontSize: "14px",
      cursor: isDisabled ? 'not-allowed' : 'default'
    };
  },
};

const SelectField = ({
  label,
  error,
  helperText,
  id,
  onChange,
  hideValue,
  onBlur,
  options,
  value: simpleValue = '',
  placeholder,
  isMulti = false,
  getOptionValue = opt => opt.value,
  componentProps,
  variant,
  additionalLabel,
  disabled,
  name,
  className,
  isClearable = true,
  isClearItem,
  onFocus
}) => {
  const value = getValue(options, simpleValue, getOptionValue, isMulti);
  const [focus, setFocus] = useState(false);
  const inputRef = useRef();

  const FormFieldTag = useMemo(() => {
    if (variant === "outlined") return FormField2
    return FormField
  }, [variant])

  return (
    <FormFieldTag
      className={className}
      id={id || name}
      label={label}
      disabled={disabled}
      additionalLabel={additionalLabel}
      error={error}
      helperText={helperText}
      expandLabel={Boolean(value) || focus}
      onClickLabel={() => inputRef?.current?.focus()}
    >
      <Select
        {...componentProps}
        components={{
          Control,
          ValueContainer,
          // Placeholder: () => null,
          IndicatorsContainer: () => null
        }}
        isMulti={isMulti}
        styles={customStyles}
        placeholder={placeholder}
        options={options}
        simpleValue={simpleValue}
        value={value}
        name={name}
        onChange={onChange}
        isClearable={isClearable}
        isOptionDisabled={(option) => option.disabled}
        isClearItem={isClearItem}
        hideValue={hideValue}
        onFocus={(e) => {
          setFocus(true);
          if (typeof onFocus === "function") onFocus(e);
        }}
        onBlur={(e) => {
          setFocus(false);
          if (typeof onBlur === "function") onBlur(e);
        }}
        isDisabled={disabled}
        classNamePrefix="custom-select"
      />
    </FormFieldTag>
  )
}

// Method for Custom Select

const ValueContainer = (props) => {
  const { children, selectProps: { placeholder, hideValue }, isClearItem, clearValue } = props;

  useEffect(() => {
    if (isClearItem) {
      clearValue()
    }
  }, [clearValue, isClearItem])

  return (
    <components.ValueContainer
      {...props}
    >
      {
        hideValue ?
          <>
            <components.Placeholder {...props}>
              {placeholder}
            </components.Placeholder>
            {[children[1]]}
          </>
          :
          children
      }
    </components.ValueContainer >
  );
};

const Control = (props) => {
  const { children, selectProps: { isClearable }, clearValue, hasValue } = props;
  const handleClickClose = (e) => {
    e.stopPropagation();
    clearValue()
  }
  return (
    <components.Control
      {...props}
      className={clsx(styles.searchField, (isClearable && hasValue) && styles.isClearable)}
    >
      <>
        {children}
        {
          (isClearable && hasValue) &&
          <button type="button" onClick={handleClickClose} className={styles.closeIcon}>
            <Close />
          </button>
        }
      </>
    </components.Control>
  )
};

function flatten(arr) {
  return arr.reduce((acc, val) => (Array.isArray(val.options)
    ? acc.concat(flatten(val.options))
    : acc.concat(val)
  ), []);
}

function getValue(opts, val, getOptVal, isMulti) {
  if (!val) return val;

  const options = flatten(opts);
  const value = isMulti
    ? options.filter(o => val.includes(getOptVal(o)))
    : options.find(o => typeof val === 'object' && Boolean(val) ? getOptVal(o) === getOptVal(val) : getOptVal(o) === val);

  return value;
}

export default SelectField;
