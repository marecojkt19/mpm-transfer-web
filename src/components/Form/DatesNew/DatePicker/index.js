import React, { useState } from 'react'
import FormField from '../../FormField'
import styles from './datePicker.module.scss'
import { DatePicker as MantineDatePicker } from '@mantine/dates';
import pakaiClass from 'pakai-class';
import 'dayjs/locale/id';

const DatePicker = ({
  label,
  error,
  helperText,
  onChange,
  value,
  placeholder,
  disabled
}) => {
  const [focus, setFocus] = useState(false);
  return (
    <FormField
      label={label}
      error={error}
      helperText={helperText}
    >
      <div
        className={pakaiClass(
          styles.datePicker,
          error && styles.error,
          focus && styles.focus,
          disabled && styles.disabled
        )}
      >
        <MantineDatePicker
          locale='id'
          onChange={onChange}
          value={value}
          placeholder={placeholder}
          inputFormat="DD/MM/YYYY"
          labelFormat="DD/MM//YYYY"
          disabled={disabled}
          onFocus={() => {
            setFocus(true)
          }}
          onBlur={() => {
            setFocus(false)
          }}
        />
      </div>
    </FormField>
  )
}

export default DatePicker
