import { useCallback, useMemo, useState } from "react";
import { DateRangePicker as MantineDateRangePicker } from '@mantine/dates';
import styles from './dateRangePicker.module.scss'
import pakaiClass from "pakai-class";
import CalendarIcon from "../../../../assets/icons/calendar";
import CloseIcon from "../../../../assets/icons/close";
import 'dayjs/locale/id';
import { format } from "date-fns";

function DateRangePicker({
  fromDate,
  toDate,
  onChange = () => { },
  onFocus = () => { },
  onBlur = () => { },
  placeholder,
  minDate,
  maxDate
}) {
  const [focus, setFocus] = useState(fromDate || toDate);
  const [open, setOpen] = useState(false)
  const [value, setValue] = useState([fromDate, toDate]);

  const handleChange = (date) => {
    onChange({
      fromDate: date[0],
      toDate: date[1]
    })
    setValue([date[0], date[1]])
    if (date[0] && date[1]) {
      setFocus(true)
      setOpen(false)
    }
  }

  const clearDate = useCallback(() => {
    setOpen(false)
    setFocus(false)
    onChange({
      fromDate: "",
      toDate: ""
    })
    setValue(["", ""]);
  }, [onChange]);

  const rightSection = useMemo(() => {
    return !focus ?
      <button onClick={() => setOpen(true)}>
        <CalendarIcon />
      </button>
      :
      <button onClick={clearDate}>
        <CloseIcon />
      </button>
  }, [focus, clearDate])

  return (
    <MantineDateRangePicker
      locale="id"
      minDate={minDate}
      maxDate={maxDate}
      placeholder={placeholder}
      dropdownOpened={open}
      onChange={(v) => handleChange(v)}
      closeCalendarOnChange={false}
      renderDay={(value) => format(new Date(value), 'd', {})}
      onFocus={(e) => {
        setFocus(true)
        setOpen(true)
        if (typeof onFocus === "function") onFocus(e);
      }}
      onBlur={(e) => {
        if (typeof onFocus === "function") onBlur(e);
      }}
      className={pakaiClass(
        styles.dateRangePicker,
        focus && styles.focus
      )}
      classNames
      rightSectionWidth={48}
      rightSection={rightSection}
      value={value}
    />
  )
}

export default DateRangePicker;
