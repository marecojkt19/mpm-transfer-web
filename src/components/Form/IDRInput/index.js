import pakaiClass from 'pakai-class';
import React, { useState } from 'react'
import NumberFormat from 'react-number-format';
import styles from './idrInput.module.scss'

const IDRInput = ({
  value,
  onChange,
  onBlur,
  onFocus,
  format,
  defaultValue,
  label,
  helperText,
  error,
  placeholder,
  className,
  disabled,
  allowNegative = true,
  allowLeadingZero = true,
  name
}) => {
  const [isFilled, setIsFilled] = useState(false);
  const [focus, setFocus] = useState(false);

  return (
    <div
      className={pakaiClass(
        styles.idrInput,
        error && styles.error,
        (focus || isFilled) && styles.focus,
        className
      )}
    >
      <div className={styles.label}>
        {label && <p>{label}</p>}
      </div>
      <div className={styles.wrapper}>
        <div className={styles.currencyCode}>
          <h5>Rp</h5>
        </div>
        <div className={styles.inputField}>
          <NumberFormat
            value={value}
            defaultValue={defaultValue}
            format={format}
            name={name}
            allowNegative={allowNegative}
            placeholder={placeholder}
            disabled={disabled}
            onValueChange={({ value }) => {
              setIsFilled(Boolean(value))
              if (typeof onChange === "function") onChange(value)
            }}
            onBlur={(e) => {
              setFocus(false);
              if (typeof onBlur === "function") onBlur(e);
            }}
            onFocus={(e) => {
              setFocus(true);
              if (typeof onFocus === "function") onFocus(e);
            }}
            thousandSeparator={"."}
            decimalSeparator={","}
            isAllowed={(values) => {
              const { value, floatValue } = values;

              if (typeof floatValue === 'undefined' || typeof value === 'undefined') {
                return true
              }
              if (allowLeadingZero) {
                return true
              } else {
                if (value.charAt(0) !== '0') {
                  return true
                }
              }
            }}
          />
          {
            (helperText || error) &&
            <div className={styles.helperText}>
              <p>
                {helperText}
              </p>
            </div>
          }
        </div>
      </div>
    </div>
  )
}

export default IDRInput
