import React from 'react'
import { ChecklistDisabled, ChecklistOff, ChecklistOn } from '../../../assets/icons'
import styles from './agreementCheckbox.module.scss'

const AgreementCheckbox = ({
  onAgree,
  isAgree,
  disabled,
  label
}) => {
  return (
    <div className={styles.agreementCheckbox}>
      <button
        type="button"
        onClick={onAgree}
        disabled={disabled}
      >
        {
          disabled ? <ChecklistDisabled />
            :
            isAgree ? <ChecklistOn /> : <ChecklistOff />
        }
      </button>
      <div className={styles.label}>{label}</div>
    </div>
  )
}

export default AgreementCheckbox
