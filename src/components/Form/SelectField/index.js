import pakaiClass from 'pakai-class';
import React, { useMemo, useRef, useState } from 'react'
import Select, { components } from 'react-select';
import { ChecklistOff, ChecklistOn, ChevronDown, Close, Search } from '../../../assets/icons';
import LoadingDots from '../../Loadings/LoadingDots';
import FormField from '../FormField'
import styles from './selectField.module.scss'

const customStyles = {
  control: (styles, state) => ({
    ...styles,
    backgroundColor: 'white',
    borderRadius: '10px',
    zIndex: '1',
    border: state.isFocused && state.selectProps.searchable ? "1px solid #E2E2E2" : "0",
    boxShadow: "none",
    maxHeight: state.isFocused && state.selectProps.searchable ? "auto" : 0,
    minHeight: "unset",
    overflow: "hidden",
  }),
  container: (styles, state) => ({
    ...styles,
    margin: '8px 0',
    position: "absolute",
    width: "100%"
  }),
  valueContainer: (styles) => ({
    ...styles,
    padding: '8px',
  }),
  input: (styles) => ({
    ...styles,
    padding: 0,
    margin: 0
  }),
  menu: (styles, state) => ({
    ...styles,
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 10,
    backgroundColor: "#ffffff",
    border: "1px solid #E2E2E2",
    overflow: "hidden",
    margin: state.selectProps.searchable ? '8px 0' : 0,
    transition: 'none',
    zIndex: 9
  }),
  menuList: (styles) => ({
    ...styles,
    padding: 0,
    margin: 0
  }),
  option: (styles) => ({
    ...styles,
    display: "flex",
    padding: "10px 16px",
    borderBottom: "1px solid #E2E2E2",
    fontSize: 16,
    lineHeight: "20px",
    backgroundColor: "white",
    color: "black",
    "&:hover": {
      backgroundColor: "#E3ECFD",
      color: "#276EF1"
    }
  }),
}

const SelectField = ({
  options,
  label,
  error,
  helperText,
  defaultValue,
  disabled,
  loading,
  name,
  onChange,
  value: simpleValue,
  isMulti = false,
  getOptionValue = opt => opt.value,
  placeholder,
  searchable,
  withCheckbox,
  isClearable,
  className
}) => {
  const selectRef = useRef();
  const [isFocused, setIsFocused] = useState(false);
  const value = getValue(options, simpleValue, getOptionValue, (isMulti || withCheckbox));

  const onFocus = () => {
    if (selectRef.current) {
      if (isFocused) {
        setIsFocused(false);
        selectRef.current.blur();
      } else {
        setIsFocused(true);
        selectRef.current.focus();
      }
    }
  }

  return (
    <FormField
      label={label}
      error={error}
      helperText={helperText}
      className={className}
    >
      <DropDown
        onFocus={onFocus}
        isDisabled={disabled}
        placeholder={placeholder}
        isFocused={isFocused}
        setIsFocused={setIsFocused}
        value={value}
        isLoading={loading}
        error={error}
        isMulti={isMulti || withCheckbox}
      >
        <Select
          components={{
            Control,
            ValueContainer,
            Option,
            IndicatorsContainer: () => null
          }}
          ref={selectRef}
          // isOptionDisabled={() => (isMulti || withCheckbox) && value?.length >= max}
          value={value}
          menuPlacement="auto"
          openMenuOnFocus={true}
          classNamePrefix="select"
          defaultValue={defaultValue}
          isDisabled={disabled}
          isLoading={loading}
          withCheckbox={withCheckbox}
          isMulti={isMulti || withCheckbox}
          closeMenuOnSelect={!withCheckbox}
          hideSelectedOptions={false}
          searchable={searchable}
          onBlur={() => {
            setIsFocused(false)
            selectRef.current.blur();
          }}
          isSearchable={true}
          onChange={(v, e) => {
            if (!withCheckbox) {
              setIsFocused(false);
              selectRef.current.blur();
            }
            onChange(v, e)
          }}
          styles={customStyles}
          placeholder={placeholder}
          name={name}
          options={options}
          isClearable={isClearable}
        />
      </DropDown>
    </FormField >
  )
}

const ValueContainer = (props) => {
  const { children } = props;

  return (
    <components.ValueContainer {...props} className={styles.selectValueContainer}>
      <>
        <Search />
        {children[1]}
      </>
    </components.ValueContainer>
  )
}

const Control = (props) => {
  const { children, selectProps: { isClearable }, clearValue, hasValue } = props;
  const handleClickClose = (e) => {
    e.stopPropagation();
    clearValue()
  }
  return (
    <components.Control
      {...props}
      className={pakaiClass(styles.searchField, (isClearable && hasValue) && styles.isClearable)}
    >
      <>
        {children}
        {
          (isClearable && hasValue) &&
          <button type="button" onClick={handleClickClose} className={styles.closeIcon}>
            <Close />
          </button>
        }
      </>
    </components.Control>
  )
};

const Option = (props) => {
  const { children, isSelected, data: { logo }, selectProps: { withCheckbox } } = props;

  return (
    <components.Option {...props} className={styles.option}>
      {withCheckbox && (isSelected ? <ChecklistOn /> : <ChecklistOff />)}
      {logo && <img src={logo} alt="" />}
      {children}
    </components.Option>
  );
};

const DropDown = ({
  children,
  value,
  placeholder,
  isFocused,
  isDisabled,
  isLoading,
  onFocus,
  error,
  isMulti
}) => {

  const newValue = useMemo(() => {
    if (isMulti) {
      if (value?.length) {
        if (value?.length > 1) {
          return <span className={styles.value}>{value.length} Pilihan</span>
        } else {
          return <span className={styles.value}>{value[0]?.label}</span>
        }
      }
    } else {
      if (value) {
        return <span className={styles.value}>{value?.label}</span>
      }
    }

    return <span className={styles.placeholder}>{!isLoading ? placeholder : <LoadingDots size={10} />}</span>
  }, [value, placeholder, isMulti, isLoading])

  return (
    <div className={pakaiClass(
      styles.selectDropdown,
      isFocused && styles.focus,
      isDisabled && styles.disabled,
      error && styles.error
    )}
    >
      <button
        type="button"
        disabled={isDisabled}
        onClick={onFocus}
      >
        {newValue}
        <label
          htmlFor="selectControl"
          className={styles.expand}
        >
          <ChevronDown />
        </label>
      </button>
      {!isLoading ? children : null}
    </div>
  )
}

// Method for Custom Select
function flatten(arr = []) {
  return arr.reduce((acc, val) => (Array.isArray(val.options)
    ? acc.concat(flatten(val.options))
    : acc.concat(val)
  ), []);
}

function getValue(opts, val, getOptVal, isMulti) {
  if (!val) return val;

  const options = flatten(opts);
  const value = isMulti
    ? options.filter(o => val.includes(getOptVal(o)))
    : options.find(o => typeof val === 'object' && Boolean(val) ? getOptVal(o) === getOptVal(val) : getOptVal(o) === val);

  return value;
}

export default SelectField
