import { useState } from "react";
import { DateRangePicker as MantineDateRangePicker } from '@mantine/dates';
import styles from './dateRangePicker.module.scss'
import pakaiClass from "pakai-class";
import 'dayjs/locale/id';

function DateRangePicker({
  fromDate,
  toDate,
  onChange,
  onFocus,
  onBlur,
  placeholder
}) {
  const [focus, setFocus] = useState(false);

  const [value, setValue] = useState([fromDate, toDate]);

  const handleChange = ((date) => {
    onChange({
      fromDate: date[0],
      toDate: date[1]
    })
    setValue([date[0], date[1]])
  })

  return (
    <MantineDateRangePicker
      locale="id"
      placeholder={placeholder}
      onChange={(v) => handleChange(v)}
      onFocus={(e) => {
        setFocus(true);
        if (typeof onFocus === "function") onFocus(e);
      }}
      onBlur={(e) => {
        setFocus(false);
        if (typeof onFocus === "function") onBlur(e);
      }}
      className={pakaiClass(
        styles.dateRangePicker,
        focus && styles.focus
      )}
      classNames
      value={value}
    />
  )
}

export default DateRangePicker;
