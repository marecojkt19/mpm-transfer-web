import pakaiClass from 'pakai-class'
import React from 'react'
import styles from './formField.module.scss'

const FormField = ({
  children,
  label,
  error,
  helperText,
  className,
  helperTextClassName,
  additionalLabel
}) => {
  return (
    <div className={pakaiClass(
      styles.formField,
      error && styles.error,
      className
    )}>
      {label &&
        <div className={styles.label}>
          <p>{label}</p>
        </div>
      }
      {additionalLabel &&
        <div className={styles.additionalLabel}>
          <p>{additionalLabel}</p>
        </div>
      }
      {children}
      {
        (helperText && error) &&
        <div className={pakaiClass(styles.helperText, helperTextClassName)}>
          <p>
            {helperText}
          </p>
        </div>
      }
    </div>
  )
}

export default FormField
