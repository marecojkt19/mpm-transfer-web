import React from 'react'
import pakaiClass from 'pakai-class'
import styles from './radio.module.scss'

const RadioItem2 = ({
  id,
  className,
  name,
  label,
  onChange,
  value,
  checked,
  inputProps,
}) => {
  return (
    <label
      className={pakaiClass(
        styles.radioRoot,
        className
      )}
    >
      <span className={styles.radioWrapper}>
        <input
          {...inputProps}
          type="radio"
          id={id || name}
          name={name}
          checked={checked}
          value={value}
          onChange={onChange}
        />
        <span className={styles.radio} />
      </span>
      <div className={pakaiClass(styles.radioLabel, checked && styles.checked)}>{label}</div>
    </label>
  )
}

export default RadioItem2
