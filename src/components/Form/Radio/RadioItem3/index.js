import React from 'react'
import styles from './radio.module.scss'
import { CheckedCircle2, UnCheckedCircle2 } from '../../../../assets/icons'
import pakaiClass from 'pakai-class'
import clsx from 'clsx'

const RadioItem3 = ({
  id,
  className,
  name,
  label,
  onChange,
  value,
  checked,
  imageLabel,
  inputProps
}) => {
  return (
    <label
      className={clsx(
        styles.radioRoot,
        {
          [styles.checked]: checked
        },
        className
      )}
    >
      <span className={styles.radioWrapper}>
        <input
          {...inputProps}
          type="radio"
          id={id || name}
          name={name}
          checked={checked}
          value={value}
          onChange={onChange}
        />
        {checked ?
          <CheckedCircle2 />
          :
          <UnCheckedCircle2 />
        }
      </span>
      {
        label &&
        <span className={pakaiClass(styles.radioLabel, { [styles.checked]: checked })}>{label}</span>
      }
      {
        imageLabel &&
        <div className={styles.imageLabel}>
          <img src={imageLabel} alt="" />
        </div>
      }
    </label>
  )
}

export default RadioItem3
