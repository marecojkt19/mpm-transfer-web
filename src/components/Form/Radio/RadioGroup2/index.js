import pakaiClass from 'pakai-class';
import React, { Children, cloneElement, forwardRef, useState } from 'react'
import FormField from '../../FormField';
import styles from './radioGroup.module.scss'

const RadioGroup2 = forwardRef((
  {
    label,
    className,
    name,
    onChange,
    value,
    defaultValue,
    children,
    error,
    helperText,
    row
  },
  ref
) => {
  const [currentValue, setCurrentValue] = useState(defaultValue);

  const handleChange = (e) => {
    setCurrentValue(e.target.value)
    if (typeof onChange === 'function') onChange(e)
  }

  return (
    <FormField
      label={label}
      error={error}
      helperText={helperText}
      className={className}
    >
      <div className={
        pakaiClass(
          styles.radioGroup,
          row ? styles.row : styles.col
        )}
      >
        {Children.map(children, (child) => (
          child ?
            cloneElement(child, {
              className: pakaiClass(children.props?.className, styles.radio),
              name: name,
              checked: Boolean(value === child?.props?.value) ?? Boolean(currentValue === child?.props?.value),
              ref: ref,
              onChange: handleChange
            })
            :
            null
        )
        )}
      </div>
    </FormField>
  )
})

export default RadioGroup2
