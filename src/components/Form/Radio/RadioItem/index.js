import React, { useState } from 'react';
import pakaiClass from 'pakai-class';
import styles from './radio.module.scss';
import clsx from 'clsx';

const RadioItem = ({
  id,
  size,
  className,
  name,
  label,
  onChange,
  value,
  checked,
  imageLabel,
  inputProps,
  autoFocus,
  onClick,
  disabled,
}) => {
  const [focus, setFocus] = useState(false);

  return (
    <label
      className={pakaiClass(
        styles.radioRoot,
        focus && styles.focus,
        size && styles[size],
        className
      )}
    >
      <span className={styles.radioWrapper}>
        <input
          {...inputProps}
          type="radio"
          disabled={disabled}
          id={id || name}
          autoFocus={autoFocus}
          name={name}
          checked={checked}
          value={value}
          onChange={onChange}
          onClick={onClick}
          onFocus={() => {
            setFocus(true)
          }}
          onBlur={() => {
            setFocus(false)
          }}
        />
      </span>
      {
        label &&
        <span className={clsx(styles.radioLabel, disabled && styles.disabled)}>{label}</span>
      }
      {
        imageLabel &&
        <div className={styles.imageLabel}>
          <img src={imageLabel} alt="" />
        </div>
      }
    </label>
  )
}

export default RadioItem
