import React from 'react'
import LoadingSpinner from './LoadingSpinner';
import MainButton from '../templates/MainButton';

const LoadingButton = ({
  children,
  className,
  variant,
  loading
}) => {
  return (
    <MainButton
      className={className}
      variant={variant}
    >
      {loading &&
        <LoadingSpinner />
      }
      {children}
    </MainButton>
  )
}

export default LoadingButton
