import React from 'react'
import styles from './circularProgress.module.scss'
import pakaiClass from 'pakai-class'

const CircularProgress2 = ({
  className
}) => {
  return (
    <div className={pakaiClass(styles.circularProgress, className)}></div>
  )
}

export default CircularProgress2
