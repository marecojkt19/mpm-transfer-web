import React, { useEffect, useCallback, useRef, Children, isValidElement, cloneElement } from 'react';
import styles from './modal.module.scss'
import ReactDOM from 'react-dom'
import { Transition } from 'react-transition-group';
import pakaiClass from 'pakai-class';
import { CloseCircle } from '../../assets/icons';

export const ModalHead = ({
  title,
  className,
  onClose,
  iconName,
  iconVariant,
  withoutBorder = true
}) => {
  return (
    <div className={pakaiClass(styles.modalHead, className, {
      [styles.withoutBorder]: withoutBorder,
      [styles.withBorder]: !withoutBorder
    })}
    >
      <div className={styles.wrapper}>
        <div className={styles.title}>
          {iconName &&
            <span className={pakaiClass("material-symbols-rounded", styles[iconVariant])}>
              {iconName}
            </span>
          }
          <p>{title}</p>
        </div>
        {onClose &&
          <button onClick={onClose}>
            <CloseCircle />
          </button>
        }
      </div>
    </div>
  )
}

export const ModalBody = ({
  children,
  className,
  withoutPadding = false
}) => {
  return (
    <div className={pakaiClass(styles.modalBody, className, {
      [styles.withoutPadding]: withoutPadding,
      [styles.withPadding]: !withoutPadding
    })}
    >
      {children}
    </div>
  )
}

export const ModalWrapper = ({ children, className, width, scrollable }) => {
  return (
    <div className={pakaiClass(styles.modalWrapper, className)} style={{ width }}>
      {children}
    </div>
  )
}

const duration = 300

const defaultStyle = {
  transition: `opacity ${duration}ms ease-in-out`,
  opacity: 0,
  visibility: 'hidden'
}

const transitionStyles = {
  entering: { opacity: 1, visibility: 'visible' },
  entered: { opacity: 1, visibility: 'visible' },
  exiting: { opacity: 0, visibility: 'visible' },
  exited: { opacity: 0, visibility: 'hidden' },
};

const Modal = ({
  onClose,
  in: inProp,
  children,
  scrollable
}) => {
  const nodeRef = useRef();
  const body = document.body

  const handleClose = useCallback(() => {
    if (typeof onClose === 'function') onClose();
  }, [onClose])

  const onEscKeyDown = useCallback(e => {
    if (e.key !== "Escape") return;
    handleClose();
  }, [handleClose]);

  const childrenWithProps = Children.map(children, child => {
    // Checking isValidElement is the safe way and avoids a typescript
    // error too.
    if (isValidElement(child)) {
      return cloneElement(child, { scrollable });
    }
    return child;
  });

  useEffect(() => {
    if (inProp) window.addEventListener("keydown", onEscKeyDown, false);
    return () => {
      if (inProp) window.removeEventListener("keydown", onEscKeyDown, false);
    }
  }, [inProp, onEscKeyDown])

  useEffect(() => {
    if (inProp) {
      document.body.style.overflow = "auto";
    } else {
      document.body.style.overflow = ""
    }
  }, [inProp]);

  return ReactDOM.createPortal(
    <Transition nodeRef={nodeRef} in={inProp} timeout={duration} unmountOnExit={true}>
      {state => (
        <div ref={nodeRef} className={pakaiClass(styles.modal, scrollable && styles.scrollable)} style={{
          ...defaultStyle,
          ...transitionStyles[state]
        }}>
          {childrenWithProps}
          <div className={styles.backdrop} onClick={handleClose}></div>
        </div >
      )}
    </Transition>,
    body
  )
}

export default Modal
