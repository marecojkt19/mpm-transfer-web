import { Accordion as MantineAccordion } from '@mantine/core';
import { ChevronExpand } from '../../assets/icons';
import "./accordion.scss"

const Accordion = ({ Items, ...rest }) => {
  return (
    <MantineAccordion
      chevron={<ChevronExpand />}
      {...rest}
    >
      {
        Items ? Items.map(item => (
          <AccordionItem key={item.value} {...item} />
        )) : "Something went Wrong~"
      }
    </MantineAccordion>
  )
}

const AccordionItem = ({ value, ControlProps, Panel }) => {
  const { heading, desc } = ControlProps;
  return (
    <MantineAccordion.Item value={value}>
      <MantineAccordion.Control {...ControlProps}>
        <ControlItem
          heading={heading}
          desc={desc}
        />
      </MantineAccordion.Control>
      <MantineAccordion.Panel>{Panel}</MantineAccordion.Panel>
    </MantineAccordion.Item>
  )
}

const ControlItem = ({ heading, desc }) => {
  return (
    <div>
      <h3 style={{ fontSize: "20px" }}>{heading}</h3>
      {desc && <p style={{ fontSize: "16px", color: "#757575", marginTop: "8px" }}>{desc}</p>}
    </div>
  )
}


export default Accordion;