import React from "react";
import ReactSVG from "react-inlinesvg";

export default function SVG(props) {
  return (
    <ReactSVG
      preProcessor={(code) =>
        code.replace(/fill=".*?"/g, 'fill="currentColor"')
      }
      {...props}
    />
  );
}
