import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Layout from "./layouts";
import createTransactionPage from "./views/CreateTransaction";
import AccountVerification from "./views/AccountVerification";
import
roleHelper,
{
  ROUTE_ACCOUNTS,
  ROUTE_CREATE_TRANSACTION,
  ROUTE_DASHBOARD,
  ROUTE_DEPOSIT,
  ROUTE_SETTINGS,
  ROUTE_TRANSACTION,
  ROUTE_VERIFICATION
} from "./utils/helpers/roleHelper";
import { useSelector } from "react-redux";
import Deposit from "./views/Deposit";
import LoadingDots from "./components/Loadings/LoadingDots";
import InternetServerError from "./views/InternetServerError";
import Dashboard from "./views/Dashboard";
import MultiAccount from "./views/MultiAccount";
import Settings from "./views/Settings";
import ListTransaction from "./views/ListTransaction";

export default function MainRoutes() {
  const { user, error, company } = useSelector(state => state.auth);

  return user && company ? (
    <Layout>
      <Switch>
        <AuthRoute verifiedStatus={company.status.verifiedAt} role={user.role} companyCode={company.code} routeKey={ROUTE_VERIFICATION} exact path="/verification" component={AccountVerification} />
        <AuthRoute verifiedStatus={company.status.verifiedAt} role={user.role} companyCode={company.code} routeKey={ROUTE_DASHBOARD} exact path="/dashboard" component={Dashboard} />
        <AuthRoute verifiedStatus={company.status.verifiedAt} role={user.role} companyCode={company.code} routeKey={ROUTE_DEPOSIT} path="/deposit" exact component={Deposit} />
        <AuthRoute verifiedStatus={company.status.verifiedAt} role={user.role} companyCode={company.code} routeKey={ROUTE_TRANSACTION} path="/transaction" component={ListTransaction} />
        <AuthRoute verifiedStatus={company.status.verifiedAt} role={user.role} companyCode={company.code} routeKey={ROUTE_CREATE_TRANSACTION} exact path="/create-transaction" component={createTransactionPage} />
        <AuthRoute verifiedStatus={company.status.verifiedAt} role={user.role} companyCode={company.code} routeKey={ROUTE_ACCOUNTS} exact path="/accounts" component={MultiAccount} />
        <AuthRoute verifiedStatus={company.status.verifiedAt} role={user.role} companyCode={company.code} routeKey={ROUTE_SETTINGS} exact path="/settings" component={Settings} />
        {company?.status?.verifiedAt ?
          <>
            <Redirect exact from="/verification" to="/dashboard" />
            <Redirect exact from="/" to="/dashboard" />
            <Redirect exact from="/login" to="/dashboard" />
          </>
          :
          <>
            <Redirect exact from="/" to="/verification" />
            <Redirect exact from="/login" to="/verification" />
            <Redirect exact from="/registration-success" to="/verification" />
          </>
        }
        <Redirect to="/error" />
      </Switch>
    </Layout>
  ) : (
    error ?
      <InternetServerError />
      :
      <div className="d-flex justify-content-center align-items-center h-100">
        <LoadingDots />
      </div>
  )
}

const AuthRoute = ({ role, companyCode, routeKey, verifiedStatus, ...rest }) => {
  return roleHelper.hasAccess(role, Boolean(verifiedStatus), routeKey, companyCode)
    ? <Route {...rest} />
    : <Redirect to="/error" />
}
