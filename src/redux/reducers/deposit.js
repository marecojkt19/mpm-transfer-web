import { depositTypes, depositActions } from "../actions/depositAction";
import { endOfDay, formatISO, startOfDay } from "date-fns";
import { call, delay, put, takeLatest } from "@redux-saga/core/effects";
import depositService from "../../axios/services/depositService";

const initialState = {
  activeTabKey: 0,
  depositId: undefined,
  step: 0,
  items: [],
  tableConfig: {
    isLoading: false,
    totalData: 0,
    limit: 10,
    sort: "",
    page: 1,
    filter: {
      startDate: '',
      endDate: '',
      search: '',
      bankName: '',
      filter: '',
      status: '',
    }
  }
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case depositTypes.LOAD_REQUESTED: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          isLoading: true,
        }
      }
    }
    case depositTypes.LOADED: {
      return {
        ...state,
        items: action.payload.data.deposits,
        tableConfig: {
          ...state.tableConfig,
          totalData: action.payload.data.count,
          isLoading: false,
        }
      };
    }
    case depositTypes.SET_TABLE_CONFIG: {
      if (action.key === "page") {
        return {
          ...state,
          tableConfig: {
            ...state.tableConfig,
            [action.key]: action.value
          }
        };
      } else {
        return {
          ...state,
          tableConfig: {
            ...initialState.tableConfig,
            [action.key]: action.value
          }
        };
      }
    }
    case depositTypes.SET_TABLE_CONFIG_FILTER: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          filter: {
            ...state.tableConfig.filter,
            [action.key]: action.value
          },
          page: 1
        }
      };
    }
    case depositTypes.CHANGE_TAB:
      return {
        ...state,
        activeTabKey: action.key,
      }
    case depositTypes.SET_ID:
      return {
        ...state,
        depositId: action.key,
      }
    case depositTypes.SET_STEP:
      return {
        ...state,
        step: action.key,
      }
    default:
      return state;
  }
}

export function* saga() {
  yield takeLatest(depositTypes.LOAD_REQUESTED, function* loadData({ tableConfig }) {
    yield delay(300);
    try {
      const search = tableConfig.filter.search ? `trxNumber|${tableConfig.filter.search},depositor|${tableConfig.filter.search},note|${tableConfig.filter.search}` : null;
      const startDate = tableConfig.filter.startDate ? formatISO(startOfDay(new Date(tableConfig.filter.startDate))) : null;
      const endDate = tableConfig.filter.endDate ? formatISO(endOfDay(new Date(tableConfig.filter.endDate))) : null;

      const { data } = yield call(depositService.historyList, {
        page: tableConfig.page,
        limit: tableConfig.limit,
        sort: tableConfig.sort,
        search,
        startDate,
        endDate,
        filter: tableConfig.filter.filter,
        bankName: tableConfig.filter.bankName,
      });
      yield put(depositActions.loaded(data));
    } catch (error) {
      console.log('error reached', error);
    }
  });
}
