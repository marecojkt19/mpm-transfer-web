import { verifyAccountTypes } from "../actions/verifyAccountAction";

const initialState = {
  user: null,
  company: null,
  docs: null,
  signature: null,
  step: 0
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case verifyAccountTypes.SET_SUMMARY: {
      const docs = action?.payload?.docs;
      const user = action?.payload?.user;
      const company = action?.payload?.company;

      return {
        ...state,
        docs,
        user,
        company
      };
    }
    case verifyAccountTypes.SET_STEP:
      return {
        ...state,
        step: action.payload.key,
      }
    case verifyAccountTypes.SET_SIGNATURE:
      return {
        ...state,
        signature: action.payload.signature,
      }
    default:
      return state;
  }
};
