import { put, takeLatest, call, delay } from "redux-saga/effects";
import { transactionDetailActions, transactionDetailTypes } from "../../actions/transactions/transactionDetailActions";
import transactionService from "../../../axios/services/transactionService";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { transactionByBulkingTypes } from "../../actions/transactions/transactionByBulkingActions";

const initialState = {
  items: [],
  transferFeeDetails: null,
  trxCode: "",
  step: 0,
  paymentMethod: "DEPOSIT",
  otpMethod: "",
  isSendInvoice: false,
  tableConfig: {
    isLoading: false,
    limit: 10,
    page: 1,
    totalData: 0,
    sort: "",
    filter: {
      search: '',
      status: '',
      bank: ''
    }
  }
};

export const reducer = persistReducer(
  {
    storage,
    key: "reducer-transaction",
    whitelist: [
      "step",
      "transferFeeDetails",
      "items",
      "otpMethod",
      "paymentMethod",
      "isSendInvoice"
    ]
  },
  (state = initialState, action) => {
    switch (action.type) {
      case transactionDetailTypes.LOAD_REQUESTED: {
        return {
          ...state,
          tableConfig: {
            ...state.tableConfig,
            isLoading: true
          }
        }
      }
      case transactionDetailTypes.LOADED: {
        return {
          ...state,
          detailItems: [],
          items: action.data,
          tableConfig: {
            ...state.tableConfig,
            totalData: action.data.count,
            isLoading: false,
          }
        };
      }
      case transactionDetailTypes.SET_TRX_CODE: {

        return {
          ...state,
          trxCode: action.trxCode
        };
      }
      case transactionDetailTypes.SET_IS_SEND_INVOICE: {

        return {
          ...state,
          isSendInvoice: action.isSendInvoice
        };
      }
      case transactionDetailTypes.PAYMENT_METHOD: {
        return {
          ...state,
          paymentMethod: action.paymentMethod
        };
      }
      case transactionDetailTypes.OTP_METHOD: {

        return {
          ...state,
          otpMethod: action.otpMethod
        };
      }
      case transactionDetailTypes.SET_TABLE_CONFIG_FILTER: {
        return {
          ...state,
          tableConfig: {
            ...state.tableConfig,
            filter: {
              ...state.tableConfig.filter,
              [action.key]: action.value
            }
          }
        };
      }
      case transactionDetailTypes.SET_TABLE_CONFIG: {
        if (action.key === "page") {
          return {
            ...state,
            tableConfig: {
              ...state.tableConfig,
              [action.key]: action.value
            }
          };
        } else {
          return {
            ...state,
            tableConfig: {
              ...initialState.tableConfig,
              [action.key]: action.value
            }
          };
        }
      }
      case transactionDetailTypes.SET_TRANSFER_FEE_DETAILS: {
        return {
          ...state,
          transferFeeDetails: action.payload.data,
        };
      }
      case transactionDetailTypes.SET_STEP: {
        return {
          ...state,
          step: action.key,
        };
      }
      case transactionByBulkingTypes.LOADED: {
        return initialState
      }
      default:
        return state;
    }
  });

export function* saga() {
  yield takeLatest(transactionDetailTypes.LOAD_REQUESTED, function* loadData({ tableConfig }) {
    yield delay(300)
    try {
      const search = tableConfig.filter.search ? `beneficiaryAccountNo|${tableConfig.filter.search},beneficiaryAccountName|${tableConfig.filter.search},destinationBank|${tableConfig.filter.search}` : null;
      const { data } = yield call(transactionService.detail, {
        page: tableConfig.page,
        limit: tableConfig.limit,
        sort: tableConfig.sort,
        search,
        bank: tableConfig.filter.bank,
        status: tableConfig.filter.status,
        trxCode: tableConfig.trxCode,
      });
      yield put(transactionDetailActions.loaded(data));
    } catch (error) {
      // console.log('error reached', error);
    }
  })
}
