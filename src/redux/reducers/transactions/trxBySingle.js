import { endOfDay, formatISO, startOfDay } from "date-fns";
import { put, takeLatest, call, delay } from "redux-saga/effects";
import transactionService from "../../../axios/services/transactionService";
import { transactionBySingleActions, transactionBySingleTypes } from "../../actions/transactions/transactionBySingleActions";

const initialState = {
  items: [],
  tableConfig: {
    isLoading: false,
    limit: 10,
    page: 1,
    totalData: 0,
    sort: "",
    filter: {
      startDate: '',
      endDate: '',
      search: '',
      filter: '',
      bank: '',
      status: ''
    }
  }
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case transactionBySingleTypes.LOAD_REQUESTED: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          isLoading: true,
        }
      }
    }
    case transactionBySingleTypes.LOADED: {
      return {
        ...state,
        items: action.data.orders,
        tableConfig: {
          ...state.tableConfig,
          totalData: action.data.count,
          isLoading: false,
        }
      };
    }
    case transactionBySingleTypes.SET_TABLE_CONFIG: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          [action.key]: action.value
        }
      };
    }
    case transactionBySingleTypes.SET_TABLE_CONFIG_FILTER: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          filter: {
            ...state.tableConfig.filter,
            [action.key]: action.value
          }
        }
      };
    }
    default:
      return state;
  }
};

export function* saga() {
  yield takeLatest(transactionBySingleTypes.LOAD_REQUESTED, function* loadData({ tableConfig }) {
    yield delay(300);
    try {
      const search = tableConfig.filter.search ? `code|${tableConfig.filter.search},accountNumber|${tableConfig.filter.search},beneficiaryAccountName|${tableConfig.filter.search}` : null;
      const startDate = tableConfig.filter.startDate ? formatISO(startOfDay(new Date(tableConfig.filter.startDate))) : null;
      const endDate = tableConfig.filter.endDate ? formatISO(endOfDay(new Date(tableConfig.filter.endDate))) : null;

      const { data } = yield call(transactionService.orderListByType, {
        page: tableConfig.page,
        limit: tableConfig.limit,
        sort: tableConfig.sort,
        search,
        startDate,
        endDate,
        filter: tableConfig.filter.filter,
        bank: tableConfig.filter.bank,
        status: tableConfig.filter.status,
        type: "SINGLE"
      });
      yield put(transactionBySingleActions.loaded(data));
    } catch (error) {
      // console.log('error reached', error);
    }
  });
}
