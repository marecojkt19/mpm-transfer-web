import { endOfDay, formatISO, startOfDay } from "date-fns";
import { put, takeLatest, call, delay } from "redux-saga/effects";
import transactionService from "../../../axios/services/transactionService";
import { transactionByBulkingActions, transactionByBulkingTypes } from "../../actions/transactions/transactionByBulkingActions";

const initialState = {
  items: [],
  tableConfig: {
    isLoading: false,
    limit: 10,
    page: 1,
    totalData: 0,
    sort: "",
    filter: {
      startDate: '',
      endDate: '',
      search: '',
      status: ''
    }
  },
  tableConfigDetail: {
    isLoading: false,
    limit: 5,
    page: 1,
    totalData: 0,
    trxCode: "",
  },
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case transactionByBulkingTypes.LOAD_REQUESTED: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          isLoading: true,
        }
      }
    }
    case transactionByBulkingTypes.LOADED: {
      return {
        ...state,
        items: action.data.orders,
        tableConfig: {
          ...state.tableConfig,
          totalData: action.data.count,
          isLoading: false,
        }
      };
    }
    case transactionByBulkingTypes.SET_TABLE_CONFIG: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          [action.key]: action.value
        }
      };
    }
    case transactionByBulkingTypes.SET_TABLE_CONFIG_FILTER: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          filter: {
            ...state.tableConfig.filter,
            [action.key]: action.value
          }
        }
      };
    }
    default:
      return state;
  }
};

export function* saga() {
  yield takeLatest(transactionByBulkingTypes.LOAD_REQUESTED, function* loadData({ tableConfig }) {
    yield delay(300);
    try {
      const search = tableConfig.filter.search ? `code|${tableConfig.filter.search},fullName|${tableConfig.filter.search}` : null;
      const startDate = tableConfig.filter.startDate ? formatISO(startOfDay(new Date(tableConfig.filter.startDate))) : null;
      const endDate = tableConfig.filter.endDate ? formatISO(endOfDay(new Date(tableConfig.filter.endDate))) : null;

      const { data } = yield call(transactionService.orderListByType, {
        page: tableConfig.page,
        limit: tableConfig.limit,
        sort: tableConfig.sort,
        search,
        startDate,
        endDate,
        status: tableConfig.filter.status,
        type: "BULKING"
      });
      yield put(transactionByBulkingActions.loaded(data));
    } catch (error) {
      // console.log('error reached', error);
    }
  });
}
