import { combineReducers } from 'redux'
import { all } from "redux-saga/effects";
import * as trxByApi from "./trxByApi";
import * as trxByBulking from "./trxByBulking";
import * as trxDetail from "./trxDetail";
import * as trxBySingle from "./trxBySingle";

export const reducer = combineReducers({
  trxDetail: trxDetail.reducer,
  trxByApi: trxByApi.reducer,
  trxByBulking: trxByBulking.reducer,
  trxBySingle: trxBySingle.reducer,
})

export function* sagas() {
  yield all([
    trxDetail.saga(),
    trxByApi.saga(),
    trxByBulking.saga(),
    trxBySingle.saga(),
  ]);
}
