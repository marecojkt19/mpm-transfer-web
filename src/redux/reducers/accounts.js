import { put, takeLatest, call, delay } from "redux-saga/effects";
import accountsService from "../../axios/services/accountsService";
import { accountsActions, accountsTypes } from "../actions/accountsActions";

const initialState = {
  items: [],
  tableConfig: {
    isLoading: false,
    limit: 10,
    sort: "createdAt|desc",
    page: 1,
    filter: {
      startDate: null,
      endDate: null,
      search: '',
      filter: ''
    }
  },
  totalData: 0,
  totalActiveAccounts: 0,
  totalInactiveAccounts: 0
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case accountsTypes.LOAD_REQUESTED: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          isLoading: true,
        }
      }
    }
    case accountsTypes.LOADED: {
      return {
        ...state,
        items: action.payload.data.users,
        totalData: action.payload.data.count,
        totalActiveAccounts: action.payload.data.countActive,
        totalInactiveAccounts: action.payload.data.countNotActive,
        tableConfig: {
          ...state.tableConfig,
          isLoading: false,
        }
      };
    }
    case accountsTypes.SET_TABLE_CONFIG: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          [action.key]: action.value
        }
      };
    }
    default:
      return state;
  }
};

export function* saga() {
  yield takeLatest(accountsTypes.LOAD_REQUESTED, function* loadData({ tableConfig }) {
    yield delay(300);
    try {
      const { data } = yield call(accountsService.list, {
        page: tableConfig.page,
        limit: tableConfig.limit,
        sort: tableConfig.sort,
        filter: tableConfig.filter.filter
      });
      yield put(accountsActions.loaded(data));
    } catch (error) {
      // console.log('error reached', error);
    }
  });
}
