import { combineReducers } from 'redux'
import { all } from "redux-saga/effects";
import * as auth from "./auth";
import common from "./common";
import * as transaction from "./transaction";
import * as createTransaction from "./createTransaction";
import * as accounts from "./accounts";
import * as deposit from "./deposit";
import * as verifyAccount from "./verifyAccount";
import * as settings from "./settings";
import * as transactions from "./transactions";

export const reducers = combineReducers({
  common,
  auth: auth.reducer,
  transaction: transaction.reducer,
  accounts: accounts.reducer,
  createTransaction: createTransaction.reducer,
  deposit: deposit.reducer,
  verifyAccount: verifyAccount.reducer,
  settings: settings.reducer,
  transactions: transactions.reducer
});

export function* sagas() {
  yield all([
    auth.saga(),
    accounts.saga(),
    transaction.saga(),
    createTransaction.saga(),
    transactions.sagas(),
    deposit.saga()
  ]);
}
