import { call, delay, put, takeLatest } from "@redux-saga/core/effects";
import createTransactionService from "../../axios/services/createTransactionService";
import { createTransactionAction, createTransactionTypes } from "../actions/createTransactionAction";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const initialState = {
  step: 0,
  method: "",
  activeTabKey: 0,
  estimateTime: 0,
  activeTabName: '',
  transactionId: null,
  transactionFile: null,
  isErrorUpload: false,
  trxCode: null,
  isLoading: false,
  items: [],
  nameItems: [],
  accountItems: [],
  inquiryItems: [],
  detail: [],
  singlePayload: null,
  errors: [],

  listAllConfig: {
    listAllLimit: 10,
    listAllSkip: 1,
    listAllTotalData: 0
  },
  listNameConfig: {
    listNameLimit: null,
    listNameSkip: 1,
    listNameTotalData: 0
  },
  listAccountConfig: {
    listAccountLimit: null,
    listAccountSkip: 1,
    listAccountTotalData: 0
  },
  createTransactionProgress: {
    precentageProggress: 0,
    transactionId: null,
    companyCode: null
  }
};

export const reducer = persistReducer(
  { storage, key: "reducer-create-transaction", whitelist: ["transactionId", "isLoading", "singlePayload", "method"] },
  (state = initialState, action) => {
    switch (action.type) {
      case createTransactionTypes.LOAD_REQUESTED: {
        return {
          ...state,
          transactionFile: action.transactionFile,
          isLoading: true,
        }
      }
      case createTransactionTypes.CREATE_TRANSACTION_PROGRESS: {
        return {
          ...state,
          createTransactionProgress: action.createTransactionProgress
        }
      }
      case createTransactionTypes.ESTIMATE_TIME_TRANSACTION: {
        return {
          ...state,
          estimateTime: action.estimateTime
        }
      }
      case createTransactionTypes.LOADED: {
        const id = action.payload.transactionId;

        return {
          ...state,
          transactionId: id,
          isLoading: false,
        };
      }
      case createTransactionTypes.SET_TRANSACTION_ID: {
        return {
          ...state,
          transactionId: action.transactionId,
        };
      }
      case createTransactionTypes.SET_SINGLE_PAYLOAD: {
        return {
          ...state,
          singlePayload: action.data,
        };
      }
      case createTransactionTypes.LIST_ALL_CONFIG: {
        if (action.key === "listAllSkip") {
          return {
            ...state,
            listAllConfig: {
              ...state.listAllConfig,
              [action.key]: action.value
            }
          };
        } else {
          return {
            ...state,
            listAllConfig: {
              ...initialState.listAllConfig,
              [action.key]: action.value
            }
          };
        }
      }
      case createTransactionTypes.LIST_NAME_CONFIG: {
        if (action.key === "listNameSkip") {
          return {
            ...state,
            listNameConfig: {
              ...state.listNameConfig,
              [action.key]: action.value
            }
          };
        } else {
          return {
            ...state,
            listNameConfig: {
              ...initialState.listNameConfig,
              [action.key]: action.value
            }
          };
        }
      }
      case createTransactionTypes.LIST_ACCOUNT_CONFIG: {
        if (action.key === "listAccountSkip") {
          return {
            ...state,
            listAccountConfig: {
              ...state.listAccountConfig,
              [action.key]: action.value
            }
          };
        } else {
          return {
            ...state,
            listAccountConfig: {
              ...initialState.listAccountConfig,
              [action.key]: action.value
            }
          };
        }
      }
      case createTransactionTypes.SET_LOADING: {
        return {
          ...state,
          isLoading: action.payload.bool,
        };
      }
      case createTransactionTypes.SET_DETAIL: {
        return {
          ...state,
          detail: action.payload.data,
        };
      }
      case createTransactionTypes.SET_TRX_CODE: {
        return {
          ...state,
          trxCode: action.payload.data
        };
      }
      case createTransactionTypes.SET_LIST: {
        return {
          ...state,
          items: action.payload.data,
          isLoading: true,
        };
      }
      case createTransactionTypes.SET_LIST_NAME: {
        return {
          ...state,
          nameItems: action.payload.data,
          isLoading: true,
        };
      }
      case createTransactionTypes.SET_LIST_ACCOUNT: {
        return {
          ...state,
          accountItems: action.payload.data,
          isLoading: true,
        };
      }
      case createTransactionTypes.SET_PAYLOAD_INQUIRY_BULK: {
        return {
          ...state,
          inquiryItems: action.payload.data,
        };
      }
      case createTransactionTypes.SET_STEP: {
        return {
          ...state,
          step: action.key,
        };
      }
      case createTransactionTypes.SET_METHOD: {
        return {
          ...state,
          method: action.key,
        };
      }
      case createTransactionTypes.RESET: {
        return {
          ...state,
          step: 0,
          activeTabKey: 0,
          activeTabName: '',
          transactionId: null,
          estimateTime: 0,
          trxCode: null,
          isLoading: false,
          errors: [],
          items: [],
          nameItems: [],
          accountItems: [],
          inquiryItems: [],
          detail: []
        };
      }
      case createTransactionTypes.CHANGE_TAB: {
        return {
          ...state,
          activeTabName: action.payload.name,
          activeTabKey: action.payload.key,
        }
      }
      case createTransactionTypes.SET_ERROR_UPLOAD: {
        return {
          ...state,
          isErrorUpload: action.payload.bool,
        }
      }
      case createTransactionTypes.SET_ERRORS: {
        return {
          ...state,
          errors: action.errors,
        }
      }
      default:
        return state;
    }
  })

export function* saga() {
  yield takeLatest(createTransactionTypes.LOAD_REQUESTED, function* loadRequest({ transactionFile }) {
    yield delay(300);
    try {
      const { data: { transactionId } } = yield call(createTransactionService.uploadClsx, { transactionFile });
      yield put(createTransactionAction.loaded(transactionId));
      yield put(createTransactionAction.setErrorUpload(false));
    } catch (error) {
      if (error?.response?.body?.message !== "Connection Timed Out") {
        const errors = error?.response?.data?.message;
        if (typeof errors !== "string") {
          yield put(createTransactionAction.setErrors(errors))
        }
        yield put(createTransactionAction.loaded());
        yield put(createTransactionAction.setErrorUpload(true));
      }
    }
  });
}
