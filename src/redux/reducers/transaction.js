import { endOfDay, formatISO, startOfDay, startOfMonth, endOfMonth } from "date-fns";
import { put, takeLatest, call, delay } from "redux-saga/effects";
import transactionService from "../../axios/services/transactionService";
import { transactionActions, transactionTypes } from "../actions/transactionAction";

const initialState = {
  trxCode: '',
  orderCode: '',
  orderId: '',
  items: [],
  itemsOrderAPI: [],
  detailItems: [],
  transferFeeDetails: [],
  paymentMethod: {
    otpMethod: "",
    method: ""
  },
  step: 0,
  tab: 0,
  tableConfigDetail: {
    isLoading: false,
    limit: 5,
    page: 1,
    totalData: 0,
    trxCode: "",
  },
  tableConfigOrderAPI: {
    isLoading: false,
    limit: 5,
    page: 1,
    totalData: 0,
    sort: "createdAt|desc",
    filter: {
      startDate: startOfMonth(new Date()),
      endDate: endOfMonth(new Date()),
      search: '',
      filter: ''
    }
  },
  tableConfig: {
    isLoading: false,
    limit: 5,
    page: 1,
    totalData: 0,
    sort: "createdAt|desc",
    filter: {
      startDate: startOfMonth(new Date()),
      endDate: endOfMonth(new Date()),
      search: '',
      filter: ''
    }
  }
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case transactionTypes.LOAD_REQUESTED: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          isLoading: true,
        }
      }
    }
    case transactionTypes.LOAD_REQUESTED_ORDER_API: {
      return {
        ...state,
        tableConfigOrderAPI: {
          ...state.tableConfigOrderAPI,
          isLoading: true,
        }
      }
    }
    case transactionTypes.LOAD_DETAIL_REQUESTED: {
      return {
        ...state,
        tableConfigDetail: {
          ...state.tableConfigDetail,
          isLoading: true,
        }
      }
    }
    case transactionTypes.LOADED: {
      return {
        ...state,
        detailItems: [],
        items: action.payload.data,
        tableConfig: {
          ...state.tableConfig,
          totalData: action.payload.data.count,
          isLoading: false,
        }
      };
    }
    case transactionTypes.LOADED_ORDER_API: {
      return {
        ...state,
        itemsOrderAPI: action.payload.data.orders,
        tableConfigOrderAPI: {
          ...state.tableConfigOrderAPI,
          totalData: action.payload.data.count,
          isLoading: false,
        }
      };
    }
    case transactionTypes.LOADED_DETAIL: {
      return {
        ...state,
        detailItems: action.payload.data,
        tableConfigDetail: {
          ...state.tableConfigDetail,
          totalData: action.payload.data.count,
          isLoading: false,
        }
      };
    }
    case transactionTypes.SET_TABLE_CONFIG: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          [action.key]: action.value
        }
      };
    }
    case transactionTypes.SET_TABLE_CONFIG_ORDER_API: {
      return {
        ...state,
        tableConfigOrderAPI: {
          ...state.tableConfigOrderAPI,
          [action.key]: action.value
        }
      };
    }
    case transactionTypes.SET_TABLE_DETAIL_CONFIG: {
      return {
        ...state,
        tableConfigDetail: {
          ...state.tableConfigDetail,
          [action.key]: action.value
        }
      };
    }
    case transactionTypes.SET_TRX_CODE: {
      return {
        ...state,
        trxCode: action.trxCode
      };
    }
    case transactionTypes.SET_ORDER_ID: {
      return {
        ...state,
        orderId: action.orderId
      };
    }
    case transactionTypes.SET_ORDER_CODE: {
      return {
        ...state,
        orderCode: action.orderCode
      };
    }
    case transactionTypes.SET_TABLE_CONFIG_FILTER: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          filter: {
            ...state.tableConfig.filter,
            [action.key]: action.value
          },
          page: 1
        }
      };
    }
    case transactionTypes.SET_TABLE_CONFIG_FILTER_ORDER_API: {
      return {
        ...state,
        tableConfigOrderAPI: {
          ...state.tableConfigOrderAPI,
          filter: {
            ...state.tableConfigOrderAPI.filter,
            [action.key]: action.value
          },
          page: 1
        }
      };
    }
    case transactionTypes.SET_DETAIL: {
      return {
        ...state,
        detailItems: action.payload.data,
      };
    }
    case transactionTypes.SET_PAYMENT_METHOD: {
      return {
        ...state,
        paymentMethod: action.payload.data,
      };
    }
    case transactionTypes.CHANGE_TAB:
      return {
        ...state,
        activeTabKey: action.key,
      }
    case transactionTypes.SET_STEP: {
      return {
        ...state,
        step: action.key,
      };
    }
    case transactionTypes.SET_TAB: {
      return {
        ...state,
        tab: action.key,
      };
    }
    case transactionTypes.SET_TRANSFER_FEE_DETAILS: {
      return {
        ...state,
        transferFeeDetails: action.payload.data,
      };
    }
    case transactionTypes.RESET: {
      return {
        idActiveDetail: '',
        items: [],
        itemsOrderAPI: [],
        detailItems: [],
        paymentMethod: {
          otpMethod: "",
          method: ""
        },
        step: 0,
        tableConfigDetail: {
          isLoading: false,
          limit: 5,
          page: 1,
          totalData: 0,
          trxCode: "",
        },
        tableConfigOrderAPI: {
          isLoading: false,
          limit: 5,
          page: 1,
          totalData: 0,
          sort: "createdAt|desc",
          filter: {
            ...state.tableConfigOrderAPI.filter
          }
        },
        tableConfig: {
          isLoading: false,
          totalData: 0,
          limit: 5,
          sort: "createdAt|desc",
          page: 1,
          filter: {
            ...state.tableConfig.filter
          }
        }
      };
    }
    default:
      return state;
  }
};

export function* saga() {
  yield takeLatest(transactionTypes.LOAD_REQUESTED, function* loadData({ tableConfig }) {
    yield delay(300);
    try {
      const search = tableConfig.filter.search ? `code|${tableConfig.filter.search}` : null;
      const startDate = tableConfig.filter.startDate ? formatISO(startOfDay(new Date(tableConfig.filter.startDate))) : null;
      const endDate = tableConfig.filter.endDate ? formatISO(endOfDay(new Date(tableConfig.filter.endDate))) : null;

      const { data } = yield call(transactionService.orderList, {
        page: tableConfig.page,
        limit: tableConfig.limit,
        sort: tableConfig.sort,
        search,
        startDate,
        endDate,
        filter: tableConfig.filter.filter
      });
      yield put(transactionActions.loaded(data));
    } catch (error) {
      // console.log('error reached', error);
    }
  });

  yield takeLatest(transactionTypes.LOAD_REQUESTED_ORDER_API, function* loadData({ tableConfigOrderAPI }) {
    yield delay(300);
    try {
      const search = tableConfigOrderAPI.filter.search ? `accountNumber|${tableConfigOrderAPI.filter.search},beneficiaryAccountName|${tableConfigOrderAPI.filter.search}` : null;
      const startDate = tableConfigOrderAPI.filter.startDate ? formatISO(startOfDay(new Date(tableConfigOrderAPI.filter.startDate))) : null;
      const endDate = tableConfigOrderAPI.filter.endDate ? formatISO(endOfDay(new Date(tableConfigOrderAPI.filter.endDate))) : null;

      const { data } = yield call(transactionService.orderByAPI, {
        page: tableConfigOrderAPI.page,
        limit: tableConfigOrderAPI.limit,
        sort: tableConfigOrderAPI.sort,
        search,
        startDate,
        endDate,
        filter: tableConfigOrderAPI.filter.filter
      });
      yield put(transactionActions.loadedOrderAPI(data));
    } catch (error) {
      // console.log('error reached', error);
    }
  });

  yield takeLatest(transactionTypes.LOAD_DETAIL_REQUESTED, function* loadData({ tableConfigDetail }) {
    yield delay(300)
    try {
      const { data } = yield call(transactionService.detail, {
        page: tableConfigDetail.page,
        limit: tableConfigDetail.limit,
        trxCode: tableConfigDetail.trxCode,
      });
      yield put(transactionActions.loadedDetail(data));
    } catch (error) {
      // console.log('error reached', error);
    }
  })
}
