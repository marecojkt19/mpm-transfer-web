// import { settingsTypes, settingsActions } from "../actions/settingsAction";
// import { endOfDay, formatISO, startOfDay, startOfMonth, endOfMonth } from "date-fns";
// import { call, delay, put, takeLatest } from "@redux-saga/core/effects";
// import settingsService from "../../axios/services/settingsService";

import { settingsTypes } from "../actions/settingsAction";
import { startOfMonth, endOfMonth } from "date-fns";

const initialState = {
  activeTabKey: 0,
  id: undefined,
  step: 0,
  items: [],
  tableConfig: {
    isLoading: false,
    totalData: 0,
    limit: 10,
    sort: "createdAt|desc",
    page: 1,
    filter: {
      startDate: startOfMonth(new Date()),
      endDate: endOfMonth(new Date()),
      search: '',
      bankName: '',
      filter: '',
      status: '',
    }
  }
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case settingsTypes.LOAD_REQUESTED: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          isLoading: true,
        }
      }
    }
    case settingsTypes.LOADED: {
      return {
        ...state,
        items: action.payload.data.deposits,
        tableConfig: {
          ...state.tableConfig,
          totalData: action.payload.data.count,
          isLoading: false,
        }
      };
    }
    case settingsTypes.SET_TABLE_CONFIG: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          [action.key]: action.value
        }
      };
    }
    case settingsTypes.SET_TABLE_CONFIG_FILTER: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          filter: {
            ...state.tableConfig.filter,
            [action.key]: action.value
          },
          page: 1
        }
      };
    }
    case settingsTypes.CHANGE_TAB:
      return {
        ...state,
        activeTabKey: action.key,
      }
    case settingsTypes.SET_ID:
      return {
        ...state,
        id: action.key,
      }
    case settingsTypes.SET_STEP:
      return {
        ...state,
        step: action.key,
      }
    default:
      return state;
  }
}

// export function* saga() {
//   yield takeLatest(settingsTypes.LOAD_REQUESTED, function* loadData({ tableConfig }) {
//     yield delay(300);
//     try {
//       const search = tableConfig.filter.search ? `trxNumber|${tableConfig.filter.search}` : null;
//       const startDate = tableConfig.filter.startDate ? formatISO(startOfDay(new Date(tableConfig.filter.startDate))) : null;
//       const endDate = tableConfig.filter.endDate ? formatISO(endOfDay(new Date(tableConfig.filter.endDate))) : null;

//       const { data } = yield call(depositService.historyList, {
//         page: tableConfig.page,
//         limit: tableConfig.limit,
//         sort: tableConfig.sort,
//         search,
//         startDate,
//         endDate,
//         filter: tableConfig.filter.filter,
//         bankName: tableConfig.filter.bankName,
//       });
//       yield put(depositActions.loaded(data));
//     } catch (error) {
//       console.log('error reached', error);
//     }
//   });
// }
