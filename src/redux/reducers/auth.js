import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { put, takeLatest, call, delay } from "redux-saga/effects";
import { REHYDRATE } from 'redux-persist/lib/constants';
import { authActionTypes, authActions } from "../actions/authActions";
import authService from "../../axios/services/authService";

const initialState = {
  user: undefined,
  company: undefined,
  authToken: undefined,
  refreshToken: undefined,
  signature: undefined,
  error: undefined,
  isNewRegistered: false
};

export const reducer = persistReducer(
  { storage, key: "reducer-auth", whitelist: ["authToken", "refreshToken"] },
  (state = initialState, action) => {
    switch (action.type) {
      case authActionTypes.LOGIN_REGISTER: {
        return {
          ...state,
          authToken: action.accessToken,
          refreshToken: action.refreshToken,
          user: action.user,
          signature: action.signature,
          company: action.company,
          isNewRegistered: action.isNewRegistered
        };
      }
      case authActionTypes.SET_IS_NEW_REGISTERED: {
        return {
          ...state,
          isNewRegistered: action.isNewRegistered
        };
      }
      case authActionTypes.LOGOUT: {
        return initialState;
      }
      case authActionTypes.USER_COMPANY_LOADED: {
        return {
          ...state,
          signature: action.signature,
          user: action.user,
          company: action.company
        };
      }
      case authActionTypes.USER_LOADED: {
        return {
          ...state,
          user: action.user
        };
      }
      case authActionTypes.COMPANY_LOADED: {
        return {
          ...state,
          company: {
            ...action.company,
            logo: state.company.logo
          }
        };
      }
      case authActionTypes.ERROR_LOADED: {
        return {
          ...state,
          error: action.error
        };
      }
      default:
        return state;
    }
  }
);

export function* saga() {
  // Automatically Request User when token retrieved by redux-persist from Storage
  yield takeLatest(REHYDRATE, function* rehydrateSaga({ payload: { authToken } }) {
    if (authToken) yield put(authActions.requestUser());
  });

  yield takeLatest(authActionTypes.REQUEST_USER, function* getUser() {
    yield delay(300)
    try {
      const { data: { user, company, signature } } = yield call(authService.me);
      yield put(authActions.userCompanyLoaded(user, company, signature));
    } catch (error) {
      yield put(authActions.errorLoaded(error));
    }
  });
}
