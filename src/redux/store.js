import { configureStore } from "@reduxjs/toolkit";
import { persistStore } from "redux-persist";
import createSagaMiddleware from "redux-saga";
import { exportAllExcelMiddleware } from "./middlewares/exportExcel";
import { reducers, sagas } from "./reducers";

const sagaMiddleware = createSagaMiddleware();
const middlewares = [
  sagaMiddleware,
  exportAllExcelMiddleware
];

export const store = configureStore({
  reducer: reducers,
  middleware: middlewares,
  devTools: process.env.NODE_ENV !== "production",
});

export const persistor = persistStore(store);

sagaMiddleware.run(sagas);
