const moduleKey = 'VERIFY_ACCOUNT';

export const verifyAccountTypes = {
  UPLOAD: moduleKey + "_UPLOAD",
  EFFECTIVE: moduleKey + "_EFFECTIVE",
  SET_STEP: moduleKey + "_SET_STEP",
  SET_SIGNATURE: moduleKey + "_SET_SIGNATURE",
  SET_POSITION: moduleKey + "SET_POSITION",
  REMOVE_FILE: moduleKey + "_REMOVE_FILE",
  SET_CHECKED: moduleKey + "_SET_CHECKED",
  SET_DOCS_FILES: moduleKey + "_SET_DOCS_FILES",
  SET_SUMMARY: moduleKey + "_SET_SUMMARY",
};

export const verifyAccountActions = {
  upload: (file, name) => ({ type: verifyAccountTypes.UPLOAD, payload: { fileDoc: { file, name } } }),
  setSummary: (data) => ({ type: verifyAccountTypes.SET_SUMMARY, payload: data }),
  removeFile: (name) => ({ type: verifyAccountTypes.REMOVE_FILE, payload: { fileDoc: { name } } }),
  setChecked: (arr) => ({ type: verifyAccountTypes.SET_CHECKED, payload: { arr } }),
  effective: (value) => ({ type: verifyAccountTypes.EFFECTIVE, payload: value }),
  setStep: (key) => ({ type: verifyAccountTypes.SET_STEP, payload: { key } }),
  setSignature: (signature) => ({ type: verifyAccountTypes.SET_SIGNATURE, payload: { signature } }),
  setPosition: (position) => ({ type: verifyAccountTypes.SET_POSITION, position }),
  setDocsFiles: (docsFiles) => ({ type: verifyAccountTypes.SET_DOCS_FILES, payload: { docsFiles } }),
};
