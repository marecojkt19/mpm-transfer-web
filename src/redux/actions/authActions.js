export const authActionTypes = {
  LOGIN_REGISTER: "LOGIN_REGISTER",
  LOGOUT: "LOGOUT",
  REQUEST_USER: "REQUEST_USER",
  USER_COMPANY_LOADED: "USER_COMPANY_LOADED",
  USER_LOADED: "USER_LOADED",
  COMPANY_LOADED: "COMPANY_LOADED",
  ERROR_LOADED: "ERROR_LOADED",
  SET_IS_NEW_REGISTERED: "SET_IS_NEW_REGISTERED"
};

export const authActions = {
  loginRegister: (user, company, accessToken, refreshToken, isNewRegistered, signature) => (
    {
      type: authActionTypes.LOGIN_REGISTER,
      user,
      company,
      accessToken,
      refreshToken,
      isNewRegistered,
      signature
    }
  ),
  setIsNewRegistered: (isNewRegistered) => ({ type: authActionTypes.SET_IS_NEW_REGISTERED, isNewRegistered }),
  logout: () => ({ type: authActionTypes.LOGOUT }),
  requestUser: () => ({ type: authActionTypes.REQUEST_USER }),
  userCompanyLoaded: (user, company, signature) => ({ type: authActionTypes.USER_COMPANY_LOADED, user, company, signature }),
  userLoaded: (user) => ({ type: authActionTypes.USER_LOADED, user }),
  companyLoaded: (company) => ({ type: authActionTypes.COMPANY_LOADED, company }),
  errorLoaded: (error) => ({ type: authActionTypes.ERROR_LOADED, error }),
};
