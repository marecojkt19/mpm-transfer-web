const moduleKey = 'SETTINGS';

export const settingsTypes = {
  LOAD_REQUESTED: moduleKey + "_LOAD_REQUESTED",
  LOADED: moduleKey + "_LOADED",
  SET_TABLE_CONFIG: moduleKey + "_SET_TABLE_CONFIG",
  SET_TABLE_CONFIG_FILTER: moduleKey + "_SET_TABLE_CONFIG_FILTER",
  CHANGE_TAB: moduleKey + '_CHANGE_TAB',
  SET_ID: moduleKey + '_SET_ID',
  SET_STEP: moduleKey + '_SET_STEP',
};

export const settingsActions = {
  loadRequested: (tableConfig) => ({ type: settingsTypes.LOAD_REQUESTED, tableConfig }),
  loaded: (data) => ({ type: settingsTypes.LOADED, payload: { data } }),
  setTableConfig: (key, value) => ({ type: settingsTypes.SET_TABLE_CONFIG, key, value }),
  setTableConfigFilter: (key, value) => ({ type: settingsTypes.SET_TABLE_CONFIG_FILTER, key, value }),
  changeTab: (key) => ({ type: settingsTypes.CHANGE_TAB, key }),
  setId: (key) => ({ type: settingsTypes.SET_ID, key }),
  setStep: (key) => ({ type: settingsTypes.SET_STEP, key })
};
