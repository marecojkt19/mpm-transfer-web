const moduleKey = 'ACCOUNTS';

export const accountsTypes = {
  LOAD_REQUESTED: moduleKey + "_LOAD_REQUESTED",
  LOADED: moduleKey + "_LOADED",
  SET_TABLE_CONFIG: moduleKey + "_SET_TABLE_CONFIG"
};

export const accountsActions = {
  loadRequested: (tableConfig) => ({ type: accountsTypes.LOAD_REQUESTED, tableConfig }),
  loaded: (data) => ({ type: accountsTypes.LOADED, payload: { data } }),
  setTableConfig: (key, value) => ({ type: accountsTypes.SET_TABLE_CONFIG, key, value }),
};
