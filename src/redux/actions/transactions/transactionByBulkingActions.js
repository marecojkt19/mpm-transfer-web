const moduleKey = 'TRANSACTION_BY_BULKING';

export const transactionByBulkingTypes = {
  LOAD_REQUESTED: moduleKey + "_LOAD_REQUESTED",
  LOADED: moduleKey + "_LOADED",
  SET_TABLE_CONFIG: moduleKey + "_SET_TABLE_CONFIG",
  SET_TABLE_CONFIG_FILTER: moduleKey + "_SET_TABLE_CONFIG_FILTER"
};

export const transactionByBulkingActions = {
  loadRequested: (tableConfig) => ({ type: transactionByBulkingTypes.LOAD_REQUESTED, tableConfig }),
  loaded: (data) => ({ type: transactionByBulkingTypes.LOADED, data }),
  setTableConfig: (key, value) => ({ type: transactionByBulkingTypes.SET_TABLE_CONFIG, key, value }),
  setTableConfigFilter: (key, value) => ({ type: transactionByBulkingTypes.SET_TABLE_CONFIG_FILTER, key, value }),
};
