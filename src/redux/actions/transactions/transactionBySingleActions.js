const moduleKey = 'TRANSACTION_BY_SINGLE';

export const transactionBySingleTypes = {
  LOAD_REQUESTED: moduleKey + "_LOAD_REQUESTED",
  LOADED: moduleKey + "_LOADED",
  SET_TABLE_CONFIG: moduleKey + "_SET_TABLE_CONFIG",
  SET_TABLE_CONFIG_FILTER: moduleKey + "_SET_TABLE_CONFIG_FILTER"
};

export const transactionBySingleActions = {
  loadRequested: (tableConfig) => ({ type: transactionBySingleTypes.LOAD_REQUESTED, tableConfig }),
  loaded: (data) => ({ type: transactionBySingleTypes.LOADED, data }),
  setTableConfig: (key, value) => ({ type: transactionBySingleTypes.SET_TABLE_CONFIG, key, value }),
  setTableConfigFilter: (key, value) => ({ type: transactionBySingleTypes.SET_TABLE_CONFIG_FILTER, key, value }),
};
