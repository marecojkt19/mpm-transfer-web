const moduleKey = 'TRANSACTION_DETAIL';

export const transactionDetailTypes = {
  LOAD_REQUESTED: moduleKey + "_LOAD_REQUESTED",
  LOADED: moduleKey + "_LOADED",
  SET_TABLE_CONFIG: moduleKey + "_SET_TABLE_CONFIG",
  SET_TRX_CODE: moduleKey + "_SET_TRX_CODE",
  SET_TABLE_DETAIL_CONFIG: moduleKey + "_SET_TABLE_DETAIL_CONFIG",
  SET_STEP: moduleKey + "_SET_STEP",
  SET_TRANSFER_FEE_DETAILS: moduleKey + "_SET_TRANSFER_FEE_DETAILS",
  RESET: moduleKey + "_RESET",
  PAYMENT_METHOD: moduleKey + "_PAYMENT_METHOD",
  OTP_METHOD: moduleKey + "_OTP_METHOD",
  SET_IS_SEND_INVOICE: moduleKey + "_SET_IS_SEND_INVOICE",
  SET_TABLE_CONFIG_FILTER: moduleKey + "_SET_TABLE_CONFIG_FILTER"
};

export const transactionDetailActions = {
  loadRequested: (tableConfig) => ({ type: transactionDetailTypes.LOAD_REQUESTED, tableConfig }),
  loaded: (data) => ({ type: transactionDetailTypes.LOADED, data }),
  setTrxCode: (trxCode) => ({ type: transactionDetailTypes.SET_TRX_CODE, trxCode }),
  setTableDetailConfig: (key, value) => ({ type: transactionDetailTypes.SET_TABLE_DETAIL_CONFIG, key, value }),
  setTableConfig: (key, value) => ({ type: transactionDetailTypes.SET_TABLE_CONFIG, key, value }),
  setTableConfigFilter: (key, value) => ({ type: transactionDetailTypes.SET_TABLE_CONFIG_FILTER, key, value }),
  setStep: (key) => ({ type: transactionDetailTypes.SET_STEP, key }),
  setPaymentMethod: (paymentMethod) => ({ type: transactionDetailTypes.PAYMENT_METHOD, paymentMethod }),
  setOtpMethod: (otpMethod) => ({ type: transactionDetailTypes.OTP_METHOD, otpMethod }),
  reset: () => ({ type: transactionDetailTypes.RESET }),
  setIsSendInvoice: (isSendInvoice) => ({ type: transactionDetailTypes.SET_IS_SEND_INVOICE, isSendInvoice }),
  setTransferFeeDetails: (data) => ({ type: transactionDetailTypes.SET_TRANSFER_FEE_DETAILS, payload: { data } }),
};
