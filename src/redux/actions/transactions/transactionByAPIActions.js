const moduleKey = 'TRANSACTION_BY_API';

export const transactionByAPITypes = {
  LOAD_REQUESTED: moduleKey + "_LOAD_REQUESTED",
  LOADED: moduleKey + "_LOADED",
  SET_TABLE_CONFIG: moduleKey + "_SET_TABLE_CONFIG",
  SET_TABLE_CONFIG_FILTER: moduleKey + "_SET_TABLE_CONFIG_FILTER"
};

export const transactionByAPIActions = {
  loadRequested: (tableConfig) => ({ type: transactionByAPITypes.LOAD_REQUESTED, tableConfig }),
  loaded: (data) => ({ type: transactionByAPITypes.LOADED, data }),
  setTableConfig: (key, value) => ({ type: transactionByAPITypes.SET_TABLE_CONFIG, key, value }),
  setTableConfigFilter: (key, value) => ({ type: transactionByAPITypes.SET_TABLE_CONFIG_FILTER, key, value }),
};
