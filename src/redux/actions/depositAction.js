const moduleKey = 'DEPOSIT';

export const depositTypes = {
  LOAD_REQUESTED: moduleKey + "_LOAD_REQUESTED",
  LOADED: moduleKey + "_LOADED",
  SET_TABLE_CONFIG: moduleKey + "_SET_TABLE_CONFIG",
  SET_TABLE_CONFIG_FILTER: moduleKey + "_SET_TABLE_CONFIG_FILTER",
  CHANGE_TAB: moduleKey + '_CHANGE_TAB',
  SET_ID: moduleKey + '_SET_ID',
  SET_STEP: moduleKey + '_SET_STEP',
};

export const depositActions = {
  loadRequested: (tableConfig) => ({ type: depositTypes.LOAD_REQUESTED, tableConfig }),
  loaded: (data) => ({ type: depositTypes.LOADED, payload: { data } }),
  setTableConfig: (key, value) => ({ type: depositTypes.SET_TABLE_CONFIG, key, value }),
  setTableConfigFilter: (key, value) => ({ type: depositTypes.SET_TABLE_CONFIG_FILTER, key, value }),
  changeTab: (key) => ({ type: depositTypes.CHANGE_TAB, key }),
  setId: (key) => ({ type: depositTypes.SET_ID, key }),
  setStep: (key) => ({ type: depositTypes.SET_STEP, key })
};
