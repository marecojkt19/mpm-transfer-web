import ReactDOMServer from 'react-dom/server';
import { toast } from 'react-toastify';
import { ExcelTable } from '../../components/Table/ExcelTable';
import ExportExcelToast from '../../components/templates/ExportExcelToast';
import { EXPORT_EXCEL_ALL } from '../../utils/enums/actionTypes';

const base64 = (s) => window.btoa(unescape(encodeURIComponent(s)));
const format = (s, c) => s.replace(/{(\w+)}/g, (m, p) => c[p]);

const exportAllExcelMiddleware = store => next => action => {
  new Promise((resolve, reject) => {
    if (action.type === EXPORT_EXCEL_ALL) {
      var isCompleted = false;

      // toast initial with 0 progress
      const toastId = toast(<ExportExcelToast filename={action.filename} progress={0} />, {
        progress: 0,
        autoClose: false,
        closeButton: true,
        draggable: false,
        hideProgressBar: false,
        closeOnClick: isCompleted,
        type: "info"
      })

      // iteration call api
      let items = []
      const recursiveAPICall = (currentPage, totalPage) => {
        let isValidToExport;
        let progress = currentPage / totalPage;
        if (totalPage === 0) isValidToExport = true;
        if (progress === 100) isCompleted = true;

        if (isValidToExport) {
          toast.update(toastId, {
            render: <ExportExcelToast isNotValid={true} />,
            progress: progress
          })
        }
        if (!isValidToExport) {
          action.api.service({
            ...action.api.APIParams,
            page: currentPage
          }).then(res => {
            toast.update(toastId, {
              render: <ExportExcelToast filename={action.filename} progress={progress} />,
              progress: progress
            })
            let newPage = currentPage + 1;
            items = [...items, ...res.data[action.api.apiResponseKey]];

            if (currentPage <= totalPage && currentPage !== totalPage) {
              recursiveAPICall(newPage, totalPage)
            } else {
              let table;
              if (action.customTable) {
                table = action.customTable({
                  columns: action.columns,
                  items: items,
                  rootItems: res.data,
                  lastLength: items?.length
                })
              } else {
                table = ExcelTable({ columns: action.columns, items })
              }

              const html = ReactDOMServer.renderToStaticMarkup(table)
              const excelHref = 'data:application/vnd.ms-excel;base64,'
              const template =
                '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-mic' +
                'rosoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><meta cha' +
                'rset="UTF-8"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:Exce' +
                'lWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/>' +
                '</x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></' +
                'xml><![endif]--></head><body>{html}</body></html>';
              const context = {
                worksheet: 'Worksheet',
                html,
              };

              let a = document.createElement('a')
              a.href = excelHref + base64(format(template, context));
              a.download = action.filename
              a.click()
            }
          })
            .catch(() => toast.dismiss(toastId))
        }
      }
      recursiveAPICall(1, action.totalPage)

    } else {
      resolve(action)
      next(action)
    }
  })
}

export { exportAllExcelMiddleware }
