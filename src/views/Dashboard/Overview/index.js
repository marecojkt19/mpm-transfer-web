import { format } from 'date-fns';
import pakaiClass from 'pakai-class';
import React, { useCallback, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { ChevronRight, PartyPopper, Info } from '../../../assets/icons';
import dashboardService from '../../../axios/services/dashboardService';
import styles from './overview.module.scss'
import { id } from 'date-fns/esm/locale'
import CountUp from 'react-countup';
import { useHistory } from 'react-router-dom';
import LoadingDots from '../../../components/Loadings/LoadingDots';
import { Tooltip } from '@mantine/core';
import { transactionByBulkingActions } from '../../../redux/actions/transactions/transactionByBulkingActions';

const Overview = () => {
  const [items, setItems] = useState(null);
  const [isLoadingReport, setIsLoadingReport] = useState(false)
  const { socket } = useSelector(state => state.common);
  const { user, company } = useSelector(state => state.auth);

  const loadReport = useCallback(() => {
    setIsLoadingReport(true)
    dashboardService.homeReport()
      .then(res => {
        setItems(res.data)
      })
      .catch(() => { })
      .finally(() => setIsLoadingReport(false))
  }, [])

  useEffect(() => {
    loadReport();
    socket.on('UPDATE_STATUS_ORDER', (event) => {
      if (event?.company?.name === company?.name) {
        loadReport()
      }
    })

  }, [socket, loadReport, company])

  return (
    <div className={styles.overview}>
      <div className={styles.welcome}>
        <h3>Selamat datang, {user.fullName}!</h3>
        <PartyPopper />
      </div>
      <div className={styles.dateNow}>
        <span>{format(new Date(), "EEEE, dd MMMM yyyy", { locale: id })}</span>
      </div>
      <div className={styles.line}></div>
      <div className={styles.overviewWrapper}>
        <div className={styles.title}>
          <h3>Overview</h3>
        </div>
        <div className={styles.row}>
          <OverviewCard
            title="Menunggu Persetujuan"
            className={styles.waiting}
            link="/transaction?status=PENDING"
            total={items?.totalTrxPending}
            loading={isLoadingReport}
            label="Jumlah transaksi bulking yang membutuhkan persetujuan Admin"
          />
          {/* <OverviewCard
            title="Selesai"
            className={styles.completed}
            total={items?.totalTrxSuccess}
            loading={isLoadingReport}
            label="Jumlah transaksi bulking yang telah disetujui oleh Admin"
          />
          <OverviewCard
            title="Dibatalkan"
            className={styles.canceled}
            total={items?.totalTrxCanceled}
            loading={isLoadingReport}
            label="Jumlah transaksi bulking yang dibatalkan oleh Admin"
          /> */}
        </div>
      </div>
    </div>
  )
}

const OverviewCard = ({
  className,
  title,
  link,
  total,
  label,
  duration = 3,
  loading
}) => {
  const dispatch = useDispatch()
  const router = useHistory();

  const action = () => {
    switch (title) {
      case "Menunggu Persetujuan": {
        dispatch(transactionByBulkingActions.setTableConfigFilter('filter', "PENDING"))
        dispatch(transactionByBulkingActions.setTableConfigFilter('startDate', null))
        dispatch(transactionByBulkingActions.setTableConfigFilter('endDate', null))
        router.push(link)
        break;
      }
      default:
        break;
    }
  }

  return (
    <button onClick={action} className={pakaiClass(styles.overviewCard, className)}>
      <div className={styles.status}>
        <span>{title}</span>
        <Tooltip
          label={label}
          withArrow
          arrowSize={12}
          sx={tooltipStyles}
        >
          <span><Info size="18" /></span>
        </Tooltip>
      </div>
      <div className={styles.total}>
        <h1>
          <CountUp
            end={total ?? 0}
            duration={duration}
          />
        </h1>
      </div>
      {
        link &&
        <div className={styles.chevronRight}>
          <ChevronRight size="38" />
        </div>
      }
      {
        loading &&
        <LoadingDots className={styles.loading} />
      }
    </button>
  )
}

const tooltipStyles = {
  color: '#545454',
  backgroundColor: '#E3ECFD',
  padding: '10px',
}

export default Overview
