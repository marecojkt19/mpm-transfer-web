import pakaiClass from 'pakai-class';
import React, { Children, cloneElement, useRef } from 'react'
import { Transition } from 'react-transition-group'
// import { ENTERED, ENTERING, EXITING } from 'react-transition-group/transition';
import styles from './tabs.module.scss'

export const Tabs = ({
  children,
  onClick,
  activeKey,
  animationDuration = 200
}) => {
  const nodeRef = useRef();

  return (
    <div className={styles.tabs}>
      <div className={styles.wrapper}>
        {Children.map(children, (child, i) =>
          child
            ?
            cloneElement(child, {
              index: i,
              onClick: child.props?.onClick ?? onClick,
              active: child.props?.active ?? activeKey === i
            })
            : null
        )
        }
      </div>
      <div className={styles.content}>
        {Children.map(children, (child, i) =>
          child ? (
            <Transition
              nodeRef={nodeRef}
              in={child.props?.active ?? activeKey === i}
              timeout={animationDuration}
              unmountOnExit
            >
              {(state) => (
                <div
                  ref={nodeRef}
                  style={{
                    transition: `transform ${animationDuration}ms ease-in-out, opacity ${animationDuration}ms ease-in-out`,
                    opacity: state === 1,
                    transform:
                      state === true
                        ? "translateY(5px)"
                        : state === true
                          ? "translateY(-5px)"
                          : "translateY(0)",
                    position: state === true ? "absolute" : "static",
                    top: 0,
                    left: 0,
                    width: "100%",
                    height: state === true ? 0 : "auto",
                  }}
                >
                  <div className={styles.tabsContent}>
                    {child.props.children}
                  </div>
                </div>
              )}
            </Transition>
          ) : null
        )}
      </div>
    </div>
  )
}

export const Tab = ({
  title,
  className,
  active,
  index,
  onClick
}) => {
  return (
    <button
      className={pakaiClass(
        styles.tab,
        active && styles.active,
        className
      )}
      onClick={() => onClick(index)}
    >
      <span>{title}</span>
    </button>
  )
}
