import React, { useCallback, useEffect, useMemo, useState } from 'react'
import ReactApexChart from 'react-apexcharts'
import ApexCharts from 'apexcharts';
import ReactDomServer from 'react-dom/server';
import SelectField from '../../../components/Form/SelectField'
import toIDR from '../../../utils/helpers/toIDR'
import { Tab, Tabs } from '../Tabs'
import styles from './analytic.module.scss'
import CountUp from 'react-countup';
import dashboardService from '../../../axios/services/dashboardService';
import { eachDayOfInterval, eachWeekOfInterval, format, subDays, subMonths } from 'date-fns';
import { id } from 'date-fns/esm/locale'
import { Reload } from '../../../assets/icons';
import LoadingDots from '../../../components/Loadings/LoadingDots';
import { useSelector } from 'react-redux';

const Analytic = () => {
  const [tab, setTab] = useState(0);
  const { user } = useSelector(state => state.auth)

  const {
    daysOfWeek,
    dailyCategories
  } = useMemo(() => {
    const today = new Date();
    const sevenDaysAgo = subDays(today, 6);

    const sevenDays = eachDayOfInterval({ start: sevenDaysAgo, end: today });

    return {
      daysOfWeek: sevenDays.map((day) => format(new Date(day), "dd MMM yy")),
      dailyCategories: sevenDays.map((day) => format(new Date(day), "dd MMM yy", { locale: id }))
    }
  }, []);

  const {
    monthlyCategories,
    daysOfYear,
    // weeksOfyear
  } = useMemo(() => {
    const today = new Date();
    const oneYearAgo = subMonths(today, 12);

    const oneYearOnWeek = eachWeekOfInterval({ start: oneYearAgo, end: today });
    const oneYearOnDay = eachDayOfInterval({ start: oneYearAgo, end: today });
    const weeksMonthOfyear = oneYearOnWeek.map((day) => format(new Date(day), "MMMM", { locale: id }));
    const weeksOfyear = oneYearOnWeek.map((day) => format(new Date(day), "dd MMMM yyyy"));

    return {
      weeksOfyear,
      daysOfYear: oneYearOnDay.map((day) => format(new Date(day), "dd MMM yy")),
      monthlyCategories: weeksMonthOfyear.map((v, i, arr) => v === arr[i + 1] ? "" : v)
    }
  }, []);

  const {
    daysOfMonth,
    weeklyCategories
  } = useMemo(() => {
    const today = new Date();
    const oneMonthAgo = subDays(subMonths(today, 1), -1);

    const oneMonth = eachDayOfInterval({ start: oneMonthAgo, end: today });

    return {
      daysOfMonth: oneMonth.map((day) => format(new Date(day), "dd MMM yy")),
      weeklyCategories: oneMonth.map((_, i) => (i + 1) % 7 ? "" : format(new Date(_), "dd MMM yy"))
      // weeklyCategories: oneMonth.map((_, i) => (i + 1) % 7 ? "" : `Minggu ${i === 6 ? "1" : i === 13 ? "2" : i === 20 ? "3" : "4"}`)
      // weeklyCategories: oneMonth.map((day) => format(new Date(day), "dd MMM yy", { locale: id }))
    }
  }, []);

  return (
    <div className={styles.analytic}>
      <div className={styles.welcome}>
        <h3>Selamat datang, {user.fullName}!</h3>
        {/* <PartyPopper /> */}
      </div>
      <div className={styles.dateNow}>
        <span>{format(new Date(), "EEEE, dd MMMM yyyy", { locale: id })}</span>
      </div>
      <div className={styles.line}></div>
      <Tabs activeKey={tab} onClick={setTab}>
        <Tab title="Minggu ini">
          <AnalyticContent
            completedTrxCount={123}
            completedTotalTrx={8000000}
            timeRange="ONE_WEEK"
            initialData={daysOfWeek}
            categories={dailyCategories}
          />
        </Tab>
        <Tab title="Bulan ini">
          <AnalyticContent
            completedTrxCount={123}
            completedTotalTrx={8000000}
            timeRange="ONE_MONTH"
            initialData={daysOfMonth}
            categories={weeklyCategories}
          />
        </Tab>
        <Tab title="Tahun ini">
          <AnalyticContent
            completedTrxCount={123}
            completedTotalTrx={8000000}
            timeRange="ONE_YEAR"
            initialData={daysOfYear}
            additionalCategories={daysOfYear}
            categories={monthlyCategories}
          />
        </Tab>
      </Tabs>
    </div>
  )
}

const AnalyticContent = ({
  duration = 3,
  timeRange,
  categories,
  initialData,
  additionalCategories
}) => {
  const [status, setStatus] = useState();
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState(null);

  const load = useCallback(() => {
    setLoading(true)
    dashboardService.analytic({
      timeRange,
      filter: status
    })
      .then(res => {
        setData(res.data)
        setLoading(false)
      })
      .finally(() => {
      })
      .catch(() => { })
  }, [status, timeRange]);

  const successArr = useMemo(() => {
    let init = initialData.map(_ => [new Date(_).getTime(), 0]);

    if (data) {
      data.successTrxs.forEach((v) => {
        const date = format(new Date(v.date), 'dd MMM yy');
        const index = initialData.findIndex(o => o === date);

        if (index !== -1) {
          init[index] = [new Date(v.date).getTime(), v.totalAmount]
        }
      })
    }
    return init;
  }, [initialData, data]);

  const yearlySuccessArr = useMemo(() => {
    // const res = [[]];
    // let mark = 0;
    // successArr.forEach(ele => {
    //   if (res[mark].length > 6) {
    //     res.push([ele]);
    //     mark++;
    //   } else {
    //     res[mark].push(ele);
    //   }
    // });

    // console.log(successArr)

    // const accPerWeek = res.map(v => v.reduce((a, b) => a + b));
    return successArr
  }, [successArr]);

  const cancelArr = useMemo(() => {
    let init = initialData.map(_ => [new Date(_).getTime(), 0]);

    if (data) {
      data.failedTrxs.forEach((v) => {
        const updatedAt = format(new Date(v.date), 'dd MMM yy');
        const index = initialData.findIndex(o => o === updatedAt);

        if (index !== -1) {
          init[index] = [new Date(v.date).getTime(), v.totalAmount]
        }
      })
    }

    return init;
  }, [data, initialData]);

  const yearlyCancelArr = useMemo(() => {
    // const res = [[]];
    // let mark = 0;

    // cancelArr.forEach(ele => {
    //   if (res[mark].length > 6) {
    //     res.push([ele]);
    //     mark++;
    //   }
    //   else {
    //     res[mark].push(ele);
    //   }
    // });
    // const accPerWeek = res.map(v => v.reduce((a, b) => a + b));
    return cancelArr
  }, [cancelArr]);

  const successByTime = useMemo(() => {
    if (timeRange === "ONE_YEAR") return yearlySuccessArr
    else return successArr
  }, [timeRange, yearlySuccessArr, successArr]);

  const cancelByTime = useMemo(() => {
    if (timeRange === "ONE_YEAR") return yearlyCancelArr
    else return cancelArr
  }, [timeRange, yearlyCancelArr, cancelArr])

  useEffect(() => {
    load();
  }, [load])

  const handleRefresh = () => {
    load()
  }

  const toggle = (e) => {
    if (e === "Berhasil") {
      ApexCharts.exec('graphID', 'showSeries', "Berhasil");
      ApexCharts.exec('graphID', 'hideSeries', "Gagal");
    } else {
      ApexCharts.exec('graphID', 'hideSeries', "Berhasil");
      ApexCharts.exec('graphID', 'showSeries', "Gagal");
    }
  }

  const chartOptions = useMemo(() => {
    const getTickAmount = () => {
      switch (timeRange) {
        case "ONE_YEAR":
          return 12
        case "ONE_MONTH":
          return 4
        default:
          return 6
      }
    };
    const getXLabel = (timeRange, value) => {
      switch (timeRange) {
        case "ONE_YEAR":
          return format(new Date(value), "MMMM yy", { locale: id })
        case "ONE_MONTH":
          return format(new Date(value), "dd MMMM yy", { locale: id })
        default:
          return format(new Date(value), "dd MMMM yy", { locale: id })
      }
    }

    return {
      colors: ['#05944F', '#E11900'],
      legend: {
        height: 44,
        markers: {
          width: 14,
          heigh: 14,
          radius: 2
        }
      },
      grid: {
        width: 3,
        borderColor: "#E3ECFD",
      },
      fill: {
        type: "gradient",
        gradient: {
          type: "vertical",
          opacityFrom: .5,
          opacityTo: .3,
        }
      },
      chart: {
        id: "graphID",
        floating: true,
        toolbar: {
          show: false
        },
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth',
        width: 2
      },
      xaxis: {
        type: 'numeric',
        tickAmount: getTickAmount(),
        categories,
        axisBorder: {
          show: true,
          color: "#E5E5EA"
        },
        labels: {
          show: true,
          style: {
            fontSize: '12px',
            fontFamily: 'Inter, sans-serif',
          },
          formatter: function (value) {
            return getXLabel(timeRange, value)
          }
        },
        group: {
          groups: [{
            title: "ABC",
            cols: 3
          }, {
            title: "XYZ",
            cols: 4
          }]
        },
        crosshairs: {
          stroke: {
            color: '#276ef1',
            width: 1,
            dashArray: 0
          }
        },
        tooltip: { enabled: false }
      },
      yaxis: {
        axisBorder: {
          show: true,
          color: "#E5E5EA"
        },
        tickAmount: 8,
        labels: {
          show: true,
          style: {
            fontSize: '12px',
            fontFamily: 'Inter, sans-serif',
          },
          formatter: function (value) {
            return toIDR(value, false)
          }
        }
      },
      markers: {
        colors: 'white',
        strokeColor: '#276ef1',
        shape: "circle",
        strokeWidth: 2,
        strokeDashArray: 2,
      },
      tooltip: {
        custom: (e) => CustomTooltip(
          e, timeRange === "ONE_YEAR" ? additionalCategories : initialData,
          data?.successTrxs,
          data?.failedTrxs
        ),
      }
    }
  }, [categories, timeRange, additionalCategories, initialData, data?.successTrxs, data?.failedTrxs]);

  const chartSeries = useMemo(() => {
    return [{
      name: 'Berhasil',
      data: successByTime,

    }, {
      name: 'Gagal',
      data: cancelByTime
    }]
  }, [successByTime, cancelByTime]);

  return (
    <div>
      <div className={styles.wrapperToolBar}>
        <div className={styles.filter}>
          <SelectField
            isClearable
            options={[
              {
                value: "COMPLETED",
                label: "Berhasil"
              },
              {
                value: "FAILED",
                label: "Gagal"
              }
            ]}
            placeholder="Status"
            name="placeOfBirth"
            value={status}
            onChange={(v) => {
              console.log(v)
              setStatus(v?.value)
              toggle(v?.label)
            }}
          />
        </div>
        <button className={styles.btnReload} onClick={handleRefresh}>
          <Reload />
        </button>
      </div>
      <div className={styles.amount}>
        <div className={styles.item}>
          <p>Total Volume Transaksi</p>
          <h1>
            <CountUp
              end={data?.totalTrx ?? 0}
              duration={duration}
            />
          </h1>
        </div>
        <div className={styles.item}>
          <p>Total Nominal Transaksi</p>
          <h1>Rp
            <CountUp
              end={data?.amountTrx ?? 0}
              duration={duration}
              formattingFn={(v) => toIDR(v, false)}
            />,-
          </h1>
        </div>
      </div>
      <div className={styles.chart}>
        <ReactApexChart options={chartOptions} series={chartSeries} type="area" height={600} />
        {
          loading &&
          <div className={styles.loading}>
            <LoadingDots className={styles.loading} />
          </div>
        }
      </div>
    </div>
  )
}

const CustomTooltip = ({
  dataPointIndex,
  series,
  seriesIndex,
  w,
}, categories, successTrxs, failedTrxs) => {
  const isSuccessTrx = Boolean(series[0][dataPointIndex] !== 0)
  const isFailedTrx = Boolean(series[1][dataPointIndex] !== 0)
  const noTrx = (!isFailedTrx && !isSuccessTrx)
  const date = format(new Date(categories[dataPointIndex]), "yyyy-MM-dd")
  const successVolume = successTrxs.filter(volume => volume.date === date)[0]?.volumeTrx;
  const failedVolume = failedTrxs.filter(volume => volume.date === date)[0]?.volumeTrx;

  return ReactDomServer.renderToString(
    <div className={styles.wrapperChartToolTip}>
      <div className={styles.innerChartToolTip}>
        <p className={styles.date}>{format(new Date(categories[dataPointIndex]), "dd MMMM yyyy", { locale: id })}</p>
        {
          !Boolean(noTrx)
          &&
          <div className={styles.trx}>
            {isSuccessTrx && (
              <div className={styles.success}>
                <p>Berhasil</p>
                <span>{successVolume}</span>
                <span>/</span>
                <span className={styles.nominal}>{toIDR(series[0][dataPointIndex], false)}</span>
              </div>
            )}
            {isFailedTrx && (
              <div className={styles.failed}>
                <p>Gagal</p>
                <span>{failedVolume}</span>
                <span>/</span>
                <span className={styles.nominal}>{toIDR(series[1][dataPointIndex], false)}</span>
              </div>
            )}
          </div>
        }
      </div>
    </div>
  );

  // return ('<div style="text-align: center; padding: 10px 24px; display: block;font-weight: 400;font-size: 12px;line-height: 16px;">' +
  //   'Transaksi ' + status + ' di<br/>' + format(new Date(categories[dataPointIndex]), "dd MMMM yyyy", { locale: id }) + '<br/>' +
  //   '<span style="color:' + color + '">Rp' +
  //   toIDR(series[seriesIndex][dataPointIndex], false) +
  //   ';-</span>' +
  //   '</div>'
  // )
}

export default Analytic
