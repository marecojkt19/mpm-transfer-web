import React from 'react'
import Analytic from './Analytic'
import styles from './dashboard.module.scss'
// import Overview from './Overview'

const Dashboard = () => {
  return (
    <div className={styles.dashboard}>
      <div className={styles.heading}>
        <h1>Beranda</h1>
      </div>
      {/* <Overview /> */}
      <Analytic />
    </div>
  )
}

export default Dashboard
