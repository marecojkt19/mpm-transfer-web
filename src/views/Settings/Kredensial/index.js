import React, { useState } from 'react'
import TextField from '../../../components/Form/TextField';
import styles from './kredensial.module.scss'
import MainButton from '../../../components/Form/MainButton';
import Modal from '../../../components/ModalNew';
import credentialService from '../../../axios/services/crendentialService';

const Kredensial = ({
  id
}) => {
  const [showConfirm, setShowConfirm] = useState(false);
  const [showConfirmUpdatePublicKey, setShowConfirmUpdatePublicKey] = useState(false);
  const [loading, setLoading] = useState(false);
  const [credentials, setCredentials] = useState();
  const [publicKey, setPublicKey] = useState();

  const onGenerate = () => {
    setLoading(true)
    credentialService.generateCredentials()
      .then(({ data }) => {
        setCredentials(data)
      })
      .catch(() => { })
      .finally(() => {
        setLoading(false)
        setShowConfirm(false)
      })
  }

  const onUpdatePublicKey = () => {
    setLoading(true)
    const payload = {
      publicKey,
      clientId: credentials.clientId,
      clientSecret: credentials.clientSecret
    }
    credentialService.updatePublicKey(id, payload)
      .catch(() => { })
      .finally(() => {
        setLoading(false)
        setShowConfirmUpdatePublicKey(false)
      })
  }

  return (
    <>
      <div className={styles.kredensial}>
        <div className={styles.wrapper}>
          <form>
            <div className={styles.formWrapper}>
              <div className={styles.input}>
                <TextField
                  name="clientId"
                  label="Client ID"
                  value={credentials?.clientId}
                  disabled
                />
              </div>
              <div className={styles.input}>
                <TextField
                  name="clientKey"
                  label="Client Key"
                  value={credentials?.clientSecret}
                  disabled
                />
              </div>
              <div className={styles.button}>
                <MainButton
                  variant='secondary'
                  type="button"
                  onClick={() => setShowConfirm(true)}
                >
                  Generate
                </MainButton>
              </div>

              <hr />

              <div className={styles.formGroup}>
                <div className={styles.input}>
                  <TextField
                    name="name"
                    label="Public Key"
                    value={publicKey || credentials?.publicKey}
                    onChange={(e) => setPublicKey(e.target.value)}
                    multiline
                  />
                </div>
              </div>
            </div>
            <div className={styles.button}>
              <MainButton
                type="button"
                disabled={!credentials}
                onClick={() => setShowConfirmUpdatePublicKey(true)}
              >
                Simpan
              </MainButton>
            </div>
          </form>
        </div>
      </div>
      <Modal in={showConfirm} onClose={() => setShowConfirm(false)}>
        <div className={styles.modalWrapper}>
          <div className={styles.modalCard}>
            <div className={styles.heading}>
              <h1>Generate Kredensial</h1>
            </div>
            <div className={styles.modalLine}></div>
            <div className={styles.infoWrapper}>
              <p>Apakah Anda yakin ingin men-generate kredensial baru?</p>
            </div>
            <div className={styles.buttons}>
              <MainButton
                variant="secondary"
                onClick={() => setShowConfirm(false)}
              >
                Batalkan
              </MainButton>
              <MainButton
                onClick={onGenerate}
                loading={loading}
                disabled={loading}
              >
                Ya, Lanjutkan
              </MainButton>
            </div>
          </div>
        </div>
      </Modal>
      <Modal in={showConfirmUpdatePublicKey} onClose={() => setShowConfirmUpdatePublicKey(false)}>
        <div className={styles.modalWrapper}>
          <div className={styles.modalCard}>
            <div className={styles.heading}>
              <h1>Perbarui Public Key</h1>
            </div>
            <div className={styles.modalLine}></div>
            <div className={styles.infoWrapper}>
              <p>Apakah Anda yakin ingin memperbarui public key ini?</p>
            </div>
            <div className={styles.buttons}>
              <MainButton
                variant="secondary"
                onClick={() => setShowConfirmUpdatePublicKey(false)}
              >
                Batalkan
              </MainButton>
              <MainButton
                onClick={onUpdatePublicKey}
                loading={loading}
                disabled={loading}
              >
                Ya, Lanjutkan
              </MainButton>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default Kredensial
