import React, { useState } from 'react'
import { useToasts } from 'react-toast-notifications'
import settingService from '../../../axios/services/settingService'
import IDRInput from '../../../components/Form/IDRInput'
import MainButton from '../../../components/Form/MainButton'
import styles from './notification.module.scss'
import { Badge, Divider, Space, Switch } from '@mantine/core'
import Modal, { ModalBody, ModalHead, ModalWrapper } from '../../../components/ModalNew'

const Notification = ({
  data,
  companyItems
}) => {
  const [amount, setAmount] = useState(data?.minimumAmount.toString());
  const [isDepositMinimum, setIsDepositMinimum] = useState(Boolean(companyItems?.status?.sendNotifminDeposit));
  const [isRecipientTrxBulking, setIsRecipientTrxBulking] = useState(Number(companyItems?.status?.alwaysSendEmailReceiveTrx));
  const [loading, setLoading] = useState(false);
  const [isSubmit, setIsSubmit] = useState(false);
  const [showExample, setShowExample] = useState(false);
  const { addToast } = useToasts();

  const onSubmit = async (e) => {
    e.preventDefault()
    let isSuccess = false;

    const preparedData = {
      companyName: companyItems?.name,
      emailCompany: companyItems?.email.join(","),
      files: companyItems?.logo?.url
    }
    setLoading(true)

    await settingService.updateProfile({
      id: companyItems._id,
      data: { alwaysSendEmailReceiveTrx: Number(isRecipientTrxBulking), ...preparedData }
    })
      .then(() => {
        isSuccess = true;
      })
      .finally(() => setLoading(false))
    await settingService.notification({
      amount,
      sendNotifminDeposit: Boolean(isDepositMinimum),
    }, companyItems._id)
      .then(() => {
        isSuccess = true;
      })
      .finally(() => setLoading(false))

    if (isSuccess) {
      addToast("Berhasil Tersimpan", { appearance: 'success' })
    } else {
      addToast("Something when wrong", { appearance: 'danger' })
    }

    setIsSubmit(true)
  }

  const isValuesChanging =
    amount === data?.minimumAmount.toString()
    && isDepositMinimum === companyItems?.status?.sendNotifminDeposit
    && isRecipientTrxBulking === companyItems?.status?.alwaysSendEmailReceiveTrx;

  return (
    <>
      <div className={styles.notification}>
        <div className={styles.heading}>
          <p>Notifikasi Via Email</p>
        </div>
        <form onSubmit={onSubmit}>
          <div className={styles.wrapperForm}>
            <div className={styles.input}>
              <div className={styles.WrapperCheckbox}>
                <Switch
                  checked={isDepositMinimum}
                  className={styles.checkbox}
                  labelPosition="left"
                  label={
                    <div>
                      <h4>Notifikasi Deposit kurang dari batas minimal</h4>
                      <p>Beritahu jika deposit kurang dari batas minimal deposit yang telah ditentukan</p>
                    </div>
                  }
                  aria-label="Notifikasi Deposit kurang dari batas minimal"
                  onChange={(e) => setIsDepositMinimum(Boolean(e.currentTarget.checked))}
                  size="md"
                />
              </div>
              <Space h="xs" />
              <Divider my="sm" variant="dashed" />
              <Space h="xs" />
              <IDRInput
                label="Batas minimal deposit"
                placeholder="Minimal 10.000"
                value={amount}
                allowNegative={false}
                allowLeadingZero={false}
                onChange={(v) => setAmount(v.toString())}
              />
            </div>
            <div className={styles.input}>
              <div className={styles.WrapperCheckbox}>
                <Switch
                  checked={isRecipientTrxBulking}
                  className={styles.checkbox}
                  labelPosition="left"
                  label={
                    <div>
                      <h4>Notifikasi Penerima Dana Transaksi Massal <Badge variant="filled" size='xs' className={styles.badge}>Baru!</Badge></h4>
                      <p>Email akan dikirimkan secara otomatis kepada penerima dana setelah dana diterima. {" "}
                        <button
                          type="button"
                          onClick={() => setShowExample(true)}
                          className="font-500 text-primary"
                        >
                          Lihat Contoh Bukti Transaksi
                        </button>
                      </p>
                    </div>
                  }
                  onChange={(e) => setIsRecipientTrxBulking(Number(e.currentTarget.checked))}
                  size="md"
                />
              </div>
            </div>
          </div>
          <div className={styles.btnWrapper}>
            <MainButton
              type="submit"
              loading={loading}
              disabled={isValuesChanging && !isSubmit}
            >
              Simpan
            </MainButton>
          </div>
        </form>
      </div>
      <Modal
        in={showExample}
        onClose={() => setShowExample(false)}
      >
        <ModalWrapper width="50%">
          <ModalHead
            title="Contoh Bukti Transaksi"
            onClose={() => setShowExample(false)}
          />
          <ModalBody>
            <div className={styles.modal}>
              <div className={styles.example}>
                <img
                  src="/assets/example/reciept.png"
                  alt=""
                />
              </div>
            </div>
          </ModalBody>
        </ModalWrapper>
      </Modal>
    </>
  )
}

export default Notification
