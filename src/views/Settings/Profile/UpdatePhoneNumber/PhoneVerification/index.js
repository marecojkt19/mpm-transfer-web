import { useCallback, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import settingService from "../../../../../axios/services/settingService";
import Button from "../../../../../components/Button";
import MainButton from "../../../../../components/Form/MainButton";
import OTPInput from "../../../../../components/Form/OTPInput";
import timeConvert from "../../../../../utils/helpers/timeConverter";
import styles from './phoneVerification.module.scss'

const PhoneVerification = ({
  setPage,
  setStep,
  companyDetail
}) => {
  const [value, setValue] = useState('')
  const [error, setError] = useState('')
  const [loading, setLoading] = useState(false);
  const { company } = useSelector(state => state.auth);

  const [resendTimer, setResendTimer] = useState(120)
  const [resend, setResend] = useState(true)

  const validateOtp = useCallback(() => {
    setLoading(true)
    settingService.validateOtp({
      type: "OLD_PHONE_NUMBER",
      otp: value
    }, company?._id)
      .then(({ data }) => {
        if (data) {
          setStep(1)
        }
        else setError('Kode OTP tidak sesuai')
      })
      .catch(err => setError(err.response.data.message))
      .finally(() => setLoading(false))
  }, [value, setStep, company])

  const resendOTP = () => {
    settingService.requestOtp(
      {
        type: "OLD_PHONE_NUMBER"
      },
      company?._id
    ).then(() => {
      setResend(true);
      setResendTimer(120);
    })
      .catch(({ response: { data: { message } } }) => setError(message))
  }

  useEffect(() => {
    const tryAgain = setInterval(() => {
      if (resendTimer) setResendTimer(prev => prev - 1)
      else {
        clearInterval(tryAgain)
        setResend(false)
      }
    }, 1000);
    return () => {
      clearInterval(tryAgain)
    }
  }, [resendTimer, setResendTimer, setResend])

  return (
    <>
      <div className={styles.phoneVerification}>
        <div className={styles.card}>
          <div className={styles.wrapper}>
            <div className={styles.headingText}>
              <h5>Kode Verifikasi Nomor Handphone telah dikirim!</h5>
              <p>Dipay Disbursement telah mengirimkan kode verifikasi ke:</p>
              <h6>{`+62 ***-****-**${companyDetail?.phoneNumber.substring(companyDetail?.phoneNumber.length - 2)}`}</h6>
            </div>
            <div className={styles.image}>
              <img
                src="/assets/media/others/otp-phone.png"
                alt=""
              />
            </div>
            <div className={styles.note}>
              <span>
                Silakan cek inbox Anda dan masukkan kode verifikasi
              </span>
            </div>
            <div className={styles.input}>
              <OTPInput
                length={6}
                numberOnly
                autoFocus
                value={value}
                onChange={v => {
                  if (error) setError('')
                  setValue(v)
                }}
                error={error}
              />
            </div>
            <div className={styles.buttonWrapper}>
              <MainButton
                disabled={!(value.length === 6)}
                loading={loading}
                onClick={validateOtp}
              >
                Selanjutnya
              </MainButton>
            </div>
            <div className="mb-8">
              {resendTimer ?
                <div className={styles.resend}>
                  <span>Kirim ulang dalam <b>{resend && ` (${timeConvert(resendTimer)})`}</b></span>
                </div>
                :
                <div className={styles.timer}>
                  <span>Belum menerima kode verifikasi?</span>
                  <Button
                    onClick={resendOTP}
                  >
                    Kirim Ulang
                  </Button>
                </div>
              }
            </div>
            <div className={styles.timer}>
              <span>Batalkan perubahan nomor handphone?</span>
              <Button
                onClick={() => setPage(1)}
              >
                Kembali
              </Button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default PhoneVerification;
