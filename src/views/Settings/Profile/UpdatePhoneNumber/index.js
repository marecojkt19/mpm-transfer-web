import { useState } from "react";
import Steps, { Step } from "../../../../components/Steps";
import InputPhone from "./InputPhone";
import PhoneVerification from "./PhoneVerification";
import UpdateSuccess from "./UpdateSuccess";

const UpdatePhoneNumber = ({ setPage, companyDetail }) => {
  const [step, setStep] = useState(0);
  const [data, setData] = useState();

  return (
    <Steps
      activeStep={step}
      className="my-24"
    >
      <Step label="Verifikasi">
        <PhoneVerification companyDetail={companyDetail} setStep={setStep} setPage={setPage} />
      </Step>
      <Step label="Ubah Nomor">
        <InputPhone setData={setData} data={data} setStep={setStep} setPage={setPage} />
      </Step>
      <Step label="Selesai">
        <UpdateSuccess data={data} setPage={setPage} />
      </Step>
    </Steps>
  )
}

export default UpdatePhoneNumber;
