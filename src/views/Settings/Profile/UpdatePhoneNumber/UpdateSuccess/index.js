import React from 'react'
import MainButton from '../../../../../components/Form/MainButton'
import styles from './success.module.scss'

const UpdateSuccess = ({
  setPage,
  data
}) => {
  return (
    <div className={styles.success}>
      <div className={styles.card}>
        <div className={styles.wrapper}>
          <div className={styles.headingText}>
            <h5>Nomor Handphone Perusahaan Anda Berhasil Diubah!</h5>
            <p>Nomor Handphone Perusahaan Anda diubah ke {`+62 ***-****-**${data?.phoneNumber.substring(data?.phoneNumber.length - 2)}`}</p>
          </div>
          <div className={styles.banner}>
            <img src="/assets/media/others/verified.png" alt="" />
          </div>
          <div className={styles.button}>
            <MainButton
              onClick={() => setPage(1)}
            >
              Kembali
            </MainButton>
          </div>
        </div>
      </div>
    </div>
  )
}

export default UpdateSuccess
