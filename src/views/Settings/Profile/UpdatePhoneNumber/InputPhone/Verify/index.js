import { useCallback, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import settingService from "../../../../../../axios/services/settingService";
import Button from "../../../../../../components/Button";
import MainButton from "../../../../../../components/Form/MainButton";
import OTPInput from "../../../../../../components/Form/OTPInput";
import Modal from "../../../../../../components/ModalNew";
import timeConvert from "../../../../../../utils/helpers/timeConverter";
import styles from './verify.module.scss'

const Verify = ({
  data,
  setSubStep,
  setStep
}) => {
  const [value, setValue] = useState('')
  const [error, setError] = useState('')
  const [loading, setLoading] = useState(false);
  const { company } = useSelector(state => state.auth);
  const [showConfirm, setShowConfirm] = useState(false);

  const [resendTimer, setResendTimer] = useState(120)
  const [resend, setResend] = useState(true)

  const validateOtp = useCallback(() => {
    setLoading(true)
    settingService.validateOtp({
      type: "NEW_PHONE_NUMBER",
      phoneNumber: data?.phoneNumber,
      otp: value
    }, company?._id)
      .then(({ data }) => {
        if (data) {
          setStep(2)
        }
        else setError('Kode OTP tidak sesuai')
      })
      .catch(err => setError(err.response.data.message))
      .finally(() => setLoading(false))
  }, [value, data, company, setStep])

  const resendOTP = () => {
    settingService.requestOtp({
      type: "NEW_PHONE_NUMBER",
      phoneNumber: data?.phoneNumber
    }, company?._id)
      .then(() => {
        setResend(true);
        setResendTimer(120);
      })
      .catch(({ response: { data: { message } } }) => setError(message))
  }

  useEffect(() => {
    const tryAgain = setInterval(() => {
      if (resendTimer) setResendTimer(prev => prev - 1)
      else {
        clearInterval(tryAgain)
        setResend(false)
      }
    }, 1000);
    return () => {
      clearInterval(tryAgain)
    }
  }, [resendTimer, setResendTimer, setResend])

  return (
    <>
      <div className={styles.phoneVerification}>
        <div className={styles.card}>
          <div className={styles.wrapper}>
            <div className={styles.headingText}>
              <h5>Verifikasi Nomor HP</h5>
              <p>Verifikasi nomor HP untuk meningkatkan keamanan transaksi Anda</p>
            </div>
            <div className={styles.image}>
              <img
                src="/assets/media/others/otp-phone.png"
                alt=""
              />
            </div>
            <div className={styles.note}>
              <span>
                Kode verifikasi telah dikirim via SMS ke {data.phoneNumber} masukkan 6 digit kode verifikasi yang Anda terima
              </span>
            </div>
            <div className={styles.input}>
              <OTPInput
                length={6}
                numberOnly
                autoFocus
                value={value}
                onChange={v => {
                  if (error) setError('')
                  setValue(v)
                }}
                error={error}
              />
            </div>
            <div className={styles.buttonWrapper}>
              <MainButton
                disabled={!(value.length === 6)}
                loading={loading}
                onClick={validateOtp}
              >
                Selanjutnya
              </MainButton>
            </div>
            <div className="mb-8">
              {resendTimer ?
                <div className={styles.resend}>
                  <span>Kirim ulang dalam <b>{resend && ` (${timeConvert(resendTimer)})`}</b></span>
                </div>
                :
                <div className={styles.timer}>
                  <span>Belum menerima kode verifikasi?</span>
                  <Button
                    onClick={resendOTP}
                  >
                    Kirim Ulang
                  </Button>
                </div>
              }
            </div>
            <div className={styles.timer}>
              <span>Bukan nomor HP Anda?</span>
              <Button
                onClick={() => setShowConfirm(true)}
              >
                Ubah Nomor
              </Button>
            </div>
          </div>
        </div>
      </div>
      <Modal in={showConfirm} onClose={() => setShowConfirm(false)}>
        <div className={styles.modalWrapper}>
          <div className={styles.modalCard}>
            <div className={styles.heading}>
              <h1>Ubah Nomor HP</h1>
            </div>
            <div className={styles.modalLine}></div>
            <div className={styles.infoWrapper}>
              <p>Apakah Anda yakin ingin mengubah nomor HP Anda?</p>
            </div>
            <div className={styles.buttons}>
              <MainButton
                variant="secondary"
                onClick={() => setShowConfirm(false)}
              >
                Batalkan
              </MainButton>
              <MainButton
                onClick={() => setSubStep(1)}
              >
                Ya, Lanjutkan
              </MainButton>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default Verify;
