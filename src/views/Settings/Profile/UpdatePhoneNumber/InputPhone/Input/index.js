import MainButton from '../../../../../../components/Form/MainButton';
import styles from './input.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useEffect, useState } from 'react';
import PhoneInput from '../../../../../../components/Form/PhoneInput';
import Button from '../../../../../../components/Button';
import settingService from '../../../../../../axios/services/settingService';
import { useSelector } from 'react-redux';

const schema = yup.object().shape({
  phoneNumber: yup.string().required('No. Handphone tidak boleh kosong')
})

const Input = ({ setSubStep, setData, setPage }) => {
  const [loading, setLoading] = useState(false);
  const [errorResponse, setErrorResponse] = useState('');
  const [error, setError] = useState('');
  const { company } = useSelector(state => state.auth)
  const {
    register,
    handleSubmit,
    errors,
    watch,
    formState: { isValid },
    unregister,
    setValue
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema)
  });
  const { phoneNumber } = watch(["phoneNumber"])

  const onSubmit = () => {
    setLoading(true)
    settingService.checkPhoneNumber({ phoneNumber: `+62${phoneNumber}` }, company?._id)
      .then(({ data: { isExist, message } }) => {
        if (!isExist) {
          settingService.requestOtp({
            type: "NEW_PHONE_NUMBER",
            phoneNumber: `+62${phoneNumber}`
          }, company?._id)
            .then(() => {
              setData({ phoneNumber: `+62${phoneNumber}` });
              setSubStep(2)
            })
            .catch((err) => {
              if (err.response) {
                if (err.response.body) {
                  setErrorResponse(err.response.body.message);
                }

                if (typeof err.response.data.message === "string") {
                  setErrorResponse(err.response.data.message);
                } else {
                  for (const fields of err.response.data.message) {
                    for (const message of Object.keys(fields.constraints)) {
                      setErrorResponse(fields.constraints[message])
                    }
                  }
                }
              } else {
                setErrorResponse(err);
              }
            })
        }
        else setError(message)
      })
      .catch((err) => {
        if (err.response) {
          if (err.response.body) {
            setErrorResponse(err.response.body.message);
          }

          if (typeof err.response.data.message === "string") {
            setErrorResponse(err.response.data.message);
          } else {
            for (const fields of err.response.data.message) {
              for (const message of Object.keys(fields.constraints)) {
                setErrorResponse(fields.constraints[message])
              }
            }
          }
        } else {
          setErrorResponse(err);
        }
      })
      .finally(() => setLoading(false))
  }

  useEffect(() => {
    register('phoneNumber')
    return () => {
      unregister('phoneNumber')
    }
  }, [register, unregister])

  return (
    <div className={styles.inputPhone}>
      <div className={styles.card}>
        <div className={styles.wrapper}>
          <div className={styles.headingText}>
            <h5>Masukkan Nomor Handphone</h5>
            <p>Pastikan Nomor Handphone Anda aktif dan belum pernah digunakan di Dipay Disbursement sebelumnya</p>
          </div>
          <div className={styles.image}>
            <img
              src="/assets/media/others/otp-phone.png"
              alt=""
            />
          </div>
          <form onSubmit={handleSubmit(onSubmit)}>
            <PhoneInput
              name="phoneNumber"
              label="Nomor HP yang aktif"
              placeholder="Contoh : 8119956067"
              error={errors.phoneNumber || Boolean(errorResponse) || Boolean(error)}
              helperText={errors.phoneNumber?.message || errorResponse || error}
              className="mb-16"
              value={phoneNumber}
              allowLeadingZero={false}
              allowDot={false}
              maxLength={13}
              onChange={(v) => {
                setError('')
                setValue("phoneNumber", v, { shouldValidate: true })
              }}
            />
            <MainButton
              type="submit"
              disabled={!isValid}
              loading={loading}
            >
              Selanjutnya
            </MainButton>
          </form>
        </div>
        <div className={styles.timer}>
          <span>Batalkan perubahan nomor handphone?</span>
          <Button
            onClick={() => setPage(1)}
          >
            Kembali
          </Button>
        </div>
      </div>
    </div>
  )
}

export default Input;
