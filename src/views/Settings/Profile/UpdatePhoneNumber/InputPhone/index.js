import React, { useMemo, useState } from 'react'
import Input from './Input';
import Verify from './Verify';

const PhoneNumber = ({ setPage, setStep, setData, data, companyDetail }) => {
  const [subStep, setSubStep] = useState(1);

  const view = useMemo(() => {
    if (subStep === 1) return <Input companyDetail={companyDetail} setPage={setPage} setSubStep={setSubStep} setData={setData} />
    else if (subStep === 2) return <Verify setStep={setStep} setSubStep={setSubStep} data={data} />
    return null
  }, [subStep, data, setPage, setStep, setData, companyDetail])

  return view
}

export default PhoneNumber
