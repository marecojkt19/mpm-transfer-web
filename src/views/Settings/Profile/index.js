import React, { useMemo, useState } from 'react'
import UpdateProfile from './UpdateProfile';
import UpdatePhoneNumber from './UpdatePhoneNumber';
import { useSelector } from 'react-redux';

const Profile = ({ data }) => {
  const [page, setPage] = useState(1);
  const { company } = useSelector(state => state.auth);

  const renderView = useMemo(() => {
    if (page === 1) {
      return (
        <UpdateProfile setPage={setPage} data={{ ...data, companyId: company._id }} />
      )
    }
    if (page === 2) {
      return (
        <UpdatePhoneNumber companyDetail={{ ...data, companyId: company._id }} setPage={setPage} />
      )
    }
  }, [page, data, company])

  return renderView
}

export default Profile
