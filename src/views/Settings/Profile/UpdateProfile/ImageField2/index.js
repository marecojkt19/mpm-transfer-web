import React, { useMemo, useState } from 'react'
import styles from './uploadLogo.module.scss'
import pakaiClass from 'pakai-class';
import { toAbsoluteUrl } from '../../../../../utils/helpers/pathHelper';
import { IMAGE_FORMATS } from '../../../../../utils/helpers/fileFormats';
import { useDropzone } from 'react-dropzone';

const spesificFormatValidator = (file, format) => {
  const supportedSpecificFormat = format.map(v => v.split('/')[1]);
  const isFormatSpecific = supportedSpecificFormat.includes(file?.name.split('.')[file?.name.split('.').length - 1].toLowerCase());

  if (!isFormatSpecific) {
    return {
      code: "file-invalid-type",
      message: "File gagal diunggah"
    };
  }

  return null;
};

const defaultImage = toAbsoluteUrl('/assets/media/imageField/company.png')

const ImageField = ({
  onChange,
  format = [...IMAGE_FORMATS],
  className,
  disabled,
  onFocus,
  maxSize,
  minSize,
  previousImageUrl
}) => {
  const [files, setFiles] = useState();

  const {
    getRootProps,
    getInputProps,
    fileRejections,
    open
  } = useDropzone({
    accept: Object.assign(...format.map(v => { return { [v]: [] } })),
    maxSize,
    minSize,
    disabled: disabled,
    validator: (file) => spesificFormatValidator(file, format),
    onFileDialogOpen: (e) => {
      if (typeof onFocus === 'function') onFocus();
    },
    onDrop: e => {
      const file = e[0];
      setFiles(Object.assign(URL.createObjectURL(file)));

      if (!file) return
      if (typeof onChange === 'function') onChange(file);
    }
  });

  const uploadState = useMemo(() => {
    if (fileRejections[0]?.errors[0]?.code === "file-invalid-type") {
      return "File Logo harus dalam berbetuk JPG, JPEG, atau PNG"
    } else if (fileRejections[0]?.errors[0]?.code === "file-too-large") {
      return "Ukuran Logo terlalu besar dan harus kurang dari 512KB"
    }

    return "Ukuran logo harus lebih besar dari 128x128 dan kurang dari 512KB. Direkomendasikan logo 1:1 (persegi)."
  }, [fileRejections])

  const selectedImage = useMemo(() => {
    if (files) return files
    if (previousImageUrl) return previousImageUrl
    return defaultImage;
  }, [files, previousImageUrl])

  return (
    <div className={styles.imageField}>
      <div className={styles.label}>
        <h5>Logo Perusahaan</h5>
      </div>
      <div className={styles.imageWrapper}>
        <div
          {...getRootProps({ className: pakaiClass(styles.uploadLogo, className) })}
        >
          <img
            src={selectedImage}
            alt={files?.name}
          />
          <input {...getInputProps()} />
        </div>
      </div>
      <div className={styles.button}>
        <button type="button" onClick={open}>
          <span>Unggah Logo</span>
        </button>
      </div>
      <div
        className={pakaiClass(
          styles.helperText,
          (fileRejections?.length) && styles.error
        )}>
        <span>{uploadState}</span>
      </div>
    </div>
  )
}

export default ImageField
