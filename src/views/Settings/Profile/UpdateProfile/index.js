import { yupResolver } from '@hookform/resolvers/yup'
import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import * as yup from 'yup';
import { Dangerous, ShieldChecked } from '../../../../assets/icons';
import TextField from '../../../../components/Form/TextField';
import ImageField from './ImageField2'
import styles from './profile.module.scss'
import MainButton from '../../../../components/Form/MainButton';
import { useDispatch } from 'react-redux';
import pakaiClass from 'pakai-class';
import settingService from '../../../../axios/services/settingService';
import { authActions } from '../../../../redux/actions/authActions';
import { useToasts } from 'react-toast-notifications';
import PhoneInput from '../../../../components/Form/PhoneInput';
import ConfirmAlert from '../../../../components/templates/ConfirmAlert';
import Modal from '../../../../components/ModalNew';

const schema = yup.object().shape({
  logo: yup.mixed(),
  name: yup.string().required('Nama perusahaan tidak boleh kosong'),
  email: yup.string().email('Format email salah!'),
  emailArr: yup.array().of(yup.string().email())
})
const UpdateProfile = ({
  setPage,
  data
}) => {
  // const [emailArr, setEmailArr] = useState([]);
  const [error, setError] = useState("");
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isSendingOtp, setIsSendingOtp] = useState(false);
  const dispatch = useDispatch();
  const { addToast } = useToasts();

  const {
    register,
    unregister,
    handleSubmit,
    errors,
    setValue,
    watch,
    formState: { isDirty, isValid }
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      logo: data?.logo?.url,
      name: data?.name,
      emailArr: data?.email ?? []
    }
  });

  const {
    logo,
    email,
    emailArr
  } = watch([
    "logo",
    "email",
    "emailArr"
  ])

  const onSubmit = (values) => {
    const preparedData = {
      companyName: values?.name,
      emailCompany: emailArr.join(","),
      files: logo
    }
    setLoading(true)
    settingService.updateProfile({ id: data?.companyId, data: preparedData })
      .then(() => {
        dispatch(authActions.requestUser());
        addToast("Berhasil Tersimpan", { appearance: 'success' })
      })
      .catch((err) => {
        if (err.response) {
          if (err.response.body) {
            addToast(err.response.body.message, { appearance: 'danger' });
          }
          if (err.response.data) {
            if (typeof err.response.data.message === "string") {
              addToast(err.response.data.message, { appearance: 'danger' });
            } else {
              for (const fields of err.response.data.message) {
                for (const message of Object.keys(fields.constraints)) {
                  addToast(fields.constraints[message], { appearance: 'danger' });
                }
              }
            }
          }
        } else {
          addToast(err, { appearance: 'danger' });
        }
      })
      .finally(() => setLoading(false))
  }

  const addEmail = () => {
    if (email && !emailArr.includes(email.toLowerCase())) {
      setValue("emailArr", [...emailArr, email.toLowerCase()], { shouldDirty: true });
      setValue("email", "", { shouldValidate: true })
      setError("")
    } else {
      setError("Email telah ditambahkan!");
      setValue("email", "", { shouldValidate: true })
    }
  }

  const removeEmail = (r) => {
    setValue("emailArr", emailArr.filter(v => v !== r), { shouldDirty: true });
  }

  const onSendOtp = () => {
    setIsSendingOtp(true)
    settingService.requestOtp(
      {
        type: "OLD_PHONE_NUMBER"
      },
      data?.companyId
    )
      .then(() => {
        setPage(2)
      })
      .catch(() => { })
      .finally(() => setIsSendingOtp(false))
  }

  useEffect(() => {
    register("email");
    register("logo");
    register("logoFile");
    register("emailArr");

    return () => {
      unregister("email");
      unregister("logo");
      unregister("logoFile");
      unregister("emailArr");
    }
  }, [register, unregister]);

  return (
    <>
      <div className={styles.profile}>
        <div className={styles.wrapper}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className={styles.profilePic}>
              <ImageField
                name="logo"
                maxSize={512000}
                previousImageUrl={logo}
                onChange={(v) => setValue("logo", v, { shouldValidate: true, shouldDirty: true })}
              />
            </div>
            <div className={styles.line}></div>
            <div className={styles.formWrapper}>
              <TextField
                ref={register}
                name="name"
                label="Nama Perusahaan"
                placeholder="Masukkan nama perusahaan"
                error={errors.name}
                helperText={errors.name?.message}
              />
              <div className={styles.additionalEmail}>
                <div className={styles.formGroup}>
                  <div className={styles.input}>
                    <TextField
                      name="email"
                      label="Email Kontak Perusahaan"
                      placeholder="Masukan email yang valid"
                      error={errors?.email ?? error}
                      helperText={errors?.email?.message ?? error}
                      value={email}
                      onChange={(e) => setValue("email", e?.target?.value, { shouldValidate: true })}
                    />
                  </div>
                  <div className={pakaiClass(styles.button, (error || errors?.email?.message) && styles.error)}>
                    <button disabled={errors?.email} type="button" onClick={addEmail}>
                      <span>Tambah ke daftar</span>
                    </button>
                  </div>
                </div>
                <div className={styles.itemList}>
                  {emailArr?.map((v, i) => {
                    return (
                      <div className={styles.item} key={i}>
                        <h6>{v}</h6>
                        <button onClick={() => removeEmail(v)}>
                          <Dangerous color="#545454" />
                        </button>
                      </div>
                    )
                  })}
                </div>
                <div className={styles.note}>
                  <p>Nama perusahaan dan email kontak perusahaan adalah informasi identitas yang akan Dipay Disbursement berikan sebagai notifikasi ketika melakukan transaksi, perubahan dan informasi terkait.</p>
                </div>
              </div>
              <div className={styles.button}>
                <MainButton
                  type="submit"
                  loading={loading}
                  disabled={!(isValid && isDirty)}
                >
                  Simpan
                </MainButton>
              </div>
            </div>
          </form>
          <div className={styles.line}></div>
          <div className={styles.phoneNumber}>
            <div className={styles.input}>
              <PhoneInput
                label="Nomor Handphone perusahaan"
                placeholder={`***-****-**${data?.phoneNumber?.substring(data?.phoneNumber.length - 2)}`}
                disabled
              />
            </div>
            <div className={styles.row}>
              <div className={styles.verifiedAlert}>
                <ShieldChecked />
                <span>Terverifikasi</span>
              </div>
              <div className={styles.changeBtn}>
                <button onClick={() => setShowDeleteModal(true)}>Ganti Nomor</button>
              </div>
            </div>

          </div>
          <div className={styles.note}>
            <p>Nomor HP Perusahaan akan digunakan untuk mengirimkan kode OTP setiap ada transaksi yang menggunakan deposit Dipay Disbursement Anda.</p>
          </div>
        </div>
      </div>
      <Modal in={showDeleteModal} onClose={() => setShowDeleteModal(false)}>
        <ConfirmAlert
          onClose={() => setShowDeleteModal(false)}
          onSubmit={onSendOtp}
          title="Apakah Anda yakin ingin mengganti nomor 
          handphone perusahaan?"
          description={`Jika Anda ingin mengganti nomor HP Perusahaan, kami akan mengirimkan kode verifikasi ke nomor : +62 ***-****-**${data?.phoneNumber?.substring(data?.phoneNumber.length - 2)}`}
          customNextTitle="Kirim Kode Verifikasi"
          loading={isSendingOtp}
        />
      </Modal>
    </>
  )
}

export default UpdateProfile
