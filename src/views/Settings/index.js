import React, { useState, useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import settingService from '../../axios/services/settingService';
import authService from '../../axios/services/authService';
import { Tab, Tabs } from '../../components/Tabs/Tabs'
import { settingsActions } from '../../redux/actions/settingsAction';
// import __Notification from './__Notification';
import Profile from './Profile';
import styles from './settings.module.scss'
import LoadingDots from '../../components/Loadings/LoadingDots';
// import Kredensial from './Kredensial';
import Notification from './Notification';

const Settings = () => {
  const dispatch = useDispatch();
  const { activeTabKey } = useSelector(state => state.settings);
  const { company } = useSelector(state => state.auth);
  const [items, setItems] = useState([])
  const [companyItems, setCompanyItems] = useState()

  const handleTab = key => {
    dispatch(settingsActions.changeTab(key))
    dispatch(settingsActions.setStep(0))
  }

  const load = useCallback(() => {
    settingService
      .detailProfile({ id: company._id })
      .then(({ data }) => {
        setItems(data)
      })
      .catch(() => { })
    authService.me()
      .then(({ data }) => {
        setCompanyItems(data.company)
      })
      .catch(() => { })
  }, [company._id])

  useEffect(() => {
    if (activeTabKey || company?._id) {
      load()
    }
  }, [load, activeTabKey, company?._id])

  return (
    <div className={styles.settings}>
      <div className={styles.heading}>
        <h1>Pengaturan</h1>
      </div>
      <Tabs
        activeKey={activeTabKey}
        onClick={handleTab}
      >
        <Tab title="Profil">
          {items?.name ?
            <Profile data={items} />
            :
            <LoadingDots className={styles.loading} />
          }
        </Tab>
        <Tab title="Notifikasi">
          {items?.name ?
            <Notification data={items} companyItems={companyItems} />
            :
            <LoadingDots className={styles.loading} />
          }
        </Tab>
        {/* <Tab title="Kredensial">
          <Kredensial id={company._id} />
        </Tab> */}
      </Tabs >
    </div >
  )
}

export default Settings
