import React from 'react'
import styles from './error.module.scss'

const InternetServerError = () => {
  return (
    <div className={styles.error}>
      <div className={styles.wrapper}>
        <div className={styles.image}>
          <img src="/assets/no-internet.png" alt="" />
        </div>
        <div className={styles.message}>
          <h1>500 Internal Server Error</h1>
          <p>Maaf halaman ini sedang dalam perbaikan, mohon dicoba beberapa saat lagi ya :)</p>
        </div>
      </div>
    </div>
  )
}

export default InternetServerError
