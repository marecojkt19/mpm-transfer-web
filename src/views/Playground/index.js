import React from 'react'
import PhoneInput from '../../components/Form/PhoneInput'
import SimpleCard from '../../components/templates/SimpleCard'

const Playground = () => {
  return (
    <SimpleCard>
      <PhoneInput
        label="Nomor HP yang aktif"
        placeholder="Contoh : 8119956067"
        className="mb-16"
      />
    </SimpleCard>
  )
}

export default Playground
