import React from 'react'
import styles from './inputMethod.module.scss'
import UploadExcelIcon from '../../../assets/icons/create/upload-excel'
import KeyboardExternalIcon from '../../../assets/icons/create/keyboard-external'
import { createTransactionAction } from '../../../redux/actions/createTransactionAction'
import { useDispatch } from 'react-redux'

const InputMethod = () => {
  const dispatch = useDispatch();
  return (
    <div className={styles.inputMethod}>
      <button
        className={styles.methodCard}
        onClick={() => {
          dispatch(createTransactionAction.setStep(1))
          dispatch(createTransactionAction.setMethod("BULKING"))
        }}
      >
        <div className={styles.icon}>
          <UploadExcelIcon />
        </div>
        <h5>Transaksi Unggah Excel</h5>
        <p>
          Lakukan transaksi manual dengan mengunggah file Excel yang dapat berisi hingga 10.000 penerima dalam satu transaksi.
        </p>
      </button>
      <button
        className={styles.methodCard}
        onClick={() => {
          dispatch(createTransactionAction.setStep(1))
          dispatch(createTransactionAction.setMethod("SINGLE"))
        }}
      >
        <div className={styles.icon}>
          <KeyboardExternalIcon />
        </div>
        <h5>Transaksi Single</h5>
        <p>
          Lakukan transaksi instan kepada satu penerima secara efisien untuk memastikan pengiriman dana dengan cepat dan aman.
        </p>
      </button>
    </div>
  )
}

export default InputMethod
