import { useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import createTransactionService from "../../axios/services/createTransactionService";
import Modal from "../../components/ModalNew";
import Table from "../../components/Table";
import MainButton from "../../components/Form/MainButton";
import SimpleCard from "../../components/templates/SimpleCard";
import { createTransactionAction } from "../../redux/actions/createTransactionAction";
import toIDR from "../../utils/helpers/toIDR";
import styles from './createTransaction.module.scss';

const Create = ({ items }) => {
  const { total, totalTransfer, totalTransferFee, sender } = items;
  const columns = [
    { title: 'Bank Penerima', key: 'destinationBank', render: v => v.replace("_", " ") || '-', },
    { title: 'Biaya Admin', key: 'bankFee', render: v => `Rp${toIDR(v, false)}` || '-', },
    { title: 'Jumlah Transaksi', key: 'transactionQty', render: v => v || '-', },
    { title: 'Subtotal', key: 'subtotal', align: 'right', render: v => `Rp${toIDR(v, false)}` || '-', }
  ];

  const totalTransactionQty = useMemo(() => {
    return items?.detailTransfer.reduce((accumulator, currentValue) => {
      return accumulator + currentValue.transactionQty;
    }, 0)
  }, [items]);

  return (
    <SimpleCard className={styles.createWrapper}>
      <div className={styles.label}>
        <span>Rincian Transaksi</span>
      </div>
      <div className={styles.detailTrx}>
        <div className={styles.content}>
          <div>
            <span>Nama Pengirim</span>
            <span>:</span>
            <span>{sender}</span>
          </div>
          <div>
            <span>Jumlah Penerima</span>
            <span>:</span>
            <span>{totalTransactionQty} Penerima</span>
          </div>
          <div>
            <span>Nominal Transfer</span>
            <span>:</span>
            <span>Rp{toIDR(totalTransfer, false)}</span>
          </div>
        </div>
      </div>
      <div className={styles.label}>
        <span>Detail Biaya Admin</span>
      </div>
      <Table
        data={items?.detailTransfer}
        config={{
          columns: columns,
          pagination: false,
        }}
      />
      <div className={styles.footerCreate}>
        <div className={styles.content}>
          <div>
            <span>Total Biaya Admin:</span>
            <span>Rp{toIDR(totalTransferFee, false)}</span>
          </div>
          <div>
            <span>Nominal Transfer: </span>
            <span>Rp{toIDR(totalTransfer, false)}</span>
          </div>
          <div className={styles.lineDash} />
          <div>
            <span>Total Transfer & Admin:</span>
            <span className={styles.primary}>Rp{toIDR(total, false)}</span>
          </div>
        </div>
        <div className={styles.btnAction}>
          <BackButton />
          <ConfirmButton />
        </div>
      </div>
    </SimpleCard>
  )
}

const BackButton = () => {
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      <MainButton
        variant="primary-light"
        onClick={() => setIsOpen(true)}
      >
        Kembali
      </MainButton>
      <Modal
        in={isOpen}
        onClose={() => setIsOpen(false)}
      >
        <div className={styles.confimationCard}>
          <div className={styles.header}>
            <h4>Kembali ke input data</h4>
          </div>
          <div className={styles.line}></div>
          <div className={styles.content}>
            <h4 className="mb-8">Apakah Anda ingin kembali ke halaman sebelumnya?</h4>
            <p>Anda masih dapat merubah inputan data Anda sebelum buat transaksi.</p>
          </div>
          <div className={styles.wrapperBtn}>
            <div className={styles.btn}>
              <MainButton
                onClick={() => dispatch(createTransactionAction.setStep(0))}
                variant="primary-light"
              >
                <span>Ya</span>
              </MainButton>
              <MainButton
                onClick={() => setIsOpen(false)}
                variant="primary"
              >
                <span>Tidak</span>
              </MainButton>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

const ConfirmButton = () => {
  const dispatch = useDispatch();
  const { transactionId } = useSelector(state => state.createTransaction);
  const [loading, setLoading] = useState(false);
  const [isOpen, setIsOpen] = useState(false);

  const onConfirm = () => {
    setLoading(true)
    createTransactionService
      .create(transactionId)
      .then(({ data }) => {
        dispatch(createTransactionAction.setTrxCode(data?.transactionCode))
      })
      .finally(() => {
        dispatch((2))
        setIsOpen(false)
        setLoading(false)
      })
  };

  return (
    <>
      <MainButton onClick={() => setIsOpen(true)}>
        Lanjutkan
      </MainButton>
      <Modal
        in={isOpen}
        onClose={() => setIsOpen(false)}
      >
        <div className={styles.confimationCard}>
          <div className={styles.header}>
            <h4>Konfirmasi Buat Transfer</h4>
          </div>
          <div className={styles.line}></div>
          <div className={styles.content}>
            <h4 className="mb-8">Apakah Anda yakin dengan biaya dan nilai nominal yang tertera?</h4>
            <p>Pastikan biaya dan nominal transfer sesuai, lanjutkan jika sudah sesuai.</p>
          </div>
          <div className={styles.wrapperBtn}>
            <div className={styles.btn}>
              <MainButton
                onClick={() => setIsOpen(false)}
                variant="primary-light"
              >
                <span>Batalkan</span>
              </MainButton>
              <MainButton
                onClick={onConfirm}
                variant="primary"
                loading={loading}
                disabled={loading}
              >
                <span>Konfirmasi</span>
              </MainButton>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default Create;
