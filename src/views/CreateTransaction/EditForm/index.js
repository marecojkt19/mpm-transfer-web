import React, { useCallback, useEffect, useState } from 'react'
import { useForm } from "react-hook-form";
import * as yup from 'yup';
import { yupResolver } from "@hookform/resolvers/yup";
import { emailRegex } from "../../../utils/types/regexTypes";

import styles from './editForm.module.scss';
import SelectField from "../../../components/Form/SelectField";
import bankService from "../../../axios/services/bankService";
import TextField from "../../../components/Form/TextField";
import pakaiClass from "pakai-class";
import LoadingDots from '../../../components/Loadings/LoadingDots';
import { Cancel, ChecklistOn } from '../../../assets/icons';

const formatterRequired = (text) => {
  return <>{text}<span style={{ color: "#E11900" }}>*</span></>
}

const schema = yup.object().shape({
  destinationBank: yup.string().required("Bank tujuan tidak boleh kosong!"),
  description: yup.string().max(15, "Maksimal 15 karakter!"),
  beneficiaryEmail: yup
    .string()
    .matches(emailRegex, "Format email salah"),
  beneficiaryAccountName: yup.string(),
  beneficiaryAccountNo: yup.string().required("Nomor rekening tidak boleh kosong!"),
  amount: yup.string()
    .required('Nominal tidak boleh kosong')
    .when('destinationBank', {
      is: (val) => val === "DIPAY",
      then: yup.string().test(
        'max',
        'Nominal melebihi Rp20.000.000',
        (value) => +value <= 20000000,
      ),
      otherwise: yup.string().test(
        'max',
        'Nominal melebihi Rp500.000.000',
        (value) => +value <= 500000000,
      ),
    })
    .test(
      'min',
      'Nominal kurang dari Rp10.000',
      (value) => +value >= 10000,
    ),
})

const EditForm = ({
  defaultValues,
  onSubmit,
  setIsDirty = () => { },
  module = "ALL"
}) => {
  const [showCheckBankAlert, setShowCheckBankAlert] = useState(false);
  const [isBankExist, setIsBankExist] = useState(false);
  const [bankUsername, setBankUsername] = useState();
  const [isCheckingBankAccount, setIsCheckingBankAccount] = useState(false);
  const [banks, setBanks] = useState([]);
  const [isGettingBanks, setIsGettingBanks] = useState(false);

  const {
    setValue,
    unregister,
    register,
    watch,
    setError,
    handleSubmit,
    formState: { errors, dirtyFields, isDirty }
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      ...defaultValues
    }
  });

  useEffect(() => {
    if (isDirty) {
      setIsDirty(isDirty)
    }
  }, [isDirty, setIsDirty])

  const {
    destinationBank,
    description,
    beneficiaryEmail,
    amount,
    beneficiaryAccountNo,
    beneficiaryAccountName
  } = watch([
    "destinationBank",
    "description",
    "beneficiaryEmail",
    "amount",
    "beneficiaryAccountNo",
    "beneficiaryAccountName"
  ])

  const loadBankList = useCallback(() => {
    setIsGettingBanks(true)
    bankService
      .list({})
      .then(({ data }) => {
        const opts = data.map(bank => ({
          value: bank?.enum,
          label: bank?.name
        }));
        setBanks(opts)
        setIsGettingBanks(false)
      })
  }, []);

  useEffect(() => {
    loadBankList()
  }, [loadBankList])

  useEffect(() => {
    register("description");
    register("destinationBank");
    register("beneficiaryAccountNo");
    register("beneficiaryAccountName");
    register("amount");
    register("beneficiaryEmail");

    return () => {
      unregister("description");
      unregister("destinationBank");
      unregister("beneficiaryAccountNo");
      unregister("beneficiaryAccountName");
      unregister("amount");
      unregister("beneficiaryEmail");
    }
  }, [register, unregister])

  const checkAccountNumber = () => {
    setShowCheckBankAlert(true)
    const payload = {
      accountNumber: beneficiaryAccountNo,
      bank: destinationBank
    }
    setIsCheckingBankAccount(true)
    bankService.checkAccountNumber(payload)
      .then(({ data }) => {
        setBankUsername(data?.name);
        if (data?.name) {
          setIsBankExist(true)
        } else {
          setIsBankExist(false)
        }
      })
      .catch((err) => {
        setIsBankExist(false)
        if (err.response) {
          if (err.response.body) {
            if (destinationBank === "DIPAY") {
              setBankUsername("No. HP tidak valid");
            } else {
              setBankUsername(err.response.body.message);
            }
          }
          if (err.response.data) {
            if (typeof err.response.data.message === "string") {
              if (destinationBank === "DIPAY") {
                setBankUsername("No. HP tidak valid");
              } else {
                setBankUsername(err.response.data.message);
              }
            } else {
              for (const fields of err.response.data.message) {
                for (const message of Object.keys(fields.constraints)) {
                  if (destinationBank === "DIPAY") {
                    setBankUsername("No. HP tidak valid");
                  } else {
                    setBankUsername(fields.constraints[message]);
                  }
                }
              }
            }
          }
        } else {
          setBankUsername(err);
        }
      })
      .finally(() => setIsCheckingBankAccount(false))
  }

  useEffect(() => {
    if (dirtyFields?.beneficiaryAccountNo || dirtyFields?.destinationBank) {
      setIsBankExist(false)
    }
  }, [dirtyFields?.beneficiaryAccountNo, dirtyFields?.destinationBank])

  const submit = (values) => {
    if (isBankExist && (bankUsername ?? beneficiaryAccountName)) {
      onSubmit({ ...values, beneficiaryAccountName: bankUsername ?? beneficiaryAccountName });
    } else {
      setError("beneficiaryAccountNo", { message: "Validasi pengguna rekening bank" })
    }
  }

  useEffect(() => {
    if (beneficiaryAccountName) {
      setIsBankExist(true);
    }
  }, [beneficiaryAccountName])

  return (
    <div className={styles.editForm}>
      <form onSubmit={handleSubmit(submit)} id="transactionForm">
        <div className={styles.formWrapper}>
          <SelectField
            options={banks}
            searchable
            loading={isGettingBanks}
            label={formatterRequired("Bank Tujuan")}
            placeholder="Pilih Bank"
            name="destinationBank"
            disabled={module === "NAME"}
            value={destinationBank}
            error={errors.destinationBank}
            helperText={errors.destinationBank?.message}
            onChange={(v) => {
              setValue("beneficiaryAccountNo", "")
              setValue("destinationBank", v?.value, { shouldValidate: true, shouldDirty: true })
            }}
          />
          <div className={styles.bankAccountNumber}>
            <div className={styles.bankAccountNumberWrapper}>
              <TextField
                name="beneficiaryAccountNo"
                label={formatterRequired(destinationBank === "DIPAY" ? "Masukkan nomor HP anda" : "Nomor Rekening Perusahaan")}
                placeholder={destinationBank === "DIPAY" ? "Nomor HP" : "Nomor rekening"}
                error={errors.beneficiaryAccountNo}
                helperText={errors.beneficiaryAccountNo?.message}
                value={beneficiaryAccountNo}
                format={Number}
                disabled={module === "NAME"}
                allowDot={false}
                className="w-100"
                allowLeadingZero
                onChange={(v) => {
                  if (bankUsername || isBankExist || showCheckBankAlert) {
                    setBankUsername("")
                    setShowCheckBankAlert(false)
                    setIsBankExist(false)
                  }
                  setValue("beneficiaryAccountNo", v, { shouldValidate: true, shouldDirty: true })
                }}
                maxLength={16}
              />
              <div className={pakaiClass(
                styles.checkNameBtn,
                errors.beneficiaryAccountNo && styles.error,
                (!beneficiaryAccountNo || !destinationBank) && styles.disabled
              )}>
                <button
                  type="button"
                  onClick={checkAccountNumber}
                  disabled={!beneficiaryAccountNo || !destinationBank}
                >
                  Validasi Nomor
                </button>
              </div>
            </div>
            {showCheckBankAlert ?
              <div className={pakaiClass(styles.alert, !isCheckingBankAccount ? isBankExist ? styles.primary : styles.error : styles.loading)}>
                {!isCheckingBankAccount
                  ?
                  <span>{bankUsername}</span>
                  :
                  <LoadingDots size={10} />}
                {
                  !isCheckingBankAccount ?
                    !isBankExist ?
                      <Cancel />
                      :
                      <ChecklistOn size={18} />
                    :
                    null
                }
              </div>
              :
              null
            }
          </div>

          <TextField
            name="amount"
            value={amount}
            error={errors?.amount}
            helperText={errors?.amount?.message}
            className={pakaiClass(styles.textInput)}
            helperTextClassName={styles.helperText}
            disabled={module !== "ALL"}
            moneyInput={true}
            maxLength={destinationBank === "DIPAY" ? 8 : 9}
            allowLeadingZero={false}
            label={formatterRequired("Jumlah Nominal (Rp)")}
            placeholder="Rp 10.000"
            onChange={(v) => {
              setValue("amount", v, { shouldValidate: true, shouldDirty: true })
            }}
          />
          <TextField
            name="beneficiaryEmail"
            value={beneficiaryEmail}
            className={pakaiClass(styles.textInput)}
            error={errors?.beneficiaryEmail}
            disabled={module !== "ALL"}
            helperText={errors?.beneficiaryEmail?.message}
            helperTextClassName={styles.helperText}
            label="Email Penerima (Opsional)"
            placeholder="Masukkan Email"
            onChange={(v) => {
              setValue("beneficiaryEmail", v.target.value, { shouldValidate: true, shouldDirty: true })
            }}
          />
          <TextField
            name="description"
            value={description}
            error={errors?.description}
            disabled={module !== "ALL"}
            helperText={errors?.description?.message}
            className={pakaiClass(styles.textInput)}
            helperTextClassName={styles.helperText}
            label="Berita (Opsional)"
            placeholder="Masukkan Berita"
            onChange={(v) => {
              setValue("description", v.target.value, { shouldValidate: true, shouldDirty: true })
            }}
          />
        </div>
      </form>
    </div>
  )
}

export default EditForm
