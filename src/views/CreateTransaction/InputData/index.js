import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import createTransactionService from '../../../axios/services/createTransactionService';
import { createTransactionAction } from '../../../redux/actions/createTransactionAction';
import styles from '../createTransaction.module.scss';
import TransactionTable from './Table';
import Upload from './Upload';

const InputData = () => {
  const {
    transactionId,
    // isLoading,
    listAllConfig: {
      listAllSkip,
      listAllLimit
    },
    listNameConfig: {
      listNameSkip,
      listNameLimit
    },
    listAccountConfig: {
      listAccountSkip,
      listAccountLimit
    }
  } = useSelector(state => state.createTransaction);
  const dispatch = useDispatch();

  const load = useCallback(() => {
    dispatch(createTransactionAction.setLoading(true))
    createTransactionService
      .list({
        id: transactionId,
        limit: listAllLimit,
        page: listAllSkip
      })
      .then(({ data }) => {
        if (data?.transaction?.length) {
          dispatch(createTransactionAction.setList(data))
        } else {
          dispatch(createTransactionAction.reset())
          dispatch(createTransactionAction.setStep(1))
        }
      })
      .catch(() => { })
      .finally(() => {
        dispatch(createTransactionAction.setLoading(false))
      })

    createTransactionService
      .list({
        id: transactionId,
        isNameMatch: "0",
        validateAccount: "1",
        limit: listNameLimit ?? 10,
        page: listNameSkip
      })
      .catch(() => { })
      .then((result) => {
        if (result) {
          dispatch(createTransactionAction.setListName(result?.data))
        }
      })
      .finally(() => {
        dispatch(createTransactionAction.setLoading(false))
      })

    createTransactionService
      .list({
        id: transactionId,
        isNameMatch: "0",
        validateAccount: "0",
        limit: listAccountLimit ?? 10,
        page: listAccountSkip
      })
      .catch(() => { })
      .then(({ data }) => dispatch(createTransactionAction.setListAccount(data)))
      .finally(() => {
        dispatch(createTransactionAction.setLoading(false))
      })
  }, [dispatch, transactionId, listAccountSkip, listAllSkip, listNameSkip, listAllLimit, listAccountLimit, listNameLimit])

  useEffect(() => {
    if (transactionId) {
      load()
    }
  }, [load, transactionId])

  return transactionId ?
    <TransactionTable load={load} />
    :
    (
      <div className={styles.inputDataWrapper}>
        <Upload />
        {/* {!isLoading ?
          <CardIntegrationAPI />
          :
          null
        } */}
      </div>
    )
}

export default InputData;
