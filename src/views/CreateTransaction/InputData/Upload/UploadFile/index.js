import pakaiClass from 'pakai-class';
import { useMemo } from 'react';
import { useDropzone } from 'react-dropzone';
import { XLS_FORMAT } from '../../../../../utils/helpers/fileFormats';
import { DriveFolderUpload, Failed, WarnV2 } from '../../../../../assets/icons';
import MainButton from '../../../../../components/Buttons/MainButton';
import styles from './uploadFile.module.scss'
import { useSelector } from 'react-redux';
import fileUpload from './fileUpload.json'
import Lottie from 'lottie-react';

const UploadFile = ({
  onChange,
  format = [...XLS_FORMAT],
  // format = [...XLS_FORMAT, ...CSV_FORMAT],
  variant,
  className,
  uploading,
  disabled,
  onFocus,
  maxSize,
  minSize,
  percentage,
  onCancel
}) => {
  const {
    acceptedFiles,
    getRootProps,
    getInputProps,
    fileRejections
  } = useDropzone({
    accept: Object.assign(...format.map(v => { return { [v]: [] } })),
    maxSize,
    minSize,
    disabled: disabled || uploading,
    onFileDialogOpen: (e) => {
      if (typeof onFocus === 'function') onFocus();
    },
    onDrop: (e) => {
      const file = e[0];
      if (!file) return
      if (typeof onChange === 'function') onChange(file);
    }
  });

  const { isErrorUpload } = useSelector(state => state.createTransaction);

  const uploadState = useMemo(() => {
    if (!uploading || isErrorUpload) {
      if (isErrorUpload) {
        return (
          <>
            <WarnV2 />
            <div className={styles.info}>
              <span>{acceptedFiles[0]?.name}</span>
            </div>
            <div className={styles.label}>
              <h5>Format dokumen tabel pengiriman dana tidak sesuai</h5>
              <h6>Silakan cek dan unggah ulang dokumen</h6>
              <MainButton
                variant="primary-light"
              >
                Pilih File
              </MainButton>
            </div>
          </>
        )
      } else if (fileRejections[0]?.errors[0]?.code === "file-too-large") {
        return (
          <>
            <Failed />
            <div className={styles.info}>
              <span>{fileRejections[0].file.name}</span>
            </div>
            <div className={styles.label}>
              <h5>Ukuran file lebih dari 10MB</h5>
              <h6>Ukuran maksimal file yang boleh diunggah: 10 MB</h6>
              <MainButton
                variant="primary-light"
              >
                Pilih File
              </MainButton>
            </div>
          </>
        )
      } else if (fileRejections[0]?.errors[0]?.code === "file-invalid-type") {
        return (
          <>
            <Failed />
            <div className={styles.info}>
              <span>{fileRejections[0].file.name}</span>
            </div>
            <div className={styles.label}>
              <h5>Format tidak sesuai, <br /> pastikan unduh template tabel pengiriman dana</h5>
              <h6>Ukuran maksimal file yang boleh diunggah: 10 MB</h6>
              <MainButton
                variant="primary-light"
              >
                Pilih File
              </MainButton>
            </div>
          </>
        )
      } else {
        return (
          <div className={styles.default}>
            <div className={styles.label}>
              <h3>Taruh File Excel di sini</h3>
            </div>
            <DriveFolderUpload />
            <br />
            <div className={styles.label}>
              <span>atau</span>
            </div>
            <div className={styles.label}>
              <button>
                Pilih File
              </button>
            </div>
            <div className={styles.info}>
              <span>Ukuran maksimal yang boleh diunggah: 10MB</span>
            </div>
          </div>
        )
      }
    } else {
      return (
        <div className={styles.loading}>
          <div className={styles.loadingWrapper}>
            <div className={styles.loadingProgress}>
              <Lottie animationData={fileUpload} style={{ height: 300, width: 300 }} />
              <div className={styles.progressWrapper}>
                <div className={styles.progress}>
                  <div className={styles.content} style={{ width: `${Math.floor(percentage)}%` }}></div>
                </div>
              </div>
              <div className={styles.percentage}>
                <h5>{Math.floor(percentage)}%</h5>
              </div>
              <MainButton
                variant='primary-light'
                onClick={onCancel}
              >
                Batalkan Transaksi
              </MainButton>
            </div>
          </div>
        </div>
      )
    }
  }, [uploading, acceptedFiles, fileRejections, isErrorUpload, percentage, onCancel])

  const wrapper = useMemo(() => {
    return (
      <div
        className={
          pakaiClass(
            styles.cardWrapper,
            fileRejections.length && styles.error,
            (uploading) && styles.loading,
            isErrorUpload && styles.warning
          )
        }
      >
        <div className={styles.wrapper}>
          {uploadState}
        </div>
      </div>
    )
  }, [fileRejections.length, uploading, isErrorUpload, uploadState])

  return (
    <div
      {...getRootProps({ className: pakaiClass(styles.uploadFile, styles[variant], className) })}
    >
      <div className={styles.uploadCard} style={{ height: !uploading || isErrorUpload ? "368px" : "552px" }}>
        {wrapper}
        <input {...getInputProps()} />
      </div>
    </div>
  )
}

export default UploadFile
