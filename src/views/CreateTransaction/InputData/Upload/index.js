import React, { useCallback, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { createTransactionAction } from '../../../../redux/actions/createTransactionAction';
import createTransactionService from '../../../../axios/services/createTransactionService';
import SimpleCard from '../../../../components/templates/SimpleCard';
import { Link } from 'react-router-dom';
import Download from '../../../../assets/icons/download';
import Alert from '../../../../components/Alert';
import Modal, { ModalBody, ModalHead, ModalWrapper } from '../../../../components/ModalNew';
import MainButton from '../../../../components/Form/MainButton';
import styles from './upload.module.scss'
import { Info, Warning } from '../../../../assets/icons';
import UploadFile from './UploadFile';

const Upload = () => {
  const dispatch = useDispatch();
  const [showCancelAlert, setShowCancelAlert] = useState(false);
  const [isCanceling, setIsCanceling] = useState(false);
  const [showAllErrors, setShowAllErrors] = useState(false);
  const {
    isLoading,
    estimateTime,
    items,
    errors,
    transactionFile,
    createTransactionProgress: {
      precentageProggress
    }
  } = useSelector(state => state.createTransaction);

  const handleUpload = useCallback((v) => {
    if (v) {
      dispatch(createTransactionAction.setLoading(true))
      dispatch(createTransactionAction.setErrorUpload(false))
      dispatch(createTransactionAction.loadRequest(v))
      dispatch(createTransactionAction.setErrors([]))
    }
  }, [dispatch])

  const onCancel = () => {
    setIsCanceling(true)
    createTransactionService.cancelUpload()
      .then(() => {
        setIsCanceling(false)
        setShowCancelAlert(false)
        dispatch(createTransactionAction.reset())
      })
      .catch(() => {
        setIsCanceling(false)
        setShowCancelAlert(false)
      })
  }

  useEffect(() => {
    if (items) {
      dispatch(createTransactionAction.createTransactionProgress({ precentageProggress: 0 }))
      dispatch(createTransactionAction.setLoading(false))
    }
  }, [items, dispatch])

  return (
    <>
      <SimpleCard className={styles.wrapperFileUpload}>
        {
          !isLoading
            ?
            <div className={styles.heading}>
              <h3>Buat Transaksi via Excel</h3>
            </div>
            : null
        }
        <div className={styles.upload}>
          {!isLoading ?
            <>
              <div className={styles.stepOne}>
                <div className={styles.step}>1</div>
                <div>
                  <p className='mb-16'>Silakan, unduh Template Tabel Pengiriman Dana</p>
                  <Link to="/assets/files/Dipay_Disbursement_Template.xlsx" target="_blank" download>
                    <button className='mb-8'>
                      <Download size="24" />
                      <span>Excel</span>
                    </button>
                  </Link>
                  <p>Data yang diisi <b>tidak boleh melebihi 10.000 transaksi</b></p>
                </div>
              </div>
              <div className={styles.stepTwo}>
                <div className={styles.step}>2</div>
                <div>
                  <p>Unggah file Excel Anda untuk membuat transaksi di bawah ini.</p>
                </div>
              </div>
            </>
            :
            <Alert
              color="primary"
              className="mb-24"
              iconItem={<Info className="mr-16" color="#276EF1" />}
            >
              <span className="font-size-14">Sistem Dipay sedang melakukan pengecekan penghitungan. Proses ini akan membutuhkan waktu kurang lebih {estimateTime}.</span>
            </Alert>
          }
          {errors?.length ?
            <div className={styles.errorAlert}>
              <div className={styles.desc}>
                <Warning />
                <span>
                  Terdapat <b>{errors?.length} kesalahan</b> dalam ada file {transactionFile?.name} yang Anda unggah. Cek lagi, ya.
                </span>
              </div>
              <ul>
                {errors?.slice(0, 5)?.map((v, i) => {
                  return (
                    <li key={i}>
                      <span>{v}</span>
                    </li>
                  )
                })}
              </ul>
              {errors?.length > 5 ?
                <div className={styles.seeMoreBtn}>
                  <button
                    onClick={() => {
                      setShowAllErrors(true)
                    }}
                  >
                    <span>Lihat Semua</span>
                  </button>
                </div>
                :
                null
              }
            </div>
            : null
          }
          <div>
            <UploadFile
              onChange={handleUpload}
              maxSize={2000000}
              uploading={isLoading}
              percentage={precentageProggress}
              onCancel={() => setShowCancelAlert(true)}
            />
          </div>
        </div>
      </SimpleCard>

      <Modal
        in={showCancelAlert}
        onClose={() => setShowCancelAlert(false)}
      >
        <div className={styles.modalCard}>
          <div className={styles.header}>
            <h4>Batalkan Buat Transaksi</h4>
          </div>
          <div className={styles.line}></div>
          <div className={styles.content}>
            <h4 className='mb-8 mt-24'>Apakah Anda yakin untuk membatalkan transaksi?</h4>
            <p className='mb-0 font-size-14'>Jika Anda batalkan maka data yang Anda input akan hilang.</p>
          </div>
          <div className={styles.wrapperBtn}>
            <div className={styles.btn}>
              <MainButton
                loading={isCanceling}
                disabled={isCanceling}
                onClick={onCancel}
                variant="danger"
              >
                Ya, Lanjutkan
              </MainButton>
              <MainButton
                onClick={() => setShowCancelAlert(false)}
                variant="primary-light"
              >
                Kembali
              </MainButton>
            </div>
          </div>
        </div>
      </Modal>

      <Modal
        in={showAllErrors}
        onClose={() => setShowAllErrors(false)}
        scrollable
      >
        <ModalWrapper>
          <ModalHead
            title="Kesalahan Data Excel"
            onClose={() => setShowAllErrors(false)}
          />
          <ModalBody>
            <div className={styles.errorAlert}>
              <div className={styles.desc}>
                <Warning />
                <span>
                  Terdapat <b>{errors?.length} kesalahan</b> dalam ada file Template Excel.xlsx yang Anda unggah. Cek lagi, ya.
                </span>
              </div>
              <ul>
                {errors?.map((v, i) => {
                  return (
                    <li key={i}>
                      <span>{v}</span>
                    </li>
                  )
                })}
              </ul>
            </div>
          </ModalBody>
        </ModalWrapper>
      </Modal>
    </>
  )
}

export default Upload
