import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import SimpleCard from '../../../../../components/templates/SimpleCard';
import MainButton from '../../../../../components/Form/MainButton';
import Modal, { ModalBody, ModalHead, ModalWrapper } from '../../../../../components/ModalNew';
import Alert from '../../../../../components/Alert';
import styles from './cardIntegrationApi.module.scss';
import { Info } from '../../../../../assets/icons';

const CardIntegrationAPI = () => {
  const { company } = useSelector(state => state.auth)
  const [showModal, setShowModal] = useState(false);

  if (company.fitur.trxApiActive) return null
  return (
    <>
      <SimpleCard className={styles.cardIntegrationApi}>
        <div className={styles.asset}>
          <img src="/assets/media/others/integration-api.png" alt="" />
        </div>
        <div className={styles.content}>
          <div className={styles.heading}>
            <h3>Coba Transaksi dengan Integrasi API</h3>
            <p>Optimalkan kebutuhan bisnis anda melalui transaksi dengan integrasi API yang aman dan cepat. Hubungi kami untuk dapat menggunakan fitur Transaksi integrasi API untuk bisnis Anda.</p>
          </div>
          <div className={styles.groupBtn}>
            <a href="https://wa.me/+6281117261717" rel="noreferrer" target="_blank">
              <MainButton>Hubungi Kami</MainButton>
            </a>
            <button onClick={() => setShowModal(true)} className={styles.subtle}>Pelajari Selengkapnya</button>
          </div>
        </div>
      </SimpleCard>
      <Modal
        in={showModal}
        onClose={() => setShowModal(false)}
      >
        <ModalWrapper width="720px">
          <div className={styles.modal}>
            <ModalHead
              withoutBorder
              onClose={() => setShowModal(false)}
              title="Transaksi Integrasi API"
              iconName="integration_instructions"
              iconVariant="primary"
            />
            <ModalBody withoutPadding>
              <div className={styles.body}>
                <p className={styles.desc}>API merupakan sebuah antarmuka pemrograman aplikasi, yang berfungsi sebagai penghubung antara sistem Anda dengan sistem Dipay Disbursements. Hal ini memungkinkan Anda untuk melakukan integrasi antara kedua sistem tersebut sehingga proses transaksi dapat dilakukan secara otomatis tanpa memerlukan input manual.</p>
                <div className={styles.listContent}>
                  <p>Keunggulan Transaksi Integrasi API</p>
                  <ol>
                    <li>
                      Transaksi API dapat dilakukan secara real-time, mengurangi waktu yang dibutuhkan untuk proses transaksi dibandingkan dengan metode manual.
                    </li>
                    <li>
                      Penggunaan API memungkinkan transaksi dilakukan secara otomatis sehingga dapat mengurangi risiko kesalahan manusia dan mempercepat proses bisnis.
                    </li>
                    <li>
                      API memiliki tingkat keamanan yang baik untuk memastikan hanya pihak yang memiliki kewenangan dapat mengakses dan menggunakan layanan transaksi tersebut.
                    </li>
                  </ol>
                </div>
                <div className={styles.listContent}>
                  <p>Bagaimana saya melakukan Transaksi Integrasi API?</p>
                  <ol>
                    <li>
                      Pastikan perusahaan Anda memiliki tim IT yang berpengalaman dalam proses integrasi API.
                    </li>
                    <li>
                      Jika belum yakin apakah tim IT Anda dapat melakukan integrasi API atau tidak, Anda dapat berdiskusi dengan tim Dipay Disbursements. Hubungi Sales Support kami di 0811 1726 1717 untuk konsultasi lebih lanjut.
                    </li>
                    <li>
                      Jika perusahaan Anda belum siap melakukan transaksi integrasi API, Anda tetap dapat melakukan transaksi di Dipay Disbursements melalui fitur Transaksi Massal.
                    </li>
                  </ol>
                </div>
                <Alert
                  color="primary"
                  className="mb-24"
                  iconItem={<Info size={32} className="mr-16" color="#276EF1" />}
                >
                  <span className="font-size-14">Catatan: Hubungi Sales Support kami di <a href="https://wa.me/6281117261717" rel="noreferrer" target="_blank"><b><u>0811 1726 1717</u></b></a> untuk melakukan Transaksi Integrasi API.</span>
                </Alert>
              </div>
            </ModalBody>
          </div>
        </ModalWrapper>
      </Modal>
    </>
  )
}

export default CardIntegrationAPI
