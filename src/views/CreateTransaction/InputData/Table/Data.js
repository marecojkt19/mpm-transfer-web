import { yupResolver } from "@hookform/resolvers/yup";
import pakaiClass from "pakai-class";
import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import * as yup from 'yup';
import { Delete2, Rename } from "../../../../assets/icons";
import createTransactionService from "../../../../axios/services/createTransactionService";
import Button from "../../../../components/Button";
import SelectField from "../../../../components/Form/SelectField";
import TextField from "../../../../components/Form/TextField";
import Modal from "../../../../components/ModalNew";
import Table from "../../../../components/Table";
import MainButton from "../../../../components/templates/MainButton";
import { createTransactionAction } from "../../../../redux/actions/createTransactionAction";
import { censoredName, removeWhitespace } from "../../../../utils/helpers/stringManipulation";
import toIDR from "../../../../utils/helpers/toIDR";
import styles from '../../createTransaction.module.scss';
import bankService from "../../../../axios/services/bankService";
import { useCallback } from "react";

const Data = ({
  data,
  load,
  rowToEdit,
  setRowToEdit,
  setTableConfig,
  tableConfig: {
    skip,
    limit,
    skipLabel,
    limitLabel
  }
}) => {
  const dispatch = useDispatch();
  const [rowItems, setRowItems] = useState({});
  const [items, setItems] = useState([]);
  const [errors, setErrors] = useState([]);
  const { isLoading } = useSelector(state => state.createTransaction);
  const item = data?.transaction?.filter(obj => obj._id === rowToEdit)[0];

  const newItem = items.length ? [...items] : [...data?.transaction]
  const handleTemporaryChange = () => {
    const payload = {
      no: item?.no,
      _id: item?._id,
      destinationBank: rowItems?.destinationBank ?? item?.destinationBank,
      destinationBankCode: rowItems?.destinationBankCode ?? item?.destinationBankCode,
      description: rowItems?.description ?? item?.description,
      beneficiaryEmail: rowItems?.beneficiaryEmail ?? item?.beneficiaryEmail,
      beneficiaryAccountNo: rowItems?.beneficiaryAccountNo ?? item?.beneficiaryAccountNo,
      beneficiaryAccountNameFromBank: removeWhitespace(rowItems?.beneficiaryAccountNameFromBank) ?? item?.beneficiaryAccountNameFromBank,
      beneficiaryAccountName: removeWhitespace(rowItems?.beneficiaryAccountName) ?? item?.beneficiaryAccountName,
      amount: rowItems?.amount ?? item?.amount,
      status: item?.status
    }
    const index = data?.transaction.findIndex((obj) => obj._id === rowToEdit);
    if (index !== -1) {
      newItem.splice(index, 1);
    }
    const newItems = [...newItem, payload].sort((a, b) => a.no - b.no);
    setItems(newItems);
    setRowToEdit('')
  }

  const onChangePage = (v) => setTableConfig(skipLabel)(v);

  useEffect(() => {
    if (data?.transaction.length) {
      dispatch(createTransactionAction.setPayloadInquiryBulk(items))
    }
  }, [data?.transaction, data?.transaction.length, dispatch, items])

  return (
    <Table
      data={items.length ? items : data?.transaction}
      onChangePage={onChangePage}
      onLimit={(v) => setTableConfig(limitLabel)(v)}
      config={{
        columns: [
          {
            title: '#', key: 'no', render: v => `${v}.`
          },
          {
            title: 'Baris ke-', key: 'excelIndex'
          },
          {
            title: 'Bank Tujuan',
            key: 'destinationBank',
            render: (v, row) => {
              if (rowToEdit === row?._id) return <SelectBankItem rowKey='destinationBank' value={v} setRowItems={setRowItems} />
              else return <span>{v.replace("_", " ")}</span>
            }
          },
          {
            title: 'Nomor Rekening',
            key: 'beneficiaryAccountNo',
            render: (v, row) => {
              if (rowToEdit === row?._id) return <TextInputItem className={!row?.status?.isAccountNumberValid ? "error" : ""} rowKey='beneficiaryAccountNo' value={v} setRowItems={setRowItems} setErrors={setErrors} />
              if (!row?.status?.isAccountNumberValid) return <span className="text-danger">{v}</span>
              else return <span>{v}</span>
            }
          },
          {
            title: 'Nama Penerima', key: 'beneficiaryAccountName', render: (v, row) => {
              if (rowToEdit === row?._id) return <TextInputItem rowKey='beneficiaryAccountName' value={v} setRowItems={setRowItems} setErrors={setErrors} />
              if (!row?.status?.isNameMatch && row?.status?.isAccountNumberValid) return <span className="text-danger">{v}</span>
              else return <span>{v}</span>
            }
          },
          {
            title: 'Nama Penerima dari Bank', key: 'beneficiaryAccountNameFromBank', render: (v, row) => {
              if (row?.status?.isNameMatch) return <span className="text-success">{censoredName(v)}</span>
              else return <span>{censoredName(v) || "-"}</span>
            }
          },
          {
            title: 'Nominal (Rp)', key: 'amount', render: (v, row) => {
              if (rowToEdit === row?._id) return <TextInputItem rowKey='amount' value={v} setRowItems={setRowItems} setErrors={setErrors} />
              else return <span>{toIDR(v, false)}</span>
            }
          },
          {
            title: 'Berita', key: 'description', render: (v, row) => {
              if (rowToEdit === row?._id) return <TextInputItem rowKey='description' value={v} setRowItems={setRowItems} setErrors={setErrors} />
              else return <span>{v}</span>
            }
          },
          {
            title: 'Email Penerima', key: 'beneficiaryEmail', render: (v, row) => {
              if (rowToEdit === row?._id) return <TextInputItem rowKey='beneficiaryEmail' value={v} setRowItems={setRowItems} setErrors={setErrors} />
              else return <span>{v}</span>
            }
          },
          {
            title: 'Status', key: 'status', render: v => {
              if (!v?.isAccountNumberValid) return <span className='text-danger'>No. rekening salah</span>
              if (!v?.isNameMatch) return <span className='text-danger'>Nama tidak sesuai</span>
              else return <span className='text-success'>OK</span>
            }
          },
          {
            title: 'Action', key: '_id', render: (v, row) => {
              return (
                <div className={styles.editMode}>
                  {
                    rowToEdit === v
                      ?
                      <Button
                        onClick={handleTemporaryChange}
                        className={styles.saveBtn}
                        disabled={Boolean(errors?.length)}
                      >
                        Simpan
                      </Button>
                      :
                      <button onClick={() => setRowToEdit(v)}>
                        <Rename />
                      </button>
                  }
                  <DeleteAction
                    load={load}
                    id={v}
                  />
                </div>
              )
            }
          }
        ],
        total: data?.count,
        limit: limit,
        loading: isLoading,
        currentPage: skip,
        showRender: (from, to, total) => `${from}-${to} dari ${total} transaksi`,
      }}
    />
  )
}

const schema = yup.object().shape({
  destinationBank: yup.string().required(),
  beneficiaryAccountNo: yup.number().required(),
  beneficiaryAccountName: yup.string().required(),
  amount: yup.number().required()
})

const TextInputItem = ({ rowKey, value, className, setRowItems, setErrors, ...rest }) => {
  const { register, setValue, control } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      [rowKey]: value
    }
  });

  // return rowKey === "amount" ? (
  //   <TextField
  //     {...rest}
  //     name="amount"
  //     value={value}
  //     moneyInput
  //     allowLeadingZero={false}
  //     className={pakaiClass(styles.textInput, styles[className])}
  //     onChange={(v) => {
  //       setValue(rowKey, v)
  //       if (errors[rowKey]?.message) {
  //         setErrors(prev => [prev, rowKey])
  //       } else if (!Boolean(errors[rowKey]?.message)) {
  //         setErrors([])
  //       }
  //     }}
  //     onBlur={(v) => setRowItems(prev => ({ ...prev, [rowKey]: v.target.value }))}
  //     error={errors[rowKey]?.message}
  //   />
  // )
  //   :
  //   (
  //     <TextField
  //       {...rest}
  //       ref={register}
  //       name={rowKey}
  //       className={pakaiClass(styles.textInput, styles[className])}
  //       onChange={(v) => setValue(rowKey, v.target.value)}
  //       onBlur={(v) => setRowItems(prev => ({ ...prev, [rowKey]: v.target.value }))}
  //     />
  //   )

  return rowKey === "amount" ? (
    <Controller
      control={control}
      name="amount"
      defaultValue={value}
      render={(field) => {
        return (
          <TextField
            {...field}
            moneyInput
            variant="normal"
            className={pakaiClass(styles.textInput, styles[className])}
            onBlur={(v) => setRowItems(prev => ({ ...prev, [rowKey]: v.target.value.split('.').join("") }))}
          />
        )
      }}
    />
  ) : (
    <TextField
      {...rest}
      ref={register}
      name={rowKey}
      className={pakaiClass(styles.textInput, styles[className])}
      onChange={(v) => setValue(rowKey, v.target.value)}
      onBlur={(v) => setRowItems(prev => ({ ...prev, [rowKey]: v.target.value }))}
    />
  )
}

const SelectBankItem = ({ value, setRowItems, ...rest }) => {
  const [banks, setBanks] = useState([]);
  const { register, unregister, watch, setValue } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      destinationBank: value
    }
  });
  const { destinationBank } = watch(["destinationBank"])

  useEffect(() => {
    register("destinationBank");
    return () => {
      unregister("destinationBank");
    }
  }, [register, unregister])

  const load = useCallback(() => {
    bankService
      .list({})
      .then(({ data }) => {
        const opts = data.map(bank => ({
          value: bank?.enum,
          label: bank?.name
        }));
        setBanks(opts)
      })
  }, []);

  useEffect(() => {
    load()
  }, [load])

  return (
    <div className={styles.bankSelect}>
      <SelectField
        {...rest}
        name="destinationBank"
        options={banks}
        value={destinationBank}
        onChange={v => {
          setValue('destinationBank', v.value)
          setRowItems(prev => ({ ...prev, destinationBank: v?.value }))
        }}
      />
    </div>
  )
}


const DeleteAction = ({ load, id }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [loading, setLoading] = useState(false);

  const onHandle = () => {
    setLoading(true)
    if (id) {
      createTransactionService
        .delete(id)
        .then(() => {
          setIsOpen(false)
          load()
        })
        .finally(() => {
          setLoading(true)
        })
    }
  }
  return (
    <>
      <button onClick={() => setIsOpen(true)}>
        <Delete2 color="#E11900" />
      </button>
      <Modal
        in={isOpen}
        onClose={() => setIsOpen(false)}
      >
        <div className={styles.modalCard}>
          <div className={styles.header}>
            <h4>Hapus Baris Data</h4>
          </div>
          <div className={styles.line}></div>
          <div className={styles.content}>
            <h4 className='mb-8 mt-24'>Apakah Anda yakin ingin menghapus baris data ini?</h4>
            <p className='mb-0 font-size-14'>Jika Anda hapus maka data yang Anda input pada baris tersebut akan hilang.</p>
          </div>
          <div className={styles.wrapperBtn}>
            <div className={styles.btn}>
              <MainButton
                onClick={() => setIsOpen(false)}
                variant="primary-light"
              >
                Batalkan
              </MainButton>
              <MainButton
                onClick={onHandle}
                variant="primary"
                disabled={loading}
                loading={loading}
              >
                Hapus
              </MainButton>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default Data
