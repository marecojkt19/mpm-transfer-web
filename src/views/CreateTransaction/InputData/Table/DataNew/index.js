import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Rename } from "../../../../../assets/icons";
import Table from "../../../../../components/Table";
import { censoredName } from "../../../../../utils/helpers/stringManipulation";
import toIDR from "../../../../../utils/helpers/toIDR";
import styles from './data.module.scss';
import DeleteAction from "./DeleteAction";
import Modal, { ModalBody, ModalHead, ModalWrapper } from "../../../../../components/ModalNew";
import EditForm from "../../../EditForm";
import MainButton from "../../../../../components/Form/MainButton";
import createTransactionService from "../../../../../axios/services/createTransactionService";
import { toast } from "react-toastify";

const Data = ({
  data,
  load,
  rowToEdit,
  setRowToEdit,
  setTableConfig,
  module,
  tableConfig: {
    skip,
    limit,
    skipLabel,
    limitLabel
  }
}) => {
  const { isLoading } = useSelector(state => state.createTransaction);
  const item = data?.transaction?.filter(obj => obj._id === rowToEdit)[0];
  const [showEditForm, setShowEditForm] = useState(false);
  const [isUpdating, setIsUpdating] = useState(false);
  const [isDirty, setIsDirty] = useState(false);

  const onChangePage = (v) => setTableConfig(skipLabel)(v);

  const columns = [
    {
      title: '#', key: 'no'
    },
    {
      title: 'Baris ke-', key: 'excelIndex'
    },
    {
      title: 'Bank Tujuan',
      key: 'destinationBank',
      render: (v, row) => <span>{v?.replace("_", " ")}</span>
    },
    {
      title: 'Nomor Rekening',
      key: 'beneficiaryAccountNo',
      render: (v, row) => <span className={!row?.status?.isAccountNumberValid ? "text-danger" : ""}>{v}</span>
    },
    {
      title: 'Nama Penerima', key: 'beneficiaryAccountName', render: (v, row) => (
        <span className={(!row?.status?.isNameMatch && row?.status?.isAccountNumberValid) ? "text-danger" : ""}>{v}</span>
      )
    },
    {
      title: 'Nama Penerima dari Bank', key: 'beneficiaryAccountNameFromBank', render: (v, row) => (
        <span className={(row?.status?.isNameMatch) ? "text-success" : ""}>{censoredName(v) || '-'}</span>
      )
    },
    {
      title: 'Nominal (Rp)', key: 'amount', render: (v) => <span>{toIDR(v, false)}</span>
    },
    {
      title: 'Berita', key: 'description', render: (v) => <span>{v || '-'}</span>
    },
    {
      title: 'Email Penerima', key: 'beneficiaryEmail', render: (v, row) => <span>{v}</span>
    },
    {
      title: 'Status', key: 'status', render: v => {
        if (!v?.isAccountNumberValid) return <span className='text-danger'>No. rekening salah</span>
        if (!v?.isNameMatch) return <span className='text-danger'>Nama tidak sesuai</span>
        else return <span className='text-success'>OK</span>
      }
    },
    {
      title: 'Action', key: '_id', render: (v, row) => {
        return (
          <div className={styles.editMode}>
            {module !== "NAME" ?
              <button
                onClick={() => {
                  setShowEditForm(true)
                  setRowToEdit(v)
                }}
              >
                <Rename />
              </button>
              : null
            }
            <DeleteAction
              load={load}
              id={v}
            />
          </div>
        )
      }
    }
  ]

  const onSubmit = (values) => {
    setRowToEdit("")
    setIsUpdating(true)
    createTransactionService.updateTransaction(rowToEdit, values)
      .then(() => {
        load()
        toast.success("Berhasil Tersimpan")
        setIsUpdating(false)
        setShowEditForm(false)
      })
      .catch(() => {
        setIsUpdating(false)
        setShowEditForm(false)
      })
  }

  return (
    <>
      <Table
        data={data?.transaction}
        onChangePage={onChangePage}
        onLimit={(v) => setTableConfig(limitLabel)(v)}
        config={{
          columns,
          total: data?.count,
          limit: limit,
          loading: isLoading,
          currentPage: skip,
          showRender: (from, to, total) => `${from}-${to} dari ${total} transaksi`,
        }}
      />
      <Modal
        in={showEditForm}
        onClose={() => {
          setRowToEdit("")
          setShowEditForm(false)
        }}
      >
        <ModalWrapper width="560px">
          <ModalHead
            title="Ubah Transaksi"
            onClose={() => {
              setRowToEdit("")
              setShowEditForm(false)
            }}
          />
          <ModalBody>
            <EditForm
              module={module}
              onSubmit={onSubmit}
              setIsDirty={(v) => setIsDirty(v)}
              defaultValues={item}
            />
            <div className={styles.buttons}>
              <MainButton
                variant="primary-light"
                onClick={() => {
                  setRowToEdit("")
                  setShowEditForm(false)
                }}
              >
                Kembali
              </MainButton>
              <MainButton
                loading={isUpdating}
                form="transactionForm"
                disabled={!isDirty}
              >
                Simpan
              </MainButton>
            </div>
          </ModalBody>
        </ModalWrapper>
      </Modal>
    </>
  )
}

export default Data
