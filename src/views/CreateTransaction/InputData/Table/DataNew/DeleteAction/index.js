import React, { useState } from 'react'
import Modal, { ModalBody, ModalHead, ModalWrapper } from '../../../../../../components/ModalNew';
import createTransactionService from '../../../../../../axios/services/createTransactionService';
import { Delete2 } from '../../../../../../assets/icons';
import MainButton from '../../../../../../components/Form/MainButton';
import styles from './delete.module.scss'

const DeleteAction = ({ load, id }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [loading, setLoading] = useState(false);

  const onHandle = () => {
    setLoading(true)
    if (id) {
      createTransactionService
        .delete(id)
        .then(() => {
          setIsOpen(false)
          setLoading(false)
          load()
        })
        .catch(() => {
          setIsOpen(false)
          setLoading(false)
        })
    }
  }
  return (
    <div className={styles.deleteAction}>
      <button onClick={() => setIsOpen(true)}>
        <Delete2 color="#E11900" />
      </button>
      <Modal
        in={isOpen}
        onClose={() => setIsOpen(false)}
      >
        <ModalWrapper>
          <ModalHead
            title="Hapus Baris Data"
          />
          <ModalBody>
            <div className={styles.content}>
              <h4 className='mb-8 mt-24'>Apakah Anda yakin ingin menghapus baris data ini?</h4>
              <p className='mb-0 font-size-14'>Jika Anda hapus maka data yang Anda input pada baris tersebut akan hilang.</p>
            </div>
            <div className={styles.buttons}>
              <MainButton
                onClick={() => setIsOpen(false)}
                variant="primary-light"
              >
                Batalkan
              </MainButton>
              <MainButton
                onClick={onHandle}
                variant="danger"
                disabled={loading}
                loading={loading}
              >
                Hapus
              </MainButton>
            </div>
          </ModalBody>
        </ModalWrapper>
      </Modal>
    </div>
  )
}

export default DeleteAction
