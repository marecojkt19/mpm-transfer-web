import { useSelector } from "react-redux";
import { Warning } from "../../../../assets/icons"
import Alert from "../../../../components/Alert"

const TableAlert = ({
  rowToEdit
}) => {
  const {
    nameItems,
    accountItems
  } = useSelector(state => state.createTransaction);

  if ((Boolean(nameItems.count || accountItems.count) && !Boolean(rowToEdit))) {
    return (
      <Alert
        color="danger"
        className="mb-24"
        iconItem={<Warning className="mr-16" />}
      >
        <span className="font-size-14 font-bold">
          Terdapat kesalahan dalam input data. Klik pada filter <b>“Nama Tidak Sesuai”</b> dan/atau <b>“Rekening Salah”</b> untuk melihat data yang bermasalah.
        </span>
      </Alert>
    )
  } else if (Boolean(rowToEdit)) {
    return (
      <Alert
        color="warning"
        className="mb-24"
        iconItem={<Warning className="mr-16" color="#FF9500" />}
      >
        <span className="font-size-14 font-bold">
          Pastikan data yang Anda edit sudah benar lalu klik <b>“Simpan”</b>
        </span>
      </Alert>
    )
  } else {
    return <></>
  }
}

export default TableAlert