import pakaiClass from "pakai-class";
import { useState } from "react";
import { useSelector } from "react-redux";
// import Modal from "../../../../components/ModalNew";
// import LessBalancePopUp from "../LessBalancePopUp";
import styles from '../../createTransaction.module.scss';
import TableTabs from "./Tabs";

const TransactionTable = ({ load }) => {
  const [rowToEdit, setRowToEdit] = useState('');
  // const [isLessBalanceOpen, setIsLessBalanceOpen] = useState(false);
  const { nameItems, accountItems } = useSelector(state => state.createTransaction)
  const isShowAlert = (Boolean(nameItems.count || accountItems.count) || Boolean(rowToEdit))

  return (
    <>
      <div className={pakaiClass(styles.transactionTableWrapper, isShowAlert && styles.showAlert)}>
        <TableTabs
          load={load}
          rowToEdit={rowToEdit}
          setRowToEdit={setRowToEdit}
        // setIsLessBalanceOpen={setIsLessBalanceOpen}
        />
      </div>
      {/* <Modal
        in={isLessBalanceOpen}
        onClose={() => setIsLessBalanceOpen(false)}
      >
        <LessBalancePopUp
          onClose={() => setIsLessBalanceOpen(false)}
          title="Saldo deposit tidak cukup"
          heading="Nominal transfer melebihi saldo deposit"
          subHeading="Lakukan top-up deposit dan unggah ulang file Excel"
        />
      </Modal> */}
    </>
  )
}
export default TransactionTable;
