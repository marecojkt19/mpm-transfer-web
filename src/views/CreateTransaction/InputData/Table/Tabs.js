import { useDispatch, useSelector } from "react-redux";
import { Tab, Tabs } from "../../../../components/Tabs/Tabs"
import { createTransactionAction } from "../../../../redux/actions/createTransactionAction";
import Data from "./DataNew";
import toIDR from "../../../../utils/helpers/toIDR";
import TableAlert from "./Alert";
import Footer from "./Footer";
import styles from '../../createTransaction.module.scss';
import { useCallback, useEffect } from "react";

const TableTabs = ({
  load,
  rowToEdit,
  setRowToEdit,
  setIsLessBalanceOpen
}) => {
  const dispatch = useDispatch();
  const {
    activeTabKey,
    items,
    nameItems,
    accountItems,
    listAllConfig: {
      listAllSkip,
      listAllLimit
    },
    listNameConfig: {
      listNameSkip,
      listNameLimit
    },
    listAccountConfig: {
      listAccountSkip,
      listAccountLimit
    }
  } = useSelector(state => state.createTransaction);

  const handleTab = useCallback((key, name) => {
    dispatch(createTransactionAction.changeTab(key, name))
  }, [dispatch])

  useEffect(() => {
    if (!accountItems.length) {
      dispatch(createTransactionAction.changeTab(0))
    }
  }, [accountItems, dispatch])

  return (
    <Tabs
      activeKey={activeTabKey}
      onClick={handleTab}
    >
      <Tab title={`Semua Transaksi (${items?.count ?? 0})`} name="all">
        <TableAlert
          rowToEdit={rowToEdit}
        />
        <Data
          load={load}
          data={items}
          module="ALL"
          rowToEdit={rowToEdit}
          setRowToEdit={setRowToEdit}
          setTableConfig={key => value => dispatch(createTransactionAction.listAllConfig(key, value))}
          tableConfig={{
            skipLabel: 'listAllSkip',
            limitLabel: 'listAllLimit',
            skip: listAllSkip,
            limit: listAllLimit
          }}
        />
        <Nominal />
        <Footer
          load={load}
          rowToEdit={rowToEdit}
          setIsLessBalanceOpen={setIsLessBalanceOpen}
        />
      </Tab>
      {
        Boolean(accountItems?.count)
        &&
        <Tab title={`Rekening Salah (${accountItems.count})`} name="accountFilter">
          <TableAlert
            rowToEdit={rowToEdit}
          />
          <Data
            load={load}
            module="ACCOUNT"
            rowToEdit={rowToEdit}
            setRowToEdit={setRowToEdit}
            data={accountItems}
            setTableConfig={key => value => dispatch(createTransactionAction.listAccountConfig(key, value))}
            tableConfig={{
              skipLabel: 'listAccountSkip',
              limitLabel: 'listAccountLimit',
              skip: listAccountSkip,
              limit: listAccountLimit ?? listAllLimit
            }}
          />
          <Nominal />
          <Footer
            load={load}
            rowToEdit={rowToEdit}
            setIsLessBalanceOpen={setIsLessBalanceOpen}
          />
        </Tab>
      }
      {
        Boolean(nameItems?.count)
        &&
        <Tab title={`Nama Tidak Sesuai (${nameItems.count})`} name="nameFilter">
          <TableAlert
            rowToEdit={rowToEdit}
          />
          <Data
            load={load}
            data={nameItems}
            module="NAME"
            rowToEdit={rowToEdit}
            setRowToEdit={setRowToEdit}
            setTableConfig={key => value => dispatch(createTransactionAction.listNameConfig(key, value))}
            tableConfig={{
              skipLabel: 'listNameSkip',
              limitLabel: 'listNameLimit',
              skip: listNameSkip,
              limit: listNameLimit ?? listAllLimit
            }}
          />
          <Nominal />
          <Footer
            load={load}
            rowToEdit={rowToEdit}
            setIsLessBalanceOpen={setIsLessBalanceOpen}
          />
        </Tab>
      }
    </Tabs>
  )
}

const Nominal = () => {
  const {
    items,
    activeTabKey
  } = useSelector(state => state.createTransaction);

  return (
    <div className={styles.nominal}>
      {
        activeTabKey === 0
        &&
        <div className={styles.totalAmount}>
          <div className={styles.heading}>
            <p>Nominal Transfer</p>
          </div>
          <h3>Rp{toIDR(items.totalAmount, false)}</h3>
        </div>
      }
    </div>
  )
}


export default TableTabs
