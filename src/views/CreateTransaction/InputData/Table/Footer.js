import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Autorenew, Info } from '../../../../assets/icons';
import createTransactionService from '../../../../axios/services/createTransactionService';
import Modal from '../../../../components/ModalNew';
import MainButton from '../../../../components/Form/MainButton';
import { createTransactionAction } from '../../../../redux/actions/createTransactionAction';
import styles from '../../createTransaction.module.scss';
import { useHistory } from 'react-router-dom';

const Footer = ({
  load,
  rowToEdit,
  setIsLessBalanceOpen
}) => {
  return (
    <div className={styles.footer}>
      <div className={styles.btn}>
        {/* <CancelCreate /> */}
        {/* <UpdateList load={load} /> */}
        <AdjustmentNameBulk load={load} />
        <NextToDetail
          rowToEdit={rowToEdit}
          setIsLessBalanceOpen={setIsLessBalanceOpen}
        />
      </div>
    </div>
  )
}

// const CancelCreate = () => {
//   const [loading, setLoading] = useState(false)
//   const [isOpen, setIsOpen] = useState(false)
//   const { transactionId } = useSelector(state => state.createTransaction);
//   const dispatch = useDispatch();

//   const onCancel = () => {
//     setLoading(true)
//     if (transactionId) {
//       createTransactionService
//         .cancelCreate(transactionId)
//         .then(() => {
//           setLoading(false)
//           dispatch(createTransactionAction.reset())
//         })
//         .finally(() => {
//           setLoading(false)
//           setIsOpen(false)
//         })
//     }
//   }

//   return (
//     <>
//       <MainButton
//         onClick={() => setIsOpen(true)}
//         className={styles.adjustBtn}
//         variant="primary-light"
//         loading={loading}
//       >
//         <span>Batalkan</span>
//       </MainButton>
//       <Modal
//         in={isOpen}
//         onClose={() => setIsOpen(false)}
//       >
//         <div className={styles.modalCard}>
//           <div className={styles.header}>
//             <h4>Batalkan Buat Transaksi</h4>
//           </div>
//           <div className={styles.line}></div>
//           <div className={styles.content}>
//             <h4 className='mb-8 mt-24'>Apakah Anda yakin untuk membatalkan transaksi?</h4>
//             <p className='mb-0 font-size-14'>Jika Anda batalkan maka data yang Anda input akan hilang.</p>
//           </div>
//           <div className={styles.wrapperBtn}>
//             <div className={styles.btn}>
//               <MainButton
//                 loading={loading}
//                 disabled={loading}
//                 onClick={onCancel}
//                 variant="danger"
//               >
//                 Ya, Lanjutkan
//               </MainButton>
//               <MainButton
//                 onClick={() => setIsOpen(false)}
//                 variant="primary-light"
//               >
//                 Kembali
//               </MainButton>
//             </div>
//           </div>
//         </div>
//       </Modal>
//     </>
//   )
// }

const AdjustmentNameBulk = ({ load }) => {
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false)
  const {
    nameItems,
    isLoading,
    inquiryItems,
    activeTabKey
  } = useSelector(state => state.createTransaction);

  const onHandle = () => {
    dispatch(createTransactionAction.setLoading(true))

    const transactionId = nameItems?.transaction?.[0]?.transactionId;

    createTransactionService
      .adjustmentNameBulk(transactionId)
      .then(() => {
        load()
      })
      .finally(() => {
        dispatch(createTransactionAction.changeTab(0))
        dispatch(createTransactionAction.setLoading(false))
      })
    setIsOpen(false)
  }

  if (Boolean(inquiryItems.length) || activeTabKey !== 2) return <></>
  return (
    <>
      <MainButton
        onClick={() => setIsOpen(true)}
        className={styles.adjustBtn}
        variant="primary"
        disabled={!Boolean(nameItems.count)}
        icon={<Autorenew
          size={24}
          color="#FFFFFF"
        />}
      >
        Sesuaikan Nama
      </MainButton>
      <Modal
        in={isOpen}
        onClose={() => setIsOpen(false)}
      >
        <div className={styles.modalCard}>
          <div className={styles.header}>
            <h4>Sesuaikan Semua Nama</h4>
          </div>
          <div className={styles.line}></div>
          <div className={styles.content}>
            <p>Terdapat <span className="text-primary font-bold">{nameItems.count}</span> <b>Nama Pemilik Rekening</b> yang perlu disesuaikan</p>
            <p>Silakan klik tombol <b>“Sesuaikan Nama”</b> di bawah untuk <br /> menyamakan nama yang Anda input dengan nama dari sistem bank</p>
          </div>
          <div className={styles.notes}>
            <div>
              <Info size="23" color="#276EF1" />
            </div>
            <p>Kesalahan pada rekening penerima yang sudah Anda konfirmasi, merupakan di luar tanggung jawab Dipay Disbursement.</p>
          </div>
          <div className={styles.wrapperBtn}>
            <div className={styles.btn}>
              <MainButton
                onClick={() => setIsOpen(false)}
                variant="primary-light"
              >
                Kembali
              </MainButton>
              <MainButton
                loading={isLoading}
                disabled={isLoading}
                onClick={onHandle}
                variant="primary"
              >
                Sesuaikan Nama
              </MainButton>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

const NextToDetail = ({ rowToEdit, setIsLessBalanceOpen }) => {
  const dispatch = useDispatch();
  const { push } = useHistory();
  const [loading, setLoading] = useState(false);
  const {
    transactionId,
    // items,
    nameItems,
    accountItems,
    inquiryItems,
    activeTabKey
  } = useSelector(state => state.createTransaction);
  // const { company } = useSelector(state => state.auth);

  const onHandle = () => {
    setLoading(true)
    if (transactionId) {
      createTransactionService
        .detail(transactionId)
        .then(({ data }) => {
          dispatch(createTransactionAction.setDetail(data))
          dispatch(createTransactionAction.setSinglePayload(data))
          dispatch(createTransactionAction.setStep(2))
          push(`/create-transaction?transactionId=${transactionId}`)
        })
        .catch(() => { })
        .finally(() => {
          setLoading(false)
        })
    }
  }

  if (inquiryItems.length || activeTabKey) return <></>
  return (
    <>
      <MainButton
        onClick={onHandle}
        className={styles.mainButton}
        loading={loading}
        disabled={nameItems.count || accountItems.count || loading || Boolean(rowToEdit)}
      >
        <span>Lanjutkan</span>
      </MainButton>
    </>
  )
}

export default Footer
