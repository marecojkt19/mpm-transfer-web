import React from 'react'
import { useHistory } from 'react-router-dom'
import { Failed } from '../../../../assets/icons'
import { ModalWrapper } from '../../../../components/Modal'
import MainButton from '../../../../components/Form/MainButton'
import styles from './lessBalancePopUp.module.scss'

const LessBalancePopUp = ({
  onClose,
  heading,
  subHeading,
  title
}) => {
  const { push } = useHistory();

  return (
    <ModalWrapper className={styles.lessBalanceModal}>
      <div className={styles.modalHead}>
        <h1>{title}</h1>
      </div>
      <div className={styles.modalBody}>
        <div className={styles.wrapper}>
          <div className={styles.icon}>
            <Failed />
          </div>
          <div className={styles.copy}>
            <h3>{heading}</h3>
            <h5>{subHeading}</h5>
          </div>
        </div>
        <div className={styles.btnWrapper}>
          <MainButton
            onClick={onClose}
            variant="primary-light"
          >
            <span>Kembali</span>
          </MainButton>
          <MainButton
            variant="primary"
            onClick={() => push("/deposit")}
          >
            <span>Deposit</span>
          </MainButton>
        </div>
      </div>
    </ModalWrapper>
  )
}

export default LessBalancePopUp
