import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Succeed } from "../../assets/icons";
import MainButton from "../../components/Form/MainButton";
import SimpleCard from "../../components/templates/SimpleCard";
import { createTransactionAction } from "../../redux/actions/createTransactionAction";
import styles from './createTransaction.module.scss';

const Confirmation = () => {
  const dispatch = useDispatch()
  const { trxCode } = useSelector(state => state.createTransaction);

  return (
    <SimpleCard className={styles.confirmationWrapper}>
      <div>
        <Succeed className="mb-32" />
        <p className="font-700 font-size-20 mb-8">{`Transaksi #${trxCode} berhasil dibuat`}</p>
        <p className="text-dark-gray font-size-16 mb-32">Untuk melanjutkan pembayaran silakan cek daftar transaksi</p>
        <Link to="/transaction">
          <MainButton onClick={() => dispatch(createTransactionAction.reset())} className="font-size-16">Lihat Daftar Transaksi</MainButton>
        </Link>
      </div>
    </SimpleCard>
  )
}

export default Confirmation;
