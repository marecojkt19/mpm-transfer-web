import React, { useState } from 'react'
import styles from './manualInput.module.scss'
import { useDispatch } from 'react-redux'
import MainButton from '../../../components/Form/MainButton'
import { createTransactionAction } from '../../../redux/actions/createTransactionAction'
import createTransactionService from '../../../axios/services/createTransactionService'
import EditForm from '../EditForm'

const ManualInput = ({ setIsDirty }) => {
  const dispatch = useDispatch();
  const [isCreateSingle, setIsCreateSingle] = useState(false);

  const onSubmit = (values) => {
    setIsCreateSingle(true)
    const data = {
      ...values,
      amount: +values.amount
    };
    createTransactionService.inquirySingle(data)
      .then(({ data: { transactionId } }) => {
        dispatch(createTransactionAction.setSinglePayload(data))
        dispatch(createTransactionAction.setTransactionId(transactionId))
        dispatch(createTransactionAction.setStep(2))
      })
      .catch((err) => console.log(err.response))
      .finally(() => setIsCreateSingle(false))
  }

  return (
    <div className={styles.manualInput}>
      <div className={styles.heading}>
        <h5>Formulir Transaksi</h5>
        <p>
          Silakan isi formulir untuk melakukan transfer single (<span>*</span>) wajib diisi
        </p>
      </div>
      <EditForm onSubmit={onSubmit} setIsDirty={setIsDirty} />
      <div className={styles.buttons}>
        <MainButton
          loading={isCreateSingle}
          form="transactionForm"
        >
          Lanjut
        </MainButton>
      </div>
    </div>
  )
}

export default ManualInput
