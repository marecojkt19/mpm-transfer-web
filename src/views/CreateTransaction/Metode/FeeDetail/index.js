import React, { useMemo } from 'react'
import styles from './feeDetail.module.scss'
import toIDR from '../../../../utils/helpers/toIDR'
import Table from '../../../../components/Table'
import { useSelector } from 'react-redux'

const FeeDetail = ({
  detail
}) => {
  const { company } = useSelector(state => state.auth)

  const columns = useMemo(() => [
    { title: 'Bank Penerima', key: 'destinationBank', render: v => v || '-' },
    { title: 'Biaya Transfer', key: 'bankFee', render: v => `Rp${toIDR(v, false)}` || '-' },
    { title: 'Jumlah Transaksi', key: 'transactionQty', render: v => v || '-' },
    { title: 'Subtotal', key: 'subtotal', align: 'right', render: v => `Rp${toIDR(v, false)}` || '-' },
  ], [])

  return detail ? (
    <div className={styles.feeDetail}>
      <div className={styles.transferAmount}>
        <div className={styles.tableCard}>
          <div className={styles.row}>
            <span>Nama Pengirim</span>
            <span>:</span>
            <span>{company?.name}</span>
          </div>
          <div className={styles.row}>
            <span>Jumlah Penerima</span>
            <span>:</span>
            <span>{detail?.numberOfRecipient} Penerima</span>
          </div>
          <div className={styles.row}>
            <span>Nominal Transfer</span>
            <span>:</span>
            <span>Rp{toIDR(detail.totalTransfer, false)}</span>
          </div>
        </div>
      </div>
      <div className={styles.table}>
        <div className={styles.label}>
          <span>Detail Biaya Admin</span>
        </div>
        <Table
          data={detail?.detailTransfer}
          config={{
            // loading: isLoading,
            pagination: false,
            columns: columns
          }}
        />
        <div className={styles.footerCreate}>
          <div className={styles.content}>
            <div>
              <span>Total Biaya Admin:</span>
              <span>Rp{toIDR(detail?.totalTransferFee, false)}</span>
            </div>
            <div>
              <span>Nominal Transfer: </span>
              <span>Rp{toIDR(detail?.totalTransfer, false)}</span>
            </div>
            <div className={styles.lineDash} />
            <div>
              <span>Total Transfer & Admin:</span>
              <span className={styles.primary}>Rp{toIDR(detail?.total, false)}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : null
}

export default FeeDetail
