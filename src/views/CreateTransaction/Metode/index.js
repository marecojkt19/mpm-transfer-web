import React, { useCallback, useEffect, useState } from 'react'
import FeeDetail from './FeeDetail'
import SimpleCard from '../../../components/templates/SimpleCard'
import styles from './metode.module.scss'
import MainButton from '../../../components/Form/MainButton'
import RadioGroup from '../../../components/Form/Radio/RadioGroup2'
import { useDispatch, useSelector } from 'react-redux'
import toIDR from '../../../utils/helpers/toIDR'
import RadioItem from '../../../components/Form/Radio/RadioItem'
import { ChecklistOff, ChecklistOn, Warning3 } from '../../../assets/icons'
import Modal, { ModalBody, ModalHead, ModalWrapper } from '../../../components/ModalNew'
import transactionService from '../../../axios/services/transactionService'
import TncTemplate from '../../../components/templates/TncTemplate'
import createTransactionService from '../../../axios/services/createTransactionService'
import { createTransactionAction } from '../../../redux/actions/createTransactionAction'
import { useHistory } from 'react-router-dom'

const Metode = ({ transactionId }) => {
  const [showExample, setShowExample] = useState(false);
  const [showTnc, setShowTnc] = useState(false);
  const [showVerification, setShowVerification] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [paymentMethod, setPaymentMethod] = useState("DEPOSIT");
  const [otpMethod, setOtpMethod] = useState();
  const [isSendInvoice, setIsSendInvoice] = useState(false);
  const [detail, setDetail] = useState();
  const [showBalanceError, setShowBalanceError] = useState(false);
  const { singlePayload, method } = useSelector(state => state.createTransaction);
  const { push } = useHistory();

  const { user, company } = useSelector(state => state.auth);
  const dispatch = useDispatch();

  const getTitle = (str) => {
    if (str === "DEPOSIT") return "Deposit";
    return str;
  }

  const getMethod = (str) => {
    if (str === "EMAIL") return user.email;
    if (str === "SMS") return company.phoneNumber;
    return str;
  }

  const onSubmit = () => {
    if (paymentMethod) {
      setIsLoading(true)
      transactionService
        .requestOTP({
          morph: getMethod(otpMethod),
          type: otpMethod
        })
        .then(() => {
          dispatch(createTransactionAction.setStep(3))
          dispatch(createTransactionAction.setSinglePayload({
            ...singlePayload,
            otpMethod,
            paymentMethod,
            transactionId,
            isSendInvoice: company?.status?.alwaysSendEmailReceiveTrx === 1 || isSendInvoice === 1 ? 1 : 0
          }))
          setIsLoading(false)
        })
    }
  }

  const load = useCallback(() => {
    createTransactionService.detail(transactionId)
      .then(({ data }) => {
        setDetail(data)
      })
      .catch(e => console.log(e))
  }, [transactionId]);

  useEffect(() => {
    load()
  }, [load])

  return (
    <>
      <div className={styles.metode}>
        <SimpleCard>
          <FeeDetail showTrxVolumes={false} detail={detail} />
          {
            (method === "SINGLE" && singlePayload?.beneficiaryEmail) || method === "BULKING" ?
              company?.status?.alwaysSendEmailReceiveTrx === 1 ?
                <div className={styles.sendInvoice}>
                  <div className={styles.content}>
                    <div className={styles.row}>
                      <h5>Bukti Transaksi akan dikirim ke email Penerima Dana 📧</h5>
                      <button onClick={() => setShowExample(true)}>Lihat Contoh</button>
                    </div>
                    <p>
                      Bukti Transaksi dikirim secara otomatis setelah dana diterima di rekening penerima dana. Buka Pengaturan untuk me-nonaktifkan notifikasi tersebut.
                    </p>
                  </div>
                </div>
                :
                <div className={styles.sendInvoice}>
                  <div className={styles.checkBox}>
                    <button
                      type="button"
                      onClick={() => setIsSendInvoice(!isSendInvoice)}
                    >
                      {isSendInvoice ? <ChecklistOn /> : <ChecklistOff />}
                    </button>
                  </div>
                  <div className={styles.content}>
                    <div className={styles.row}>
                      <button
                        type="button"
                        onClick={() => setIsSendInvoice(!isSendInvoice)}
                      >
                        <h5>Kirim Bukti Transaksi ke email Penerima Dana 📧</h5>
                      </button>
                      <button onClick={() => setShowExample(true)}>Lihat Contoh</button>
                    </div>
                    {!isSendInvoice ?
                      <p>
                        Dengan mengaktifkan fitur kirim bukti transaksi, penerima dana akan secara otomatis menerima email berisi bukti transaksi yang dapat diunduh.
                      </p>
                      :
                      <p>
                        Setelah dana diterima di rekening penerima, bukti transaksi akan dikirim secara otomatis. Penerima juga akan mendapatkan informasi mengenai dana yang diterima serta berita dari pengirim.
                      </p>
                    }
                  </div>
                </div>
              : null
          }
          <div className={styles.paymentMethod}>
            <div className={styles.heading}>
              <h5>Pilih Metode Pembayaran</h5>
            </div>
            <div className={styles.radio}>
              <RadioGroup
                name="method"
                value={paymentMethod}
                onChange={(e) => setPaymentMethod(e.target.value)}
                className="mb-32"
              >
                <RadioItem
                  size="lg"
                  value="DEPOSIT"
                  label={
                    <div className={styles.radioItem}>
                      <h5>{getTitle("DEPOSIT")}</h5>
                      <p>Deposit saat ini: <span className='text-primary'>Rp{toIDR(company?.balance?.deposit ?? 0, false)}</span></p>
                    </div>
                  }
                />
              </RadioGroup>
            </div>
          </div>

          <div className={styles.tnc}>
            <p>
              Dengan menekan tombol “Lanjutkan” saya sudah paham dengan
              {" "}
              <button
                type="button"
                onClick={() => setShowTnc(true)}
                className="font-500 text-primary"
              >
                Syarat & Ketentuan
              </button>
              {" "}
              transfer Dipay dan mengetahui transaksi yang sedang diproses tidak dapat dibatalkan.
            </p>
          </div>
          <div className={styles.buttons}>
            <div className={styles.buttonsWrapper}>
              <MainButton
                disabled={!paymentMethod}
                onClick={() => {
                  if (Number(detail.total) > Number(company.balance.deposit)) {
                    setShowBalanceError(true)
                  } else {
                    setShowVerification(true)
                  }
                }}
              >
                Lanjutkan
              </MainButton>
            </div>
          </div>
        </SimpleCard>
      </div>
      <Modal
        in={showExample}
        onClose={() => setShowExample(false)}
      >
        <ModalWrapper width="50%">
          <ModalHead
            title="Contoh Bukti Transaksi"
            onClose={() => setShowExample(false)}
          />
          <ModalBody>
            <div className={styles.modal}>
              <div className={styles.example}>
                <img
                  src="/assets/example/reciept.png"
                  alt=""
                />
              </div>
            </div>
          </ModalBody>
        </ModalWrapper>
      </Modal>
      <Modal
        in={showVerification}
        onClose={() => setShowVerification(false)}
      >
        <ModalWrapper width="50%">
          <ModalHead
            title="Kirim Kode Verifikasi Melalui:"
            onClose={() => setShowVerification(false)}
          />
          <hr />
          <ModalBody>
            <div className={styles.modal}>
              <div className={styles.content}>
                <RadioGroup
                  name="otpMethod"
                  value={otpMethod}
                  onChange={(e) => setOtpMethod(e.target.value)}
                  className="mb-16"
                >
                  <RadioItem
                    className={styles.radio}
                    label={<><b>Email: </b>{user.email}</>}
                    value="EMAIL"
                  />
                  <RadioItem
                    className={styles.radio}
                    label={<><b>Nomor handphone: </b>{company.phoneNumber ?? "-"}</>}
                    value="SMS"
                  />
                </RadioGroup>
                <p className="text-dark-gray">Pastikan Email atau Nomor Handphone Anda aktif</p>
              </div>
              <div className={styles.buttons}>
                <div className={styles.buttonsWrapper}>
                  <MainButton
                    disabled={!otpMethod}
                    loading={isLoading}
                    onClick={onSubmit}
                  >
                    Kirim Kode Verifikasi
                  </MainButton>
                </div>
              </div>
            </div>
          </ModalBody>
        </ModalWrapper>
      </Modal>

      <Modal in={showTnc} onClose={() => setShowTnc(false)}>
        <TncTemplate />
      </Modal>

      <Modal
        in={showBalanceError}
        onClose={() => setShowBalanceError(false)}
      >
        <ModalWrapper width={560}>
          <ModalBody>
            <div className={styles.warning}>
              <div className={styles.content}>
                <Warning3 />
                <h1>Saldo Deposit Tidak Cukup</h1>
                <p>Saldo deposit tidak kurang dari nominal transfer. Silakan lakukan top-up deposit dan setujui transaksi kembali.</p>
              </div>

              <div className={styles.buttons}>
                <MainButton
                  variant="secondary"
                  onClick={() => setShowBalanceError(false)}
                >
                  Kembali
                </MainButton>
                <MainButton
                  onClick={() => push("/deposit")}
                >
                  Top Up Saldo
                </MainButton>
              </div>
            </div>
          </ModalBody>
        </ModalWrapper>
      </Modal>
    </>
  )
}

export default Metode
