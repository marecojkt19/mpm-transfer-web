import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import Steps, { Step } from '../../components/Steps';
import styles from './createTransaction.module.scss';
import InputMethod from './InputMethod';
import ManualInput from './ManualInput';
import Metode from './Metode';
import { createTransactionAction } from '../../redux/actions/createTransactionAction';
import Modal from '../../components/ModalNew';
import ConfirmAlert from '../../components/templates/ConfirmAlert';
import { useHistory } from 'react-router-dom';
import Verification from './Verification';
import InputData from './InputData'
import BackButton from '../../components/templates/BackButton';

const CreateTransactionPage = () => {
  const { step, method, transactionId } = useSelector(state => state.createTransaction);
  const dispatch = useDispatch();
  const [showAlertBack, setShowAlertBack] = useState(false);
  const [showAlertBackVerification, setShowAlertBackVerification] = useState(false);
  const [dirty, setIsDirty] = useState(false);
  const { push } = useHistory();

  const onBack = () => {
    if (method === "BULKING") {
      if (step === 1 && transactionId) {
        setShowAlertBack(true)
      } else if (step === 3) {
        setShowAlertBackVerification(true)
      } else {
        dispatch(createTransactionAction.setStep(step - 1));
      }
    } else {
      if (dirty || transactionId) {
        setShowAlertBack(true)
      } else if (step === 3) {
        setShowAlertBackVerification(true)
      } else {
        dispatch(createTransactionAction.setStep(step - 1));
      }
    }
  }

  useEffect(() => {
    if (transactionId) {
      if (method === "SINGLE") {
        dispatch(createTransactionAction.setStep(2))
      } else {
        dispatch(createTransactionAction.setStep(1))
      }
    }
  }, [dispatch, transactionId, method, push])

  return (
    <>
      <div className={styles.createTransaction}>
        <Helmet title="Buat Transaksi" />
        {step > 0 ?
          <BackButton onClick={() => onBack()} />
          : null
        }
        <div className={styles.pageHeading}>
          {step > 1 ?
            <h1>Buat Transaksi</h1>
            :
            method === "BULKING" ?
              <h1>Transaksi Unggah Excel</h1>
              :
              <h1>Transaksi Single</h1>
          }
        </div>
        <Steps
          activeStep={step}
          className="my-24"
        >
          <Step label="Metode Input">
            <InputMethod />
          </Step>
          <Step label="Input Data">
            {method === "BULKING" ?
              <InputData />
              :
              <ManualInput setIsDirty={setIsDirty} />
            }
          </Step>
          <Step label="Metode Pembayaran">
            <Metode transactionId={transactionId} />
          </Step>
          <Step label="Kirim">
            <Verification setShowAlertBackVerification={setShowAlertBackVerification} transactionId={transactionId} />
          </Step>
        </Steps>
      </div >
      <Modal in={showAlertBack} onClose={() => setShowAlertBack(false)}>
        <ConfirmAlert
          onClose={() => setShowAlertBack(false)}
          onSubmit={() => {
            dispatch(createTransactionAction.reset());
            if (method === "SINGLE" && step === 2) {
              dispatch(createTransactionAction.setStep(1));
            } else {
              dispatch(createTransactionAction.setStep(step - 1));
            }
          }}
          title="Data Yang Diisi Akan Hilang"
          description="Ketika Anda keluar dari halaman ini maka akan kehilangan data yang sudah diinput pada halaman ini."
          customNextTitle="Ya, Saya Yakin"
        />
      </Modal>

      <Modal in={showAlertBackVerification} onClose={() => setShowAlertBackVerification(false)}>
        <ConfirmAlert
          onClose={() => setShowAlertBackVerification(false)}
          onSubmit={() => dispatch(createTransactionAction.setStep(step - 1))}
          title="Kode OTP Akan Hilang"
          description="Ketika Anda keluar dari halaman ini maka akan kehilangan data OTP yang sudah terkirim."
          customNextTitle="Ya, Saya Yakin"
        />
      </Modal>
    </>
  )
}

export default CreateTransactionPage
