import React from 'react'
import { useSelector } from 'react-redux'
// import accountVerificationServices from '../../../axios/services/accountVerificationServices'
// import MainButton from '../../../components/Form/MainButton'
import styles from './requestSuccess.module.scss'

const RequestSuccess = ({
  setPage
}) => {
  const { user } = useSelector(state => state.auth)
  // const handleRequest = () => {
  //   setPage(2)
  //   const direcetToStep = {
  //     step: "stepFour",
  //     value: false
  //   }
  //   accountVerificationServices
  //     .changeStep(direcetToStep, user._id)
  //     .catch(() => { })
  // }

  return (
    <div className={styles.landingPage}>
      <div className={styles.wrapper}>
        <div className={styles.verifiedImage}>
          <img src="/assets/media/others/verified.png" alt="" />
        </div>
        <div className={styles.success}>
          <div className={styles.heading}>
            <h2>Verifikasi Data berhasil diajukan!</h2>
            <p><b>{user?.fullName}</b>, Terima kasih sudah menyelesaikan proses verifikasi. <br /> Dokumen Anda sedang dalam proses peninjauan oleh Tim Dipay Disbursement. </p>
            <p>Kami akan menghubungi Anda paling lambat dalam 1 hari kerja. <br /> Proses pengecekan akan dilakukan maks.3 hari kerja setelah Anda dihubungi. <br /> <b>Anda akan menerima email notifikasi</b> apabila sudah bisa menggunakan layanan Dipay Disbursement  </p>
            <p>Ada pertanyaan? <br /> Hubungi tim Dipay via email ke <a href="mailto:enterprise@dipay.id">enterprise@dipay.id</a> <br /> atau ke <a href="https://wa.me/6281117261717" rel="noreferrer" target="_blank">+6281117261717</a> (Whatsapp)</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default RequestSuccess;
