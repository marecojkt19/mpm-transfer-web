import React from 'react'
import { useSelector } from 'react-redux'
import MainButton from '../../../components/Form/MainButton'
import styles from './landingPage.module.scss'

const LandingPage = ({
  setPage
}) => {
  const { user } = useSelector(state => state.auth)
  return (
    <div className={styles.landingPage}>
      <div className={styles.wrapper}>
        <div className={styles.verifiedImage}>
          <img src="/assets/media/others/verified.png" alt="" />
        </div>
        <div className={styles.welcome}>
          <div className={styles.heading}>
            <h6>Selamat datang di Dipay Disbursement, {user?.fullName}!</h6>
            <p>Buka semua fitur Dipay Disbursement, dengan melanjutkan verifikasi akun Anda.</p>
          </div>
          <div className={styles.line}></div>
          <div className={styles.button}>
            <MainButton
              type="button"
              onClick={() => setPage(2)}
            >
              Lanjutkan Verifikasi Akun
            </MainButton>
          </div>
        </div>
      </div>
    </div>
  )
}

export default LandingPage
