import React, { useEffect, useMemo, useState } from 'react'
import { Helmet } from 'react-helmet';
import { useSelector } from 'react-redux';
import LandingPage from './LandingPage';
import VerificationSteps from './VerificationSteps';
import styles from './accountVerification.module.scss'
import RegistrationSuccess from '../Auth/Onboarding/RegistrationSuccess';
import pakaiClass from 'pakai-class';
import VerificationSuccess from './VerificationSuccess';

const AccountVerification = () => {
  const [page, setPage] = useState(1);
  const { step } = useSelector(state => state.verifyAccount);
  const { user, isNewRegistered } = useSelector(state => state.auth);
  const { verifyStep: { stepFour } } = user;

  const view = useMemo(() => {
    if (page === 1) {
      return <>
        <h1 className="mb-40">Verifikasi Akun</h1>
        <LandingPage setPage={setPage} />
      </>
    } else if (page === 2) {
      return <>
        <h1 className="mb-40">Verifikasi Akun</h1>
        <VerificationSteps setPage={setPage} />
      </>
    } else if (page === 3) {
      return <VerificationSuccess />
    }
  }, [page])

  useEffect(() => {
    if (user?.phoneNumber) setPage(2)
    if (stepFour === true && step === 5) setPage(3)
  }, [step, stepFour, user])

  return (
    <>
      <Helmet title="Verifikasi Akun" />
      <div className={styles.accountVerification}>
        {view}
      </div>
      <div className={pakaiClass(styles.registrationSuccess, isNewRegistered && styles.show)}>
        <RegistrationSuccess />
      </div>
    </>
  )
}

export default AccountVerification
