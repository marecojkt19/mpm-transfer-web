import pakaiClass from 'pakai-class'
import React from 'react'
import { useSelector } from 'react-redux';
import { removeWhitespace } from '../../../utils/helpers/stringManipulation';
import styles from './verificationSuccess.module.scss'

const VerificationSuccess = () => {
  const { user } = useSelector(state => state.auth);

  return (
    <div className={styles.verificationSuccess}>
      <div className={styles.wrapper}>
        <div className={styles.verifiedImage}>
          <img src="/assets/media/others/verified.png" alt="" />
        </div>
        <div className={styles.successText}>
          <h3>Verifikasi Data berhasil diajukan!</h3>
          <div className={pakaiClass(styles.textGroup, "mb-28")}>
            <p><b>{removeWhitespace(user?.fullName)},</b> Terima kasih sudah menyelesaikan proses verifikasi.</p>
            <p className="mb-8">Dokumen Anda sedang dalam proses peninjauan oleh Tim Dipay Disbursement.</p>
          </div>
          <div className={pakaiClass(styles.textGroup, "mb-20")}>
            <p>Kami akan menghubungi Anda paling lambat dalam 1 hari kerja.</p>
            <p>Proses pengecekan akan dilakukan maks. 3 hari kerja setelah Anda dihubungi.</p>
            <p><b>Anda akan menerima email notifikasi</b> apabila sudah bisa menggunakan layanan Dipay Disbursement</p>
          </div>
          <div className={styles.textGroup}>
            <p>Ada pertanyaan?</p>
            <p>Hubungi tim Dipay via email ke <a href="mailto:enterprise@dipay.id">enterprise@dipay.id</a></p>
            <p>atau ke <a href="https://wa.me/6281117261717" rel="noreferrer" target="_blank">+62 811-1726-1717</a> (Whatsapp)</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default VerificationSuccess
