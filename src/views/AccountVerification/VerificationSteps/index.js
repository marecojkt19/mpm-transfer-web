import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Steps, { Step } from "../../../components/Steps";
import { verifyAccountActions } from "../../../redux/actions/verifyAccountAction";
import DataVerification from "./DataVerification";
import PhoneNumber from "./PhoneNumber";
import Summary from "./Summary";
import UploadDocument from "./UploadDocument";

const VerificationSteps = ({ setPage }) => {
  const { step } = useSelector(state => state.verifyAccount);
  const { user } = useSelector(state => state.auth);
  const { verifyStep: { stepOne, stepTwo, stepThree, stepFour } } = user;
  const dispatch = useDispatch();

  useEffect(() => {
    if (stepOne) dispatch(verifyAccountActions.setStep(1))
    if (stepTwo) dispatch(verifyAccountActions.setStep(2))
    if (stepThree) dispatch(verifyAccountActions.setStep(3))
    if (stepFour) dispatch(verifyAccountActions.setStep(5))
  }, [dispatch, stepFour, stepOne, stepThree, stepTwo])

  return (
    <Steps
      activeStep={step}
      className="my-24"
    >
      <Step label="Nomor HP">
        <PhoneNumber />
      </Step>
      <Step label="Verifikasi Data">
        <DataVerification />
      </Step>
      <Step label="Unggah Dokumen">
        <UploadDocument />
      </Step>
      <Step label="Ringkasan Data">
        <Summary setPage={setPage} />
      </Step>
    </Steps>
  )
}

export default VerificationSteps;
