import MainButton from '../../../../../components/Form/MainButton';
import styles from './inputPhone.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useEffect, useState } from 'react';
import PhoneInput from '../../../../../components/Form/PhoneInput';
import userVerificationService from '../../../../../axios/services/userVerificationService';
import { verifyAccountActions } from '../../../../../redux/actions/verifyAccountAction';
import { useDispatch } from 'react-redux';

const schema = yup.object().shape({
  phoneNumber: yup.string().required('No. Handphone tidak boleh kosong')
})

const InputPhone = ({ setSubStep, setData }) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [errorResponse, setErrorResponse] = useState('');
  const {
    register,
    handleSubmit,
    errors,
    watch,
    formState: { isValid },
    unregister,
    setValue
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema)
  });
  const { phoneNumber } = watch(["phoneNumber"])

  const onSubmit = () => {
    setLoading(true)
    userVerificationService.requestOtp({
      phoneNumber: `+62${phoneNumber}`
    })
      .then(({ data: { signature } }) => {
        dispatch(verifyAccountActions.setSignature(signature));
        setData({ phoneNumber: `+62${phoneNumber}` });
        setSubStep(2)
      })
      .catch((err) => {
        if (err.response) {
          if (err.response.body) {
            setErrorResponse(err.response.body.message);
          }
          if (err.response.data) {
            if (typeof err.response.data.message === "string") {
              setErrorResponse(err.response.data.message);
            } else {
              for (const fields of err.response.data.message) {
                for (const message of Object.keys(fields.constraints)) {
                  setErrorResponse(fields.constraints[message]);
                }
              }
            }
          }
        } else {
          setErrorResponse(err);
        }
      })
      .finally(() => setLoading(false))
  }

  useEffect(() => {
    register('phoneNumber')
    return () => {
      unregister('phoneNumber')
    }
  }, [register, unregister])

  return (
    <div className={styles.inputPhone}>
      <div className={styles.card}>
        <div className={styles.wrapper}>
          <div className={styles.headingText}>
            <h5>Verifikasi Nomor HP</h5>
            <p>Verifikasi nomor HP untuk meningkatkan keamanan transaksi Anda</p>
          </div>
          <div className={styles.image}>
            <img
              src="/assets/media/others/otp-phone.png"
              alt=""
            />
          </div>
          <form onSubmit={handleSubmit(onSubmit)}>
            <PhoneInput
              name="phoneNumber"
              label="Nomor HP yang aktif"
              placeholder="Contoh : 8119956067"
              error={errors.phoneNumber || Boolean(errorResponse)}
              helperText={errors.phoneNumber?.message || errorResponse}
              className="mb-16"
              allowDot={false}
              value={phoneNumber}
              allowLeadingZero={false}
              maxLength={13}
              onChange={(v) => setValue("phoneNumber", v, { shouldValidate: true })}
            />
            <MainButton
              type="submit"
              disabled={!isValid}
              loading={loading}
            >
              Selanjutnya
            </MainButton>
          </form>
        </div>
      </div>
    </div>
  )
}

export default InputPhone;
