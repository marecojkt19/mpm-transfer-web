import React, { useEffect, useMemo, useState } from 'react'
import InputPhone from './InputPhone';
import PhoneVerification from './PhoneVerification';

const PhoneNumber = () => {
  const [subStep, setSubStep] = useState(1);
  const [data, setData] = useState();

  const view = useMemo(() => {
    if (subStep === 1) return <InputPhone setSubStep={setSubStep} setData={setData} />
    else if (subStep === 2) return <PhoneVerification setSubStep={setSubStep} data={data} />
    return null
  }, [subStep, data])

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return view
}

export default PhoneNumber
