import { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Warning2 } from "../../../../../assets/icons";
import userVerificationService from "../../../../../axios/services/userVerificationService";
import Button from "../../../../../components/Button";
import MainButton from "../../../../../components/Form/MainButton";
import OTPInput from "../../../../../components/Form/OTPInput";
import Modal from "../../../../../components/ModalNew";
import { verifyAccountActions } from "../../../../../redux/actions/verifyAccountAction";
import timeConvert from "../../../../../utils/helpers/timeConverter";
import styles from './phoneVerification.module.scss'
import { decryptCode } from "../../../../../utils/helpers/decryptCode";

const PhoneVerification = ({
  data,
  setSubStep
}) => {
  const dispatch = useDispatch();
  const [value, setValue] = useState('')
  const [error, setError] = useState('')
  const [loading, setLoading] = useState(false);
  const { signature } = useSelector(state => state.verifyAccount);
  const [showConfirm, setShowConfirm] = useState(false);

  const [resendTimer, setResendTimer] = useState(120)
  const [expiredCode, setExpiredCode] = useState(300)
  const [resend, setResend] = useState(true)

  const validateOtp = useCallback(() => {
    setLoading(true)
    userVerificationService.validateOtp({
      otp: value
    }, signature)
      .then(({ data: { signature, code } }) => {
        if (decryptCode(code) === '200') {
          dispatch(verifyAccountActions.setSignature(signature))
          dispatch(verifyAccountActions.setStep(1))
        }
      })
      .catch(err => setError(err.response.data.message))
      .finally(() => setLoading(false))
  }, [dispatch, value, signature])

  const resendOTP = () => {
    userVerificationService.requestOtp({
      phoneNumber: data.phoneNumber
    })
      .then(({ data: { signature } }) => {
        dispatch(verifyAccountActions.setSignature(signature));
        setResend(true);
        setResendTimer(120);
        setExpiredCode(300);
      })
      .catch(({ response: { data: { message } } }) => setError(message))
  }

  // useEffect(() => {
  //   if (value.length === 6) validateOtp()
  // }, [value, validateOtp])

  useEffect(() => {
    const tryAgain = setInterval(() => {
      if (resendTimer) setResendTimer(prev => prev - 1)
      else {
        clearInterval(tryAgain)
        setResend(false)
      }
    }, 1000);
    return () => {
      clearInterval(tryAgain)
    }
  }, [resendTimer, setResendTimer, setResend])

  useEffect(() => {
    const tryAgain = setInterval(() => {
      if (expiredCode) setExpiredCode(prev => prev - 1)
      else {
        clearInterval(tryAgain)
      }
    }, 1000);
    return () => {
      clearInterval(tryAgain)
    }
  }, [expiredCode, setExpiredCode, setResend])

  return (
    <>
      <div className={styles.phoneVerification}>
        <div className={styles.card}>
          <div className={styles.wrapper}>
            <div className={styles.headingText}>
              <h5>Verifikasi Nomor HP</h5>
              <p>Verifikasi nomor HP untuk meningkatkan keamanan transaksi Anda</p>
            </div>
            <div className={styles.image}>
              <img
                src="/assets/media/others/otp-phone.png"
                alt=""
              />
            </div>
            <div className={styles.note}>
              <span>
                Kode verifikasi telah dikirim via SMS ke {data.phoneNumber} masukkan 6 digit kode verifikasi yang Anda terima
              </span>
            </div>
            {!expiredCode &&
              <div className={styles.resendAlert}>
                <Warning2 />
                <div className={styles.message}>
                  <p>Waktu verifikasi nomor HP Anda telah habis, silakan kirim ulang kode verifikasi HP.</p>
                </div>
              </div>
            }
            <div className={styles.input}>
              <OTPInput
                length={6}
                numberOnly
                autoFocus
                value={value}
                onChange={v => {
                  if (error) setError('')
                  setValue(v)
                }}
                error={error}
              />
            </div>
            <div className={styles.buttonWrapper}>
              <MainButton
                disabled={!(value.length === 6)}
                loading={loading}
                onClick={validateOtp}
              >
                Selanjutnya
              </MainButton>
            </div>
            <div className="mb-8">
              {resendTimer ?
                <div className={styles.resend}>
                  <span>Kirim ulang dalam <b>{resend && ` (${timeConvert(resendTimer)})`}</b></span>
                </div>
                :
                <div className={styles.timer}>
                  <span>Belum menerima kode verifikasi?</span>
                  <Button
                    onClick={resendOTP}
                  >
                    Kirim Ulang
                  </Button>
                </div>
              }
            </div>
            <div className={styles.timer}>
              <span>Bukan nomor HP Anda?</span>
              <Button
                onClick={() => setShowConfirm(true)}
              >
                Ubah Nomor
              </Button>
            </div>
          </div>
        </div>
      </div>
      <Modal in={showConfirm} onClose={() => setShowConfirm(false)}>
        <div className={styles.modalWrapper}>
          <div className={styles.modalCard}>
            <div className={styles.heading}>
              <h1>Ubah Nomor HP</h1>
            </div>
            <div className={styles.modalLine}></div>
            <div className={styles.infoWrapper}>
              <p>Apakah Anda yakin ingin mengubah nomor HP Anda?</p>
            </div>
            <div className={styles.buttons}>
              <MainButton
                variant="secondary"
                onClick={() => setShowConfirm(false)}
              >
                Batalkan
              </MainButton>
              <MainButton
                onClick={() => setSubStep(1)}
              >
                Ya, Lanjutkan
              </MainButton>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default PhoneVerification;
