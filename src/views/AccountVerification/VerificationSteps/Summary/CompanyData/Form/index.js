import styles from '../../form.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import TextField from '../../../../../../components/Form/TextField';
import SelectField from '../../../../../../components/Form/SelectField';
import MainButton from '../../../../../../components/Form/MainButton';
import commonService from '../../../../../../axios/services/commonService';
import accountVerificationServices from '../../../../../../axios/services/accountVerificationServices';
import { authActions } from '../../../../../../redux/actions/authActions';
import { useToasts } from 'react-toast-notifications';
import useAsync from '../../../../../../components/hooks/useAsync';

const schema = yup.object().shape({
  companyName: yup.string().required('Nama Perusahaan tidak boleh kosong'),
  productDesc: yup.string().required('Produk/Jasa tidak boleh kosong'),
  nib: yup.string().min(13, "Minimal 13 karakter").required('NIB tidak boleh kosong'),
  npwp: yup.string().min(15, "Minimal 15 karakter").required('NPWP tidak boleh kosong'),
  niu: yup.string(),
  companyAddress: yup.string().required('Alamat Perusahaan tidak boleh kosong'),
  companyProvince: yup.string().required('Provinsi Perusahaan tidak boleh kosong'),
  companyCity: yup.string().required('Kota Perusahaan tidak boleh kosong'),
  companyPostalCode: yup.string().required('Kode pos Perusahaan tidak boleh kosong'),
})

const formatterRequired = (text) => {
  return <>{text}<span style={{ color: "#E11900" }}>*</span></>
}

const Form = ({
  setIsEdit,
  onSuccess
}) => {
  const [isGettingCities, setIsGettingCities] = useState(false);
  const [specifyCities, setSpecifyCities] = useState([]);

  const { user, company } = useSelector(state => state.auth);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const { addToast } = useToasts();
  const { register, unregister, watch, handleSubmit, formState: { isDirty }, errors, setValue } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      ...user,
      ...company,
      dateOfBirth: user?.dateOfBirth ? new Date(user?.dateOfBirth) : new Date("1995-01-01"),
      occupation: user?.occupation,
      companyName: company?.name,
      productDesc: company?.product,
      usagePurposes: company?.usagePurposes,
      bank: company?.bankAccount?.bankName,
      bankBranch: company?.bankAccount?.branch,
      bankAccountNumber: company?.bankAccount?.accountNumber,
      niu: company?.niu,

      companyProvince: company?.province,
      companyProvinceCode: company?.province.split("|")[1],
      companyCity: company?.addressCity,
      companyPostalCode: company?.postalCode,
    }
  });

  const {
    nib,
    npwp,
    companyPostalCode,
    companyProvinceCode,
    companyCity
  } = watch([
    "nib",
    "npwp",
    "companyPostalCode",
    "companyProvinceCode",
    "companyCity"
  ])

  const {
    pending: isGettingProvince,
    value: { data: { provinces: provinceList = [] } = {} } = {}
  } = useAsync(commonService.province, true);

  const provinces = useMemo(() => provinceList.map(el => ({ label: el.province, value: el.provinceCode })), [provinceList]);

  const onSubmit = (values) => {
    if (isDirty) {
      setLoading(true)
      const updatedData = {
        user: {
          ownerName: user.ownerName,
          identificationType: user.identificationType,
          identificationNumber: user.identificationNumber,
          placeOfBirth: user.placeOfBirth,
          dateOfBirth: user.dateOfBirth,
          occupation: user.occupation,
          city: user.city,
          address: user.address,
        },
        company: {
          name: values.companyName,
          product: values.productDesc,
          nib: values.nib,
          npwp: values.npwp,
          niu: values.niu,
          companyAddress: values.companyAddress,

          addressCity: values.companyCity,
          province: values.companyProvince,
          postalCode: values.companyPostalCode,
          transactionMethod: company?.transactionMethod,
          usagePurposes: [...company?.usagePurposes],
          otherPurpose: company?.otherPurpose ?? "",
          bankAccount: { ...company?.bankAccount }
        }
      }
      accountVerificationServices.dataVerification(updatedData, user._id)
        .then(() => {
          dispatch(authActions.requestUser())
          onSuccess()
          setIsEdit(false)
        })
        .catch((err) => {
          setIsEdit(false)
          if (err.response) {
            if (err.response.body) {
              addToast(err.response.body.message, { appearance: 'danger' });
            }
            if (err.response.data) {
              if (typeof err.response.data.message === "string") {
                addToast(err.response.data.message, { appearance: 'danger' });
              } else {
                for (const fields of err.response.data.message) {
                  for (const message of Object.keys(fields.constraints)) {
                    addToast(fields.constraints[message], { appearance: 'danger' });
                  }
                }
              }
            }
          } else {
            addToast(err, { appearance: 'danger' });
          }
        })
        .finally(() => setLoading(false))
    } else {
      setIsEdit(false)
    }
  }
  const getSpecifyCities = (code) => {
    setIsGettingCities(true)
    commonService.cities(code)
      .then(({ data }) => {
        const citiesData = data?.cities.map((v) => ({ label: v?.location, value: v?.code }))
        setSpecifyCities(citiesData)
      })
      .catch(() => { })
      .finally(() => setIsGettingCities(false))
  }

  useEffect(() => {
    if (companyProvinceCode) {
      getSpecifyCities(companyProvinceCode)
    }
  }, [companyProvinceCode])

  useEffect(() => {
    register("nib");
    register("npwp");
    register("niu");
    register("companyCity");
    register("companyProvince");
    register("companyPostalCode");
    register("companyProvinceCode");

    return () => {
      unregister("nib");
      unregister("npwp");
      unregister("niu");
      unregister("companyCity");
      unregister("companyProvince");
      unregister("companyPostalCode");
      unregister("companyProvinceCode");
    }
  }, [register, unregister]);

  return (
    <div className={styles.form}>
      <form onSubmit={handleSubmit(onSubmit)} autoComplete="nope">
        <div className={styles.formWrapper}>
          <TextField
            ref={register}
            name="companyName"
            label={formatterRequired("Nama Perusahaan")}
            placeholder="Masukkan Nama Perusahaan"
            error={errors.companyName}
            helperText={errors.companyName?.message}
          />
          <TextField
            ref={register}
            name="productDesc"
            label={formatterRequired("Produk/Jasa Perusahaan")}
            placeholder="Masukkan produk atau jasa"
            error={errors.productDesc}
            helperText={errors.productDesc?.message}
          />

          <TextField
            name="nib"
            label={formatterRequired("NIB (Nomor Induk Berusaha)")}
            placeholder="Masukkan NIB"
            allowDot={false}
            error={errors.nib}
            helperText={errors.nib?.message}
            value={nib}
            format={Number}
            allowLeadingZero={true}
            onChange={(v) => setValue("nib", v, { shouldValidate: true, shouldDirty: true })}
            maxLength={13}
          />
          {/* <TextField
            ref={register}
            name="niu"
            label="Nomor Izin Usaha (Optional)"
            placeholder="Masukkan nomor izin usaha"
            additionalLabel="Dari Bank Indonesia"
            error={errors.niu}
            helperText={errors.niu?.message}
          /> */}
          <TextField
            name="npwp"
            label={formatterRequired("NPWP Perusahaan")}
            placeholder="Nomor NPWP perusahaan"
            error={errors.npwp}
            allowDot={false}
            helperText={errors.npwp?.message}
            value={npwp}
            format={Number}
            allowLeadingZero
            onChange={(v) => setValue("npwp", v, { shouldValidate: true, shouldDirty: true })}
            maxLength={16}
          />
          <div className={styles.inputGroup}>
            <div className={styles.item}>
              <SelectField
                options={provinces}
                searchable
                label={formatterRequired("Provinsi")}
                placeholder="Pilih Provinsi"
                name="companyProvince"
                value={companyProvinceCode}
                loading={isGettingProvince}
                error={errors.companyProvince}
                helperText={errors.companyProvince?.message}
                onChange={(v) => {
                  setValue("companyProvince", `${v?.label}|${v?.value}`, { shouldValidate: true, shouldDirty: true });
                  setValue("companyProvinceCode", v?.value);
                  getSpecifyCities(v?.value)
                }}
              />
            </div>
            <div className={styles.item}>
              <SelectField
                options={specifyCities}
                searchable
                label={formatterRequired("Kota/Kabupaten")}
                placeholder="Pilih Kota/Kabupaten"
                name="companyCity"
                value={companyCity}
                loading={isGettingCities}
                error={errors.companyCity}
                disabled={!companyProvinceCode}
                helperText={errors.companyCity?.message}
                onChange={(v) => {
                  setValue("companyCity", v?.value, { shouldValidate: true, shouldDirty: true })
                }}
              />
            </div>
            <div>
              <TextField
                name="companyPostalCode"
                label={formatterRequired("Kode Pos")}
                placeholder="Tulis Kode Pos"
                allowDot={false}
                error={errors.companyPostalCode}
                helperText={errors.companyPostalCode?.message}
                value={companyPostalCode}
                format={Number}
                allowLeadingZero
                onChange={(v) => setValue("companyPostalCode", v, { shouldValidate: true, shouldDirty: true })}
                maxLength={13}
              />
            </div>
          </div>
          <TextField
            ref={register}
            name="companyAddress"
            label={formatterRequired("Alamat Perusahaan")}
            placeholder="Alamat perusahaan saat ini (Contoh: Komplek Melati No. 11, Jl. Margonda Raya, Depok)"
            error={errors.companyAddress}
            helperText={errors.companyAddress?.message}
          />
        </div>
        <div className={styles.buttonWrapper}>
          <MainButton
            variant='plain'
            onClick={() => setIsEdit(false)}
          >
            Batal
          </MainButton>
          <MainButton
            variant='secondary'
            loading={loading}
            disabled={!isDirty}
          >
            Simpan
          </MainButton>
        </div>
      </form>
    </div>
  )
}

export default Form
