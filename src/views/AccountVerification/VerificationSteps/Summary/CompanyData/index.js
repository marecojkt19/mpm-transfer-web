import styles from '../summary.module.scss'
import { useSelector } from 'react-redux';
import MainButton from '../../../../../components/Form/MainButton';
import Form from './Form';
import { useState } from 'react';

const CompanyData = ({ loadSummary, company, pending }) => {
  const [isEdit, setIsEdit] = useState(false);
  const { user } = useSelector(state => state.auth);

  return !pending && (
    <div className={styles.summarySection}>
      <div className={styles.heading}>
        <h3>Data Pemilik Perusahaan</h3>
      </div>
      <div className={styles.card}>
        {isEdit ?
          <Form setIsEdit={setIsEdit} onSuccess={() => loadSummary(user?._id)} />
          :
          <>
            <div className={styles.content}>
              <div className={styles.field}>
                <h6>Nama Perusahaan</h6>
                <p>{company?.name}</p>
              </div>
              <div className={styles.field}>
                <h6>Produk/Jasa</h6>
                <p>{company?.product}</p>
              </div>
              <div className={styles.field}>
                <h6>NIB (Nomor Induk Berusaha)</h6>
                <p>{company?.nib}</p>
              </div>
              {/* <div className={styles.field}>
                <h6>Nomor Izin Usaha</h6>
                <p>{company?.niu || "-"}</p>
              </div> */}
              <div className={styles.field}>
                <h6>Nomor NPWP Perusahaan</h6>
                <p>{company?.npwp}</p>
              </div>
              <div className={styles.field}>
                <h6>Alamat Perusahaan</h6>
                <p>{company?.address}</p>
              </div>
            </div>
            <div className={styles.buttonWrapper}>
              <MainButton
                variant='secondary'
                onClick={() => setIsEdit(true)}
              >
                Ubah
              </MainButton>
            </div>
          </>
        }
      </div>
    </div>
  )
}

export default CompanyData
