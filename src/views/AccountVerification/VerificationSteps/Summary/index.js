import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import accountVerificationServices from '../../../../axios/services/accountVerificationServices'
import AgreementCheckbox from '../../../../components/Form/AgreementCheckbox'
import MainButton from '../../../../components/Form/MainButton'
import useAsync from '../../../../components/hooks/useAsync'
import Modal from '../../../../components/Modal'
import Bank from './Bank'
import CompanyData from './CompanyData'
import OwnerData from './OwnerData'
import styles from './summary.module.scss'
import TransferMethod from './TransferMethod'
import CompanyDocument from './Upload/CompanyDocument'
import OwnerDocument from './Upload/OwnerDocument'

const Summary = ({
  setPage
}) => {
  const { user, signature } = useSelector(state => state.auth);
  const [showConfirm, setShowConfirm] = useState(false);
  const [nextLoading, setNextLoading] = useState(false);
  const [agree, setAgree] = useState(false);

  const handleRequest = () => {
    setNextLoading(false)
    setPage(3)
    const direcetToStep = {
      step: "stepFour",
      value: true
    }
    accountVerificationServices.changeStep(direcetToStep, user._id, signature)
      .catch(() => { })
      .finally(() => {
        setNextLoading(false)
        setShowConfirm(false)
      })
  }

  const {
    execute: loadSummary,
    pending,
    value: { data: { docs, user: userData, company: companyData } = {} } = {}
  } = useAsync(accountVerificationServices.getSummary, { id: user?._id, signature });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <div className={styles.summary}>
        <div className={styles.heading}>
          <h5>Ringkasan Data Verifikasi</h5>
          <p>Berikut adalah ringkasan data yang akan diverifikasi. Pastikan data dan dokumen sudah benar.</p>
        </div>
        <div className={styles.container}>
          <div className={styles.wrapper}>
            <OwnerData userData={userData} loadSummary={loadSummary} pending={pending} />
            <CompanyData company={companyData} loadSummary={loadSummary} pending={pending} />
            <TransferMethod company={companyData} loadSummary={loadSummary} pending={pending} />
            <Bank company={companyData} loadSummary={loadSummary} pending={pending} />
            {docs?.thirdPartyName &&
              <div className={styles.summarySection}>
                <div className={styles.heading}>
                  <h3>Data Representatif/PIC</h3>
                </div>
                <div className={styles.card}>
                  <div className={styles.content}>
                    <div className={styles.field}>
                      <h6>Nama Representatif/PIC</h6>
                      <p>{docs?.thirdPartyName}</p>
                    </div>
                  </div>
                </div>
              </div>
            }
            <OwnerDocument loadSummary={loadSummary} pending={pending} docs={docs} />
            <CompanyDocument docs={docs} loadSummary={loadSummary} pending={pending} />
            <div className={styles.line}></div>
            <div className={styles.agreement}>
              <AgreementCheckbox
                onAgree={() => setAgree(!agree)}
                isAgree={agree}
                label="Saya memastikan kebenaran data dan dokumen yang telah diunggah sesuai."
              />
            </div>
            <div className={styles.nextButton}>
              <MainButton
                onClick={() => setShowConfirm(true)}
                disabled={!agree}
              >
                Selanjutnya
              </MainButton>
            </div>
          </div>
        </div>
      </div>
      <Modal in={showConfirm} onClose={() => setShowConfirm(false)}>
        <div className={styles.modalWrapper}>
          <div className={styles.modalCard}>
            <div className={styles.heading}>
              <h1>Yakin data yang Anda isi sudah benar?</h1>
            </div>
            <div className={styles.modalLine}></div>
            <div className={styles.infoWrapper}>
              <p>Setelah ini Anda tidak dapat mengubah data lagi.</p>
              <p>Pastikan kembali data yang telah Anda input.</p>
            </div>
            <div className={styles.buttons}>
              <MainButton
                variant="secondary"
                onClick={() => setShowConfirm(false)}
              >
                Kembali
              </MainButton>
              <MainButton
                onClick={handleRequest}
                loading={nextLoading}
              >
                Ya, Lanjutkan
              </MainButton>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default Summary
