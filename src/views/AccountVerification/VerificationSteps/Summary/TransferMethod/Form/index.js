import styles from '../../form.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import TextField from '../../../../../../components/Form/TextField';
import SelectField from '../../../../../../components/Form/SelectField';
import MainButton from '../../../../../../components/Form/MainButton';
import accountVerificationServices from '../../../../../../axios/services/accountVerificationServices';
import { authActions } from '../../../../../../redux/actions/authActions';
import { useToasts } from 'react-toast-notifications';
import useAsync from '../../../../../../components/hooks/useAsync';
import RadioItem from '../../../../../../components/Form/Radio/RadioItem';
import RadioGroup from '../../../../../../components/Form/Radio/RadioGroup';
import optionsService from '../../../../../../axios/services/optionsService';
import { TransferMethod } from '../../../../../../utils/enums/transferMethod';

const schema = yup.object().shape({
  usagePurposes: yup.array().max(2, "Maximal 2 pilihan").required('Tujuan Penggunaan tidak boleh kosong'),
  transactionMethod: yup.string().required('Metode Transfer tidak boleh kosong'),
  otherPurpose: yup.string()
    .when('usagePurposes', {
      is: (val) => val.length ? val.includes("OTHER_PURPOSE") : false,
      then: yup.string().max(225, "Maksimal 225 karakter").required("Tujuan Lainnya tidak boleh kosong!"),
      otherwise: yup.string()
    })
})

const formatterRequired = (text) => {
  return <>{text}<span style={{ color: "#E11900" }}>*</span></>
}

const Form = ({
  setIsEdit,
  onSuccess
}) => {
  const { user, company } = useSelector(state => state.auth);
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();
  const { addToast } = useToasts();
  const { register, unregister, watch, handleSubmit, formState: { isDirty }, errors, setValue } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      ...user,
      ...company,
      dateOfBirth: user?.dateOfBirth ? new Date(user?.dateOfBirth) : new Date("1995-01-01"),
      occupation: user?.occupation,
      companyName: company?.name,
      productDesc: company?.product,
      usagePurposes: company?.usagePurposes,
      bank: company?.bankAccount?.bankName,
      bankBranch: company?.bankAccount?.branch,
      bankAccountNumber: company?.bankAccount?.accountNumber,
      niu: company?.niu,

      companyProvince: company?.province,
      companyProvinceCode: company?.province.split("|")[1],
      companyCity: company?.addressCity,
      companyPostalCode: company?.postalCode,
    }
  });

  const {
    usagePurposes,
    transactionMethod
  } = watch([
    "usagePurposes",
    "transactionMethod"
  ])

  const {
    isGettingUsagePurpose,
    value: { data: { purposes = [] } = {} } = {}
  } = useAsync(optionsService.usagePurposeList, {});

  const onSubmit = (values) => {
    if (isDirty) {
      setLoading(true)
      const data = {
        user: {
          ownerName: user.ownerName,
          identificationType: user.identificationType,
          identificationNumber: user.identificationNumber,
          placeOfBirth: user.placeOfBirth,
          dateOfBirth: user.dateOfBirth,
          occupation: user.occupation,
          city: user.city,
          address: user.address,
        },
        company: {
          name: company.name,
          product: company.product,
          nib: company.nib,
          npwp: company.npwp,
          niu: company.niu,
          companyAddress: company.companyAddress,

          addressCity: company.addressCity,
          province: company.province,
          postalCode: company.postalCode,
          transactionMethod: values.transactionMethod,
          usagePurposes: [...values.usagePurposes],
          otherPurpose: values.otherPurpose ?? company.otherPurpose ?? "",
          bankAccount: { ...company?.bankAccount }
        }
      }

      accountVerificationServices.dataVerification(data, user._id)
        .then(() => {
          dispatch(authActions.requestUser())
          onSuccess()
          setIsEdit(false)
        })
        .catch((err) => {
          setIsEdit(false)
          if (err.response) {
            if (err.response.body) {
              addToast(err.response.body.message, { appearance: 'danger' });
            }
            if (err.response.data) {
              if (typeof err.response.data.message === "string") {
                addToast(err.response.data.message, { appearance: 'danger' });
              } else {
                for (const fields of err.response.data.message) {
                  for (const message of Object.keys(fields.constraints)) {
                    addToast(fields.constraints[message], { appearance: 'danger' });
                  }
                }
              }
            }
          } else {
            addToast(err, { appearance: 'danger' });
          }
        })
        .finally(() => setLoading(false))
    } else {
      setIsEdit(false)
    }
  }

  useEffect(() => {
    register("usagePurposes");
    register("transactionMethod");

    return () => {
      unregister("usagePurposes");
      unregister("transactionMethod");
    }
  }, [register, unregister]);

  return (
    <div className={styles.form}>
      <form onSubmit={handleSubmit(onSubmit)} autoComplete="nope">
        <div className={styles.formWrapper}>
          <RadioGroup
            name="transactionMethod"
            label={formatterRequired("Metode transfer yang digunakan saat ini")}
            error={errors.transactionMethod}
            helperText={errors.transactionMethod?.message}
            value={transactionMethod}
            onChange={e => setValue('transactionMethod', e.target.value, { shouldValidate: true, shouldDirty: true })}
          >
            <RadioItem
              label="Menggunakan Internet Banking"
              value={TransferMethod.INTERNET_BANKING}
            />
            <RadioItem
              label="Menggunakan layanan pengiriman uang pihak ketiga"
              value={TransferMethod.THIRD_PARTY}
            />
            <RadioItem
              label="Bekerja sama dengan host to host dengan bank"
              value={TransferMethod.H2H}
            />
          </RadioGroup>

          <SelectField
            options={purposes}
            loading={isGettingUsagePurpose}
            searchable
            label={formatterRequired("Tujuan pengunaan Dipay Disbursement")}
            placeholder="Maksimal 2 pilihan atau tujuan lainnya (sebutkan)"
            name="usagePurposes"
            withCheckbox={true}
            max={2}
            value={usagePurposes}
            error={errors.usagePurposes}
            helperText={errors.usagePurposes?.message}
            onChange={(v) => {
              setValue("usagePurposes", !v?.length ? '' : v.map(el => el.value), { shouldValidate: true, shouldDirty: true })
            }}
          />
          {usagePurposes?.includes("OTHER_PURPOSE") &&
            <TextField
              ref={register}
              name="otherPurpose"
              label={formatterRequired("Tujuan Lainnya (Sebutkan)")}
              placeholder="Maksimal 225 karakter"
              error={errors.otherPurpose}
              helperText={errors.otherPurpose?.message}
              multiline
              maxLength={225}
            />
          }
        </div>
        <div className={styles.buttonWrapper}>
          <MainButton
            variant='plain'
            onClick={() => setIsEdit(false)}
          >
            Batal
          </MainButton>
          <MainButton
            variant='secondary'
            loading={loading}
            disabled={!isDirty}
          >
            Simpan
          </MainButton>
        </div>
      </form>
    </div>
  )
}

export default Form
