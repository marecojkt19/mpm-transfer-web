import styles from '../summary.module.scss'
import { useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import MainButton from '../../../../../components/Form/MainButton';
import Form from './Form';

const TransferMethod = ({ company, loadSummary, pending }) => {
  const [isEdit, setIsEdit] = useState(false);
  const { user } = useSelector(state => state.auth);

  const usagePurposes = useMemo(() => {
    if (company) {
      if (company?.otherPurpose) {
        return [
          ...company?.usagePurposes,
          company?.otherPurpose
        ]
      }

      return company?.usagePurposes
    }

    return []
  }, [company])

  return !pending && (
    <div className={styles.summarySection}>
      <div className={styles.card}>
        {isEdit ?
          <Form setIsEdit={setIsEdit} onSuccess={() => loadSummary(user?._id)} />
          :
          <>
            <div className={styles.content}>
              <div className={styles.field}>
                <h6>Metode Transfer Saat Ini</h6>
                <p>{company?.transactionMethod}</p>
              </div>
              <div className={styles.field}>
                <h6>Tujuan Penggunaan</h6>
                {usagePurposes.join(", ")}
              </div>
            </div>
            <div className={styles.buttonWrapper}>
              <MainButton
                variant='secondary'
                onClick={() => setIsEdit(true)}
              >
                Ubah
              </MainButton>
            </div>
          </>
        }
      </div>
    </div>
  )
}

export default TransferMethod
