import { format } from 'date-fns'
import { id } from 'date-fns/esm/locale'
import styles from '../summary.module.scss'
import { useSelector } from 'react-redux';
import MainButton from '../../../../../components/Form/MainButton';
import Form from './Form';
import { useState } from 'react';

const OwnerData = ({ userData, loadSummary, pending }) => {
  const [isEdit, setIsEdit] = useState(false);
  const { user } = useSelector(state => state.auth);

  return !pending && (
    <div className={styles.summarySection}>
      <div className={styles.heading}>
        <h3>Data Pemilik Perusahaan</h3>
      </div>
      <div className={styles.card}>
        {isEdit ?
          <Form setIsEdit={setIsEdit} onSuccess={() => loadSummary(user?._id)} />
          :
          <>
            <div className={styles.content}>
              <div className={styles.field}>
                <h6>Nomor Identitas</h6>
                <p>{userData?.identificationType} - {userData?.identificationNumber}</p>
              </div>
              <div className={styles.field}>
                <h6>Nama Sesuai Dokumen Identitas</h6>
                <p>{userData?.ownerName}</p>
              </div>
              <div className={styles.fieldGroup}>
                <div className={styles.field}>
                  <h6>Tempat Lahir</h6>
                  <p>{userData?.birthPlace}</p>
                </div>
                <div className={styles.field}>
                  <h6>Tanggal Lahir</h6>
                  <p>{userData?.birthDate && format(new Date(userData?.birthDate), "dd/MM/yyyy", { locale: id })}</p>
                </div>
              </div>
              <div className={styles.field}>
                <h6>Kota/Kabupaten Tempat Tinggal Saat ini</h6>
                <p>{userData?.city}</p>
              </div>
              <div className={styles.field}>
                <h6>Alamat Terkini</h6>
                <p>{userData?.address}</p>
              </div>
            </div>
            <div className={styles.buttonWrapper}>
              <MainButton
                variant='secondary'
                onClick={() => setIsEdit(true)}
              >
                Ubah
              </MainButton>
            </div>
          </>
        }
      </div>
    </div>
  )
}

export default OwnerData
