import styles from '../../form.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import TextField from '../../../../../../components/Form/TextField';
import SelectField from '../../../../../../components/Form/SelectField';
import DatePicker from '../../../../../../components/Form/Dates/DatePicker';
import MainButton from '../../../../../../components/Form/MainButton';
import useAsync from '../../../../../../components/hooks/useAsync';
import commonService from '../../../../../../axios/services/commonService';
import optionsService from '../../../../../../axios/services/optionsService';
import accountVerificationServices from '../../../../../../axios/services/accountVerificationServices';
import { authActions } from '../../../../../../redux/actions/authActions';
import { useToasts } from 'react-toast-notifications';

const schema = yup.object().shape({
  ownerName: yup.string().required('Nama tidak boleh kosong'),
  identificationType: yup.string().required('Jenis Identitas tidak boleh kosong'),
  identificationNumber: yup.string()
    .required('Nomor Identitas tidak boleh kosong')
    .matches(/^[a-zA-Z0-9]+$/g, "Tidak boleh mengandung simbol")
    .when('identificationType', {
      is: (val) => val === "Paspor",
      then: yup.string()
        .min(8, "Minimal 8 karakter")
        .max(8, "Maximum 8 karakter"),
      otherwise: yup.string()
        .min(16, "Minimal 16 karakter")
        .max(16, "Maximum 16 karakter")
    }),
  placeOfBirth: yup.string().required('Tempat lahir tidak boleh kosong'),
  dateOfBirth: yup.string().required("Tanggal lahir tidak boleh kosong")
    .test({
      message: "Umur tidak boleh dibawah 17 tahun",
      test: (value) => {
        return !(Math.floor((new Date() - new Date(value).getTime()) / 3.15576e+10) <= 17)
      }
    }),
  occupation: yup.string().required('Pekerjaan tidak boleh kosong'),
  city: yup.string().required('Kota/Kabupaten tinggal tidak boleh kosong'),
  address: yup.string().required('Alamat tidak boleh kosong')
})

const formatterRequired = (text) => {
  return <>{text}<span style={{ color: "#E11900" }}>*</span></>
}

const Form = ({
  setIsEdit,
  onSuccess
}) => {
  const [cities, setCities] = useState([]);
  const [isGettingCities, setIsGettingCities] = useState(false);
  const { user, company } = useSelector(state => state.auth);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const { addToast } = useToasts();
  const { register, unregister, watch, handleSubmit, formState: { isDirty }, errors, setValue } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      ...user,
      ...company,
      dateOfBirth: user?.dateOfBirth ? new Date(user?.dateOfBirth) : new Date("1995-01-01"),
      occupation: user?.occupation,
      companyName: company?.name,
      productDesc: company?.product,
      usagePurposes: company?.usagePurposes,
      bank: company?.bankAccount?.bankName,
      bankBranch: company?.bankAccount?.branch,
      bankAccountNumber: company?.bankAccount?.accountNumber,
      niu: company?.niu,

      companyProvince: company?.province,
      companyProvinceCode: company?.province.split("|")[1],
      companyCity: company?.addressCity,
      companyPostalCode: company?.postalCode,
    }
  });

  const {
    identificationType,
    dateOfBirth,
    placeOfBirth,
    occupation,
    city,
    identificationNumber
  } = watch([
    "identificationType",
    "dateOfBirth",
    "placeOfBirth",
    "occupation",
    "city",
    "identificationNumber"
  ])

  const getCities = useCallback(() => {
    setIsGettingCities(true)
    commonService.cities()
      .then(({ data }) => {
        const citiesData = data?.cities.map((v) => ({ label: v?.location, value: v?.code }))
        setCities(citiesData)
      })
      .catch(() => { })
      .finally(() => setIsGettingCities(false))
  }, []);

  const {
    isGettingOccupations,
    value: { data: { occupations = [] } = {} } = {}
  } = useAsync(optionsService.occupationList, {});

  const onSubmit = (values) => {
    if (isDirty) {
      setLoading(true)
      const updatedData = {
        user: {
          ownerName: values.ownerName,
          identificationType: values.identificationType,
          identificationNumber: values.identificationNumber,
          placeOfBirth: values.placeOfBirth,
          dateOfBirth: values.dateOfBirth,
          occupation: values.occupation,
          city: values.city,
          address: values.address,
        },
        company: {
          name: company?.name,
          product: company?.product,
          nib: company?.nib,
          npwp: company?.npwp,
          niu: company?.niu,
          companyAddress: company?.companyAddress,
          addressCity: company?.addressCity,
          province: company?.province,
          postalCode: company?.postalCode,
          transactionMethod: company?.transactionMethod,
          usagePurposes: [...company?.usagePurposes],
          otherPurpose: company?.otherPurpose ?? "",
          bankAccount: { ...company?.bankAccount }
        }
      }
      accountVerificationServices.dataVerification(updatedData, user._id)
        .then(() => {
          dispatch(authActions.requestUser())
          onSuccess()
          setIsEdit(false)
        })
        .catch((err) => {
          setIsEdit(false)
          if (err.response) {
            if (err.response.body) {
              addToast(err.response.body.message, { appearance: 'danger' });
            }
            if (err.response.data) {
              if (typeof err.response.data.message === "string") {
                addToast(err.response.data.message, { appearance: 'danger' });
              } else {
                for (const fields of err.response.data.message) {
                  for (const message of Object.keys(fields.constraints)) {
                    addToast(fields.constraints[message], { appearance: 'danger' });
                  }
                }
              }
            }
          } else {
            addToast(err, { appearance: 'danger' });
          }
        })
        .finally(() => setLoading(false))
    } else {
      setIsEdit(false)
    }
  }

  useEffect(() => {
    getCities()
  }, [getCities])

  useEffect(() => {
    register("identificationType");
    register("identificationNumber");
    register("dateOfBirth");
    register("placeOfBirth");
    register("occupation");
    register("city");

    return () => {
      unregister("identificationType");
      unregister("identificationNumber");
      unregister("dateOfBirth");
      unregister("placeOfBirth");
      unregister("occupation");
      unregister("city");
    }
  }, [register, unregister]);

  return (
    <div className={styles.form}>
      <form onSubmit={handleSubmit(onSubmit)} autoComplete="nope">
        <div className={styles.formWrapper}>
          <div className={styles.formGroup}>
            <div className={styles.label}>
              <p>{formatterRequired("Nomor Identitas Pemilik Perusahaan")}</p>
            </div>
            <div className={styles.inputGroup}>
              <div className={styles.idType}>
                <SelectField
                  options={[
                    {
                      value: "KTP",
                      label: "KTP"
                    }
                  ]}
                  placeholder="Pilih Identitas"
                  name="identificationType"
                  value={identificationType}
                  error={errors.identificationType}
                  helperText={errors.identificationType?.message}
                  onChange={(v) => {
                    setValue("identificationType", v?.value, { shouldValidate: true, shouldDirty: true })
                    setValue("identificationNumber", "", { shouldValidate: true, shouldDirty: true })
                  }}
                />
              </div>
              <div className={styles.idNumber}>
                <TextField
                  disabled={!identificationType}
                  name="identificationNumber"
                  placeholder="Masukkan nomor identitas di sini"
                  error={errors.identificationNumber}
                  helperText={errors.identificationNumber?.message}
                  value={identificationNumber}
                  format={identificationType === "Paspor" ? null : Number}
                  allowDot={false}
                  onChange={(v) => {
                    if (identificationType === "Paspor") {
                      setValue("identificationNumber", v.target.value, { shouldValidate: true, shouldDirty: true })
                    } else {
                      setValue("identificationNumber", v, { shouldValidate: true, shouldDirty: true })
                    }
                  }}
                  maxLength={identificationType === "Paspor" ? 8 : 16}
                />
              </div>
            </div>
          </div>
          <TextField
            ref={register}
            name="ownerName"
            label={formatterRequired("Nama Pemilik Perusahaan Sesuai Dokumen Identitas")}
            allowNumeric={false}
            placeholder="Tulis nama perusahaan"
            error={errors.ownerName}
            helperText={errors.ownerName?.message}
          />
          <div className={styles.formGroup}>
            <div className={styles.inputGroup}>
              <div className={styles.placeOfBirth}>
                <SelectField
                  options={cities}
                  searchable
                  label={formatterRequired("Tempat Lahir")}
                  placeholder="Tempat lahir"
                  name="placeOfBirth"
                  value={placeOfBirth}
                  loading={isGettingCities}
                  error={errors.placeOfBirth}
                  helperText={errors.placeOfBirth?.message}
                  onChange={(v) => {
                    setValue("placeOfBirth", v?.value, { shouldValidate: true, shouldDirty: true })
                  }}
                />
              </div>
              <div className={styles.dateOfBirth}>
                <DatePicker
                  label={formatterRequired("Tanggal Lahir")}
                  value={dateOfBirth}
                  placeholder="DD/MM//YYYY"
                  onChange={v => setValue('dateOfBirth', v ? v : '', { shouldValidate: true, shouldDirty: true })}
                  error={errors.dateOfBirth}
                  helperText={errors.dateOfBirth?.message}
                />
              </div>
            </div>
          </div>
          <SelectField
            options={occupations}
            loading={isGettingOccupations}
            searchable
            label={formatterRequired("Pekerjaan")}
            placeholder="Pilih Pekerjaan"
            name="occupation"
            value={occupation}
            error={errors.occupation}
            helperText={errors.occupation?.message}
            onChange={(v) => {
              setValue("occupation", v?.value, { shouldValidate: true, shouldDirty: true })
            }}
          />
          <SelectField
            options={cities}
            loading={isGettingCities}
            searchable
            label={formatterRequired("Kota/Kabupaten Tempat Tinggal Saat Ini")}
            placeholder="Pilih kota/kabupaten"
            name="city"
            value={city}
            error={errors.city}
            helperText={errors.city?.message}
            onChange={(v) => {
              setValue("city", v?.value, { shouldValidate: true, shouldDirty: true })
            }}
          />
          <TextField
            ref={register}
            name="address"
            label={formatterRequired("Alamat Terkini")}
            placeholder="Alamat tinggal saat ini. Contoh: Komplek Melati No. 11, Jl. Margonda Raya, Depok"
            error={errors.address}
            helperText={errors.address?.message}
          />
        </div>
        <div className={styles.buttonWrapper}>
          <MainButton
            variant='plain'
            onClick={() => setIsEdit(false)}
          >
            Batal
          </MainButton>
          <MainButton
            variant='secondary'
            loading={loading}
            disabled={!isDirty}
          >
            Simpan
          </MainButton>
        </div>
      </form>
    </div>
  )
}

export default Form
