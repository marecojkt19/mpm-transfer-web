import styles from '../summary.module.scss'
import MainButton from '../../../../../components/Form/MainButton';
import TooltipTemplate from '../../../../../components/templates/TooltipTemplate2';
import { Info } from '../../../../../assets/icons';
import Form from './Form';
import { useState } from 'react';
import { useSelector } from 'react-redux';

const Bank = ({ loadSummary, company, pending }) => {
  const [tooltip, setTooltip] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const { user } = useSelector(state => state.auth);

  return !pending && (
    <div className={styles.summarySection}>
      <div className={styles.heading}>
        <div className={styles.headingWrapper}>
          <h3>Rekening Perusahaan</h3>
          <button type="button" onClick={() => setTooltip(true)}>
            <Info size={16} color="#276EF1" />
          </button>
          <TooltipTemplate
            onClose={() => setTooltip(false)}
            show={tooltip}
            heading="Nomor Rekening Perusahaan"
            content="Nomor Rekening Perusahaan digunakan untuk Transaksi Top Up Deposit dan Refund Dana Deposit."
          />
        </div>
      </div>
      <div className={styles.card}>
        {isEdit ?
          <Form setIsEdit={setIsEdit} onSuccess={() => loadSummary(user?._id)} />
          :
          <>
            <div className={styles.content}>
              <div className={styles.field}>
                <h6>Bank</h6>
                <p>{company?.bankAccount?.bankName}</p>
              </div>
              <div className={styles.field}>
                <h6>Cabang Bank</h6>
                <p>{company?.bankAccount?.branchName}</p>
              </div>
              <div className={styles.field}>
                <h6>Nomor Rekening</h6>
                <p>{company?.bankAccount?.accountNumber}</p>
              </div>
            </div>
            <div className={styles.buttonWrapper}>
              <MainButton
                variant='secondary'
                onClick={() => setIsEdit(true)}
              >
                Ubah
              </MainButton>
            </div>
          </>
        }
      </div>
    </div>
  )
}

export default Bank
