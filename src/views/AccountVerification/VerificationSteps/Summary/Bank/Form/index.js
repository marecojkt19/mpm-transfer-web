import styles from '../../form.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import TextField from '../../../../../../components/Form/TextField';
import SelectField from '../../../../../../components/Form/SelectField';
import MainButton from '../../../../../../components/Form/MainButton';
import accountVerificationServices from '../../../../../../axios/services/accountVerificationServices';
import { authActions } from '../../../../../../redux/actions/authActions';
import { useToasts } from 'react-toast-notifications';
import { Cancel, ChecklistOn } from '../../../../../../assets/icons';
import LoadingDots from '../../../../../../components/Loadings/LoadingDots';
import bankService from '../../../../../../axios/services/bankService';
import { BankOpts } from '../../../../../../utils/enums/bankTypes';
import pakaiClass from 'pakai-class';

const schema = yup.object().shape({
  bank: yup.string().required('Bank tidak boleh kosong'),
  bankAccountNumber: yup.string().required('Nomor Rekening tidak boleh kosong'),
})

const formatterRequired = (text) => {
  return <>{text}<span style={{ color: "#E11900" }}>*</span></>
}

const Form = ({
  setIsEdit,
  onSuccess
}) => {
  const { user, company } = useSelector(state => state.auth);
  const [loading, setLoading] = useState(false);

  const [isCheckingBankAccount, setIsCheckingBankAccount] = useState(false);
  const [isBankExist, setIsBankExist] = useState(false);
  const [bankUsername, setBankUsername] = useState(false);
  const [showCheckBankAlert, setShowCheckBankAlert] = useState(false);

  const dispatch = useDispatch();
  const { addToast } = useToasts();
  const { register, unregister, watch, handleSubmit, formState: { isDirty }, errors, setValue, setError } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      ...user,
      ...company,
      dateOfBirth: user?.dateOfBirth ? new Date(user?.dateOfBirth) : new Date("1995-01-01"),
      occupation: user?.occupation,
      companyName: company?.name,
      productDesc: company?.product,
      usagePurposes: company?.usagePurposes,
      bank: company?.bankAccount?.bankName,
      bankBranch: company?.bankAccount?.branch,
      bankAccountNumber: company?.bankAccount?.accountNumber,
      niu: company?.niu,

      companyProvince: company?.province,
      companyProvinceCode: company?.province.split("|")[1],
      companyCity: company?.addressCity,
      companyPostalCode: company?.postalCode,
    }
  });

  const {
    bankAccountNumber,
    bank
  } = watch([
    "bankAccountNumber",
    "bank"
  ])

  const checkAccountNumber = () => {
    setShowCheckBankAlert(true)
    const payload = {
      accountNumber: bankAccountNumber,
      bank: "BNI" // ------------------- Temporary Hardcoded ------------------- //
    }
    setIsCheckingBankAccount(true)
    bankService.checkAccountNumber(payload)
      .then(({ data }) => {
        setBankUsername(data?.name)
        setIsBankExist(true)
      })
      .catch((err) => {
        if (err.response) {
          if (err.response.body) {
            setBankUsername(err.response.body.message);
          }
          if (err.response.data) {
            if (typeof err.response.data.message === "string") {
              setBankUsername(err.response.data.message);
            } else {
              for (const fields of err.response.data.message) {
                for (const message of Object.keys(fields.constraints)) {
                  setBankUsername(fields.constraints[message]);
                }
              }
            }
          }
        } else {
          addToast(err, { appearance: 'danger' });
        }
      })
      .finally(() => setIsCheckingBankAccount(false))
  }

  const onSubmit = (values) => {
    if (isDirty) {
      if (isBankExist || Number(values.bankAccountNumber) === Number(company?.bankAccount?.accountNumber)) {
        setLoading(true)
        const data = {
          user: {
            ownerName: user.ownerName,
            identificationType: user.identificationType,
            identificationNumber: user.identificationNumber,
            placeOfBirth: user.placeOfBirth,
            dateOfBirth: user.dateOfBirth,
            occupation: user.occupation,
            city: user.city,
            address: user.address,
          },
          company: {
            name: company.name,
            product: company.product,
            nib: company.nib,
            npwp: company.npwp,
            niu: company.niu,
            companyAddress: company.companyAddress,

            addressCity: company.addressCity,
            province: company.province,
            postalCode: company.postalCode,
            transactionMethod: company.transactionMethod,
            usagePurposes: [...company.usagePurposes],
            otherPurpose: company.otherPurpose ?? "",
            bankAccount: {
              bankName: values.bank,
              branch: values?.bankBranch || "",
              accountNumber: values.bankAccountNumber
            }
          }
        }

        accountVerificationServices.dataVerification(data, user._id)
          .then(() => {
            dispatch(authActions.requestUser())
            onSuccess()
            setIsEdit(false)
          })
          .catch((err) => {
            setIsEdit(false)
            if (err.response) {
              if (err.response.body) {
                addToast(err.response.body.message, { appearance: 'danger' });
              }
              if (err.response.data) {
                if (typeof err.response.data.message === "string") {
                  addToast(err.response.data.message, { appearance: 'danger' });
                } else {
                  for (const fields of err.response.data.message) {
                    for (const message of Object.keys(fields.constraints)) {
                      addToast(fields.constraints[message], { appearance: 'danger' });
                    }
                  }
                }
              }
            } else {
              addToast(err, { appearance: 'danger' });
            }
          })
          .finally(() => setLoading(false))
      } else {
        setError("bankAccountNumber", { message: "Validasi pengguna rekening bank" })
      }
    } else {
      setIsEdit(false)
    }
  }

  useEffect(() => {
    register("bank");
    register("bankAccountNumber");

    return () => {
      unregister("bank");
      unregister("bankAccountNumber");
    }
  }, [register, unregister]);

  return (
    <div className={styles.form}>
      <form onSubmit={handleSubmit(onSubmit)} autoComplete="nope">
        <div className={styles.formWrapper}>
          <SelectField
            options={BankOpts}
            searchable
            label={formatterRequired("Bank")}
            placeholder="Jenis rekening bank milik perusahaan"
            name="bank"
            value={bank}
            error={errors.bank}
            helperText={errors.bank?.message}
            onChange={(v) => {
              setValue("bank", v?.value, { shouldValidate: true, shouldDirty: true })

              if (String(company?.bankAccount?.bankName) !== String(v?.value)) {
                setValue("bankAccountNumber", "", { shouldValidate: true, shouldDirty: true })
              } else if (bankAccountNumber === "") {
                setValue("bankAccountNumber", company?.bankAccount?.bankAccountNumber, { shouldValidate: true, shouldDirty: true })
              }
            }}
          />
          <TextField
            ref={register}
            name="bankBranch"
            label="Cabang (Optional)"
            placeholder="Masukkan cabang bank"
            error={errors.bankBranch}
            helperText={errors.bankBranch?.message}
          />
          <div className={styles.bankAccountNumber}>
            <div className={styles.bankAccountNumberWrapper}>
              <TextField
                name="bankAccountNumber"
                label={formatterRequired("Nomor Rekening Perusahaan")}
                placeholder="Nomor rekening"
                error={errors.bankAccountNumber}
                helperText={errors.bankAccountNumber?.message}
                value={bankAccountNumber}
                format={Number}
                allowDot={false}
                className="w-100"
                allowLeadingZero
                onChange={(v) => setValue("bankAccountNumber", v, { shouldValidate: true, shouldDirty: true })}
                maxLength={16}
              />
              <div className={pakaiClass(styles.checkNameBtn, errors.bankAccountNumber && styles.error)}>
                <button
                  type="button"
                  onClick={checkAccountNumber}
                  disabled={!bankAccountNumber}
                >
                  Cek Nama
                </button>
              </div>
            </div>
            {showCheckBankAlert ?
              <div className={pakaiClass(styles.alert, !isCheckingBankAccount ? isBankExist ? styles.primary : styles.error : styles.loading)}>
                {!isCheckingBankAccount
                  ?
                  <span>{bankUsername || "Nomor rekening tidak valid"}</span>
                  :
                  <LoadingDots size={10} />}
                {
                  !isCheckingBankAccount ?
                    !isBankExist ?
                      <Cancel />
                      :
                      <ChecklistOn size={18} />
                    :
                    null
                }
              </div>
              :
              null
            }
          </div>
        </div>
        <div className={styles.buttonWrapper}>
          <MainButton
            variant='plain'
            onClick={() => setIsEdit(false)}
          >
            Batal
          </MainButton>
          <MainButton
            variant='secondary'
            loading={loading}
            disabled={!isDirty}
          >
            Simpan
          </MainButton>
        </div>
      </form>
    </div>
  )
}

export default Form
