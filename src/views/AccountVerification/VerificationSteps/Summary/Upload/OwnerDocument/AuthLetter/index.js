import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import FileField from '../../../../../../../components/templates/Upload/FileField';
import Form from './Form';

const AuthLetter = ({ pending, loadSummary, docs }) => {
  const { user } = useSelector(state => state.auth)
  const [isEdit, setIsEdit] = useState(false);

  if (user?.identificationArchive?.position === "OWNER") return <></>
  return !pending && isEdit ?
    <Form setIsEdit={setIsEdit} onSuccess={() => loadSummary(user?._id)} docs={docs} />
    :
    <FileField
      file={docs?.authLetter?.url}
      fileName={docs?.authLetter?.name}
      onEdit={() => setIsEdit(true)}
    />
}

export default AuthLetter
