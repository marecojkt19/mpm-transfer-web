import React from 'react'
import styles from '../../summary.module.scss'
import IdCard from './IdCard';
import SelfPhoto from './SelfPhoto';
import AuthLetter from './AuthLetter';

const OwnerDocument = ({ loadSummary, pending, docs }) => {
  return (
    <div className={styles.summarySection}>
      <div className={styles.heading}>
        <h3>Dokumen Pemilik Perusahaan</h3>
      </div>
      <div className={styles.fileCard}>
        <div className={styles.fileWrapper}>
          <div className={styles.fileHeading}>
            <h3>Dokumen Identitas</h3>
          </div>
          <IdCard docs={docs} loadSummary={loadSummary} pending={pending} />
          <SelfPhoto docs={docs} loadSummary={loadSummary} pending={pending} />
          <AuthLetter docs={docs} loadSummary={loadSummary} pending={pending} />
        </div>
      </div>
    </div>
  )
}

export default OwnerDocument
