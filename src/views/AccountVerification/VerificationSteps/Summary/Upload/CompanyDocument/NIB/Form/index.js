import React, { useEffect, useMemo, useState } from 'react'
import UploadFile from '../../../../../../../../components/Form/UploadFile';
import Accordion from '../../../../../../../../components/templates/Upload/Accordion';
import styles from '../../../../form.module.scss'
import { useDispatch, useSelector } from 'react-redux';
import accountVerificationServices from '../../../../../../../../axios/services/accountVerificationServices';
import { authActions } from '../../../../../../../../redux/actions/authActions';
import Notes from '../../../../../../../../components/templates/Upload/Notes';

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { getFileFromUrl } from '../../../../../../../../utils/helpers/getFileFromUrl';

export const dirtyValues = (
  dirtyFields,
  allValues
) => {
  // NOTE: Recursive function.

  // If *any* item in an array was modified, the entire array must be submitted, because there's no
  // way to indicate "placeholders" for unchanged elements. `dirtyFields` is `true` for leaves.
  if (dirtyFields === true || Array.isArray(dirtyFields)) {
    return allValues;
  }

  // Here, we have an object.
  return Object.fromEntries(
    Object.keys(dirtyFields).map((key) => [
      key,
      dirtyValues(dirtyFields[key], allValues[key])
    ])
  );
};

const schema = yup.object().shape({
  nibFile: yup.mixed().required()
})

const Form = ({
  setIsEdit,
  onSuccess,
  docs
}) => {
  const dispatch = useDispatch();
  const { user, company, signature } = useSelector(state => state.auth)
  const [loading, setLoading] = useState(false);

  const {
    register,
    unregister,
    watch,
    handleSubmit,
    formState: { isDirty, dirtyFields },
    setValue
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      position: user?.identificationArchive?.position ?? "OWNER",
      isEffectivePermit: company?.archive?.businessPermit?.isEffective ?? false,

      thirdParty: user?.identificationArchive?.thirdPartyName ?? "",
      idCardUrl: user?.identificationArchive?.idCard?.file?.url,
      idCard: user?.identificationArchive?.idCard?.file,
      selfPhotoUrl: user?.identificationArchive?.selfPhoto?.file?.url,
      selfPhoto: user?.identificationArchive?.selfPhoto?.file,
      authLetter: user?.identificationArchive?.authLetter?.file,
      apadFile: company?.archive?.apad?.file,
      appFile: company?.archive?.app?.file,
      isApp: Boolean(company?.archive?.app?.file),
      isApad: Boolean(company?.archive?.apad?.file),
      npwpFile: company?.archive?.npwp?.file,
      nibFile: company?.archive?.nib?.file,
      businessPermit: company?.archive?.businessPermit?.file,
      birthCertificate: company?.archive?.birthCertificate?.file,
      skFile: company?.archive?.sk?.file
    }
  });

  const {
    position,
    isApp,
    isApad,

    idCardUrl,

    idCard,
    selfPhoto,
    authLetter,
    apadFile,
    appFile,
    npwpFile,
    nibFile,
    birthCertificate,
    skFile
  } = watch([
    "position",
    "isApp",
    "isApad",

    "idCardUrl",

    "idCard",
    "selfPhoto",
    "authLetter",
    "apadFile",
    "appFile",
    "npwpFile",
    "nibFile",
    "birthCertificate",
    "skFile"
  ])

  const onSubmit = (values) => {
    if (isDirty) {
      setLoading(true)
      let data = {
        ...dirtyValues(dirtyFields, values),
        position: docs?.position,
        nibFile: values.nibFile,
        idCard
      }

      accountVerificationServices
        .uploadDocument(data, user._id, signature)
        .then(() => {
          dispatch(authActions.requestUser());
          onSuccess();
          setIsEdit(false)
        })
        .catch((e) => console.log(e.message))
        .finally(() => {
          setLoading(false)
        })
    } else {
      setIsEdit(false)
    }
  }

  const isNext = useMemo(() => {
    if (
      position &&
      idCard &&
      selfPhoto &&
      (Boolean(position === "PIC") ? Boolean(authLetter) : true) &&
      (Boolean(isApad) ? Boolean(apadFile) : true) &&
      (Boolean(isApp) ? Boolean(appFile) : true) &&
      npwpFile &&
      nibFile &&
      birthCertificate &&
      skFile
    ) return true;

    return false;
  }, [
    position,
    isApp,
    isApad,

    idCard,
    selfPhoto,
    authLetter,
    apadFile,
    appFile,
    npwpFile,
    nibFile,
    birthCertificate,
    skFile
  ])

  useEffect(() => {
    register("nibFile")

    return () => {
      unregister("nibFile")
    }
  }, [register, unregister])

  useEffect(() => {
    async function fetchData() {
      if (idCardUrl) {
        const filename = idCardUrl.split("/")[4].split("_")[0];
        const format = idCardUrl.split("/")[4].split(".")[1];
        const file = await getFileFromUrl(idCardUrl, `${filename}.${format}`);

        setValue("idCard", file, { shouldValidate: true });
      }
    }
    fetchData();
  }, [idCardUrl, setValue]);

  return (
    <div className={styles.form}>
      <form onSubmit={handleSubmit(onSubmit)} autoComplete="nope">
        <div className={styles.formWrapper}>
          <Accordion
            title="Nomor Induk Berusaha (NIB)"
            isOpen={nibFile}
            editConfig={{
              isEdit: true,
              onclose: () => setIsEdit(false),
              disabled: !isNext,
              loading
            }}
          >
            <UploadFile
              onChange={(v) => setValue("nibFile", v, { shouldValidate: true, shouldDirty: true })}
              className="mb-32"
              maxSize={10485760}
              defaultLabel={nibFile?.name}
            />
            <Notes />
          </Accordion>
        </div>
      </form>
    </div>
  )
}

export default Form
