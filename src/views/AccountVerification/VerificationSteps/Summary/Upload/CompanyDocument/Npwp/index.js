import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import FileField from '../../../../../../../components/templates/Upload/FileField';
import Form from './Form';

const Npwp = ({ loadSummary, pending, docs }) => {
  const { user } = useSelector(state => state.auth)
  const [isEdit, setIsEdit] = useState(false);

  return !pending && isEdit ?
    <Form setIsEdit={setIsEdit} onSuccess={() => loadSummary(user?._id)} docs={docs} />
    :
    <FileField
      file={docs?.npwp?.url}
      fileName={docs?.npwp?.name}
      onEdit={() => setIsEdit(true)}
    />
}

export default Npwp
