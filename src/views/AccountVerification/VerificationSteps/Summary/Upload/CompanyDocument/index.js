import React from 'react'
import styles from '../../summary.module.scss'
import Npwp from './Npwp';
import BirthCertificate from './BirthCertificate';
import SK from './SK';
import APAD from './APAD';
import APP from './APP';
import NIB from './NIB';
import BusinessPermit from './BusinessPermit';

const CompanyDocument = ({ loadSummary, pending, docs }) => {
  return (
    <div className={styles.summarySection}>
      <div className={styles.heading}>
        <h3>Dokumen Perusahaan</h3>
      </div>
      <div className={styles.fileCard}>
        <div className={styles.fileWrapper}>
          <div className={styles.fileHeading}>
            <h3>NPWP Perusahaan</h3>
          </div>
          <Npwp docs={docs} loadSummary={loadSummary} pending={pending} />
        </div>
        <div className={styles.fileWrapper}>
          <div className={styles.fileHeading}>
            <h3>Akta Pendirian & SK Kemenkumham</h3>
            <p>Disertai Akta Perubahan Anggaran Dasar & perubahan pengurus beserta SK Kemenkumham, jika ada.</p>
          </div>
          <BirthCertificate docs={docs} loadSummary={loadSummary} pending={pending} />
          <SK docs={docs} loadSummary={loadSummary} pending={pending} />
          <APAD docs={docs} loadSummary={loadSummary} pending={pending} />
          <APP docs={docs} loadSummary={loadSummary} pending={pending} />
        </div>
        <div className={styles.fileWrapper}>
          <div className={styles.fileHeading}>
            <h3>Nomor Induk Berusaha (NIB)</h3>
          </div>
          <NIB docs={docs} loadSummary={loadSummary} pending={pending} />
        </div>
        <div className={styles.fileWrapper}>
          <div className={styles.fileHeading}>
            <h3>Izin Usaha dan Sertifikasi Standar</h3>
          </div>
          <BusinessPermit docs={docs} loadSummary={loadSummary} pending={pending} />
        </div>
      </div>
    </div>
  )
}

export default CompanyDocument
