import styles from './dataVerification.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import TextField from '../../../../components/Form/TextField';
import SelectField from '../../../../components/Form/SelectField';
import DatePicker from '../../../../components/Form/Dates/DatePicker';
import MainButton from '../../../../components/Form/MainButton';
import RadioItem from '../../../../components/Form/Radio/RadioItem';
import useAsync from '../../../../components/hooks/useAsync';
import commonService from '../../../../axios/services/commonService';
import optionsService from '../../../../axios/services/optionsService';
import { verifyAccountActions } from '../../../../redux/actions/verifyAccountAction';
import accountVerificationServices from '../../../../axios/services/accountVerificationServices';
import { TransferMethod } from '../../../../utils/enums/transferMethod';
import { BankOpts } from '../../../../utils/enums/bankTypes';
import { authActions } from '../../../../redux/actions/authActions';
import { useToasts } from 'react-toast-notifications';
import pakaiClass from 'pakai-class'
import { Cancel, ChecklistOn, Info } from '../../../../assets/icons';
import TooltipTemplate from '../../../../components/templates/TooltipTemplate2';
import bankService from '../../../../axios/services/bankService';
import LoadingDots from '../../../../components/Loadings/LoadingDots';
import RadioGroup2 from '../../../../components/Form/Radio/RadioGroup2';

const schema = yup.object().shape({
  ownerName: yup.string().required('Nama tidak boleh kosong'),
  identificationType: yup.string().required('Jenis Identitas tidak boleh kosong'),
  identificationNumber: yup.string()
    .required('Nomor Identitas tidak boleh kosong')
    .matches(/^[a-zA-Z0-9]+$/g, "Tidak boleh mengandung simbol")
    .when('identificationType', {
      is: (val) => val === "Paspor",
      then: yup.string()
        .min(8, "Minimal 8 karakter")
        .max(8, "Maximum 8 karakter"),
      otherwise: yup.string()
        .min(16, "Minimal 16 karakter")
        .max(16, "Maximum 16 karakter")
    }),
  placeOfBirth: yup.string().required('Tempat lahir tidak boleh kosong'),
  dateOfBirth: yup.string().required("Tanggal lahir tidak boleh kosong")
    .test({
      message: "Umur tidak boleh dibawah 17 tahun",
      test: (value) => {
        return !(Math.floor((new Date() - new Date(value).getTime()) / 3.15576e+10) <= 17)
      }
    }),
  occupation: yup.string().required('Pekerjaan tidak boleh kosong'),
  city: yup.string().required('Kota/Kabupaten tinggal tidak boleh kosong'),
  address: yup.string().required('Alamat tidak boleh kosong'),

  companyName: yup.string().required('Nama Perusahaan tidak boleh kosong'),
  productDesc: yup.string().required('Produk/Jasa tidak boleh kosong'),
  nib: yup.string().min(13, "Minimal 13 karakter").required('NIB tidak boleh kosong'),
  npwp: yup.string().min(15, "Minimal 15 karakter").required('NPWP tidak boleh kosong'),
  niu: yup.string(),
  companyAddress: yup.string().required('Alamat Perusahaan tidak boleh kosong'),
  companyProvince: yup.string().required('Provinsi Perusahaan tidak boleh kosong'),
  companyCity: yup.string().required('Kota Perusahaan tidak boleh kosong'),
  companyPostalCode: yup.string().required('Kode pos Perusahaan tidak boleh kosong'),
  bank: yup.string().required('Bank tidak boleh kosong'),
  bankAccountNumber: yup.string().required('Nomor Rekening tidak boleh kosong'),
  usagePurposes: yup.array().max(2, "Maximal 2 pilihan").required('Tujuan Penggunaan tidak boleh kosong'),
  transactionMethod: yup.string().required('Metode Transfer tidak boleh kosong'),
  otherPurpose: yup.string()
    .when('usagePurposes', {
      is: (val) => val.length ? val.includes("OTHER_PURPOSE") : false,
      then: yup.string().max(225, "Maksimal 225 karakter").required("Tujuan Lainnya tidak boleh kosong!"),
      otherwise: yup.string()
    })
})

const formatterRequired = (text) => {
  return <>{text}<span style={{ color: "#E11900" }}>*</span></>
}

const DataVerification = () => {
  const { user, company, signature } = useSelector(state => state.auth);
  const { verifyStep: { stepThree } } = user;
  const [loading, setLoading] = useState(false);
  const [tooltip, setTooltip] = useState(false);
  const [isCheckingBankAccount, setIsCheckingBankAccount] = useState(false);
  const [isBankExist, setIsBankExist] = useState(false);
  const [bankUsername, setBankUsername] = useState(false);
  const [showCheckBankAlert, setShowCheckBankAlert] = useState(false);

  const [isGettingCities, setIsGettingCities] = useState(false);
  const [cities, setCities] = useState([]);
  const [specifyCities, setSpecifyCities] = useState([]);

  const dispatch = useDispatch();
  const { addToast } = useToasts();
  const { register, unregister, watch, handleSubmit, formState: { isDirty }, errors, setValue, setError } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      ...user,
      ...company,
      dateOfBirth: user?.dateOfBirth ? new Date(user?.dateOfBirth) : new Date("1995-01-01"),
      occupation: user?.occupation,
      companyName: company?.name,
      productDesc: company?.product,
      usagePurposes: company?.usagePurposes,
      bank: company?.bankAccount?.bankName,
      bankBranch: company?.bankAccount?.branch,
      bankAccountNumber: company?.bankAccount?.accountNumber,
      niu: company?.niu,

      companyProvince: company?.province,
      companyProvinceCode: company?.province?.split("|")?.[1],
      companyCity: company?.addressCity,
      companyPostalCode: company?.postalCode,
    }
  });

  const {
    identificationType,
    dateOfBirth,
    placeOfBirth,
    occupation,
    city,
    bank,
    usagePurposes,
    transactionMethod,
    bankAccountNumber,

    identificationNumber,
    nib,
    npwp,
    companyPostalCode,
    companyProvinceCode,
    companyCity
  } = watch([
    "identificationType",
    "dateOfBirth",
    "placeOfBirth",
    "occupation",
    "city",
    "bank",
    "usagePurposes",
    "transactionMethod",
    "bankAccountNumber",
    "identificationNumber",
    "nib",
    "npwp",
    "companyPostalCode",
    "companyProvinceCode",
    "companyCity"
  ])

  const getCities = useCallback(() => {
    setIsGettingCities(true)
    commonService.cities()
      .then(({ data }) => {
        const citiesData = data?.cities.map((v) => ({ label: v?.location, value: v?.code }))
        setCities(citiesData)
      })
      .catch(() => { })
      .finally(() => setIsGettingCities(false))
  }, []);

  const getSpecifyCities = (code) => {
    setIsGettingCities(true)
    commonService.cities(code)
      .then(({ data }) => {
        const citiesData = data?.cities.map((v) => ({ label: v?.location, value: v?.code }))
        setSpecifyCities(citiesData)
      })
      .catch(() => { })
      .finally(() => setIsGettingCities(false))
  }

  useEffect(() => {
    getCities();
    if (companyProvinceCode) {
      getSpecifyCities(companyProvinceCode)
    }
  }, [getCities, companyProvinceCode])

  const {
    pending: isGettingProvince,
    value: { data: { provinces: provinceList = [] } = {} } = {}
  } = useAsync(commonService.province, true)

  const provinces = useMemo(() => provinceList.map(el => ({ label: el.province, value: el.provinceCode })), [provinceList]);

  const {
    isGettingOccupations,
    value: { data: { occupations = [] } = {} } = {}
  } = useAsync(optionsService.occupationList, {});

  const {
    isGettingUsagePurpose,
    value: { data: { purposes = [] } = {} } = {}
  } = useAsync(optionsService.usagePurposeList, {});

  const onSubmit = (values) => {
    if (isDirty) {
      if (isBankExist) {
        setLoading(true)
        const data = {
          user: {
            ownerName: values.ownerName,
            identificationType: values.identificationType,
            identificationNumber: values.identificationNumber,
            placeOfBirth: values.placeOfBirth,
            dateOfBirth: values.dateOfBirth,
            occupation: values.occupation,
            city: values.city,
            address: values.address,
          },
          company: {
            name: values.companyName,
            product: values.productDesc,
            nib: values.nib,
            npwp: values.npwp,
            niu: values.niu,
            companyAddress: values.companyAddress,

            addressCity: values.companyCity,
            province: values.companyProvince,
            postalCode: values.companyPostalCode,
            transactionMethod: values.transactionMethod,
            usagePurposes: [...values.usagePurposes],
            otherPurpose: values.otherPurpose ?? "",
            bankAccount: {
              bankName: values.bank,
              branch: values?.bankBranch || "",
              accountNumber: values.bankAccountNumber
            }
          }
        }

        accountVerificationServices.dataVerification(data, user._id, signature)
          .then(() => {
            dispatch(authActions.requestUser())
            if (stepThree === true) dispatch(verifyAccountActions.setStep(3))
            else dispatch(verifyAccountActions.setStep(2))
          })
          .catch((err) => {
            if (err.response) {
              if (err.response.body) {
                addToast(err.response.body.message, { appearance: 'danger' });
              }
              if (err.response.data) {
                if (typeof err.response.data.message === "string") {
                  addToast(err.response.data.message, { appearance: 'danger' });
                } else {
                  for (const fields of err.response.data.message) {
                    for (const message of Object.keys(fields.constraints)) {
                      addToast(fields.constraints[message], { appearance: 'danger' });
                    }
                  }
                }
              }
            } else {
              addToast(err, { appearance: 'danger' });
            }
          })
          .finally(() => setLoading(false))
      } else {
        setError("bankAccountNumber", { message: "Validasi pengguna rekening bank" })
      }
    } else {
      dispatch(verifyAccountActions.setStep(2))
    }
  }

  const checkAccountNumber = () => {
    setShowCheckBankAlert(true)
    const payload = {
      accountNumber: bankAccountNumber,
      bank
    }
    setIsCheckingBankAccount(true)
    bankService.checkAccountNumber(payload)
      .then(({ data }) => {
        setBankUsername(data?.name);
        if (data?.name) {
          setIsBankExist(true)
        } else {
          setIsBankExist(false)
        }
      })
      .catch((err) => {
        setIsBankExist(false)
        if (err.response) {
          if (err.response.body) {
            setBankUsername(err.response.body.message);
          }
          if (err.response.data) {
            if (typeof err.response.data.message === "string") {
              setBankUsername(err.response.data.message);
            } else {
              for (const fields of err.response.data.message) {
                for (const message of Object.keys(fields.constraints)) {
                  setBankUsername(fields.constraints[message]);
                }
              }
            }
          }
        } else {
          setBankUsername(err);
        }
      })
      .finally(() => setIsCheckingBankAccount(false))
  }

  useEffect(() => {
    register("identificationType");
    register("dateOfBirth");
    register("placeOfBirth");
    register("occupation");
    register("city");
    register("bank");
    register("usagePurposes");
    register("transactionMethod");
    register("bankAccountNumber");


    register("identificationNumber");
    register("nib");
    register("npwp");
    register("niu");
    register("companyCity");
    register("companyProvince");
    register("companyProvinceCode");
    register("companyPostalCode");

    return () => {
      unregister("identificationType");
      unregister("dateOfBirth");
      unregister("placeOfBirth");
      unregister("occupation");
      unregister("city");
      unregister("bank");
      unregister("usagePurposes");
      unregister("transactionMethod");
      unregister("bankAccountNumber");

      unregister("identificationNumber");
      unregister("nib");
      unregister("npwp");
      unregister("niu");
      unregister("companyCity");
      unregister("companyProvince");
      unregister("companyProvinceCode");
      unregister("companyPostalCode");
    }
  }, [register, unregister]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <div className={styles.dataVerification}>
      <div className={styles.heading}>
        <h5>Verifikasi Data</h5>
        <p>Sesuai dengan <a href="https://www.bi.go.id/id/publikasi/peraturan/Documents/PBI_191017.pdf" target="_blank" rel="noreferrer"><span>peraturan dari Bank Indonesia</span></a>, kami perlu melakukan verifikasi data sebelum Anda dapat melakukan transaksi.</p>
      </div>
      <div className={styles.line}></div>
      <div className={styles.wrapper}>
        <form onSubmit={handleSubmit(onSubmit)} autoComplete="nope">
          <div className={styles.form}>
            <div className={styles.heading}>
              <h5>Data Pemilik Perusahaan</h5>
            </div>
            <div className={styles.formWrapper}>
              <TextField
                ref={register}
                name="ownerName"
                label={formatterRequired("Nama Pemilik Perusahaan Sesuai Dokumen Identitas")}
                allowNumeric={false}
                placeholder="Tulis nama perusahaan"
                error={errors.ownerName}
                helperText={errors.ownerName?.message}
              />
              <div className={styles.formGroup}>
                <div className={styles.label}>
                  <p>{formatterRequired("Nomor Identitas Pemilik Perusahaan")}</p>
                </div>
                <div className={styles.inputGroup}>
                  <div className={styles.idType}>
                    <SelectField
                      options={[
                        {
                          value: "KTP",
                          label: "KTP"
                        }
                      ]}
                      placeholder="Pilih Identitas"
                      name="identificationType"
                      value={identificationType}
                      error={errors.identificationType}
                      helperText={errors.identificationType?.message}
                      onChange={(v) => {
                        setValue("identificationType", v?.value, { shouldValidate: true, shouldDirty: true })
                        setValue("identificationNumber", "", { shouldValidate: true, shouldDirty: true })
                      }}
                    />
                  </div>
                  <div className={styles.idNumber}>
                    <TextField
                      disabled={!identificationType}
                      name="identificationNumber"
                      placeholder="Masukkan nomor identitas di sini"
                      error={errors.identificationNumber}
                      helperText={errors.identificationNumber?.message}
                      value={identificationNumber}
                      format={identificationType === "Paspor" ? null : Number}
                      allowDot={false}
                      onChange={(v) => {
                        if (identificationType === "Paspor") {
                          setValue("identificationNumber", v.target.value, { shouldValidate: true, shouldDirty: true })
                        } else {
                          setValue("identificationNumber", v, { shouldValidate: true, shouldDirty: true })
                        }
                      }}
                      maxLength={identificationType === "Paspor" ? 8 : 16}
                    />
                  </div>
                </div>
              </div>
              <div className={styles.formGroup}>
                <div className={styles.inputGroup}>
                  <div className={styles.placeOfBirth}>
                    <SelectField
                      options={cities}
                      searchable
                      label={formatterRequired("Tempat Lahir")}
                      placeholder="Tempat lahir"
                      name="placeOfBirth"
                      value={placeOfBirth}
                      loading={isGettingCities}
                      error={errors.placeOfBirth}
                      helperText={errors.placeOfBirth?.message}
                      onChange={(v) => {
                        setValue("placeOfBirth", v?.value, { shouldValidate: true, shouldDirty: true })
                      }}
                    />
                  </div>
                  <div className={styles.dateOfBirth}>
                    <DatePicker
                      label={formatterRequired("Tanggal Lahir")}
                      value={dateOfBirth}
                      placeholder="DD/MM//YYYY"
                      onChange={v => setValue('dateOfBirth', v ? v : '', { shouldValidate: true, shouldDirty: true })}
                      error={errors.dateOfBirth}
                      helperText={errors.dateOfBirth?.message}
                    />
                  </div>
                </div>
              </div>
              <SelectField
                options={occupations}
                loading={isGettingOccupations}
                searchable
                label={formatterRequired("Pekerjaan")}
                placeholder="Pilih Pekerjaan"
                name="occupation"
                value={occupation}
                error={errors.occupation}
                helperText={errors.occupation?.message}
                onChange={(v) => {
                  setValue("occupation", v?.value, { shouldValidate: true, shouldDirty: true })
                }}
              />
              <SelectField
                options={cities}
                loading={isGettingCities}
                searchable
                label={formatterRequired("Kota/Kabupaten Tempat Tinggal Saat Ini")}
                placeholder="Pilih kota/kabupaten"
                name="city"
                value={city}
                error={errors.city}
                helperText={errors.city?.message}
                onChange={(v) => {
                  setValue("city", v?.value, { shouldValidate: true, shouldDirty: true })
                }}
              />
              <TextField
                ref={register}
                name="address"
                label={formatterRequired("Alamat Terkini")}
                placeholder="Alamat tinggal saat ini. Contoh: Komplek Melati No. 11, Jl. Margonda Raya, Depok"
                error={errors.address}
                helperText={errors.address?.message}
              />
            </div>
          </div>
          <div className={styles.line}></div>
          <div className={styles.form}>
            <div className={styles.heading}>
              <h5>Data Perusahaan</h5>
            </div>
            <div className={styles.formWrapper}>
              <TextField
                ref={register}
                name="companyName"
                label={formatterRequired("Nama Perusahaan")}
                placeholder="Masukkan Nama Perusahaan"
                error={errors.companyName}
                helperText={errors.companyName?.message}
              />
              <TextField
                ref={register}
                name="productDesc"
                label={formatterRequired("Produk/Jasa Perusahaan")}
                placeholder="Masukkan produk atau jasa"
                error={errors.productDesc}
                helperText={errors.productDesc?.message}
              />

              <TextField
                name="nib"
                label={formatterRequired("NIB (Nomor Induk Berusaha)")}
                placeholder="Masukkan NIB"
                allowDot={false}
                error={errors.nib}
                helperText={errors.nib?.message}
                value={nib}
                format={Number}
                onChange={(v) => setValue("nib", v, { shouldValidate: true, shouldDirty: true })}
                maxLength={13}
              />
              {/* <TextField
                ref={register}
                name="niu"
                label="Nomor Izin Usaha (Optional)"
                placeholder="Masukkan nomor izin usaha"
                additionalLabel="Dari Bank Indonesia"
                error={errors.niu}
                helperText={errors.niu?.message}
              /> */}
              <TextField
                name="npwp"
                label={formatterRequired("NPWP Perusahaan")}
                placeholder="Nomor NPWP perusahaan"
                error={errors.npwp}
                allowDot={false}
                helperText={errors.npwp?.message}
                value={npwp}
                format={Number}
                onChange={(v) => setValue("npwp", v, { shouldValidate: true, shouldDirty: true })}
                maxLength={16}
              />
              <div className={styles.inputGroup}>
                <div className={styles.item}>
                  <SelectField
                    options={provinces}
                    searchable
                    label={formatterRequired("Provinsi")}
                    placeholder="Pilih Provinsi"
                    name="companyProvince"
                    value={companyProvinceCode}
                    loading={isGettingProvince}
                    error={errors.companyProvince}
                    helperText={errors.companyProvince?.message}
                    onChange={(v) => {
                      setValue("companyProvince", `${v?.label}|${v?.value}`, { shouldValidate: true, shouldDirty: true });
                      setValue("companyProvinceCode", v?.value);
                      getSpecifyCities(v?.value)
                    }}
                  />
                </div>
                <div className={styles.item}>
                  <SelectField
                    options={specifyCities}
                    searchable
                    label={formatterRequired("Kota/Kabupaten")}
                    placeholder="Pilih Kota/Kabupaten"
                    name="companyCity"
                    value={companyCity}
                    loading={isGettingCities}
                    error={errors.companyCity}
                    disabled={!companyProvinceCode}
                    helperText={errors.companyCity?.message}
                    onChange={(v) => {
                      setValue("companyCity", v?.value, { shouldValidate: true, shouldDirty: true })
                    }}
                  />
                </div>
                <div>
                  <TextField
                    name="companyPostalCode"
                    label={formatterRequired("Kode Pos")}
                    placeholder="Tulis Kode Pos"
                    allowDot={false}
                    error={errors.companyPostalCode}
                    helperText={errors.companyPostalCode?.message}
                    value={companyPostalCode}
                    format={Number}
                    allowLeadingZero
                    onChange={(v) => setValue("companyPostalCode", v, { shouldValidate: true, shouldDirty: true })}
                    maxLength={13}
                  />
                </div>
              </div>
              <TextField
                ref={register}
                name="companyAddress"
                label={formatterRequired("Alamat Perusahaan")}
                placeholder="Alamat perusahaan saat ini (Contoh: Komplek Melati No. 11, Jl. Margonda Raya, Depok)"
                error={errors.companyAddress}
                helperText={errors.companyAddress?.message}
              />
              <RadioGroup2
                name="transactionMethod"
                label={formatterRequired("Metode transfer yang digunakan saat ini")}
                error={errors.transactionMethod}
                helperText={errors.transactionMethod?.message}
                value={transactionMethod}
                onChange={e => setValue('transactionMethod', e.target.value, { shouldValidate: true, shouldDirty: true })}
              >
                <RadioItem
                  label="Menggunakan Internet Banking"
                  value={TransferMethod.INTERNET_BANKING}
                />
                <RadioItem
                  label="Menggunakan layanan pengiriman uang pihak ketiga"
                  value={TransferMethod.THIRD_PARTY}
                />
                <RadioItem
                  label="Bekerja sama dengan host to host dengan bank"
                  value={TransferMethod.H2H}
                />
              </RadioGroup2>

              <SelectField
                options={purposes}
                loading={isGettingUsagePurpose}
                searchable
                label={formatterRequired("Tujuan pengunaan Dipay Disbursement")}
                placeholder="Maksimal 2 pilihan atau tujuan lainnya (sebutkan)"
                name="usagePurposes"
                withCheckbox={true}
                max={2}
                value={usagePurposes}
                error={errors.usagePurposes}
                helperText={errors.usagePurposes?.message}
                onChange={(v) => {
                  setValue("usagePurposes", !v?.length ? '' : v.map(el => el.value), { shouldValidate: true, shouldDirty: true })
                }}
              />
              {usagePurposes?.includes("OTHER_PURPOSE") &&
                <TextField
                  ref={register}
                  name="otherPurpose"
                  label={formatterRequired("Tujuan Lainnya (Sebutkan)")}
                  placeholder="Maksimal 225 karakter"
                  error={errors.otherPurpose}
                  helperText={errors.otherPurpose?.message}
                  multiline
                  maxLength={225}
                />
              }
              <div className={styles.line2}></div>
              <div className={styles.heading}>
                <div className={styles.headingWrapper}>
                  <h5>Rekening Perusahaan</h5>
                  <button type="button" onClick={() => setTooltip(true)}>
                    <Info size={16} color="#276EF1" />
                  </button>
                  <TooltipTemplate
                    onClose={() => setTooltip(false)}
                    show={tooltip}
                    heading="Nomor Rekening Perusahaan"
                    content="Nomor Rekening Perusahaan digunakan untuk Transaksi Top Up Deposit dan Refund Dana Deposit."
                  />
                </div>
              </div>
              <SelectField
                options={BankOpts}
                searchable
                label={formatterRequired("Bank")}
                placeholder="Jenis rekening bank milik perusahaan"
                name="bank"
                value={bank}
                error={errors.bank}
                helperText={errors.bank?.message}
                onChange={(v) => {
                  setValue("bank", v?.value, { shouldValidate: true, shouldDirty: true })
                }}
              />
              <TextField
                ref={register}
                name="bankBranch"
                label="Cabang (Optional)"
                placeholder="Masukkan cabang bank"
                error={errors.bankBranch}
                helperText={errors.bankBranch?.message}
              />
              <div className={styles.bankAccountNumber}>
                <div className={styles.bankAccountNumberWrapper}>
                  <TextField
                    name="bankAccountNumber"
                    label={formatterRequired("Nomor Rekening Perusahaan")}
                    placeholder="Nomor rekening"
                    error={errors.bankAccountNumber}
                    helperText={errors.bankAccountNumber?.message}
                    value={bankAccountNumber}
                    format={Number}
                    allowDot={false}
                    className="w-100"
                    allowLeadingZero
                    onChange={(v) => setValue("bankAccountNumber", v, { shouldValidate: true, shouldDirty: true })}
                    maxLength={16}
                  />
                  <div className={pakaiClass(styles.checkNameBtn, errors.bankAccountNumber && styles.error)}>
                    <button
                      type="button"
                      onClick={checkAccountNumber}
                      disabled={!bankAccountNumber}
                    >
                      Cek Nama
                    </button>
                  </div>
                </div>
                {showCheckBankAlert ?
                  <div className={pakaiClass(styles.alert, !isCheckingBankAccount ? isBankExist ? styles.primary : styles.error : styles.loading)}>
                    {!isCheckingBankAccount
                      ?
                      <span>{bankUsername || "Nomor rekening tidak valid"}</span>
                      :
                      <LoadingDots size={10} />}
                    {
                      !isCheckingBankAccount ?
                        !isBankExist ?
                          <Cancel />
                          :
                          <ChecklistOn size={18} />
                        :
                        null
                    }
                  </div>
                  :
                  null
                }
              </div>
            </div>
          </div>
          <div className={styles.buttons}>
            <MainButton
              type="submit"
              loading={loading}
            >
              Selanjutnya
            </MainButton>
          </div>
        </form>
      </div>
    </div>
  )
}

export default DataVerification;
