import React, { useEffect, useMemo, useState } from 'react'
import RadioGroup from '../../../../components/Form/Radio/RadioGroup'
import RadioItem from '../../../../components/Form/Radio/RadioItem'
import styles from './uploadDocument.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import UploadFile from '../../../../components/Form/UploadFile';
import MainButton from '../../../../components/Form/MainButton';
import Checkbox from '../../../../components/Form/Checkbox';
import TextField from '../../../../components/Form/TextField';
import { verifyAccountActions } from '../../../../redux/actions/verifyAccountAction';
import { useDispatch, useSelector } from 'react-redux';
import accountVerificationServices from '../../../../axios/services/accountVerificationServices';
import { authActions } from '../../../../redux/actions/authActions';
import { IMAGE_FORMATS, PDF_FORMATS } from '../../../../utils/helpers/fileFormats';
import Notes from '../../../../components/templates/Upload/Notes';
import UploadSelfieExample from '../../../../components/templates/Upload/UploadSelfieExample';
import UploadExample from '../../../../components/templates/Upload/UploadExample';
import Accordion from '../../../../components/templates/Upload/Accordion';
import { getFileFromUrl } from '../../../../utils/helpers/getFileFromUrl';

export const dirtyValues = (
  dirtyFields,
  allValues
) => {
  // NOTE: Recursive function.

  // If *any* item in an array was modified, the entire array must be submitted, because there's no
  // way to indicate "placeholders" for unchanged elements. `dirtyFields` is `true` for leaves.
  if (dirtyFields === true || Array.isArray(dirtyFields)) {
    return allValues;
  }

  // Here, we have an object.
  return Object.fromEntries(
    Object.keys(dirtyFields).map((key) => [
      key,
      dirtyValues(dirtyFields[key], allValues[key])
    ])
  );
};

const formatterRequired = (text) => {
  return <>{text}<span style={{ color: "#E11900" }}>*</span></>
}

const schema = yup.object().shape({
  thirdParty: yup
    .string()
    .when('position', {
      is: (val) => val === "PIC",
      then: yup.string().required("Nama pihak ketiga tidak boleh kosong!"),
      otherwise: yup.string()
    }),
  position: yup.string().required(),
  idCard: yup.mixed().required(),
  selfPhoto: yup.mixed().required(),
  authLetter: yup
    .mixed()
    .when('position', {
      is: (val) => val === "PIC",
      then: yup.mixed().required(),
      otherwise: yup.mixed()
    }),
  apadFile: yup
    .mixed()
    .when('isApad', {
      is: true,
      then: yup.mixed().required(),
      otherwise: yup.mixed()
    }),
  appFile: yup
    .mixed()
    .when('isApp', {
      is: true,
      then: yup.mixed().required(),
      otherwise: yup.mixed()
    }),
  isApp: yup.boolean(),
  isApad: yup.boolean(),
  npwpFile: yup.mixed().required(),
  nibFile: yup.mixed().required(),
  businessPermit: yup.mixed(),
  birthCertificate: yup.mixed().required(),
  skFile: yup
    .mixed()
    .when('position', {
      is: (val) => val === "PIC",
      then: yup.mixed().required(),
      otherwise: yup.mixed()
    }),
  isEffectivePermit: yup.boolean().required()
})

const UploadDocument = () => {
  const dispatch = useDispatch();
  const { user, company, signature } = useSelector(state => state.auth);
  const [loading, setLoading] = useState(false);

  const {
    register,
    unregister,
    watch,
    handleSubmit,
    formState: { isDirty, dirtyFields },
    errors,
    setValue
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      position: user?.identificationArchive?.position ?? "OWNER",
      isEffectivePermit: company?.archive?.businessPermit?.isEffective ?? false,

      thirdParty: user?.identificationArchive?.thirdPartyName ?? "",
      idCardUrl: user?.identificationArchive?.idCard?.file?.url,
      idCard: user?.identificationArchive?.idCard?.file,
      selfPhotoUrl: user?.identificationArchive?.selfPhoto?.file?.url,
      selfPhoto: user?.identificationArchive?.selfPhoto?.file,
      authLetter: user?.identificationArchive?.authLetter?.file,
      apadFile: company?.archive?.apad?.file,
      appFile: company?.archive?.app?.file,
      isApp: Boolean(company?.archive?.app?.file),
      isApad: Boolean(company?.archive?.apad?.file),
      npwpFile: company?.archive?.npwp?.file,
      nibFile: company?.archive?.nib?.file,
      businessPermit: company?.archive?.businessPermit?.file,
      birthCertificate: company?.archive?.birthCertificate?.file,
      skFile: company?.archive?.sk?.file
    }
  });

  const {
    position,
    isApp,
    isApad,
    isEffectivePermit,

    thirdParty,

    idCardUrl,

    idCard,
    selfPhoto,
    authLetter,
    apadFile,
    appFile,
    npwpFile,
    nibFile,
    businessPermit,
    birthCertificate,
    skFile
  } = watch([
    "position",
    "isApp",
    "isApad",
    "isEffectivePermit",

    "thirdParty",

    "idCardUrl",

    "idCard",

    "selfPhoto",
    "authLetter",
    "apadFile",
    "appFile",
    "npwpFile",
    "nibFile",
    "businessPermit",
    "birthCertificate",
    "skFile"
  ])

  const toStep = (step) => dispatch(verifyAccountActions.setStep(step))

  const onSubmit = (values) => {
    if (isDirty) {
      setLoading(true)
      let data;
      if (idCardUrl) {
        data = {
          ...dirtyValues(dirtyFields, values),
          position: values.position,
          isEffectivePermit: values.isEffectivePermit,
          thirdParty: !Boolean(position === "OWNER") ? values.thirdParty : "",
          idCard
        }
      } else {
        data = {
          // fieldname utk dokumen pemilik usaha
          idCard: values.idCard,
          selfPhoto: values.selfPhoto,
          authLetter: values.authLetter,
          npwpFile: values.npwpFile,
          // fieldname ut dokumen perusahaan
          birthCertificate: values.birthCertificate,
          skFile: values.skFile,
          apadFile: isApad ? values.apadFile : null,
          appFile: isApp ? values.appFile : null,
          nibFile: values.nibFile,
          businessPermit: values.businessPermit,
          // body utk dokumen user
          position: values.position,
          thirdParty: !Boolean(position === "OWNER") ? values.thirdParty : "",
          // body utk dokumen perusahaan
          isEffectivePermit: values.isEffectivePermit
        }
      }

      accountVerificationServices
        .uploadDocument(data, user._id, signature)
        .then(({ data }) => {
          toStep(3)
          dispatch(authActions.requestUser())
        })
        .catch((e) => console.log(e.message))
        .finally(() => {
          setLoading(false)
        })
    } else {
      dispatch(verifyAccountActions.setStep(3))
    }
  }

  const isNext = useMemo(() => {
    if (
      position &&
      idCard &&
      selfPhoto &&
      (Boolean(position === "PIC") ? Boolean(authLetter) : true) &&
      (Boolean(isApad) ? Boolean(apadFile) : true) &&
      (Boolean(isApp) ? Boolean(appFile) : true) &&
      npwpFile &&
      nibFile &&
      birthCertificate &&
      skFile
    ) return true;

    return false;
  }, [
    position,
    isApp,
    isApad,

    idCard,
    selfPhoto,
    authLetter,
    apadFile,
    appFile,
    npwpFile,
    nibFile,
    birthCertificate,
    skFile
  ])

  useEffect(() => {
    register("position");
    register("thirdParty");

    register("idCard")
    register("selfPhoto")
    register("authLetter")
    register("npwpFile")
    register("nibFile")
    register("businessPermit")

    register("isEffectivePermit")
    register("isApad")
    register("isApp")

    register("birthCertificate")
    register("skFile")
    register("apadFile")
    register("appFile")

    return () => {
      unregister("position")
      unregister("thirdParty");

      unregister("idCard")
      unregister("selfPhoto")
      unregister("authLetter")
      unregister("npwpFile")
      unregister("nibFile")
      unregister("businessPermit")
      unregister("isEffectivePermit")
      unregister("isApad")
      unregister("isApp")

      unregister("birthCertificate")
      unregister("skFile")
      unregister("apadFile")
      unregister("appFile")
    }
  }, [register, unregister])

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    async function fetchData() {
      if (idCardUrl) {
        const filename = idCardUrl.split("/")[4].split("_")[0];
        const format = idCardUrl.split("/")[4].split(".")[1];
        const file = await getFileFromUrl(idCardUrl, `${filename}.${format}`);

        setValue("idCard", file, { shouldValidate: true });
        // toDataUrl(idCardUrl, function (myBase64) {
        //   setValue("idCard", dataURItoBlob(myBase64)); // myBase64 is the base64 string
        // });
      }
    }
    fetchData();
  }, [idCardUrl, setValue]);

  return (
    <div className={styles.uploadDocument}>
      <div className={styles.heading}>
        <h5>Verifikasi Data</h5>
        <p>Sesuai dengan <a href="https://www.bi.go.id/id/publikasi/peraturan/Documents/PBI_191017.pdf" target="_blank" rel="noreferrer"><span>peraturan dari Bank Indonesia</span></a>, kami perlu melakukan verifikasi data sebelum Anda dapat melakukan transaksi.</p>
      </div>
      <div className={styles.line}></div>
      <div className={styles.wrapper}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className={styles.form}>
            <div className={styles.subHeading}>
              <h5>Data Pemilik Perusahaan</h5>
            </div>
            <RadioGroup
              row
              name="position"
              value={position}
              onChange={(e) => setValue("position", e.target.value, { shouldValidate: true, shouldDirty: true })}
              label="Posisi Anda:"
              className="mb-32"
            >
              <RadioItem
                label="Direktur/Pemilik Perusahaan"
                value="OWNER"
                checked={position === "OWNER"}
              />
              <RadioItem
                label="Representatif/PIC"
                value="PIC"
                checked={position === "PIC"}
              />
            </RadioGroup>

            <div className="mb-24">
              <Notes
                withoutPin
                text={
                  position === "OWNER" ?
                    <p>Kesesuaian data akan diperiksa dengan yang tertera pada <b>Akta Pendirian Perusahaan</b></p>
                    :
                    <p>Anda perlu mengunggah <b>Surat Kuasa dari Direktur/Pemilik Perusahaan.</b></p>
                }
              />
            </div>
            {position === "PIC" &&
              <TextField
                // ref={register}
                name="thirdParty"
                label={formatterRequired("Nama Representatif/PIC")}
                placeholder="Tulis nama representatif/PIC"
                error={errors.thirdParty}
                helperText={errors.thirdParty?.message}
                value={thirdParty}
                onChange={(e) => setValue("thirdParty", e?.target?.value, { shouldValidate: true, shouldDirty: true })}
              />
            }

            <div className={styles.line}></div>

            <div className={styles.formWrapper}>
              <Accordion
                title="Kartu Identitas Direktur"
                subTitle="Sesuai dengan jenis kartu identitas yang dipilih sebelumnya"
                isOpen={idCard}
              >
                <UploadFile
                  onChange={(v) => setValue("idCard", v, { shouldValidate: true, shouldDirty: true })}
                  className="mb-32"
                  maxSize={10485760}
                  defaultLabel={idCard?.name}
                />
                <Notes />
                <UploadExample />
              </Accordion>
              <Accordion
                title="Foto Selfie"
                subTitle="Foto selfie harus sesuai dengan identitas direktur"
                isOpen={selfPhoto}
              >
                <UploadFile
                  onChange={(v) => setValue("selfPhoto", v, { shouldValidate: true, shouldDirty: true })}
                  className="mb-32"
                  maxSize={10485760}
                  defaultLabel={selfPhoto?.name}
                />
                <Notes />
                <UploadSelfieExample />
              </Accordion>
              {position === "PIC" &&
                <Accordion
                  title="Surat Kuasa"
                  subTitle="Unggah surat kuasa dari direktur perusahaan"
                  isOpen={authLetter}
                >
                  <UploadFile
                    onChange={(v) => setValue("authLetter", v, { shouldValidate: true, shouldDirty: true })}
                    className="mb-32"
                    maxSize={10485760}
                    format={[...IMAGE_FORMATS, ...PDF_FORMATS]}
                    defaultLabel={authLetter?.name}
                  />
                  <Notes />
                </Accordion>
              }
            </div>
          </div>
          <div className={styles.line}></div>
          <div className={styles.form}>
            <div className={styles.subHeading}>
              <h5>Dokumen Perusahaan</h5>
            </div>
            <div className={styles.formWrapper}>
              <Accordion
                title="NPWP Perusahaan"
                isOpen={npwpFile}
              >
                <UploadFile
                  onChange={(v) => setValue("npwpFile", v, { shouldValidate: true, shouldDirty: true })}
                  className="mb-32"
                  maxSize={10485760}
                  defaultLabel={npwpFile?.name}
                  format={[...IMAGE_FORMATS, ...PDF_FORMATS]}
                />
                <Notes />
              </Accordion>
              <Accordion
                title="Akta Pendirian dan SK Kemenkumham"
                subTitle="Disertai Akta Perubahan Anggaran Dasar dan perubahan pengurus beserta SK Kemenkumham, jika ada."
                isOpen={Boolean(birthCertificate || skFile)}
              >
                <div className={styles.uploadSection}>
                  <div className={styles.label}>
                    <h3>Akta Pendirian</h3>
                  </div>
                  <UploadFile
                    onChange={(v) => setValue("birthCertificate", v, { shouldValidate: true, shouldDirty: true })}
                    className="mb-32"
                    maxSize={10485760}
                    defaultLabel={birthCertificate?.name}
                    format={[...IMAGE_FORMATS, ...PDF_FORMATS]}
                  />
                </div>
                <div className={styles.uploadSection}>
                  <div className={styles.label}>
                    <h3>SK Kemenkumham</h3>
                  </div>
                  <UploadFile
                    onChange={(v) => setValue("skFile", v, { shouldValidate: true, shouldDirty: true })}
                    className="mb-32"
                    maxSize={10485760}
                    defaultLabel={skFile?.name}
                    format={[...IMAGE_FORMATS, ...PDF_FORMATS]}
                  />
                </div>
                <div className={styles.additionalUploads}>
                  <div>
                    <div className={styles.label}>
                      <h3>Centang apabila Anda memiliki Akta Perubahan Anggaran Dasar dan/atau Akta Perubahan Pengurus</h3>
                    </div>
                    <div className={styles.checks}>
                      <Checkbox
                        color="primary"
                        variant="secondary"
                        label="Akta Perubahan Anggaran Dasar"
                        checked={isApad}
                        onChange={(e) => setValue("isApad", e.target.checked, { shouldValidate: true })}
                      />
                      <Checkbox
                        color="primary"
                        variant="secondary"
                        label="Akta Perubahan Pengurus"
                        checked={isApp}
                        onChange={(e) => setValue("isApp", e.target.checked, { shouldValidate: true })}
                      />
                    </div>
                  </div>
                </div>
                {isApad &&
                  <div className={styles.uploadSection}>
                    <div className={styles.label}>
                      <h3>Akta Perubahan Anggaran Dasar</h3>
                    </div>
                    <UploadFile
                      onChange={(v) => setValue("apadFile", v, { shouldValidate: true, shouldDirty: true })}
                      className="mb-32"
                      maxSize={10485760}
                      defaultLabel={apadFile?.name}
                      format={[...IMAGE_FORMATS, ...PDF_FORMATS]}
                    />
                  </div>
                }
                {isApp &&
                  <div className={styles.uploadSection}>
                    <div className={styles.label}>
                      <h3>Akta Perubahan Pengurus</h3>
                    </div>
                    <UploadFile
                      onChange={(v) => setValue("appFile", v, { shouldValidate: true, shouldDirty: true })}
                      className="mb-32"
                      maxSize={10485760}
                      defaultLabel={appFile?.name}
                      format={[...IMAGE_FORMATS, ...PDF_FORMATS]}
                    />
                  </div>
                }
                <Notes />
              </Accordion>
              <Accordion
                title="Nomor Induk Berusaha (NIB)"
                isOpen={nibFile}
              >
                <UploadFile
                  onChange={(v) => setValue("nibFile", v, { shouldValidate: true, shouldDirty: true })}
                  className="mb-32"
                  maxSize={10485760}
                  format={[...IMAGE_FORMATS, ...PDF_FORMATS]}
                  defaultLabel={nibFile?.name}
                />
                <Notes />
              </Accordion>
              <Accordion
                title="Izin Usaha dan Sertifikat Standar"
                isOpen={businessPermit}
              >
                <div className={styles.isEffective}>
                  <div>
                    <h3>Apakah Izin Usaha ini sudah berlaku secara efektif?</h3>
                    <div className={styles.checkboxs}>
                      <Checkbox
                        color="primary"
                        label="Ya, sudah berlaku efektif"
                        checked={isEffectivePermit}
                        onChange={() => setValue("isEffectivePermit", true, { shouldValidate: true, shouldDirty: true })}
                      />
                      <Checkbox
                        color="primary"
                        label="Belum efektif"
                        checked={!isEffectivePermit}
                        onChange={() => setValue("isEffectivePermit", false, { shouldValidate: true, shouldDirty: true })}
                      />
                    </div>
                  </div>
                </div>
                <UploadFile
                  onChange={(v) => setValue("businessPermit", v, { shouldValidate: true, shouldDirty: true })}
                  className="mb-32"
                  maxSize={10485760}
                  format={[...IMAGE_FORMATS, ...PDF_FORMATS]}
                  defaultLabel={businessPermit?.name}
                />
                <Notes />
              </Accordion>
            </div>
          </div>
          <div className={styles.buttons}>
            <MainButton
              variant="secondary"
              type="button"
              onClick={() => toStep(1)}
            >
              Kembali
            </MainButton>
            <MainButton
              type="submit"
              disabled={!isNext}
              loading={loading}
            >
              Selanjutnya
            </MainButton>
          </div>
        </form>
      </div>
    </div>
  )
}

export default UploadDocument
