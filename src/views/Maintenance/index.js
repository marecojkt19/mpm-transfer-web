import React from 'react'
import styles from './maintenance.module.scss'
import MainButton from '../../components/Form/MainButton'

const Maintenance = () => {
  return (
    <div className={styles.maintenance}>
      <div className={styles.wrapper}>
        <img
          src="/assets/maintenance.png"
          alt=''
        />
        <h3>Sedang Ada Perbaikan Sistem</h3>
        <p>Saat ini Dipay sedang meningkatkan kualitas untuk bisa menjadi lebih baik. Harap kembali lagi nanti ya.</p>
        <MainButton onClick={() => window.close()}>
          Tutup Halaman Ini
        </MainButton>
      </div>
    </div>
  )
}

export default Maintenance
