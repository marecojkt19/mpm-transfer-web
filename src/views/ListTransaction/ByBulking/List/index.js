import { endOfDay, format, formatISO, startOfDay } from "date-fns";
import { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import transactionService from '../../../../axios/services/transactionService';
import DateRangePicker from '../../../../components/Form/DatesNew/DateRangePicker';
import SelectField from '../../../../components/Form/SelectFieldNew';
import Table from '../../../../components/Table';
import { OrderStatusOpts } from '../../../../utils/enums/orderStatusTypes';
import { getStatus } from '../../../../utils/helpers/getStatus';
import toIDR from '../../../../utils/helpers/toIDR';
import styles from './list.module.scss';
import { transactionByBulkingActions } from "../../../../redux/actions/transactions/transactionByBulkingActions";
import { statusTypes } from "../../../../utils/enums/statusTypes";
import { Link, useLocation } from "react-router-dom";
import id from "date-fns/locale/id";

const List = () => {
  const dispatch = useDispatch();
  const {
    items,
    tableConfig: {
      isLoading,
      page,
      totalData,
      limit,
      sort,
      search,
      filter,
    }
  } = useSelector(state => state.transactions.trxByBulking);
  const query = new URLSearchParams(useLocation().search)
  const status = query.get("status");

  const setTableConfig = useCallback(
    key => value => dispatch(transactionByBulkingActions.setTableConfig(key, value)),
    [dispatch],
  );

  const setTableConfigFilter = useCallback(
    key => value => dispatch(transactionByBulkingActions.setTableConfigFilter(key, value)),
    [dispatch],
  );

  const load = useCallback(
    () => {
      const tConfig = {
        limit,
        page,
        sort,
        filter
      }
      return dispatch(transactionByBulkingActions.loadRequested(tConfig))
    },
    [limit, page, sort, filter, dispatch],
  );

  useEffect(() => {
    load()
  }, [load])

  useEffect(() => {
    if (status) {
      setTableConfigFilter('status')(status)
    }
  }, [setTableConfigFilter, status])

  const BtnDetail = ({ v, row }) => (
    <Link
      className={styles.action}
      to={(v === statusTypes.PENDING) ? `/transaction/bulking/${row?.transactionCode}/approval` : `/transaction/bulking/${row?.transactionCode}/review`}
    >
      Lihat Detail
    </Link>
  );

  const columns = useMemo(() => [
    { title: 'ID Transaksi', key: 'code' },
    { title: 'Waktu dibuat', key: 'createdAt', sortable: true, render: v => `${format(new Date(v), 'dd MMM yyyy, HH:mm:ss', { locale: id })} WIB` },
    { title: 'Nama', key: 'actor.fullName', sortable: true },
    { title: 'Peran', key: 'actor.role' },
    { title: 'Nominal (Rp)', key: 'totalAmount', sortable: true, render: v => v ? toIDR(v, false) : "-" },
    { title: 'Metode', key: 'method', render: v => <span className="text-dark-gray">{v}</span> },
    { title: 'Status', key: 'status', render: v => v ? getStatus({ status: v }) : "-" },
    {
      title: 'Volume Transaksi', key: 'transactions', render: (v, row) => {
        if (row?.status === statusTypes.COMPLETED) {
          return (
            <div className={styles.trxVolume}>
              <span>{v.filter(j => j.status === "COMPLETED").length} Berhasil</span>
              <span>|</span>
              <span>{v.filter(j => j.status === "FAILED").length} Gagal</span>
            </div>
          )
        }
        return "-"
      }
    },
    { title: 'Action', key: 'status', render: (v, row) => <BtnDetail v={v} row={row} /> }
  ], []);

  const excelColumns = useMemo(() => [
    { title: 'ID Transaksi', key: 'code' },
    { title: 'Waktu dibuat', key: 'createdAt', render: v => `${format(new Date(v), 'd MMM yyyy, HH:mm')} WIB` },
    { title: 'Nama', key: 'actor.fullName' },
    { title: 'Peran', key: 'actor.role' },
    { title: 'Nominal (Rp)', key: 'totalAmount', render: v => v ? toIDR(v, false) : "-" },
    { title: 'Metode', key: 'method', render: v => <span className="text-dark-gray">{v}</span> },
    { title: 'Status', key: 'status' },
  ], []);

  const APIParamsForExcel = useMemo(() => ({
    page: page,
    limit: limit,
    sort,
    search,
    startDate: filter.startDate ? formatISO(startOfDay(new Date(filter.startDate))) : null,
    endDate: filter.endDate ? formatISO(endOfDay(new Date(filter.endDate))) : null,
    type: "SINGLE",
    status: filter.status,
    ...filter,
  }), [filter, sort, limit, page, search]);

  return (
    <div className={styles.listTransaction}>
      <Table
        data={items}
        onChangePage={(v) => setTableConfig('page')(v)}
        onSort={(v) => setTableConfig('sort')(v)}
        onLimit={(v) => setTableConfig('limit')(v)}
        onSearch={(v) => setTableConfigFilter('search')(v)}
        load={load}
        search={true}
        searchPlaceholder="Cari ID, Nama, ..."
        config={{
          columns: columns,
          withIndex: true,
          total: totalData,
          limit,
          currentPage: page,
          loading: isLoading,
          sort,
          showRender: (from, to, total) => `Tampilkan ${from}-${to} dari ${total} Transaksi`
        }}
        excel={{
          columns: excelColumns,
          filename: 'Table Transaksi Massal',
          allPage: true,
          api: {
            apiResponseKey: 'orders',
            service: transactionService.orderListByType,
            APIParams: APIParamsForExcel
          }
        }}
      >
        <SelectField
          variant="outlined"
          placeholder="Status"
          options={OrderStatusOpts}
          value={filter.status}
          onChange={v => setTableConfigFilter('status')(v?.value)}
          isClearable
        />
        <SelectField
          variant="outlined"
          placeholder="Urutkan berdasarkan"
          options={[
            { label: 'Waktu Dibuat: Terbaru', value: 'createdAt|desc' },
            { label: 'Waktu Dibuat: Terlama', value: 'createdAt|asc' },
            { label: 'Nama: A-Z', value: 'actor.fullName|asc' },
            { label: 'Nama: Z-A', value: 'actor.fullName|desc' },
            { label: 'Nominal Terendah', value: 'totalAmount|asc' },
            { label: 'Nominal Tertinggi', value: 'totalAmount|desc' }
          ]}
          value={sort}
          onChange={v => {
            if (v?.value !== sort) {
              setTableConfig('sort')(v?.value)
            } else {
              setTableConfig('sort')("")
            }
          }}
          isClearable
        />
        <DateRangePicker
          amountOfMonths={2}
          placeholder="Rentang Tanggal"
          fromDate={filter.startDate}
          toDate={filter.endDate}
          maxDate={new Date()}
          onChange={({ fromDate, toDate }) => {
            if (fromDate && toDate) {
              setTableConfigFilter('startDate')(fromDate)
              setTableConfigFilter('endDate')(toDate)
            } else if (!fromDate && !toDate) {
              setTableConfigFilter('startDate')()
              setTableConfigFilter('endDate')()
            }
          }}
        />
      </Table>
    </div>
  )
}

export default List
