
import React from "react";
import getPropByString from "../../../utils/helpers/getPropByString";

export const ExcelTable = ({ columns, items, lastLength }) => {
  return (
    <>
      <table border="1">
        <thead>
          <tr style={{ textAlign: "center" }}>
            <th>#</th>
            {columns.map((col, i) => {
              return <th key={i}>{col.title}</th>
            })}
          </tr>
        </thead>
        <tbody>
          {
            items.map((row, i) => {
              return (
                <tr key={i}>
                  <td>{i + 1}</td>
                  {columns.map((col, key) =>
                    <td align={col.align} key={key}>
                      {
                        typeof col.render === "function" ?
                          col.render(getPropByString(row, col.key), row)
                          :
                          getPropByString(row, col.key)
                      }
                    </td>
                  )}
                </tr>
              )
            })
          }
        </tbody>
      </table>
      <table>
        <thead></thead>
        <tbody>
          <tr></tr>
          <tr>
            <td></td>
            <td>Nominal Transfer</td>
            <td></td>
            <td></td>
            <td></td>
            <td>{`=SUM(F2:F${lastLength + 1})`}</td>
          </tr>
          <tr>
            <td></td>
            <td>Biaya Admin</td>
            <td></td>
            <td></td>
            <td></td>
            <td>{`=SUM(G2:G${lastLength + 1})`}</td>
          </tr>
          <tr></tr>
          <tr>
            <td></td>
            <td><b>Grand Total</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td>{`=SUM(F${lastLength + 3}:F${lastLength + 4})`}</td>
          </tr>
        </tbody>
      </table>
    </>
  )
}
