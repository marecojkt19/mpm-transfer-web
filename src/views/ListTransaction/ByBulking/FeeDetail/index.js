import React, { useMemo } from 'react'
import styles from './feeDetail.module.scss'
import toIDR from '../../../../utils/helpers/toIDR'
import Table from '../../../../components/Table'
import { useSelector } from 'react-redux'
import { FailedCircleSmall, SuccessCircleSmall } from '../../../../assets/icons'
import clsx from 'clsx'
import { statusTypes } from '../../../../utils/enums/statusTypes'

const FeeDetail = ({
  showTrxVolumes = true
}) => {
  const { company } = useSelector(state => state.auth)
  const {
    items: {
      statusPayment,
      detailTrx
    },
    tableConfig: {
      isLoading
    }
  } = useSelector(state => state.transactions.trxDetail);

  const columns = useMemo(() => [
    { title: 'Bank Penerima', key: 'bank', render: v => v || '-' },
    { title: 'Biaya Transfer', key: 'fee', render: v => `Rp${toIDR(v, false)}` || '-' },
    { title: 'Jumlah Transaksi', key: 'volumeTrx', render: v => v || '-' },
    { title: 'Subtotal', key: 'subTotal', align: 'right', render: v => `Rp${toIDR(v, false)}` || '-' },
  ], [])

  return (
    <div className={styles.feeDetail}>
      <div className={styles.transferAmount}>
        <div className={styles.tableCard}>
          <div className={styles.row}>
            <span>Nama Pengirim</span>
            <span>:</span>
            <span>{company?.name}</span>
          </div>
          <div className={styles.row}>
            <span>Jumlah Penerima</span>
            <span>:</span>
            <span>{detailTrx?.totalRecipient} Penerima</span>
          </div>
          <div className={styles.row}>
            <span>Nominal Transfer</span>
            <span>:</span>
            <span>Rp{toIDR(detailTrx?.totalTrf, false)}</span>
          </div>
        </div>
        {(statusPayment === statusTypes.COMPLETED && showTrxVolumes) ?
          <div className={styles.trxVolumes}>
            <div className={styles.volume}>
              <div className={styles.icon}>
                <SuccessCircleSmall />
              </div>
              <div className={clsx(styles.info, styles.success)}>
                <h5>Transaksi Berhasil</h5>
                <div className={styles.detail}>
                  <div className={styles.row}>
                    <p>Volume Transaksi</p>
                    <span>{detailTrx?.volumeTrxSuccess}</span>
                  </div>
                  <div className={styles.row}>
                    <p>Nominal Transaksi</p>
                    <span>Rp{toIDR(detailTrx?.nominalTrxSuccess, false)}</span>
                  </div>
                </div>
              </div>
            </div>
            <div className={styles.divider}></div>
            <div className={styles.volume}>
              <div className={styles.icon}>
                <FailedCircleSmall />
              </div>
              <div className={clsx(styles.info, styles.failed)}>
                <h5>Transaksi Gagal</h5>
                <div className={styles.detail}>
                  <div className={styles.row}>
                    <p>Volume Transaksi</p>
                    <span>{detailTrx?.volumeTrxFailed}</span>
                  </div>
                  <div className={styles.row}>
                    <p>Nominal Transaksi</p>
                    <span>Rp{toIDR(detailTrx?.nominalTrxFailed, false)}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          : null
        }
      </div>
      {statusPayment !== statusTypes.COMPLETED || (statusPayment === statusTypes.COMPLETED && detailTrx?.volumeTrxSuccess) ?
        <div className={styles.table}>
          <div className={styles.label}>
            <span>Detail Biaya Admin</span>
          </div>
          <Table
            data={detailTrx?.detailFeeByBank}
            config={{
              loading: isLoading,
              pagination: false,
              columns: columns
            }}
          />
          <div className={styles.footerCreate}>
            <div className={styles.content}>
              <div>
                <span>Total Biaya Admin:</span>
                <span>Rp{toIDR(detailTrx?.feeTrx, false)}</span>
              </div>
              <div>
                <span>Nominal Transfer: </span>
                <span>Rp{toIDR(detailTrx?.nominalTrx, false)}</span>
              </div>
              <div className={styles.lineDash} />
              <div>
                <span>Total Transfer & Admin:</span>
                <span className={styles.primary}>Rp{toIDR(detailTrx?.totalNominalPlusFee, false)}</span>
              </div>
            </div>
          </div>
        </div>
        :
        null
      }
    </div>
  )
}

export default FeeDetail
