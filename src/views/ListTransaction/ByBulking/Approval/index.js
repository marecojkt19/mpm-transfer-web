import { useSelector } from "react-redux";
import Steps, { Step } from "../../../../components/Steps";
import Review from "../Review";
import Metode from "./Metode";
import Verification from "./Verification";

const Approval = () => {
  const { step } = useSelector(state => state.transactions.trxDetail);

  return (
    <Steps
      activeStep={step}
      className="my-24"
    >
      <Step label="Persetujuan">
        <Review />
      </Step>
      <Step label="Metode Pembayaran">
        <Metode />
      </Step>
      <Step label="Verifikasi">
        <Verification />
      </Step>
    </Steps>
  )
}

export default Approval;
