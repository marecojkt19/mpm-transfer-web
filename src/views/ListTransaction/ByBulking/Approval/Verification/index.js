import React, { useEffect, useMemo, useState } from 'react'
import styles from './verification.module.scss'
import transactionService from '../../../../../axios/services/transactionService'
import { useDispatch, useSelector } from 'react-redux';
import { transactionDetailActions } from '../../../../../redux/actions/transactions/transactionDetailActions';
import OTPInput from '../../../../../components/Form/OTPInput';
import MainButton from '../../../../../components/Form/MainButton';
import Button from '../../../../../components/Button';
import timeConvert from '../../../../../utils/helpers/timeConverter';
import SimpleCard from '../../../../../components/templates/SimpleCard';
import { useToasts } from 'react-toast-notifications';
import Success from './Success';

const Verification = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [confirmed, setConfirmed] = useState(false);
  const { addToast } = useToasts();
  const {
    otpMethod,
    paymentMethod,
    isSendInvoice,
    items: {
      transactionId,
      orderId
    }
  } = useSelector(state => state.transactions.trxDetail);
  const { user, company } = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const [value, setValue] = useState('');
  const [error, setError] = useState('');
  const [resendTimer, setResendTimer] = useState(120);
  const [resend, setResend] = useState(true);

  const getOtpTitle = useMemo(() => {
    switch (otpMethod) {
      case "EMAIL": return "Email"
      case "SMS": return "Nomor Handphone"
      default: return null
    }
  }, [otpMethod])

  const getOtpSubtitle = useMemo(() => {
    switch (otpMethod) {
      case "EMAIL": return "email"
      case "SMS": return ""
      default: return null
    }
  }, [otpMethod])

  const getMethod = useMemo(() => {
    switch (otpMethod) {
      case "EMAIL": return user.email
      case "SMS": return company.phoneNumber
      default: return null
    }
  }, [otpMethod, user, company])

  const getAsset = useMemo(() => {
    switch (otpMethod) {
      case "EMAIL": return "/assets/media/others/otp-email.png"
      case "SMS": return "/assets/media/others/otp-phone.png"
      default: return null
    }
  }, [otpMethod])

  const onRequestOTP = () => {
    if (paymentMethod) {
      setIsLoading(true)
      transactionService
        .requestOTP({
          morph: getMethod,
          type: otpMethod
        })
        .then(() => {
          dispatch(transactionDetailActions.setStep(2))
          setIsLoading(false)
        })
    }
  }

  const onSendFund = () => {
    if (transactionId && otpMethod && value && orderId) {
      setIsLoading(true)
      transactionService
        .sendFund({
          orderId: orderId,
          type: otpMethod,
          morph: getMethod,
          otp: value,
          approveOrderDto: {
            paymentMethod,
            sendEmailReceiver: isSendInvoice ? 1 : 0
          },
        })
        .then(() => {
          setConfirmed(true)
        })
        .catch((err) => {
          if (err?.response?.data?.message) {
            addToast(err?.response?.data?.message, { appearance: 'danger' });
          }
        })
        .finally(() => {
          setIsLoading(false)
        })
    }
  }

  useEffect(() => {
    const tryAgain = setInterval(() => {
      if (resendTimer) setResendTimer(prev => prev - 1)
      else {
        clearInterval(tryAgain)
        setResend(false)
      }
    }, 1000);
    return () => {
      clearInterval(tryAgain)
    }
  }, [resendTimer, setResendTimer, setResend])

  useEffect(() => {
    const handleBeforeUnload = () => {
      dispatch(transactionDetailActions.setStep(1))
    };

    window.addEventListener('beforeunload', handleBeforeUnload);

    return () => {
      window.removeEventListener('beforeunload', handleBeforeUnload);
    };
  }, [dispatch]);

  return (
    <SimpleCard>
      {confirmed ?
        <Success />
        :
        <div className={styles.verification}>
          <div className={styles.wrapper}>
            <div className={styles.headingText}>
              <h5>Verifikasi {getOtpTitle} telah dikirim!</h5>
              <p>Dipay Disbursement telah mengirimkan kode verifikasi ke:</p>
              <h6>{getMethod}</h6>
            </div>
            <div className={styles.image}>
              <img
                src={getAsset}
                alt=""
              />
            </div>
            <div className={styles.input}>
              <OTPInput
                length={6}
                numberOnly
                value={value}
                onChange={v => {
                  if (error) setError('')
                  setValue(v)
                }}
                error={error}
              />
            </div>
            <div className={styles.note}>
              <span>
                Silakan cek inbox Anda dan masukkan kode verifikasi {getOtpSubtitle}
              </span>
            </div>
            <div className={styles.buttonWrapper}>
              <MainButton
                onClick={onSendFund}
                disabled={!(value.length === 6) || isLoading}
                loading={isLoading}
              >
                Selanjutnya
              </MainButton>
            </div>
            {
              resendTimer
                ?
                <div className={styles.resend}>
                  <span>Kirim ulang dalam <b>{resend && ` ${timeConvert(resendTimer)}`}</b></span>
                </div>
                :
                <div className={styles.timer}>
                  <span>Belum menerima kode verifikasi?</span>
                  <Button onClick={onRequestOTP}>
                    Kirim Ulang
                  </Button>
                </div>
            }
            <div className={styles.timer}>
              <span>Ingin mengubah metode?</span>
              <Button onClick={() => dispatch(transactionDetailActions.setStep(1))}>
                Kembali
              </Button>
            </div>
          </div>
        </div>
      }
    </SimpleCard>
  )
}

export default Verification
