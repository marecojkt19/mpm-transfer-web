import React, { useCallback, useEffect } from 'react'
import MainButton from '../../../../../../components/Form/MainButton'
import styles from './success.module.scss'
import { transactionDetailActions } from '../../../../../../redux/actions/transactions/transactionDetailActions'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

const Success = () => {
  const { items: { transactionId } } = useSelector(state => state.transactions.trxDetail);
  const { push } = useHistory();
  const dispatch = useDispatch();

  const onOkay = useCallback(() => {
    push("/transaction")
    dispatch(transactionDetailActions.reset())
  }, [dispatch, push])

  useEffect(() => {
    window.addEventListener('beforeunload', onOkay);

    return () => {
      window.removeEventListener('beforeunload', onOkay);
    };
  }, [onOkay]);

  return (
    <div className={styles.landingPage}>
      <div className={styles.heading}>
        <h3>Transaksi Berhasil Disetujui!</h3>
        <p>Status transaksi {transactionId} dapat dicek pada menu daftar transaksi</p>
      </div>
      <div className={styles.verifiedImage}>
        <img src="/assets/media/others/verified.png" alt="" />
      </div>
      <div className={styles.button}>
        <MainButton onClick={onOkay}>Lihat Daftar Transaksi</MainButton>
      </div>
    </div>
  )
}

export default Success
