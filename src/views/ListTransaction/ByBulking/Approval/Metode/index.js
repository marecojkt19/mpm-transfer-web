import React, { useState } from 'react'
import FeeDetail from '../../FeeDetail'
import SimpleCard from '../../../../../components/templates/SimpleCard'
import styles from './metode.module.scss'
import MainButton from '../../../../../components/Form/MainButton'
import RadioGroup from '../../../../../components/Form/Radio/RadioGroup2'
import { useDispatch, useSelector } from 'react-redux'
import toIDR from '../../../../../utils/helpers/toIDR'
import RadioItem from '../../../../../components/Form/Radio/RadioItem'
import { ChecklistOff, ChecklistOn } from '../../../../../assets/icons'
import Modal, { ModalBody, ModalHead, ModalWrapper } from '../../../../../components/ModalNew'
import { transactionDetailActions } from '../../../../../redux/actions/transactions/transactionDetailActions'
import transactionService from '../../../../../axios/services/transactionService'
import TncTemplate from '../../../../../components/templates/TncTemplate'

const Metode = () => {
  const [showExample, setShowExample] = useState(false);
  const [showTnc, setShowTnc] = useState(false);
  const [showVerification, setShowVerification] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const {
    transferFeeDetails,
    otpMethod,
    paymentMethod,
    isSendInvoice
  } = useSelector(state => state.transactions.trxDetail);
  const { user, company } = useSelector(state => state.auth);
  const dispatch = useDispatch();

  const getTitle = (str) => {
    if (str === "DEPOSIT") return "Deposit";
    return str;
  }

  const getMethod = (str) => {
    if (str === "EMAIL") return user.email;
    if (str === "SMS") return company.phoneNumber;
    return str;
  }

  const onSubmit = () => {
    if (paymentMethod) {
      setIsLoading(true)
      transactionService
        .requestOTP({
          morph: getMethod(otpMethod),
          type: otpMethod
        })
        .then(() => {
          dispatch(transactionDetailActions.setStep(2))
          setIsLoading(false)
        })
    }
  }

  return transferFeeDetails ? (
    <>
      <div className={styles.metode}>
        <SimpleCard>
          <FeeDetail showTrxVolumes={false} />
          {
            company?.status?.alwaysSendEmailReceiveTrx === 1 ?
              <div className={styles.sendInvoice}>
                <div className={styles.content}>
                  <div className={styles.row}>
                    <h5>Bukti Transaksi akan dikirim ke email Penerima Dana 📧</h5>
                    <button onClick={() => setShowExample(true)}>Lihat Contoh Bukti Transaksi</button>
                  </div>
                  <p>
                    Bukti Transaksi dikirim secara otomatis setelah dana diterima di rekening penerima dana. Buka Pengaturan untuk me-nonaktifkan notifikasi tersebut.
                  </p>
                </div>
              </div>
              :
              <div className={styles.sendInvoice}>
                <div className={styles.checkBox}>
                  <button
                    type="button"
                    onClick={() => dispatch(transactionDetailActions.setIsSendInvoice(!isSendInvoice))}
                  >
                    {isSendInvoice ? <ChecklistOn /> : <ChecklistOff />}
                  </button>
                </div>
                <div className={styles.content}>
                  <div className={styles.row}>
                    <button
                      type="button"
                      onClick={() => dispatch(transactionDetailActions.setIsSendInvoice(!isSendInvoice))}
                    >
                      <h5>Kirim Bukti Transaksi ke email Penerima Dana 📧</h5>
                    </button>
                    <button onClick={() => setShowExample(true)}>Lihat Contoh Bukti Transaksi</button>
                  </div>
                  {!isSendInvoice ?
                    <p>
                      Dengan mengaktifkan fitur kirim bukti transaksi, penerima dana akan secara otomatis menerima email berisi bukti transaksi yang dapat diunduh.
                    </p>
                    :
                    <p>
                      Setelah dana diterima di rekening penerima, bukti transaksi akan dikirim secara otomatis. Penerima juga akan mendapatkan informasi mengenai dana yang diterima serta berita dari pengirim.
                    </p>
                  }
                </div>
              </div>
          }
          <div className={styles.paymentMethod}>
            <div className={styles.heading}>
              <h5>Pilih Metode Pembayaran</h5>
            </div>
            <div className={styles.radio}>
              <RadioGroup
                name="method"
                value={paymentMethod}
                onChange={(e) => dispatch(transactionDetailActions.setPaymentMethod(e.target.value))}
                className="mb-32"
              >
                {
                  transferFeeDetails?.payment.map(item => (
                    <RadioItem
                      size="lg"
                      key={item.method}
                      value={item.method}
                      label={
                        <div className={styles.radioItem}>
                          <h5>{getTitle(item.method)}</h5>
                          <p>Deposit saat ini: <span className='text-primary'>Rp{toIDR(item.currentAmount, false)}</span></p>
                        </div>
                      }
                    />
                  ))
                }
              </RadioGroup>
            </div>
          </div>

          <div className={styles.tnc}>
            <p>
              Dengan menekan tombol “Lanjutkan” saya sudah paham dengan
              {" "}
              <button
                type="button"
                onClick={() => setShowTnc(true)}
                className="font-500 text-primary"
              >
                Syarat & Ketentuan
              </button>
              {" "}
              transfer Dipay dan mengetahui transaksi yang sedang diproses tidak dapat dibatalkan.
            </p>
          </div>
          <div className={styles.buttons}>
            <div className={styles.buttonsWrapper}>
              <MainButton
                variant="primary-light"
                onClick={() => dispatch(transactionDetailActions.setStep(0))}
              >
                Kembali
              </MainButton>
              <MainButton
                disabled={!paymentMethod}
                onClick={() => setShowVerification(true)}
              >
                Lanjutkan
              </MainButton>
            </div>
          </div>
        </SimpleCard>
      </div>
      <Modal
        in={showExample}
        onClose={() => setShowExample(false)}
      >
        <ModalWrapper width="50%">
          <ModalHead
            title="Contoh Bukti Transaksi"
            onClose={() => setShowExample(false)}
          />
          <ModalBody>
            <div className={styles.modal}>
              <div className={styles.example}>
                <img
                  src="/assets/example/reciept.png"
                  alt=""
                />
              </div>
            </div>
          </ModalBody>
        </ModalWrapper>
      </Modal>
      <Modal
        in={showVerification}
        onClose={() => setShowVerification(false)}
      >
        <ModalWrapper width="50%">
          <ModalHead
            title="Kirim Kode Verifikasi Melalui:"
            onClose={() => setShowVerification(false)}
          />
          <hr />
          <ModalBody>
            <div className={styles.modal}>
              <div className={styles.content}>
                <RadioGroup
                  name="otpMethod"
                  value={otpMethod}
                  onChange={(e) => dispatch(transactionDetailActions.setOtpMethod(e.target.value))}
                  className="mb-16"
                >
                  <RadioItem
                    className={styles.radio}
                    label={<><b>Email: </b>{user.email}</>}
                    value="EMAIL"
                  />
                  <RadioItem
                    className={styles.radio}
                    label={<><b>Nomor handphone: </b>{company.phoneNumber ?? "-"}</>}
                    value="SMS"
                  />
                </RadioGroup>
                <p className="text-dark-gray">Pastikan Email atau Nomor Handphone Anda aktif</p>
              </div>
              <div className={styles.buttons}>
                <div className={styles.buttonsWrapper}>
                  <MainButton
                    disabled={!otpMethod}
                    loading={isLoading}
                    onClick={onSubmit}
                  >
                    Kirim Kode Verifikasi
                  </MainButton>
                </div>
              </div>
            </div>
          </ModalBody>
        </ModalWrapper>
      </Modal>

      <Modal in={showTnc} onClose={() => setShowTnc(false)}>
        <TncTemplate />
      </Modal>
    </>
  ) : null
}

export default Metode
