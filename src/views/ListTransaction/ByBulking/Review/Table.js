import { useCallback, useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import Table from "../../../../components/Table";
import toIDR from "../../../../utils/helpers/toIDR";
import { censoredName } from "../../../../utils/helpers/stringManipulation";
import { transactionDetailActions } from "../../../../redux/actions/transactions/transactionDetailActions";
import transactionService from "../../../../axios/services/transactionService";
import SelectField from "../../../../components/Form/SelectFieldNew";
// import { BankOpts } from "../../../../utils/enums/bankTypes";
import { useParams } from "react-router-dom";
import useAsync from "../../../../components/hooks/useAsync";

const TableDetail = () => {
  const dispatch = useDispatch();
  const { trxCode } = useParams();
  const {
    items,
    // trxCode,
    tableConfig: {
      totalData,
      isLoading,
      limit,
      page,
      sort,
      filter
    }
  } = useSelector(state => state.transactions.trxDetail);

  const load = useCallback(
    () => {
      const tConfig = {
        limit,
        page,
        sort,
        trxCode,
        filter: {
          ...filter,
          status: filter.status,
          bank: filter.bank
        }
      }
      return dispatch(transactionDetailActions.loadRequested(tConfig))
    },
    [limit, page, sort, trxCode, dispatch, filter],
  );

  useEffect(() => {
    load()
  }, [load])

  const setTableConfig = useCallback(
    key => value => dispatch(transactionDetailActions.setTableConfig(key, value)),
    [dispatch],
  );

  const setTableConfigFilter = useCallback(
    key => value => dispatch(transactionDetailActions.setTableConfigFilter(key, value)),
    [dispatch],
  );

  const onChangePage = useMemo(
    () => setTableConfig('page'),
    [setTableConfig]
  );

  const columns = useMemo(() => [
    { title: 'Baris ke-', sortable: true, key: 'excelIndex', render: v => v || '-' },
    { title: 'Bank Tujuan', key: 'destinationBank', render: v => v || '-' },
    { title: 'Nomor Rekening', key: 'beneficiaryAccountNo', render: v => v || '-' },
    { title: 'Nama Penerima', sortable: true, key: 'beneficiaryAccountName', render: v => v || '-' },
    { title: 'Nama Penerima dari Bank', key: 'beneficiaryAccountNameFromBank', render: v => censoredName(v) || '-' },
    { title: 'Nominal (Rp)', sortable: true, key: 'amount', render: v => toIDR(v, false) || '-' },
    { title: 'Berita', key: 'description', render: v => v || '-' },
    { title: 'Email Penerima', key: 'beneficiaryEmail', render: v => v || '-' },
    {
      title: 'Status', key: 'statusDesc', render: (v, row) => v ? <span style={{ color: row?.statusColor }}>{v}</span> : "-"
    },
  ], [])

  const excelColumns = useMemo(() => [
    { title: 'Baris ke-', key: 'excelIndex', render: v => v || '-' },
    { title: 'Tujuan Transfer', key: 'destinationBank', render: v => v || '-' },
    { title: 'Nomor Rekening', key: 'beneficiaryAccountNo', render: v => v || '-' },
    { title: 'Nama Penerima', key: 'beneficiaryAccountName', render: v => v || '-' },
    { title: 'Nama Penerima dari Bank', key: 'beneficiaryAccountNameFromBank', render: v => censoredName(v) || '-' },
    { title: 'Nominal (Rp)', key: 'amount', render: v => toIDR(v, false) || '-' },
    { title: 'Berita', key: 'description', render: v => v || '-' },
    { title: 'Email Penerima', key: 'beneficiaryEmail', render: v => v || '-' },
    {
      title: 'Status', key: 'statusDesc', render: (v, row) => v ? <span style={{ color: row?.statusColor }}>{v}</span> : "-"
    },
  ], []);

  const APIParamsForExcel = useMemo(() => ({
    trxCode,
    page,
    limit,
    sort,
    ...filter
  }), [filter, sort, limit, page, trxCode]);

  const {
    pending,
    value: { data: bankOpts } = {}
  } = useAsync(transactionService.getListBank, {});

  return (
    <Table
      data={items.transaction}
      onChangePage={onChangePage}
      onSort={(v) => setTableConfig('sort')(v)}
      onLimit={(v) => setTableConfig('limit')(v)}
      load={load}
      search={true}
      onSearch={(v) => setTableConfigFilter('search')(v)}
      searchPlaceholder="Cari Nama, Rekening, ..."
      config={{
        columns: columns,
        withIndex: true,
        total: totalData,
        limit,
        currentPage: page,
        loading: isLoading,
        sort,
        showRender: (from, to, total) => `Tampilkan ${from}-${to} dari ${total} Transaksi`
      }}
      excel={{
        columns: excelColumns,
        filename: 'Table Detail Transaksi Massal',
        allPage: true,
        api: {
          apiResponseKey: 'transaction',
          service: transactionService.detail,
          APIParams: APIParamsForExcel
        }
      }}
    >
      <SelectField
        variant="outlined"
        placeholder="Bank"
        options={bankOpts}
        loading={pending}
        value={filter.bank}
        onChange={v => {
          if (v?.value !== filter.bank) {
            setTableConfigFilter('bank')(v?.value)
          } else {
            setTableConfigFilter('bank')("")
          }
        }}
        isClearable
      />
      <SelectField
        variant="outlined"
        placeholder="Status"
        options={[
          { value: "PENDING", label: "Diproses" },
          { value: "COMPLETED", label: "Berhasil" },
          { value: "FAILED", label: "Gagal" }
        ]}
        value={filter.status}
        onChange={v => {
          if (v?.value !== filter.status) {
            setTableConfigFilter('status')(v?.value)
          } else {
            setTableConfigFilter('status')("")
          }
        }}
        isClearable
      />
      <SelectField
        variant="outlined"
        placeholder="Urutkan berdasarkan"
        options={[
          { label: 'Baris Terkecil', value: 'excelIndex|asc' },
          { label: 'Baris Terbesar', value: 'excelIndex|desc' },
          { label: 'Nama: A-Z', value: 'beneficiaryAccountName|asc' },
          { label: 'Nama: Z-A', value: 'beneficiaryAccountName|desc' },
          { label: 'Nominal Terendah', value: 'amount|asc' },
          { label: 'Nominal Tertinggi', value: 'amount|desc' }
        ]}
        value={sort}
        onChange={v => {
          if (v?.value !== sort) {
            setTableConfig('sort')(v?.value)
          } else {
            setTableConfig('sort')("")
          }
        }}
        isClearable
      />
    </Table>
  )
}

export default TableDetail;
