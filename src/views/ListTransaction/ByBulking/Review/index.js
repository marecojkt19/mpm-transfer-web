import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import TableDetail from './Table';
import SimpleCard from '../../../../components/templates/SimpleCard';
import styles from './review.module.scss'
import { transactionDetailActions } from '../../../../redux/actions/transactions/transactionDetailActions';
import { statusTypes } from '../../../../utils/enums/statusTypes';
import MainButton from '../../../../components/Form/MainButton';
import Modal, { ModalBody, ModalHead, ModalWrapper } from '../../../../components/ModalNew';
import toIDR from '../../../../utils/helpers/toIDR';
import { useHistory, useParams } from 'react-router-dom';
import transactionService from '../../../../axios/services/transactionService';
import FeeDetail from '../FeeDetail';
import { format } from 'date-fns';
import id from 'date-fns/locale/id';
import Alert from '../../../../components/Alert';
import { Info } from '../../../../assets/icons';

const getStatus = (status) => {
  switch (status) {
    case statusTypes.PENDING:
      return (
        <div
          style={{ color: "#276EF1" }}
          className="font-size-16"
        >
          Menunggu Persetujuan
        </div>
      )
    case statusTypes.PROCESSING:
      return (
        <div
          style={{ color: "#FFC043" }}
          className="font-size-16"
        >
          Diproses
        </div>
      )
    case statusTypes.CANCELED:
      return (
        <div
          style={{ color: "#E11900" }}
          className="font-size-16"
        >
          Dibatalkan
        </div>
      )
    case statusTypes.COMPLETED:
      return (
        <p
          style={{ color: "#05944F" }}
          className="font-size-16"
        >
          Selesai
        </p>
      )
    default:
      return (
        <div>{status}</div>
      );
  }
}

const Review = () => {
  const dispatch = useDispatch();
  const [showDetail, setShowDetail] = useState(false);
  const [showApprove, setShowApprove] = useState(false);
  const { push } = useHistory();
  const { trxCode } = useParams();
  const [loading, setLoading] = useState(false);
  const [cancelOpen, setCancelOpen] = useState(false);

  const {
    items: {
      statusPayment,
      transactionId,
      createdAt,
      createdBy,
      count,
      method,
      role,
      detailTrx,
      orderId
    }
  } = useSelector(state => state.transactions.trxDetail);

  const onAgree = () => {
    if (trxCode) {
      setLoading(true)
      transactionService.transferFeeDetails(trxCode)
        .then(({ data }) => {
          dispatch(transactionDetailActions.setTransferFeeDetails(data))
        })
        .finally(() => {
          setShowApprove(false)
          setLoading(false)
          dispatch(transactionDetailActions.setStep(1))
        })
    }
  }

  const onCancel = () => {
    push("/transaction")
    dispatch(transactionDetailActions.reset())
    transactionService.cancel(orderId)
      .then(() => {

      })
      .catch(() => {

      })
  }

  useEffect(() => {
    return () => {
      transactionDetailActions.reset()
    }
  }, [])

  return (
    <>
      <div className={styles.detail}>
        <SimpleCard>
          <div className={styles.info}>
            <div className={styles.col}>
              <div className={styles.dataItem}>
                <span>Status</span>
                <span>:</span>
                <span>{getStatus(statusPayment)}</span>
              </div>
              <div className={styles.dataItem}>
                <span>ID Transaksi</span>
                <span>:</span>
                <span>{transactionId}</span>
              </div>
              <div className={styles.dataItem}>
                <span>Waktu Dibuat</span>
                <span>:</span>
                <span>{createdAt ? `${format(new Date(createdAt), "dd MMMM yyyy, HH:mm:ss", { locale: id })} WIB` : null}</span>
              </div>
            </div>
            <div className={styles.col}>
              <div className={styles.dataItem}>
                <span>Aktor</span>
                <span>:</span>
                <span>{createdBy} <span style={{ fontWeight: 400 }}>({role})</span></span>
              </div>
              <div className={styles.dataItem}>
                <span>Jumlah</span>
                <span>:</span>
                <span>{detailTrx?.totalRecipient}</span>
              </div>
              <div className={styles.dataItem}>
                <span>Metode</span>
                <span>:</span>
                <span>{statusPayment === statusTypes.PENDING ? "-" : method}</span>
              </div>
            </div>
          </div>
          <TableDetail />
          <div className={styles.totalTransfer}>
            <div className={styles.label}>
              <span>Total Transfer</span>
              <button
                onClick={() => setShowDetail(true)}
              >
                Lihat Detail
              </button>
            </div>
            <h3>Rp{toIDR(detailTrx?.totalTrf, false)}</h3>
          </div>
          <div className={styles.buttons}>
            <div className={styles.buttonsWrapper}>
              {statusPayment === statusTypes.PENDING ?
                <MainButton
                  variant='primary'
                  onClick={() => setShowApprove(true)}
                >
                  Lanjutkan
                </MainButton>
                : null
              }
            </div>
          </div>
        </SimpleCard>
      </div>
      <Modal
        in={showDetail}
        onClose={() => setShowDetail(false)}
        scrollable={statusPayment !== statusTypes.COMPLETED || (statusPayment === statusTypes.COMPLETED && detailTrx?.volumeTrxSuccess)}
      >
        <ModalWrapper width="60%">
          <ModalHead
            title="Rincian Transaksi"
            onClose={() => setShowDetail(false)}
          />
          <ModalBody>
            <div className={styles.modal}>
              <FeeDetail detailTrx={detailTrx} />
            </div>
          </ModalBody>
        </ModalWrapper>
      </Modal>
      <Modal
        in={showApprove}
        onClose={() => setShowApprove(false)}
      >
        <ModalWrapper width={560}>
          <ModalHead
            title="Persetujuan Transaksi"
            onClose={() => setShowApprove(false)}
          />
          <ModalBody>
            <div className={styles.summary}>
              <div className={styles.hr}></div>
              <div className={styles.heading}>
                <span>Pastikan kembali data transaksi sebelum menyetujui transaksi ini.</span>
              </div>
              <div className={styles.tableCard}>
                <div className={styles.row}>
                  <span>No. Transaksi</span>
                  <span>:</span>
                  <span>{transactionId}</span>
                </div>
                <div className={styles.row}>
                  <span>Tanggal Dibuat</span>
                  <span>:</span>
                  <span>{createdAt ? `${format(new Date(createdAt), "dd MMMM yyyy, HH:mm:ss", { locale: id })} WIB` : ""}</span>
                </div>
                <div className={styles.row}>
                  <span>Diinput oleh</span>
                  <span>:</span>
                  <span>{createdBy}</span>
                </div>
                <div className={styles.row}>
                  <span>Peran</span>
                  <span>:</span>
                  <span>{role}</span>
                </div>
                <div className={styles.row}>
                  <span>Jumlah Penerima</span>
                  <span>:</span>
                  <span>{count}</span>
                </div>
              </div>
              <div className={styles.buttons}>
                <div className={styles.buttonsWrapper}>
                  <MainButton
                    onClick={() => {
                      setCancelOpen(true)
                      setShowApprove(false)
                    }}
                    variant="danger-light"
                  >
                    Batalkan Transaksi
                  </MainButton>
                  <MainButton
                    onClick={onAgree}
                    loading={loading}
                    disabled={loading}
                  >
                    Ya, Setujui
                  </MainButton>
                </div>
              </div>
            </div>
          </ModalBody>
        </ModalWrapper>
      </Modal>
      <Modal
        in={cancelOpen}
        onClose={() => setCancelOpen(false)}
      >
        <ModalWrapper width={560}>
          <ModalHead
            title="Batalkan Transaksi"
            onClose={() => setCancelOpen(false)}
          />
          <ModalBody>
            <div className={styles.modal}>
              <div className={styles.content}>
                <p>Seluruh data transaksi akan terhapus secara permanen. Pastikan kembali data transaksi sebelum melakukan pembatalan transaksi.</p>
                <Alert
                  color="danger"
                  className="mt-24"
                  iconItem={<Info size={32} className="mr-16" color="#E11900" />}
                >
                  <span>Data transaksi yang sudah dibatalkan tidak dapat dikembalikan.</span>
                </Alert>
              </div>
              <div className={styles.buttons}>
                <div className={styles.buttonsWrapper}>
                  <MainButton
                    onClick={onCancel}
                    variant="danger"
                  >
                    Batalkan Transaksi
                  </MainButton>
                  <MainButton
                    onClick={() => setCancelOpen(false)}
                    variant="primary-light"
                  >
                    Kembali
                  </MainButton>
                </div>
              </div>
            </div>
          </ModalBody>
        </ModalWrapper>
      </Modal>
    </>
  )
}

export default Review
