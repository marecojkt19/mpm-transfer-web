import { Document, Font, Image, Link, Page, StyleSheet, Text, View } from '@react-pdf/renderer';
import InterItalic from '../../../../assets/fonts/Inter-Italic.ttf';

const DocumentDetailTransactionAPI = ({ item: {
  trxDate,
  trxId,
  printedAt,
  senderName,
  status,
  totalAmount,
  recipientName,
  recipientBankName,
  recipientBankAccount,
  transferBy
} }) => (
  <Document>
    <Page size="A4" orientation="landscape" style={styles.root}>
      <View style={styles.pageBackground}>
        <Image
          style={{ height: '100%', width: '100%' }}
          src="/assets/pdf-bg.png"
        />
      </View>
      <View style={styles.page}>
        <View style={styles.wrapperHeader}>
          <View style={styles.headerLeft}>
            <Image
              style={styles.dipayLogo}
              src="/assets/dipay-logo.png"
            />
          </View>
          <View style={styles.headerRight}>
            <Text style={styles.h2}>PT Mareco Prima Mandiri</Text>
            <Text style={[styles.p, { color: '#757575' }]}>Satrio Tower Lt. 14 Unit 2</Text>
            <Text style={[styles.p, { color: '#757575' }]}>Jl. Prof. DR. Satrio No.7, RT.7/RW.2, Kuningan, Kuningan Timur, Kecamatan Setiabudi, Kota Jakarta Selatan, Daerah Khusus Ibukota</Text>
            <Text style={[styles.p, { color: '#757575' }]}>Jakarta 12950</Text>
          </View>
        </View>
        <View style={styles.hr}></View>
        <View style={styles.wrapperContent}>
          <View style={styles.contentLeft}>
            <Text style={styles.h1}>Transaksi Berhasil</Text>
            <Text style={[styles.p, { color: '#757575' }]}>{trxDate}</Text>
            <View style={styles.hrDash}></View>
            <View style={styles.ul}>
              <Text style={[styles.li, { color: '#757575', width: 100 }]}>Pengirim</Text>
              <Text style={styles.li}>{senderName}</Text>
            </View>
            <View style={styles.ul}>
              <Text style={[styles.li, { color: '#757575', width: 100 }]}>Transfer Melalui</Text>
              <Text style={styles.li}>{transferBy}</Text>
            </View>
            <View style={{ position: 'absolute', bottom: "30%" }}>
              <Text style={[styles.li, { color: '#757575', fontSize: 8, fontStyle: 'italic', marginBottom: '3px' }]}>Resi ini merupakan bukti sah transaksi</Text>
              <Text style={[styles.li, { fontSize: 8 }]}>Silakan hubungi Dipay Call Center <Link src="https://wa.me/+6281117261717" style={{ color: '#276EF1', textDecoration: 'none' }}>+62 811 1726 1717</Link> apabila Anda membutuhkan bantuan</Text>
            </View>
            <View style={{ position: 'absolute', bottom: 0 }}>
              <Text style={[styles.li, { fontSize: 9 }]}>Dicetak pada {printedAt}</Text>
            </View>
          </View>
          <View style={styles.contentRight}>
            <View style={styles.ul}>
              <Text style={[styles.li, { color: '#757575', width: 100 }]}>ID Transaksi</Text>
              <Text style={styles.li}>{trxId}</Text>
            </View>
            <View style={styles.ul}>
              <Text style={[styles.li, { color: '#757575', width: 100 }]}>Bank Penerima</Text>
              <Text style={styles.li}>{recipientBankName}</Text>
            </View>
            <View style={styles.ul}>
              <Text style={[styles.li, { color: '#757575', width: 100 }]}>No. Rekening</Text>
              <Text style={styles.li}>{recipientBankAccount}</Text>
            </View>
            <View style={styles.ul}>
              <Text style={[styles.li, { color: '#757575', width: 100 }]}>Nama Penerima</Text>
              <Text style={styles.li}>{recipientName}</Text>
            </View>
            <View style={styles.ul}>
              <Text style={[styles.li, { color: '#757575', width: 100 }]}>Status</Text>
              <Text style={[styles.li, { color: '#05944F' }]}>{status}</Text>
            </View>
            <View style={styles.hrDash}></View>
            <View style={styles.ul}>
              <Text style={[styles.li, { color: '#757575', width: 100 }]}>Total Nominal</Text>
              <Text style={[styles.li, { color: '#276EF1', fontSize: 17, fontWeight: 700 }]}>{totalAmount}</Text>
            </View>
            <View style={styles.biWrapper}>
              <Image
                style={styles.biLogo}
                src="/assets/logo-BI-vertical.png"
              />
              <View style={{ flexDirection: 'column' }}>
                <Text style={styles.li}>Kegiatan transfer melalui Dipay ini sudah terlisensi oleh Bank Indonesia</Text>
                <Text style={styles.li}>dengan Nomor Izin 25/32/DKSP/Srt/B</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </Page>
  </Document>
);

const styles = StyleSheet.create({
  root: {
    minWidth: '100%',
    minHeight: '100%',
    display: 'block',
    height: '100%',
    width: '100%',
    padding: 0,
    margin: 0,
  },
  page: {
    flexDirection: 'column',
    padding: 30
  },
  pageBackground: {
    padding: 0,
    position: 'absolute',
    minWidth: '100%',
    minHeight: '100%',
    display: 'block',
    height: '100%',
    width: '100%',
  },
  wrapperContent: {
    flexDirection: 'row',
    padding: "10px 10px 0 0",
    height: "100%"
  },
  wrapperHeader: {
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 5
  },
  biWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 15,
    padding: 10,
    marginTop: 10,
    backgroundColor: "#EFF3FE",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    borderBottomLeftRadius: 5,
  },
  headerLeft: {
    width: "50%",
  },
  headerRight: {
    width: "50%",
  },
  contentLeft: {
    width: "50%",
    paddingRight: 30,
  },
  contentRight: {
    paddingLeft: 3,
    width: "50%",
  },
  dipayLogo: {
    width: 132,
  },
  biLogo: {
    width: 38,
    marginRight: 8
  },
  h1: {
    fontSize: 16,
    lineHeight: 1.9,
    fontWeight: 700,
    fontFamily: 'Inter'
  },
  h2: {
    fontSize: 12.,
    lineHeight: 1.7,
    fontWeight: 700,
    fontFamily: 'Inter'
  },
  p: {
    lineHeight: 1.4,
    fontSize: 10,
    fontWeight: 500,
  },
  ul: {
    flexDirection: 'row',
    alignItems: "center",
    margin: '5px 0'
  },
  li: {
    fontSize: 9,
    lineHeight: 1.5,
    fontWeight: 500,
    fontFamily: 'Inter',
  },
  hr: {
    margin: "10px 0",
    border: "0.5px solid #E2E2E2"
  },
  hrDash: {
    margin: "10px 0",
    border: "0.5px dashed #E2E2E2"
  }
});

Font.register({
  family: 'Inter',
  fonts: [
    {
      src: "https://fonts.gstatic.com/s/inter/v12/UcCO3FwrK3iLTeHuS_fvQtMwCp50KnMw2boKoduKmMEVuLyfMZhrib2Bg-4.ttf",
    },
    {
      src: InterItalic,
      fontStyle: 'italic'
    },
    {
      src: "https://fonts.gstatic.com/s/inter/v12/UcCO3FwrK3iLTeHuS_fvQtMwCp50KnMw2boKoduKmMEVuGKYMZhrib2Bg-4.ttf",
      fontWeight: 600,
    },
    {
      src: "https://fonts.gstatic.com/s/inter/v12/UcCO3FwrK3iLTeHuS_fvQtMwCp50KnMw2boKoduKmMEVuFuYMZhrib2Bg-4.ttf",
      fontWeight: 700,
    },
  ],
});

export { DocumentDetailTransactionAPI };
