import { PDFDownloadLink } from '@react-pdf/renderer';
import React, { useCallback, useEffect, useState } from 'react';
import { Download } from '../../../../assets/icons';
import transactionService from '../../../../axios/services/transactionService';
import LoadingDots from '../../../../components/Loadings/LoadingDots';
import MainButton from '../../../../components/Form/MainButton';
import SimpleCard from '../../../../components/templates/SimpleCard';
import toIDR from '../../../../utils/helpers/toIDR';
import styles from './detail.module.scss';
import { DocumentDetailTransactionAPI } from './template';
import { useParams } from 'react-router-dom';

function Detail() {
  const [item, setItem] = useState({});
  const { orderId } = useParams();

  const load = useCallback(async () => {
    await transactionService.invoice(orderId)
      .then(res => setItem(res.data))
      .catch(err => setItem({}));
  }, [orderId]);

  useEffect(() => {
    load()
  }, [load]);

  return !Boolean(item?.trxId) ?
    <div className="d-flex justify-content-center align-items-center h-50">
      <LoadingDots />
    </div>
    :
    <SimpleCard>
      <div className={styles.detail}>
        <div className={styles.logo}>
          <img src="/assets/dipay-logo.png" width="120px" height="32px" alt="" />
        </div>
        <div className={styles.line}></div>
        <div className={styles.content}>
          <div className={styles.group1}>
            <h1>Transaksi {item.status}</h1>
            <p className={styles.date}>{item.trxDate}</p>
          </div>
          <div className={styles.group2}>
            <div className={styles.row}>
              <div className={styles.rowItem}>
                <p>Pengirim</p>
                <span>{item.senderName}</span>
              </div>
              <div className={styles.rowItem}>
                <p>Transfer Melalui</p>
                <span>{item.transferBy}</span>
              </div>
            </div>
            <hr className={styles.dash} />
            <div className={styles.row}>
              <div className={styles.rowItem}>
                <p>ID Transaksi</p>
                <span>{item.trxId}</span>
              </div>
              <div className={styles.rowItem}>
                <p>Bank Penerima</p>
                <span>{item.recipientBankName}</span>
              </div>
              <div className={styles.rowItem}>
                <p>No. Rekening</p>
                <span>{item.recipientBankAccount}</span>
              </div>
              <div className={styles.rowItem}>
                <p>Nama Penerima</p>
                <span>{item.recipientName}</span>
              </div>
              <div className={styles.rowItem}>
                <p>Status</p>
                <span style={{ color: item.status === "Gagal" ? '#ff3b30' : '#05944F' }}>{item.status}</span>
              </div>
            </div>
            <hr className={styles.dash} />
            <div className={styles.row}>
              <div className={styles.rowItem} style={{ alignItems: "center" }}>
                <p>Total Nominal</p>
                <h3>{toIDR(item.totalAmount ?? 0, false)}</h3>
              </div>
            </div>
          </div>
        </div>
        <ButtonGroup item={item} />
      </div>
    </SimpleCard >
};

const ButtonGroup = ({ item }) => {
  const pdfFileName = `bukti-transaksi-transaksi-${item.trxId}.pdf`;

  return (
    <div className={styles.buttons}>
      <div className={styles.buttonsWrapper}>
        <PDFDownloadLink
          document={<DocumentDetailTransactionAPI item={item} />}
          fileName={pdfFileName}
        >
          {({ loading }) => (
            <MainButton
              variant='primary-light'
              disabled={loading}
              loading={loading}
              className={styles.pdfButton}
              icon={<Download size="24" />}
            >
              Unduh
            </MainButton>
          )}
        </PDFDownloadLink>
      </div>
    </div>
  )
};

export default Detail
