import React from 'react'
import { Tab, Tabs } from '../../components/TabsNew/Tabs'
import styles from './listTransaction.module.scss'
import { Helmet } from 'react-helmet';
import ByBulking from './ByBulking';
// import ByAPI from './ByAPI';
// import ByAPINotActived from './ByAPI/ByAPINotActived';
import { Route, Switch, useHistory, useRouteMatch } from 'react-router-dom';
import ByBulkingDetail from './ByBulking/Detail';
import ByAPIDetail from './ByAPI/Detail';
import BySingleDetail from './BySingle/Detail';
import Approval from './ByBulking/Approval';
// import { useSelector } from 'react-redux';
import BySingle from './BySingle';
import { useDispatch, useSelector } from 'react-redux';
import { transactionActions } from '../../redux/actions/transactionAction';
import BackButton from '../../components/templates/BackButton';

const ListTransaction = () => {
  const { url } = useRouteMatch();
  const { tab } = useSelector(state => state.transaction);
  const dispatch = useDispatch();
  const { push } = useHistory();
  // const { company } = useSelector(state => state.auth);

  return (
    <Switch>
      <Route exact path={url}>
        <div className={styles.listTransaction}>
          <Helmet title="Daftar Transaksi" />
          <div className={styles.pageHeading}>
            <h1>Daftar Transaksi</h1>
          </div>
          <Tabs
            activeKey={tab}
            onClick={v => dispatch(transactionActions.setTab(v))}
          >
            <Tab title="Transaksi Massal">
              <ByBulking />
            </Tab>
            {/* <Tab title="Transaksi Integrasi API">
              {company.fitur.trxApiActive ? <ByAPI /> : <ByAPINotActived />}
            </Tab> */}
            <Tab title="Transaksi Single">
              <BySingle />
            </Tab>
          </Tabs>
        </div>
      </Route>
      <Route path={`${url}/api/:orderId/detail`}>
        <div className={styles.listTransaction}>
          <Helmet title="Detail Transaksi" />
          <BackButton onClick={() => push("/transaction")} />
          <div className={styles.pageHeading}>
            <h1>Detail Transaksi</h1>
          </div>
          <ByAPIDetail />
        </div>
      </Route>
      <Route path={`${url}/single/:orderId/detail`}>
        <div className={styles.listTransaction}>
          <Helmet title="Detail Transaksi" />
          <BackButton onClick={() => push("/transaction")} />
          <div className={styles.pageHeading}>
            <h1>Detail Transaksi</h1>
          </div>
          <BySingleDetail />
        </div>
      </Route>
      <Route path={`${url}/bulking/:trxCode/review`}>
        <div className={styles.listTransaction}>
          <Helmet title="Detail Transaksi" />
          <BackButton onClick={() => push("/transaction")} />
          <div className={styles.pageHeading}>
            <h1>Detail Transaksi</h1>
          </div>
          <ByBulkingDetail />
        </div>
      </Route>
      <Route path={`${url}/bulking/:trxCode/approval`}>
        <div className={styles.listTransaction}>
          <Helmet title="Detail Transaksi" />
          <div className={styles.pageHeading}>
            <h1>Detail Transaksi</h1>
          </div>
          <Approval />
        </div>
      </Route>
    </Switch>
  )
}

export default ListTransaction
