import { endOfDay, format, startOfDay } from 'date-fns';
import { useCallback, useEffect, useMemo } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import transactionService from '../../../axios/services/transactionService';
import DateRangePicker from '../../../components/Form/Dates/DateRangePicker';
import SelectField from '../../../components/Form/SelectField';
import TextField from '../../../components/Form/TextField';
import Table from '../../../components/Table';
import SimpleCard from '../../../components/templates/SimpleCard';
import { OrderStatusOpts } from '../../../utils/enums/orderStatusTypes';
import { getStatus } from '../../../utils/helpers/getStatus';
import toIDR from '../../../utils/helpers/toIDR';
import styles from './byAPI.module.scss';
import { transactionByAPIActions } from '../../../redux/actions/transactions/transactionByAPIActions';
import { Link } from "react-router-dom";

const ByAPI = () => {
  const dispatch = useDispatch();
  const {
    items,
    tableConfig: {
      isLoading,
      page,
      totalData,
      limit,
      sort,
      search,
      filter,
    }
  } = useSelector(state => state.transactions.trxByApi);

  const setTableConfig = useCallback(
    key => value => dispatch(transactionByAPIActions.setTableConfig(key, value)),
    [dispatch],
  );

  const setTableConfigFilter = useCallback(
    key => value => dispatch(transactionByAPIActions.setTableConfigFilter(key, value)),
    [dispatch],
  );

  const onChangePage = useMemo(
    () => setTableConfig('page'),
    [setTableConfig]
  );

  const load = useCallback(
    () => {
      const tConfig = {
        limit,
        page,
        sort,
        filter
      }
      return dispatch(transactionByAPIActions.loadRequested(tConfig))
    },
    [limit, page, sort, filter, dispatch],
  );

  useEffect(() => {
    load()
  }, [load])

  const BtnDetail = ({ v }) => (
    <Link
      className={styles.action}
      to={`/transaction/api/${v}/detail`}
    >
      Lihat Detail
    </Link>
  );

  const columns = useMemo(() => [
    { title: 'Waktu Transaksi', key: 'createdAt', render: v => format(new Date(v), 'd MMM yyyy, HH:mm') },
    { title: 'Bank', key: 'transactions', render: v => v[0]?.bank || '-' },
    { title: 'Nomor Rekening', key: 'transactions', render: v => v[0]?.bankAccount || '-' },
    { title: 'Nama dari Bank', key: 'transactions', render: v => v[0]?.bankAccountName || '-' },
    { title: 'Nominal (Rp)', key: 'transactions', render: v => toIDR(v[0]?.amount, false) || '-' },
    { title: 'Berita', key: 'transactions', render: v => v[0]?.description || '-' },
    { title: 'Status', key: 'status', render: v => v ? getStatus({ status: v }) : "-" },
    { title: 'Action', key: 'transactionCode', render: (v) => <BtnDetail v={v} /> }
  ], [])

  const excelColumns = useMemo(() => [
    { title: 'Waktu Transaksi', key: 'createdAt', render: v => format(new Date(v), 'd MMM yyyy, HH:mm') },
    { title: 'Bank', key: 'transactions', render: v => v[0]?.bank || '-' },
    { title: 'Nomor Rekening', key: 'transactions', render: v => `'${v[0]?.bankAccount}` || '-' },
    { title: 'Nama dari Bank', key: 'transactions', render: v => v[0]?.bankAccountName || '-' },
    { title: 'Nominal (Rp)', key: 'transactions', render: v => toIDR(v[0]?.amount, false) || '-' },
    { title: 'Berita', key: 'transactions', render: v => v[0]?.description || '-' },
    { title: 'Status', key: 'status' },
  ], []);

  const APIParamsForExcel = useMemo(() => ({
    page: page,
    limit: limit,
    sort,
    search,
    startDate: filter.startDate ? startOfDay(new Date(filter.startDate)) : null,
    endDate: filter.endDate ? endOfDay(new Date(filter.endDate)) : null,
    ...filter,
  }), [filter, sort, limit, page, search])

  return (
    <div className={styles.listTransaction}>
      <Helmet title="Daftar Transaksi" />
      <div className={styles.heading}>
        <h1>Daftar Transaksi By API</h1>
      </div>
      <SimpleCard>
        <Table
          data={items}
          onChangePage={onChangePage}
          onSort={(v) => setTableConfig('sort')(v)}
          onLimit={(v) => setTableConfig('limit')(v)}
          config={{
            columns: columns,
            withIndex: true,
            total: totalData,
            limit: limit,
            currentPage: page,
            loading: isLoading,
            showRender: (from, to, total) => `${from}-${to} dari ${total} transaksi`
          }}
          excel={{
            columns: excelColumns,
            filename: 'Table Transaksi By API',
            allPage: true,
            api: {
              apiResponseKey: 'orders',
              service: transactionService.orderByAPI,
              APIParams: APIParamsForExcel
            }
          }}
        >
          <TextField
            variant="outlined"
            placeholder="Cari No Rekening"
            value={filter.search}
            onChange={e => setTableConfigFilter('search')(e.target.value)}
          />
          <SelectField
            variant="outlined"
            placeholder="Status"
            options={OrderStatusOpts}
            value={filter.filter}
            onChange={v => setTableConfigFilter('filter')(v?.value)}
            isClearable
          />
          <SelectField
            variant="outlined"
            placeholder="Urutkan berdasarkan"
            options={[
              { label: 'ID Transaksi', value: 'code|desc' },
              { label: 'Waktu Dibuat', value: 'createdAt|desc' },
              { label: 'Status Transaksi', value: 'status|desc' }
            ]}
            value={sort}
            onChange={v => setTableConfig('sort')(v?.value)}
            isClearable
          />
          <DateRangePicker
            amountOfMonths={2}
            placeholder="Pick dates range"
            fromDate={filter.startDate}
            toDate={filter.endDate}
            onChange={(start, end) => {
              if (start && end) {
                setTableConfigFilter('startDate')(start)
                setTableConfigFilter('endDate')(end)
              }
            }}
          />
        </Table>
      </SimpleCard>
    </div >
  )
}

export default ByAPI
