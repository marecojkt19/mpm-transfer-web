import { endOfDay, format, startOfDay } from 'date-fns';
import { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import transactionService from '../../../../axios/services/transactionService';
import DateRangePicker from '../../../../components/Form/DatesNew/DateRangePicker';
import SelectField from '../../../../components/Form/SelectFieldNew';
import Table from '../../../../components/Table';
import { getStatus } from '../../../../utils/helpers/getStatus';
import toIDR from '../../../../utils/helpers/toIDR';
import styles from './list.module.scss';
import { transactionByAPIActions } from '../../../../redux/actions/transactions/transactionByAPIActions';
import { Link } from "react-router-dom";
import id from 'date-fns/locale/id';
import useAsync from '../../../../components/hooks/useAsync';

const List = () => {
  const dispatch = useDispatch();
  const {
    items,
    tableConfig: {
      isLoading,
      page,
      totalData,
      limit,
      sort,
      search,
      filter,
    }
  } = useSelector(state => state.transactions.trxByApi);

  const setTableConfig = useCallback(
    key => value => dispatch(transactionByAPIActions.setTableConfig(key, value)),
    [dispatch],
  );

  const setTableConfigFilter = useCallback(
    key => value => dispatch(transactionByAPIActions.setTableConfigFilter(key, value)),
    [dispatch],
  );

  const onChangePage = useMemo(
    () => setTableConfig('page'),
    [setTableConfig]
  );

  const load = useCallback(
    () => {
      const tConfig = {
        limit,
        page,
        sort,
        filter
      }
      return dispatch(transactionByAPIActions.loadRequested(tConfig))
    },
    [limit, page, sort, filter, dispatch],
  );

  useEffect(() => {
    load()
  }, [load])

  const {
    pending,
    value: { data: bankOpts = [] } = {}
  } = useAsync(transactionService.getListBank, {});

  const BtnDetail = ({ row }) => (
    <Link
      className={styles.action}
      to={`/transaction/api/${row._id}/detail`}
    >
      Lihat Detail
    </Link>
  );

  const columns = useMemo(() => [
    { title: 'ID Transaksi', key: 'code', render: v => v || '-' },
    { title: 'Waktu dibuat', key: 'createdAt', sortable: true, render: v => `${format(new Date(v), 'dd MMM yyyy, HH:mm:ss', { locale: id })} WIB` },
    { title: 'Bank Tujuan', key: 'transactions', render: v => v[0]?.bank || '-' },
    { title: 'Nomor Rekening', key: 'transactions', render: v => v[0]?.bankAccount || '-' },
    { title: 'Nama Penerima', key: 'fullName', sortable: true, render: (v, row) => row?.transactions?.[0]?.bankAccountName || '-' },
    { title: 'Nominal (Rp)', key: 'totalAmount', sortable: true, render: (v) => toIDR(v, false) || '-' },
    { title: 'Berita', key: 'transactions', render: v => v[0]?.description || '-' },
    { title: 'Email Penerima', key: 'transactions', render: v => v?.[0]?.email || '-' },
    { title: 'Status', key: 'status', render: v => v ? getStatus({ status: v }) : "-" },
    { title: 'Action', key: 'transactions', render: (v, row) => row?.status === 'COMPLETED' ? <BtnDetail row={v[0]} /> : '-' }
  ], [])

  const excelColumns = useMemo(() => [
    { title: 'Waktu Transaksi', key: 'createdAt', render: v => format(new Date(v), 'd MMM yyyy, HH:mm') },
    { title: 'Bank', key: 'transactions', render: v => v[0]?.bank || '-' },
    { title: 'Nomor Rekening', key: 'transactions', render: v => `'${v[0]?.bankAccount}` || '-' },
    { title: 'Nama dari Bank', key: 'transactions', render: v => v[0]?.bankAccountName || '-' },
    { title: 'Nominal (Rp)', key: 'transactions', render: v => toIDR(v[0]?.amount, false) || '-' },
    { title: 'Berita', key: 'transactions', render: v => v[0]?.description || '-' },
    { title: 'Status', key: 'status' },
  ], []);

  const APIParamsForExcel = useMemo(() => ({
    page: page,
    limit: limit,
    sort,
    search,
    startDate: filter.startDate ? startOfDay(new Date(filter.startDate)) : null,
    endDate: filter.endDate ? endOfDay(new Date(filter.endDate)) : null,
    ...filter,
  }), [filter, sort, limit, page, search])

  return (
    <div className={styles.listTransaction}>
      <Table
        data={items}
        onChangePage={onChangePage}
        onSort={(v) => setTableConfig('sort')(v)}
        onLimit={(v) => setTableConfig('limit')(v)}
        onSearch={(v) => setTableConfigFilter('search')(v)}
        load={load}
        search={true}
        searchPlaceholder="Cari ID, Nama, Rekening, ..."
        config={{
          columns: columns,
          withIndex: true,
          total: totalData,
          limit,
          currentPage: page,
          loading: isLoading,
          sort,
          showRender: (from, to, total) => `Tampilkan ${from}-${to} dari ${total} Transaksi`
        }}
        excel={{
          columns: excelColumns,
          filename: 'Table Transaksi By API',
          allPage: true,
          api: {
            apiResponseKey: 'orders',
            service: transactionService.orderByAPI,
            APIParams: APIParamsForExcel
          }
        }}
      >
        <SelectField
          variant="outlined"
          placeholder="Bank"
          loading={pending}
          options={bankOpts}
          value={filter.bank}
          onChange={v => {
            if (v?.value !== filter.bank) {
              setTableConfigFilter('bank')(v?.value)
            } else {
              setTableConfigFilter('bank')("")
            }
          }}
          isClearable
        />
        <SelectField
          variant="outlined"
          placeholder="Status"
          options={[
            { value: "PENDING", label: "Diproses" },
            { value: "COMPLETED", label: "Berhasil" },
            { value: "FAILED", label: "Gagal" }
          ]}
          value={filter.status}
          onChange={v => {
            if (v?.value !== filter.status) {
              setTableConfigFilter('status')(v?.value)
            } else {
              setTableConfigFilter('status')("")
            }
          }}
          isClearable
        />
        <SelectField
          variant="outlined"
          placeholder="Urutkan berdasarkan"
          options={[
            { label: 'Waktu Dibuat: Terbaru', value: 'createdAt|desc' },
            { label: 'Waktu Dibuat: Terlama', value: 'createdAt|asc' },
            { label: 'Nama: A-Z', value: 'fullName|asc' },
            { label: 'Nama: Z-A', value: 'fullName|desc' },
            { label: 'Nominal Terendah', value: 'totalAmount|asc' },
            { label: 'Nominal Tertinggi', value: 'totalAmount|desc' }
          ]}
          value={sort}
          onChange={v => {
            if (v?.value !== sort) {
              setTableConfig('sort')(v?.value)
            } else {
              setTableConfig('sort')("")
            }
          }}
          isClearable
        />
        <DateRangePicker
          amountOfMonths={2}
          placeholder="Rentang Tanggal"
          fromDate={filter.startDate}
          toDate={filter.endDate}
          maxDate={new Date()}
          onChange={({ fromDate, toDate }) => {
            if (fromDate && toDate) {
              setTableConfigFilter('startDate')(fromDate)
              setTableConfigFilter('endDate')(toDate)
            } else if (!fromDate && !toDate) {
              setTableConfigFilter('startDate')()
              setTableConfigFilter('endDate')()
            }
          }}
        />
      </Table>
    </div>
  )
}

export default List
