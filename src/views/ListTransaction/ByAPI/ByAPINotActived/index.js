import React, { useState } from 'react'
import styles from './byAPINotActived.module.scss'
import SimpleCard from '../../../../components/templates/SimpleCard'
import Modal, { ModalBody, ModalHead, ModalWrapper } from '../../../../components/ModalNew'
import ApiIcon from '../../../../assets/icons/api'
import Alert from '../../../../components/Alert'
import { Info } from '../../../../assets/icons'

const ByAPINotActived = () => {
  const [showLearnMore, setShowLearnMore] = useState(false);
  return (
    <>
      <SimpleCard className={styles.byAPINotActived}>
        <div className={styles.byAPINotActivedWrapper}>
          <div className={styles.heading}>
            <h3>Transaksi Integrasi API Belum Aktif</h3>
            <p>
              Aktifkan fitur transaksi integrasi API untuk pengalaman transaksi yang lebih efisien!
            </p>
          </div>
          <div className={styles.image}>
            <img src="/assets/byAPI.png" alt="" />
          </div>
          <div className={styles.button}>
            <a href="https://wa.me/6281117261717" target="_blank" rel="noreferrer">
              <span>Ajukan Permintaan Akses</span>
            </a>
          </div>
          <div className={styles.learnMoreBtn}>
            <button onClick={() => setShowLearnMore(true)}>
              <span>Pelajari Selengkapnya</span>
            </button>
          </div>
        </div>
      </SimpleCard>
      <Modal in={showLearnMore} onClose={() => setShowLearnMore(false)}>
        <ModalWrapper width={720}>
          <ModalHead
            title={
              <div className={styles.downloadLabel}>
                <div className={styles.downloadIcon}>
                  <ApiIcon size={24} />
                </div>
                <p>Transaksi Integrasi API</p>
              </div>
            }
            onClose={() => setShowLearnMore(false)}
          />
          <ModalBody>
            <div className={styles.learnMore}>
              <p>
                API merupakan sebuah antarmuka pemrograman aplikasi, yang berfungsi sebagai penghubung antara sistem Anda dengan sistem Dipay Disbursements. Hal ini memungkinkan Anda untuk melakukan integrasi antara kedua sistem tersebut sehingga proses transaksi dapat dilakukan secara otomatis tanpa memerlukan input manual.
              </p>
              <h5>Keunggulan Transaksi Integrasi API</h5>
              <ol>
                <li>
                  <p>
                    Transaksi API dapat dilakukan secara real-time, mengurangi waktu yang dibutuhkan untuk proses transaksi dibandingkan dengan metode manual.
                  </p>
                </li>
                <li>
                  <p>
                    Penggunaan API memungkinkan transaksi dilakukan secara otomatis sehingga dapat mengurangi risiko kesalahan manusia dan mempercepat proses bisnis.
                  </p>
                </li>
                <li>
                  <p>
                    API memiliki tingkat keamanan yang baik untuk memastikan hanya pihak yang memiliki kewenangan dapat mengakses dan menggunakan layanan transaksi tersebut.
                  </p>
                </li>
              </ol>

              <h5>Keunggulan Transaksi Integrasi API</h5>
              <ol>
                <li>
                  <p>
                    Pastikan perusahaan Anda memiliki tim IT yang berpengalaman dalam proses integrasi API.
                  </p>
                </li>
                <li>
                  <p>
                    Jika belum yakin apakah tim IT Anda dapat melakukan integrasi API atau tidak, Anda dapat berdiskusi dengan tim Dipay Disbursements. Hubungi Sales Support kami di 0811 1726 1717 untuk konsultasi lebih lanjut.
                  </p>
                </li>
                <li>
                  <p>
                    Jika perusahaan Anda belum siap melakukan transaksi integrasi API, Anda tetap dapat melakukan transaksi di Dipay Disbursements melalui fitur Transaksi Massal.
                  </p>
                </li>
              </ol>

              <Alert
                color="primary"
                className="mb-24"
                iconItem={<Info size={32} className="mr-16" color="#276EF1" />}
              >
                <span className="font-size-14">Catatan: Hubungi Sales Support kami di <a href="https://wa.me/6281117261717" rel="noreferrer" target="_blank"><b><u>0811 1726 1717</u></b></a> untuk melakukan Transaksi Integrasi API.</span>
              </Alert>
            </div>
          </ModalBody>
        </ModalWrapper>
      </Modal>
    </>
  )
}

export default ByAPINotActived
