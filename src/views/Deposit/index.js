import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Tab, Tabs } from '../../components/Tabs/Tabs';
import { depositActions } from '../../redux/actions/depositAction';
import History from './History';
import TopUp from './TopUp';
import styles from './deposit.module.scss';
import { Helmet } from 'react-helmet';

const Deposit = () => {
  const dispatch = useDispatch();
  const { activeTabKey } = useSelector(state => state.deposit);

  const handleTab = key => {
    dispatch(depositActions.changeTab(key))
    dispatch(depositActions.setStep(0))
  }

  return (
    <div className={styles.deposit}>
      <Helmet title="Daftar Transaksi" />
      <div className={styles.pageHeading}>
        <h1>Deposit</h1>
      </div>
      <Tabs
        activeKey={activeTabKey}
        onClick={handleTab}
      >
        <Tab title="Top Up">
          <TopUp />
        </Tab>
        <Tab title="Riwayat">
          <History />
        </Tab>
      </Tabs>
    </div>
  )
}

export default Deposit
