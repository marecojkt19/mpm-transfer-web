import React, { useEffect, useMemo, useState } from 'react'
import Button from '../../../../components/Button'
import SimpleCard from '../../../../components/templates/SimpleCard'
import { copyTextToClipboard } from '../../../../utils/helpers/copyTextToClipboard'
import styles from './payment.module.scss'
import { useToasts } from 'react-toast-notifications'
import { CheckedCircleOutlined, ClipBoard, Dangerous } from '../../../../assets/icons'
import MainButton from '../../../../components/Form/MainButton'
import useAsync from '../../../../components/hooks/useAsync'
import depositService from '../../../../axios/services/depositService'
import { BankTypes } from '../../../../utils/enums/bankTypes'
import { useDispatch, useSelector } from 'react-redux'
import { format } from 'date-fns'
// import timeConvert from '../../../../utils/helpers/timeConverter'
import LoadingBar from '../../../../components/Loadings/LoadingBar'
import toIDR from '../../../../utils/helpers/toIDR'
import { depositActions } from '../../../../redux/actions/depositAction'
import Modal, { ModalHead, ModalWrapper } from '../../../../components/ModalNew'
import UploadProof from '../UploadProof'
import { id } from 'date-fns/esm/locale'

const Payment = () => {
  const { addToast } = useToasts();
  const { depositId, step } = useSelector(state => state.deposit);
  const [countDown, setCountDown] = useState(0);
  const [canceling, setCanceling] = useState(false)
  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);
  const [showModalUpload, setShowModalUpload] = useState(false);
  const [isCancelSuccess, setIsCancelSuccess] = useState(false);
  const { isLoading } = useSelector(state => state.deposit);

  const [isUploadFailed, setIsUploadFailed] = useState(false);
  const [isUploadSuccess, setIsUploadSuccess] = useState(false);

  const {
    pending,
    value: { data: { deposit = {}, expiredTime } = {} } = {}
  } = useAsync(depositService.detail, depositId);

  const bankLogo = useMemo(() => {
    if (deposit.bankName === BankTypes.BCA) return "/assets/media/banks/BCA.png"
    if (deposit.bankName === BankTypes.MANDIRI) return "/assets/media/banks/Mandiri.png"
    return null
  }, [deposit])

  const onCancel = () => {
    setCanceling(true)
    depositService.cancel(depositId)
      .then(() => setIsCancelSuccess(true))
      .finally(() => setCanceling(false))
  }

  const uploadView = useMemo(() => {
    if (isUploadSuccess) {
      return (
        <>
          <ModalHead title="Bukti Pembayaran" />
          <div className={styles.modal}>
            <div className={styles.confirmWrapper}>
              <CheckedCircleOutlined />
              <div className={styles.confirmText}>
                <h5>Bukti pembayaran berhasil diunggah</h5>
                <p>Status transaksi dapat di cek pada Riwayat Deposit</p>
              </div>
            </div>
            <div className={styles.buttons}>
              <MainButton
                onClick={() => {
                  dispatch(depositActions.changeTab(1))
                  setIsUploadSuccess(false)
                }}
              >
                Lanjutkan
              </MainButton>
            </div>
          </div>
        </>
      )
    } else if (isUploadFailed) {
      return (
        <>
          <ModalHead title="Bukti Pembayaran" />
          <div className={styles.modal}>
            <div className={styles.confirmWrapper}>
              <Dangerous />
              <div className={styles.confirmText}>
                <h5>Bukti pembayaran gagal diunggah</h5>
                <p>Status transaksi dapat di cek pada Riwayat Deposit</p>
              </div>
            </div>
            <div className={styles.buttons}>
              <MainButton
                onClick={() => {
                  setIsUploadFailed(false)
                }}
              >
                Coba lagi
              </MainButton>
            </div>
          </div>
        </>
      )
    }

    return (
      <UploadProof
        setShowModalUpload={setShowModalUpload}
        setIsUploadSuccess={setIsUploadSuccess}
        setIsUploadFailed={setIsUploadFailed}
      />
    )
  }, [isUploadSuccess, isUploadFailed, dispatch])

  useEffect(() => {
    const count = setInterval(() => {
      if (countDown) setCountDown(prev => prev - 1)
      else {
        clearInterval(count)
      }
    }, 1000);
    return () => {
      clearInterval(count)
    }
  }, [countDown])

  useEffect(() => {
    if (typeof expiredTime === "number") {
      if (Boolean(expiredTime > 0)) {
        setCountDown(Math.floor(expiredTime / 1000))
      } else {
        dispatch(depositActions.setStep(0))
      }
    }
  }, [expiredTime, dispatch])

  return !pending ? (
    <>
      <div className={styles.lineDash} />
      <SimpleCard className={styles.payment} loading={canceling || isLoading}>
        <div className={styles.paymentWrapper}>
          <div className={styles.heading}>
            <h3>Pembayaran</h3>
          </div>
          <div className={styles.warnCard}>
            <p>
              Transfer sebelum <b>{deposit?.expiredAt && format(new Date(deposit?.expiredAt), "dd MMM yyyy HH:mm", { locale: id })} WIB</b> atau transaksi Anda akan dibatalkan otomatis oleh sistem.
            </p>
            {/* <br />
            <p>Selesaikan transaksi Anda dalam: {timeConvert(countDown)}</p> */}
          </div>
          <p className={styles.info}>Lakukan transfer ke nomor rekening Dipay dibawah ini</p>
          <div className={styles.bankAccountCard}>
            <div className={styles.bankInfo}>
              <div className={styles.bankLogo}>
                <img src={bankLogo} alt="" />
              </div>
              <div className={styles.detail}>
                <h6 className={styles.bankName}>{BankTypes.getStr(deposit.bankName)}</h6>
                <h6 className={styles.companyName}>Mareco Prima Mandiri</h6>
              </div>
            </div>
            <div className={styles.bankAccount}>
              <h1>{deposit?.bankAccountNumber}</h1>
              <Button
                onClick={() => copyTextToClipboard(
                  deposit?.bankAccountNumber,
                  () => addToast('Nomor rekening berhasil disalin'),
                  () => addToast('Nomor rekening gagal disalin', { appearance: 'danger' })
                )}
                className={styles.btnCopy}
              >
                <span>Salin</span>
                <ClipBoard />
              </Button>
            </div>
          </div>
          <p className={styles.info}>Pastikan nominal yang dibayar sesuai hingga <b>3 digit terakhir.</b></p>
          <div className={styles.amountCard}>
            <h1>Rp{toIDR(deposit?.amount / 1000, false)}.<span className="text-primary">{deposit?.trxCode?.length > 2 ? deposit?.trxCode : `0${deposit?.trxCode}`}</span></h1>
            <Button
              onClick={() => copyTextToClipboard(
                deposit?.amount + deposit?.trxCode,
                () => addToast('Nominal berhasil disalin'),
                () => addToast('Nominal gagal disalin', { appearance: 'danger' })
              )}
              className={styles.btnCopy}
            >
              <span>Salin</span>
              <ClipBoard />
            </Button>
          </div>

          <div className={styles.note}>
            <ul>
              <li>
                <div className={styles.index}>
                  <span>1</span>
                </div>
                <p><span>Rp{toIDR(deposit?.amount / 1000, false)}.{deposit?.trxCode?.length > 2 ? deposit?.trxCode : `0${deposit?.trxCode}`}</span> akan dimasukan ke deposit anda</p>
              </li>
              <li>
                <div className={styles.index}>
                  <span>2</span>
                </div>
                <p>Kode unik <span>{deposit?.trxCode?.length > 2 ? deposit?.trxCode : `Rp${deposit?.trxCode}`}</span> untuk mempercepat proses pengecekan dan akan disimpan ke dalam deposit akun yang dapat dicairkan</p>
              </li>
            </ul>
          </div>

          <div className={styles.buttons}>
            {step === 2 &&
              <MainButton
                onClick={() => dispatch(depositActions.changeTab(1))}
              >
                Cek Riwayat Deposit
              </MainButton>
            }
            <MainButton
              variant="primary-light"
              onClick={() => setShowModalUpload(true)}
            >
              Unggah Bukti Pembayaran
            </MainButton>
            <button
              className={styles.link}
              onClick={() => setShowModal(true)}
            >
              Batalkan Transaksi
            </button>
          </div>
        </div>
      </SimpleCard>
      <Modal in={showModal} onClose={() => setShowModal(false)}>
        <ModalWrapper className={styles.modalDialog}>
          <ModalHead
            title="Batalkan Top Up Deposit"
            onClose={
              isCancelSuccess ?
                () => dispatch(depositActions.setStep(0))
                :
                () => setShowModal(false)
            } />
          <div className={styles.modal}>
            <div className={styles.confirmWrapper}>
              {isCancelSuccess ?
                <>
                  <CheckedCircleOutlined />
                  <div className={styles.confirmText}>
                    <h5>Transaksi berhasil dibatalkan</h5>
                    <p>Status transaksi dapat di cek pada Riwayat Deposit</p>
                  </div>
                </>
                :
                <div className={styles.confirmText}>
                  <h5>Top Up Deposit akan dibatalkan</h5>
                  <p>Apakah Anda yakin ingin membatalkan transaksi ini?</p>
                </div>
              }
            </div>
            <div className={styles.buttons}>
              {!isCancelSuccess &&
                <MainButton
                  variant="primary-light"
                  onClick={onCancel}
                >
                  Batalkan
                </MainButton>
              }
              <MainButton
                onClick={
                  isCancelSuccess ?
                    () => {
                      dispatch(depositActions.setStep(0))
                      setShowModal(false)
                    }
                    :
                    () => setShowModal(false)
                }
              >
                Kembali
              </MainButton>
            </div>
          </div>
        </ModalWrapper>
      </Modal>

      <Modal in={showModalUpload} onClose={() => setShowModalUpload(false)} scrollable={!(isUploadSuccess || isUploadFailed)}>
        <ModalWrapper className={(isUploadSuccess || isUploadFailed) ? styles.modalDialog : styles.modalUpload}>
          {uploadView}
        </ModalWrapper>
      </Modal>
    </>
  ) :
    <SimpleCard>
      <LoadingBar />
    </SimpleCard>
}

export default Payment
