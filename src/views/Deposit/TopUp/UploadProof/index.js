import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import depositService from '../../../../axios/services/depositService'
import MainButton from '../../../../components/Form/MainButton'
import UploadFile from '../../../../components/UploadFile'
import styles from './uploadProof.module.scss'

const UploadProof = ({
  setShowModalUpload,
  setIsUploadSuccess,
  setIsUploadFailed
}) => {
  const [file, setFile] = useState(null);
  const { depositId } = useSelector(state => state.deposit);
  const [loading, setLoading] = useState(false);

  const onSend = () => {
    setLoading(true)
    depositService.uploadProof(depositId, file)
      .then(() => setIsUploadSuccess(true))
      .catch(() => setIsUploadFailed(true))
      .finally(() => setLoading(false))
  }

  return (
    <div className={styles.uploadProof}>
      <div className={styles.heading}>
        <h3>Upload Bukti Pembayaran</h3>
        <p>Unggah bukti pembayaran dapat mempercepat verifikasi Top Up Deposit</p>
      </div>
      <div className={styles.rulesLabel}>
        <span>Pastikan bukti pembayaran menampilkan:</span>
      </div>
      <div className={styles.rules}>
        <div className={styles.item}>
          <div className={styles.number}>
            <span>1</span>
          </div>
          <div className={styles.info}>
            <h5>Tanggal/Waktu Transfer</h5>
            <p>Contoh : Tanggal 04/06/19 / Jam 07:24:06</p>
          </div>
        </div>
        <div className={styles.item}>
          <div className={styles.number}>
            <span>3</span>
          </div>
          <div className={styles.info}>
            <h5>Detail Penerima</h5>
            <p>Contoh : Transfer ke Rekening PT Mareco Prima Mandiri</p>
          </div>
        </div>
        <div className={styles.item}>
          <div className={styles.number}>
            <span>2</span>
          </div>
          <div className={styles.info}>
            <h5>Status Berhasil</h5>
            <p>Contoh : Transfer BERHASIL, Transaksi Sukses</p>
          </div>
        </div>
        <div className={styles.item}>
          <div className={styles.number}>
            <span>4</span>
          </div>
          <div className={styles.info}>
            <h5>Jumlah Transfer</h5>
            <p>Contoh : Rp.123.456.000</p>
          </div>
        </div>
      </div>
      {/* <div className={styles.cardInfo}>
        <InfoCircle />
        <span>Sistem Dipay sedang melakukan pengecekan</span>
      </div> */}
      <div className={styles.upload}>
        <UploadFile
          onChange={(v) => setFile(v)}
          onFocus={() => setFile()}
          maxSize={2000000}
          uploading={loading}
        />
      </div>
      <div className={styles.buttons}>
        <MainButton
          variant="primary-light"
          onClick={() => setShowModalUpload(false)}
        >
          Batalkan
        </MainButton>
        <MainButton
          onClick={onSend}
          disabled={!file || loading}
        >
          Kirim
        </MainButton>
      </div>
    </div>
  )
}

export default UploadProof
