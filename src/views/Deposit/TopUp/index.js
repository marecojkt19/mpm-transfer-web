import React from 'react'
import { useSelector } from 'react-redux'
import Steps, { Step } from '../../../components/Steps'
import Create from './Create'
import Payment from './Payment'

const TopUp = () => {
  const { step } = useSelector(state => state.deposit);

  return (
    <Steps
      activeStep={step}
      className="my-24"
    >
      <Step label="Top Up">
        <Create />
      </Step>
      <Step label="Pembayaran">
        <Payment />
      </Step>
    </Steps>
  )
}

export default TopUp
