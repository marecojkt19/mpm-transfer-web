import React, { useEffect, useState } from 'react'
import RadioItem3 from '../../../../components/Form/Radio/RadioItem3'
// import RadioGroup2 from '../../../../components/Forms/RadioGroup2'
import RadioGroup2 from '../../../../components/Form/Radio/RadioGroup2'
import SimpleCard from '../../../../components/templates/SimpleCard'
import styles from './create.module.scss'
import { useForm } from 'react-hook-form';
import TextField from '../../../../components/Form/TextField'
import { BankTypes } from '../../../../utils/enums/bankTypes'
import MainButton from '../../../../components/Form/MainButton'
import pakaiClass from 'pakai-class'
import depositService from '../../../../axios/services/depositService'
import { depositActions } from '../../../../redux/actions/depositAction'
import { useDispatch, useSelector } from 'react-redux'
import TncTemplate from '../../../../components/templates/TncTemplate'
import Modal from '../../../../components/ModalNew'
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

const schema = yup.object().shape({
  amount: yup.string()
    .required('Nominal tidak boleh kosong')
    .test(
      'max',
      'Nominal melebihi Rp500.000.000',
      (value) => +value <= 500000000,
    )
    .test(
      'min',
      'Nominal kurang dari Rp50.000',
      (value) => +value >= 50000,
    ),
  bankName: yup.string().required(),
  note: yup.string().max(80, "Maksimal 80 karakter!")
})

const Create = () => {
  const dispatch = useDispatch();
  const { isLoading } = useSelector(state => state.deposit);
  const [showTnc, setShowTnc] = useState(false);

  const {
    watch,
    register,
    unregister,
    handleSubmit,
    formState: { isValid, errors },
    setValue
  } = useForm({
    resolver: yupResolver(schema),
    mode: "onChange"
  });

  const {
    amount,
    bankName
  } = watch([
    "amount",
    "bankName"
  ])

  const onSubmit = (values) => {
    const data = {
      ...values,
      status: "WAITING_PAYMENT"
    }

    depositService.create(data)
      .then(({ data }) => {
        dispatch(depositActions.setId(data._id))
        dispatch(depositActions.setStep(2))
      })
      .catch(() => { })
  }

  useEffect(() => {
    register("amount");
    register("bankName");

    return () => {
      unregister("amount");
      unregister("bankName")
    }
  }, [register, unregister])

  return (
    <>
      <div className={styles.create}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <SimpleCard className={styles.topCard} loading={isLoading}>
            <div className={styles.lineDash} />
            <div className={styles.heading}>
              <h3>Top-Up</h3>
              <p>Pilih jenis bank yang akan Anda gunakan untuk mengisi Deposit rekening Anda.</p>
            </div>
            <div className={styles.amount}>
              <label className={styles.formLabel} htmlFor='nominal'>Nominal</label>
              <div className={styles.formGroup}>
                <TextField
                  placeholder={0}
                  className={styles.input}
                  value={amount}
                  error={errors?.amount}
                  helperText={errors?.amount?.message}
                  onChange={(v) => {
                    setValue("amount", v, { shouldValidate: true })
                  }}
                  variant="normal"
                  allowLeadingZero={false}
                  maxLength={9}
                  moneyInput
                  formated
                />
              </div>
            </div>

            <div className={styles.autoAmount}>
              <label className={pakaiClass(styles.formLabel, styles.hyperText)}>
                Minimal nominal Rp50.000 dan maksimal Rp500.000.000 untuk sekali transaksi.
              </label>
              <RadioGroup2
                name="amount"
                value={amount}
                onChange={e => setValue('amount', e.target.value, { shouldValidate: true })}
                row
              >
                <RadioItem3 label="Rp5 Juta" value="5000000" checked={amount === "5000000"} />
                <RadioItem3 label="Rp10 Juta" value="10000000" checked={amount === "10000000"} />
                <RadioItem3 label="Rp25 Juta" value="25000000" checked={amount === "25000000"} />
                <RadioItem3 label="Rp50 Juta" value="50000000" checked={amount === "50000000"} />
                <RadioItem3 label="Rp100 Juta" value="100000000" checked={amount === "100000000"} />
                <RadioItem3 label="Rp500 Juta" value="500000000" checked={amount === "500000000"} />
              </RadioGroup2>
            </div>

            <div className={styles.bankName}>
              <label className={styles.formLabel}>
                Rekening yang saya gunakan
              </label>
              <RadioGroup2
                name="bankName"
                value={bankName}
                onChange={e => setValue('bankName', e.target.value, { shouldValidate: true })}
                row
              >
                <RadioItem3 imageLabel="/assets/media/banks/BCA.png" value={BankTypes.BCA} />
                <RadioItem3 imageLabel="/assets/media/banks/Mandiri.png" value={BankTypes.MANDIRI} />
              </RadioGroup2>
            </div>

            <div className={styles.note}>
              <div>
                <label className={styles.formLabel}>
                  Catatan (Opsional)
                </label>
              </div>
              <TextField
                ref={register}
                name="note"
                error={errors?.note}
                helperText={errors?.note?.message}
                variant="normal"
                placeholder="Tulis catatan jika ada"
              />
            </div>
          </SimpleCard>

          <SimpleCard className={styles.bottomCard}>
            <p>
              Dengan menekan tombol{" "}
              <span className="font-500">“Lanjut ke Pembayaran”</span>{" "}
              saya sudah paham dengan{" "}
              <button
                type="button"
                onClick={() => setShowTnc(true)}
                className="font-500 text-primary"
              >
                Syarat & Ketentuan
              </button>
            </p>
            <MainButton
              type="submit"
              className={styles.btn}
              disabled={!isValid}
            >
              Lanjut ke Pembayaran
            </MainButton>
          </SimpleCard>
        </form>
      </div>
      <Modal in={showTnc} onClose={() => setShowTnc(false)}>
        <TncTemplate />
      </Modal>
    </>
  )
}

export default Create
