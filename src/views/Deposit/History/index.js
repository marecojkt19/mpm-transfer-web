import { endOfDay, format, startOfDay } from "date-fns";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import SelectField from "../../../components/Form/SelectFieldNew";
import Table from "../../../components/Table";
import { BankOpts, BankTypes } from "../../../utils/enums/bankTypes";
import styles from "./history.module.scss";
import toIDR from "../../../utils/helpers/toIDR";
import { depositActions } from "../../../redux/actions/depositAction";
import DateRangePicker from '../../../components/Form/DatesNew/DateRangePicker';
import { getStatus } from "../../../utils/helpers/getStatus";
import Modal from "../../../components/ModalNew";
import Detail from "./Detail";
import { HistoryDepositStatusOpts, HistoryDepositStatusTypes } from "../../../utils/enums/historyDepositStatusTypes";
import { id } from "date-fns/esm/locale";
import depositService from "../../../axios/services/depositService";

function History() {
  const dispatch = useDispatch();
  const {
    items,
    tableConfig: {
      isLoading,
      page,
      totalData,
      limit,
      sort,
      filter,
      bankName,
      status
    }
  } = useSelector(state => state.deposit);
  // const [activeFilters, setActiveFilters] = useState([]);
  // const [activeFiltersStatus, setActiveFiltersStatus] = useState([]);
  const [showDetail, setShowDetail] = useState(false);
  const { company } = useSelector(state => state.auth);

  const setTableConfig = useCallback(
    key => value => dispatch(depositActions.setTableConfig(key, value)),
    [dispatch],
  )

  const setTableConfigFilter = useCallback(
    key => value => dispatch(depositActions.setTableConfigFilter(key, value)),
    [dispatch],
  )

  const load = useCallback(
    () => {
      const tConfig = {
        limit,
        page,
        sort,
        filter,
        bankName,
        status
      }
      return dispatch(depositActions.loadRequested(tConfig))
    },
    [dispatch, limit, page, sort, filter, bankName, status],
  )

  const onChangePage = useMemo(() => setTableConfig('page'), [setTableConfig]);

  const onContinuePayment = useCallback((id) => {
    dispatch(depositActions.setId(id))
    dispatch(depositActions.setStep(1))
    dispatch(depositActions.changeTab(0))
  }, [dispatch])

  const columns = useMemo(() => ([
    { title: 'No Transaksi', key: 'trxNumber' },
    { title: 'Waktu Dibuat', key: 'createdAt', sortable: true, render: v => format(new Date(v), 'dd MMM yyyy, HH:mm:ss', { locale: id }) },
    { title: 'Bank', key: 'bankName', render: (v) => <div>{BankTypes.getStr(v)}</div> },
    { title: 'Nama', key: 'depositor', sortable: true, },
    { title: 'Peran', key: 'depositorRole' },
    { title: 'Nominal (Rp)', key: 'amount', sortable: true, render: v => (<div>{toIDR(v, false)}</div>) },
    { title: 'Kode Unik (Rp)', key: 'trxCode', render: v => (<div>{toIDR(v, false)}</div>) },
    { title: 'Total (Rp)', key: 'amount', render: (v, row) => (<div>{toIDR(v + row?.trxCode, false)}</div>) },
    { title: 'Catatan', key: 'note', render: v => v ? <div className={styles.note}>{v}</div> : "-" },
    { title: 'Status', key: 'status', render: (v, row) => v ? getStatus({ status: v, fontColor: row.statusColor, bgColor: row.statusBg }) : "-" },
    {
      title: 'Action', key: 'status', render: (v, row) => {
        if (v === HistoryDepositStatusTypes.getStr(HistoryDepositStatusTypes.WAITING_PAYMENT)) {
          return (
            <button
              className={styles.action}
              onClick={() => onContinuePayment(row._id)}
            >
              Lihat Detail
            </button>
          )
        } else {
          return (
            <button
              className={styles.action}
              onClick={() => {
                setShowDetail(true)
                dispatch(depositActions.setId(row._id))
              }}
            >
              Lihat Detail
            </button>
          )
        }
      }
    },
  ]), [onContinuePayment, dispatch]);

  const excelColumns = useMemo(() => [
    { title: 'Waktu Dibuat', key: 'createdAt', render: v => format(new Date(v), 'd MMM yyyy, HH:mm', { locale: id }) },
    { title: 'Bank', key: 'bankName', render: (v) => <div>{BankTypes.getStr(v)}</div> },
    { title: 'Nama', key: 'depositor' },
    { title: 'Peran', key: 'depositorRole' },
    { title: 'Nominal (Rp)', key: 'amount', render: v => (<div>{toIDR(v, false)}</div>) },
    { title: 'Kode Unik (Rp)', key: 'trxCode', render: v => (<div>{toIDR(v, false)}</div>) },
    { title: 'Total (Rp)', key: 'amount', render: (v, row) => (<div>{toIDR(v + row?.trxCode, false)}</div>) },
    { title: 'Catatan', key: 'note', render: v => v ? <div>{v}</div> : "-" },
    { title: 'Status', key: 'status' },
  ], []);

  const APIParamsForExcel = useMemo(() => ({
    page: page,
    limit: limit,
    sort,
    search: filter.search,
    startDate: filter.startDate ? startOfDay(new Date(filter.startDate)) : null,
    endDate: filter.endDate ? endOfDay(new Date(filter.endDate)) : null,
    ...filter,
  }), [filter, sort, limit, page])

  useEffect(() => {
    load()
  }, [load, company])

  return (
    <>
      <Table
        data={items}
        onChangePage={onChangePage}
        onSort={(v) => setTableConfig('sort')(v)}
        onLimit={(v) => setTableConfig('limit')(v)}
        onSearch={(v) => setTableConfigFilter('search')(v)}
        searchPlaceholder="Cari ID, Nama, Catatan, ..."
        load={load}
        search={true}
        config={{
          columns: columns,
          withIndex: true,
          total: totalData,
          limit,
          currentPage: page,
          loading: isLoading,
          sort,
          showRender: (from, to, total) => `Tampilkan ${from}-${to} dari ${total} Transaksi`
        }}
        excel={{
          columns: excelColumns,
          filename: 'Table Transaksi By API',
          allPage: true,
          api: {
            apiResponseKey: 'deposits',
            service: depositService.historyList,
            APIParams: APIParamsForExcel
          }
        }}
      >
        <SelectField
          name="bank"
          variant="outlined"
          placeholder="Bank"
          options={BankOpts.map(v => ({ value: v.value, label: v.label }))}
          value={filter.bankName}
          onChange={v => {
            if (v?.value !== filter.bankName) {
              setTableConfigFilter('bankName')(v?.value)
            } else {
              setTableConfigFilter('bankName')("")
            }
          }}
          isClearable
        />
        <SelectField
          name="status"
          variant="outlined"
          placeholder="Status"
          options={HistoryDepositStatusOpts}
          value={filter.filter}
          onChange={v => {
            if (v?.value !== filter.filter) {
              setTableConfigFilter('filter')(v?.value)
            } else {
              setTableConfigFilter('filter')("")
            }
          }}
          isClearable
        />
        <SelectField
          variant="outlined"
          placeholder="Urutkan berdasarkan"
          options={[
            { label: 'Waktu Dibuat: Terbaru', value: 'createdAt|desc' },
            { label: 'Waktu Dibuat: Terlama', value: 'createdAt|asc' },
            { label: 'Nama: A-Z', value: 'depositor|asc' },
            { label: 'Nama: Z-A', value: 'depositor|desc' },
            { label: 'Nominal Terendah', value: 'amount|asc' },
            { label: 'Nominal Tertinggi', value: 'amount|desc' }
          ]}
          value={sort}
          onChange={v => {
            if (v?.value !== sort) {
              setTableConfig('sort')(v?.value)
            } else {
              setTableConfig('sort')("")
            }
          }}
          isClearable
        />
        <DateRangePicker
          amountOfMonths={2}
          placeholder="Rentang Tanggal"
          fromDate={filter.startDate}
          toDate={filter.endDate}
          maxDate={new Date()}
          onChange={({ fromDate, toDate }) => {
            if (fromDate && toDate) {
              setTableConfigFilter('startDate')(fromDate)
              setTableConfigFilter('endDate')(toDate)
            } else if (!fromDate && !toDate) {
              setTableConfigFilter('startDate')()
              setTableConfigFilter('endDate')()
            }
          }}
        />
      </Table>
      <Modal in={showDetail} onClose={() => setShowDetail(false)} scrollable>
        <Detail
          setShowDetail={setShowDetail}
        />
      </Modal>
    </>
  )
}

export default History;
