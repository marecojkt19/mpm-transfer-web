import { format } from 'date-fns'
import React, { useMemo, useRef } from 'react'
import { CheckedCircleOutlined, Dangerous, Expired, Timer } from '../../../../assets/icons'
import depositService from '../../../../axios/services/depositService'
import useAsync from '../../../../components/hooks/useAsync'
import LoadingDots from '../../../../components/Loadings/LoadingDots'
import MainButton from '../../../../components/Form/MainButton'
import { HistoryDepositStatusTypes } from '../../../../utils/enums/historyDepositStatusTypes'
import toIDR from '../../../../utils/helpers/toIDR'
import styles from './detail.module.scss'
import { useReactToPrint } from 'react-to-print';
import pakaiClass from 'pakai-class'
import { id } from 'date-fns/esm/locale'
import { useSelector } from 'react-redux'

const Detail = ({
  setShowDetail
}) => {
  const componentRef = useRef();
  const buttonsRef = useRef();

  const { depositId } = useSelector(state => state.deposit)

  const {
    pending,
    value: { data: { deposit = {} } = {} } = {}
  } = useAsync(depositService.detail, depositId);

  const print = useReactToPrint({
    content: () => componentRef.current,
    onBeforeGetContent: () => buttonsRef.current.style.display = "none",
    onAfterPrint: () => buttonsRef.current.style.display = "flex"
  })

  const statusView = useMemo(() => {
    if (deposit?.statusHistories?.length) {
      switch (deposit?.status) {
        case HistoryDepositStatusTypes.getStr(HistoryDepositStatusTypes.SUCCESS):
          return (
            <div className={styles.status}>
              <CheckedCircleOutlined color="#05944F" />
              <h3>Transaksi Berhasil</h3>
              <h6>{deposit?.statusHistories?.length && format(new Date(deposit?.statusHistories[deposit?.statusHistories?.length - 1].createdAt), "dd MMMM yyyy, hh:mm a", { locale: id })}</h6>
              <h1>Rp{toIDR(deposit?.amount / 1000, false)}.{deposit?.trxCode?.length > 2 ? deposit?.trxCode : `0${deposit?.trxCode}`}</h1>
            </div>
          )
        case HistoryDepositStatusTypes.getStr(HistoryDepositStatusTypes.CANCELED):
          return (
            <div className={styles.status}>
              <Dangerous />
              <h3>Transaksi Dibatalkan</h3>
              <h6>{deposit?.statusHistories?.length && format(new Date(deposit?.statusHistories[deposit?.statusHistories?.length - 1].createdAt), "dd MMMM yyyy, hh:mm a", { locale: id })}</h6>
              <h1>Rp{toIDR(deposit?.amount / 1000, false)}.{deposit?.trxCode?.length > 2 ? deposit?.trxCode : `0${deposit?.trxCode}`}</h1>
            </div>
          )
        case HistoryDepositStatusTypes.getStr(HistoryDepositStatusTypes.EXPIRED):
          return (
            <div className={styles.status}>
              <Expired />
              <h3>Transaksi Kedaluwarsa</h3>
              <h6>{deposit?.statusHistories?.length && format(new Date(deposit?.statusHistories[deposit?.statusHistories?.length - 1].createdAt), "dd MMMM yyyy, hh:mm a", { locale: id })}</h6>
              <h1>Rp{toIDR(deposit?.amount / 1000, false)}.{deposit?.trxCode?.length > 2 ? deposit?.trxCode : `0${deposit?.trxCode}`}</h1>
            </div>
          )
        case HistoryDepositStatusTypes.getStr(HistoryDepositStatusTypes.WAITING_CONFIRMATION):
          return (
            <div className={styles.status}>
              <Timer />
              <h3>Transaksi Menunggu Konfirmasi</h3>
              <h6>{deposit?.statusHistories?.length && format(new Date(deposit?.statusHistories[deposit?.statusHistories?.length - 1].createdAt), "dd MMMM yyyy, hh:mm a", { locale: id })}</h6>
              <h1>Rp{toIDR(deposit?.amount / 1000, false)}.{deposit?.trxCode?.length > 2 ? deposit?.trxCode : `0${deposit?.trxCode}`}</h1>
            </div>
          )
        default:
          return null
      }
    }
  }, [deposit])

  return !pending ? (
    <div className={styles.detail} ref={componentRef}>
      <div className={styles.wrapper}>
        <div className={styles.header}>
          <img src="/assets/dipay-logo.png" alt="" />
        </div>
        {statusView}
        <div className={styles.list}>
          <div className={styles.item}>
            <h6>No. Transaksi</h6>
            <p>{deposit?.trxNumber}</p>
          </div>
          <div className={styles.item}>
            <h6>Pengirim</h6>
            <p>{deposit?.depositor}</p>
          </div>
          <div className={styles.item}>
            <h6>Bank Tujuan</h6>
            <p>{deposit?.bankName}</p>
          </div>
          <div className={styles.item}>
            <h6>No. Rekening Tujuan</h6>
            <p>{deposit?.bankAccountNumber}</p>
          </div>
          <div className={styles.item}>
            <h6>Penerima</h6>
            <p>PT Mareco Prima Mandiri</p>
          </div>
          <div className={styles.item}>
            <h6>Catatan</h6>
            <p>{deposit?.note}</p>
          </div>
        </div>
        <div className={styles.desc}>
          <span>Resi ini merupakan bukti transaksi yang sah.</span>
        </div>
        <div className={styles.buttons} ref={buttonsRef}>
          <MainButton
            variant="primary-light"
            className={styles.btn}
            onClick={() => print()}
          >
            Unduh
          </MainButton>
          <MainButton
            className={styles.btn}
            onClick={() => setShowDetail(false)}
          >
            Kembali
          </MainButton>
        </div>
      </div>
    </div>
  ) : (
    <div className={pakaiClass(styles.detail, styles.loading)}>
      <div className={styles.loading}>
        <LoadingDots />
      </div>
    </div>
  )
}

export default Detail
