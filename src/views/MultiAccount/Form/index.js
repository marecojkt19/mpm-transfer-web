import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import TextField from '../../../components/Form/TextField';
import styles from './form.module.scss'
import { UserRoleTypes } from '../../../utils/enums/userRoleTypes';
import RadioGroup2 from '../../../components/Form/Radio/RadioGroup2'
import RadioItem2 from '../../../components/Form/Radio/RadioItem2';
import pakaiClass from 'pakai-class';

const schema = yup.object().shape({
  fullName: yup.string().required('Nama lengkap tidak boleh kosong'),
  email: yup.string().email("Format email salah").required('Email tidak boleh kosong'),
  role: yup.string().required()
})

const Form = ({
  onSubmit,
  onFormValid,
  data,
  isUpdate
}) => {
  const { register, unregister, handleSubmit, errors, setValue, watch, formState: { isValid } } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      fullName: data?.fullName,
      email: data?.email,
      role: data?.role?.role
    }
  });

  const {
    role,
    fullName,
    email,
  } = watch([
    "role",
    "fullName",
    "email"
  ]);

  useEffect(() => {
    register("fullName");
    register("email");
    register("role");

    return () => {
      unregister("fullName");
      unregister("email");
      unregister("role")
    }

  }, [register, unregister])

  useEffect(() => {
    if (isValid) {
      onFormValid(isValid);
    }
  }, [onFormValid, isValid]);

  return (
    <div className={styles.form}>
      <form onSubmit={handleSubmit(onSubmit)} id="confirm">
        <div className={styles.formWrapper}>
          <TextField
            name="email"
            placeholder="johndoe@gmail.com"
            label="Email*"
            disabled={isUpdate}
            error={errors.email?.message}
            helperText={errors.email?.message}
            value={email}
            onChange={(e) => setValue("email", e?.target?.value, { shouldValidate: true })}
          />
          <TextField
            name="fullName"
            placeholder="Tulis Nama Lengkap"
            label="Nama*"
            disabled={isUpdate}
            error={errors.fullName?.message}
            helperText={errors.fullName?.message}
            value={fullName}
            onChange={(e) => setValue("fullName", e?.target?.value, { shouldValidate: true })}
          />
        </div>
        <div className={styles.line}></div>
        <div className={styles.role}>
          <RadioGroup2
            className="mb-24"
            value={role}
            onChange={e => setValue('role', e.target.value, { shouldValidate: true, shouldDirty: true })}
          >
            {/* <RadioItem2
              label={(
                <div className={styles.radioLabel}>
                  <div className={pakaiClass(styles.alert, styles.success)}>
                    <span>{UserRoleTypes.getStr(UserRoleTypes.SUPERVISOR)}</span>
                  </div>
                  <p>
                    Memiliki semua akses ke semua fitur Dipay Disbursement, dari membuat transaksi hingga mengelola multiakun serta profile
                  </p>
                </div>
              )}
              value={UserRoleTypes.SUPERVISOR}
            /> */}
            {/* <RadioItem2
              label={(
                <div className={styles.radioLabel}>
                  <div className={pakaiClass(styles.alert, styles.primary)}>
                    <span>{UserRoleTypes.getStr(UserRoleTypes.OPERATOR)}</span>
                  </div>
                  <p>
                    Dapat membuat transaksi serta melakukan Top-up Deposit
                  </p>
                </div>
              )}
              value={UserRoleTypes.OPERATOR}
            /> */}
            <RadioItem2
              label={(
                <div className={styles.radioLabel}>
                  <div className={pakaiClass(styles.alert, styles.primary)}>
                    <span>{UserRoleTypes.getStr(UserRoleTypes.SUPERVISOR)}</span>
                  </div>
                  <p>
                    Memiliki semua akses ke semua fitur Dipay Disbursement, dari membuat transaksi hingga mengelola multiakun serta profile
                  </p>
                </div>
              )}
              value={UserRoleTypes.SUPERVISOR}
            />
            {/* <RadioItem2
              label={(
                <div className={styles.radioLabel}>
                  <div className={pakaiClass(styles.alert, styles.primary)}>
                    <span>{UserRoleTypes.getStr(UserRoleTypes.DEVELOPER)}</span>
                  </div>
                  <p>
                    hanya memunculkan menu beranda & pengaturan submenu kredensial (client ID, client key, public key)
                  </p>
                </div>
              )}
              value={UserRoleTypes.DEVELOPER}
            /> */}
          </RadioGroup2>
        </div>
      </form>
    </div>
  )
}

export default Form
