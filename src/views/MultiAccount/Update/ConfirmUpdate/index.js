import React from 'react'
import MainButton from '../../../../components/Form/MainButton'
import styles from './confirmUpdate.module.scss'

const ConfirmUpdate = ({
  data,
  onSubmit,
  onBackStep,
  loading
}) => {
  return (
    <div className={styles.confirmUpdate}>
      <div className={styles.wrapper}>
        <p>Apakah Anda yakin ingin mengubah peran akun ini?</p>
        <div className={styles.summary}>
          <table>
            <tr>
              <td><span>Email</span></td>
              <td><span>:</span></td>
              <td><span>{data?.email}</span></td>
            </tr>
            <tr>
              <td><span>Nama Lengkap</span></td>
              <td><span>:</span></td>
              <td><span>{data?.fullName}</span></td>
            </tr>
            <tr>
              <td><span>Peran</span></td>
              <td><span>:</span></td>
              <td><span>{UserRoleTypes.getStr(data?.role)}</span></td>
            </tr>
          </table>
        </div>
        {/* <div className={styles.note}>
          <p>Kami akan mengirimkan pesan konfirmasi pada email di atas untuk pengaktifan Akun Dipay Disbursement</p>
        </div> */}
      </div>
      <div className={styles.buttons}>
        <div className={styles.backButton}>
          <button onClick={onBackStep}>
            <span>Kembali</span>
          </button>
        </div>
        <div className={styles.saveButton}>
          <MainButton
            onClick={onSubmit}
            loading={loading}
          >
            Simpan
          </MainButton>
        </div>
      </div>
    </div>
  )
}

export default ConfirmUpdate
