import React, { useCallback, useMemo, useState } from 'react'
import Form from '../Form'
import accountsService from '../../../axios/services/accountsService'
import { useToasts } from 'react-toast-notifications'
import styles from './update.module.scss'
import MainButton from '../../../components/Form/MainButton'
import SuccessPopUp from '../SuccessPopUp'
import { CloseCircle } from '../../../assets/icons'
import { ModalWrapper } from '../../../components/Modal'
import LoadingDots from '../../../components/Loadings/LoadingDots'
import useAsync from '../../../components/hooks/useAsync';

const Update = ({ onClose, onSuccess, userId, companyCode }) => {
  const { addToast } = useToasts();
  const [step, setStep] = useState(1);
  const [disabledConfirm, setDisabledConfirm] = useState(true);
  const [loading, setLoading] = useState(false);

  const {
    pending,
    value: { data } = {}
  } = useAsync(accountsService.detail, { userId, companyCode });

  const onUpdate = useCallback((values) => {
    const preparedData = {
      userId,
      currentRole: data?.role?.role,
      newRole: values.role,
      email: data?.email
    }
    setLoading(true)
    accountsService.update({ userId: data?.role?._id, data: preparedData })
      .then(() => {
        setStep(2);
        onSuccess();
      })
      .catch((err) => {
        if (err.response) {
          if (err.response.body) {
            addToast(err.response.body.message, { appearance: 'danger' });
          }
          if (err.response.data) {
            if (typeof err.response.data.message === "string") {
              addToast(err.response.data.message, { appearance: 'danger' });
            } else {
              for (const fields of err.response.data.message) {
                for (const message of Object.keys(fields.constraints)) {
                  addToast(fields.constraints[message], { appearance: 'danger' });
                }
              }
            }
          }
        } else {
          addToast(err, { appearance: 'danger' });
        }
      })
      .finally(() => setLoading(false))
  }, [addToast, setLoading, data, userId, onSuccess]);

  const renderView = useMemo(() => {
    if (step === 1) return (
      <ModalWrapper>
        <div className={styles.modalHead}>
          <h1>Ubah Detail Akun</h1>
          <button
            onClick={onClose}
          >
            <CloseCircle size={20} />
          </button>
        </div>
        <Form data={data} isUpdate onSubmit={onUpdate} onFormValid={(v) => setDisabledConfirm(!v)} />
        <div className={styles.btnWrapper}>
          <MainButton loading={loading} type="submit" form="confirm" disabled={disabledConfirm}>
            Ubah Peran Akun
          </MainButton>
        </div>
      </ModalWrapper>
    )
    if (step === 2) return (
      <SuccessPopUp
        onClose={onClose}
        title="Kelola Multi Akun"
        heading="Peran Akun Berhasil Diubah!"
        subHeading={`Segera cek notifikasi pada email ${data?.email} untuk mengaktifkan akun Dipay Disbursement`}
      />
    )
  }, [
    step,
    onClose,
    onUpdate,
    data,
    loading,
    disabledConfirm
  ])

  return !pending ? renderView : <LoadingDots className={styles.loading} />
}

export default Update
