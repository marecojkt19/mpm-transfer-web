import { format } from 'date-fns';
import { id } from 'date-fns/esm/locale';
import pakaiClass from 'pakai-class';
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import CountUp from 'react-countup';
import { Helmet } from 'react-helmet'
import { useDispatch, useSelector } from 'react-redux';
import { Add, CheckedCircleOutlined, Dangerous, Delete2, Lock, ShieldChecked } from '../../assets/icons';
import LoadingDots from '../../components/Loadings/LoadingDots';
import Modal from '../../components/ModalNew';
import Table from '../../components/Table';
import MainButton from '../../components/templates/MainButton';
import { accountsActions } from '../../redux/actions/accountsActions';
import { UserRoleTypes } from '../../utils/enums/userRoleTypes';
import Create from './Create';
import Delete from './Delete';
import styles from './multiAccount.module.scss';
import Update from './Update';

const MultiAccount = () => {
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [userId, setUserId] = useState();
  const [companyCode, setCompanyCode] = useState();
  const dispatch = useDispatch();
  const {
    items,
    totalActiveAccounts,
    totalInactiveAccounts,
    totalData,
    tableConfig: {
      isLoading,
      page,
      limit,
      filter,
    }
  } = useSelector(state => state.accounts);

  const load = useCallback(
    () => {
      const tConfig = {
        limit,
        page,
        filter
      }
      dispatch(accountsActions.loadRequested(tConfig))
    },
    [dispatch, limit, page, filter]
  )

  const setTableConfig = useCallback(
    key => value => dispatch(accountsActions.setTableConfig(key, value)),
    [dispatch],
  )

  useEffect(() => {
    load()
  }, [load]);

  const onChangePage = useMemo(() => setTableConfig('page'), [setTableConfig]);

  const columns = useMemo(() => [
    {
      title: 'Email', key: 'email', render: (v, row) => {
        return <EmailField email={v} isOwner={row?.status?.isOwner} />
      }
    },
    {
      title: 'Peran', key: 'role', render: v => {
        return v?.role === UserRoleTypes.SUPERVISOR ? <RoleAlert role={v?.role} /> : UserRoleTypes.getStr(v?.role)
      }
    },
    {
      title: 'Status', key: 'status', render: v => <StatusAlert status={v?.active} />
    },
    {
      title: 'Waktu dibuat', key: 'createdAt', render: v => format(new Date(v), 'd MMM yyyy HH:mm', { locale: id })
    },
    {
      title: 'Action', key: '_id', render: (v, row) => {
        return !row?.status?.isOwner && (
          <div className={styles.actionButtons}>
            {/* <button onClick={() => {
              setUserId(v)
              setCompanyCode(row?.role?.companyCode)
              setShowUpdateModal(true);
            }}>
              <Rename />
            </button> */}
            <button onClick={() => {
              setUserId(v)
              setCompanyCode(row?.role?.companyCode)
              setShowDeleteModal(true);
            }}>
              <Delete2 />
            </button>
          </div>
        )
      }
    },
  ], [])

  return (
    <>
      <div className={styles.multiAccount}>
        <Helmet title="Kelola Multi Akun" />
        <div className={styles.heading}>
          <h1>Kelola Multi Akun</h1>
        </div>
        <div className={styles.wrapper}>
          <div className={styles.widgets}>
            <Widget
              label="Total Akun Perusahaan"
              total={totalData}
              loading={isLoading}
            />
            <Widget
              label="Akun Aktif"
              total={totalActiveAccounts}
              loading={isLoading}
            />
            <Widget
              label="Akun Belum Aktif"
              total={totalInactiveAccounts}
              loading={isLoading}
            />
          </div>
          <div className={styles.createBtnWrapper}>
            <MainButton onClick={() => setShowCreateModal(true)}>
              <Add />
              <span>Tambah Akun</span>
            </MainButton>
          </div>
        </div>
        <div className={styles.tableCard}>
          <Table
            data={items}
            onChangePage={onChangePage}
            config={{
              loading: isLoading,
              columns,
              withIndex: true,
              total: totalData,
              limit,
              currentPage: page,
              showRender: (from, to, total) => `${from}-${to} dari ${total} transaksi`
            }}
          />
        </div>
      </div>

      {/* Create Modal */}
      <Modal in={showCreateModal} scrollable onClose={() => setShowCreateModal(false)}>
        <Create
          onClose={() => setShowCreateModal(false)}
          onSuccess={() => {
            setShowCreateModal(false)
            load()
          }}
        />
      </Modal>

      {/* Update Modal */}
      <Modal in={showUpdateModal} onClose={() => setShowUpdateModal(false)}>
        <Update
          userId={userId}
          companyCode={companyCode}
          onClose={() => setShowUpdateModal(false)}
          onSuccess={() => load()}
        />
      </Modal>

      {/* Delete Modal */}
      <Modal in={showDeleteModal} onClose={() => setShowDeleteModal(false)}>
        <Delete
          userId={userId}
          companyCode={companyCode}
          onClose={() => {
            load();
            setShowDeleteModal(false)
          }}
        />
      </Modal>
    </>
  )
}

const Widget = ({
  label,
  total,
  loading,
  className,
  formattingFn = v => v
}) => {
  return (
    <div className={pakaiClass(styles.widget, className)}>
      <div className={styles.widgetHeading}>
        <p>{label}</p>
      </div>
      <div className={styles.content}>
        <CountUp
          end={total ?? 0}
          duration={3}
          formattingFn={formattingFn}
        />
      </div>
      {
        loading &&
        <LoadingDots className={styles.loading} />
      }
    </div>
  )
}

const RoleAlert = ({
  role
}) => {
  return (
    <div className={styles.roleAlert}>
      <span>{UserRoleTypes.getStr(role)}</span>
      <ShieldChecked />
    </div>
  )
}

const StatusAlert = ({
  status
}) => {
  return (
    <div
      className={
        pakaiClass(
          styles.statusAlert,
          status ? styles.active : styles.inactive
        )
      }
    >
      {status ? <CheckedCircleOutlined color="#05944F" /> : <Dangerous />}
      <span>{status ? "Aktif" : "Belum Aktif"}</span>
    </div>
  )
}

const EmailField = ({
  email,
  isOwner
}) => {
  return (
    <div className={styles.emailField}>
      {email}
      {isOwner ?
        <div className={styles.isOwner}>
          <Lock />
          <span>Owner</span>
        </div>
        : null
      }
    </div>
  )
}

export default MultiAccount
