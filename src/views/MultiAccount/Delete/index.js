import React, { useCallback, useMemo, useState } from 'react'
import accountsService from '../../../axios/services/accountsService';
import useAsync from '../../../components/hooks/useAsync';
import LoadingDots from '../../../components/Loadings/LoadingDots';
import ConfirmAlert from '../../../components/templates/ConfirmAlert'
import SuccessPopUp from '../SuccessPopUp';
import styles from './delete.module.scss'

const Delete = ({
  onClose,
  userId,
  companyCode
}) => {
  const [loading, setLoading] = useState(false);
  const [step, setStep] = useState(1);

  const {
    pending,
    value: { data } = {}
  } = useAsync(accountsService.detail, { userId, companyCode });

  const onDelete = useCallback(() => {
    const preparedData = {
      userId,
      companyId: data?.currentCompany?.id,
      deletedRole: data?.role?.role,
      email: data?.email
    }
    setLoading(true)
    accountsService.delete({ userId: data?.role?._id, data: preparedData })
      .then(() => {
        setStep(2);
      })
      .catch(() => { })
      .finally(() => setLoading(false))
  }, [data, userId]);

  const renderView = useMemo(() => {
    if (step === 1) {
      return (
        <ConfirmAlert
          onClose={onClose}
          onSubmit={onDelete}
          title="Anda Yakin ingin menghapus Akun ini?"
          description="Jika Anda menghapus Akun ini, maka Akun tidak dapat dipulihkan kembali"
          loading={loading}
        />
      )
    }
    if (step === 2) {
      return (
        <SuccessPopUp
          onClose={onClose}
          title="Kelola Multi Akun"
          heading="Akun telah berhasil dihapus"
          subHeading="Akun yang telah dihapus tidak dapat mengakses kembali Dipay Disbursement."
        />
      )
    }
  }, [step, loading, onClose, onDelete])

  return !pending ? renderView : <LoadingDots className={styles.loading} />
}

export default Delete
