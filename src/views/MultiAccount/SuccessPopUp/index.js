import React from 'react'
import { CheckedCircleOutlined, CloseCircle } from '../../../assets/icons'
import MainButton from '../../../components/Form/MainButton'
import { ModalWrapper } from '../../../components/ModalNew'
import styles from './successPopUp.module.scss'

const SuccessPopUp = ({
  onClose,
  heading,
  subHeading,
  title
}) => {
  return (
    <ModalWrapper className={styles.successModal}>
      <div className={styles.modalHead}>
        <h1>{title}</h1>
        <button
          onClick={onClose}
        >
          <CloseCircle size={20} />
        </button>
      </div>
      <div className={styles.modalBody}>
        <div className={styles.wrapper}>
          <div className={styles.icon}>
            <CheckedCircleOutlined color='#05944F' />
          </div>
          <div className={styles.copy}>
            <h3>{heading}</h3>
            <h5>{subHeading}</h5>
          </div>
        </div>
        <div className={styles.btnWrapper}>
          <MainButton onClick={onClose}>
            Kembali
          </MainButton>
        </div>
      </div>
    </ModalWrapper>
  )
}

export default SuccessPopUp
