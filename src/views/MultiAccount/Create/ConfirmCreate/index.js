import React from 'react'
import MainButton from '../../../../components/Form/MainButton'
import { UserRoleTypes } from '../../../../utils/enums/userRoleTypes'
import styles from './confirmCreate.module.scss'

const ConfirmCreate = ({
  data,
  onSubmit,
  onBackStep,
  loading
}) => {
  return (
    <div className={styles.confirmCreate}>
      <div className={styles.wrapper}>
        <p>Apakah Anda yakin ingin menambahkan email ini pada akun Dipay Disbursement perusahaan Anda?</p>
        <div className={styles.summary}>
          <table>
            <tr>
              <td><span>Email</span></td>
              <td><span>:</span></td>
              <td><span>{data?.email}</span></td>
            </tr>
            <tr>
              <td><span>Nama Lengkap</span></td>
              <td><span>:</span></td>
              <td><span>{data?.fullName}</span></td>
            </tr>
            <tr>
              <td><span>Peran</span></td>
              <td><span>:</span></td>
              <td><span>{UserRoleTypes.getStr(data?.role)}</span></td>
            </tr>
          </table>
        </div>
        <div className={styles.note}>
          <p>Kami akan mengirimkan pesan konfirmasi pada email di atas untuk pengaktifan Akun Dipay Disbursement</p>
        </div>
      </div>
      <div className={styles.buttons}>
        <div className={styles.backButton}>
          <button onClick={onBackStep}>
            <span>Kembali</span>
          </button>
        </div>
        <div className={styles.saveButton}>
          <MainButton
            onClick={onSubmit}
            loading={loading}
          >
            Simpan
          </MainButton>
        </div>
      </div>
    </div>
  )
}

export default ConfirmCreate
