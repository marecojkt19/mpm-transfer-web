import React, { useCallback, useMemo, useState } from 'react'
import { useSelector } from 'react-redux'
import Form from '../Form'
import ConfirmCreate from './ConfirmCreate'
import accountsService from '../../../axios/services/accountsService'
import { useToasts } from 'react-toast-notifications'
import styles from './create.module.scss'
import MainButton from '../../../components/Form/MainButton'
import SuccessPopUp from '../SuccessPopUp'
import { CloseCircle } from '../../../assets/icons'
import { ModalWrapper } from '../../../components/ModalNew'

const Create = ({ onSuccess, onClose }) => {
  const { addToast } = useToasts();
  const [step, setStep] = useState(1);
  const [data, setData] = useState(null);
  const { company } = useSelector(state => state.auth);
  const [disabledConfirm, setDisabledConfirm] = useState(true);
  const [loading, setLoading] = useState(false);

  const onCreate = useCallback(() => {
    const preparedData = {
      ...data,
      company: company?._id
    }
    setLoading(true)
    accountsService.create(preparedData)
      .then(() => {
        onSuccess()
      })
      .catch((err) => {
        if (err.response) {
          if (err.response.body) {
            addToast(err.response.body.message, { appearance: 'danger' });
          }
          if (err.response.data) {
            if (typeof err.response.data.message === "string") {
              addToast(err.response.data.message, { appearance: 'danger' });
            } else {
              for (const fields of err.response.data.message) {
                for (const message of Object.keys(fields.constraints)) {
                  addToast(fields.constraints[message], { appearance: 'danger' });
                }
              }
            }
          }
        } else {
          addToast(err, { appearance: 'danger' });
        }
      })
      .finally(() => setLoading(false))
  }, [onSuccess, addToast, data, company, setLoading]);

  const onConfirmCreate = useCallback(
    (values) => {
      setData(values)
      setStep(2)
    },
    [])

  const renderView = useMemo(() => {
    if (step === 1) return (
      <ModalWrapper className={styles.modalWrapper}>
        <div className={styles.modalHead}>
          <h1>Tambah Akun</h1>
          <button
            onClick={onClose}
          >
            <CloseCircle size={20} />
          </button>
        </div>
        <Form data={data} onSubmit={onConfirmCreate} onFormValid={(v) => setDisabledConfirm(!v)} />
        <div className={styles.btnWrapper}>
          <MainButton type="submit" form="confirm" disabled={disabledConfirm}>
            Tambahkan Akun
          </MainButton>
        </div>
      </ModalWrapper>
    )
    if (step === 2) return (
      <ModalWrapper className={styles.modalWrapper}>
        <div className={styles.modalHead}>
          <h1>Tambah Akun</h1>
          <button
            onClick={onClose}
          >
            <CloseCircle size={20} />
          </button>
        </div>
        <ConfirmCreate
          data={data}
          onBackStep={() => setStep(1)}
          onSubmit={onCreate}
          loading={loading}
        />
      </ModalWrapper>
    )
    if (step === 3) return (
      <SuccessPopUp
        onClose={onClose}
        title="Kelola Multi Akun"
        heading="Akun Telah Berhasil Ditambahkan!"
        subHeading="Segera cek notifikasi pada email novisetyawati@gmail.com untuk mengaktifkan akun Dipay Disbursement"
      />
    )
  }, [
    step,
    onCreate,
    loading,
    onClose,
    onConfirmCreate,
    data,
    disabledConfirm
  ])

  return renderView
}

export default Create
