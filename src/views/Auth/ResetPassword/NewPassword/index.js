import React, { useState } from 'react'
import styles from './newPassword.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import MainButton from '../../../../components/Form/MainButton';
import TextField from '../../../../components/Form/TextField';
import authService from '../../../../axios/services/authService';
import { useLocation } from 'react-router-dom';
import OnboardLayout from '../../../../layouts/OnBoardLayout';

const schema = yup.object().shape({
  password: yup
    .string()
    .required('Kata sandi tidak boleh kosong')
    .matches(
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?([^\w\s]|[_])).{8,}$/,
      'Pastikan kata sandi terdiri dari 8 huruf dengan gabungan simbol, angka, dan kapital!'
    ),
  passwordConfirmation: yup
    .string()
    .required('Kata sandi tidak boleh kosong')
    .test(
      'password',
      "Konfirmasi kata sandi tidak sesuai",
      (value, context) => value === context.parent.password)
})

const NewPassword = () => {
  const query = new URLSearchParams(useLocation().search)
  const token = query.get("token");
  const [loading, setLoading] = useState(false);
  // const { push } = useHistory();

  const { register, handleSubmit, errors, formState: { isValid } } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema)
  });

  const onSubmit = (values) => {
    setLoading(true);
    authService.resetPassword(values, token)
      .then(() => {
        // push('/login')
      })
      .catch(() => { })
      .finally(() => setLoading(false))
  }

  return (
    <OnboardLayout>
      <div className={styles.newPassword}>
        <div className={styles.heading}>
          <h1>Atur Ulang Kata Sandi</h1>
        </div>
        <div className={styles.formCard}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <TextField
              ref={register}
              name="password"
              label="Kata Sandi"
              placeholder="Kata sandi (minimal 8 karakter)"
              error={errors.password?.message}
              helperText={errors.password?.message}
              password
            />
            <TextField
              ref={register}
              name="passwordConfirmation"
              label="Konfirmasi Kata Sandi"
              placeholder="Kata sandi (minimal 8 karakter)"
              error={errors.passwordConfirmation?.message}
              helperText={errors.passwordConfirmation?.message}
              password
            />
            <MainButton
              disabled={!isValid}
              loading={loading}
            >
              Simpan
            </MainButton>
          </form>
        </div>
      </div>
    </OnboardLayout>
  )
}

export default NewPassword
