import React, { useState } from 'react'
import TextField from '../../../../components/Form/TextField'
import styles from './submitEmail.module.scss'

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import MainButton from '../../../../components/Form/MainButton';
import forgotPasswordService from '../../../../axios/services/forgotPasswordService';
import { useHistory } from 'react-router-dom';

const schema = yup.object().shape({
  email: yup.string()
    .required('Email tidak boleh kosong')
    .email('Format email salah!, Contoh: email@dipay.com')
})

const SubmitEmail = ({
  setIsSuccess,
  setEmail
}) => {
  const [loading, setLoading] = useState(false);
  const router = useHistory();

  const { register, handleSubmit, errors, formState: { isValid }, setError } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema)
  });

  const onSubmit = (values) => {
    setLoading(true);
    const email = values.email;
    forgotPasswordService
      .forgotPassword({ email })
      .then(() => {
        setIsSuccess(true)
        setEmail(email)
      })
      .catch(() => {
        setError("email", { message: "Email tidak terdaftar" });
      })
      .finally(() => setLoading(false))
  }

  return (
    <div className={styles.submitEmail}>
      <div className={styles.heading}>
        <h1>Lupa Kata Sandi?</h1>
        <p>Silakan reset kata sandi Anda di sini</p>
      </div>
      <div className={styles.formCard}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <TextField
            ref={register}
            className="mb-24"
            label="E-mail"
            placeholder="Masukkan email Anda"
            name="email"
            error={errors.email?.message}
            helperText={errors.email?.message}
          />
          <MainButton
            disabled={!isValid}
            loading={loading}
          >
            Lanjutkan
          </MainButton>
        </form>
        <div className={styles.bottom}>
          <button type="button" onClick={() => router.push("/login")}>
            <h5>Kembali ke Halaman Login</h5>
          </button>
        </div>
      </div>
    </div>
  )
}

export default SubmitEmail
