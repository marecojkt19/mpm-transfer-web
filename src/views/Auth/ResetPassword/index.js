import React, { useMemo, useState } from 'react'
import OnboardLayout from '../../../layouts/OnBoardLayout';
import SubmitEmail from './SubmitEmail'
import Success from './Success';

const ResetPassword = () => {
  const [isSuccess, setIsSuccess] = useState(false);
  const [email, setEmail] = useState();

  const view = useMemo(() => {
    if (isSuccess) return <Success email={email} />
    return <SubmitEmail setEmail={setEmail} setIsSuccess={setIsSuccess} />
  }, [isSuccess, email])

  return (
    <OnboardLayout>
      {view}
    </OnboardLayout>
  )
}

export default ResetPassword
