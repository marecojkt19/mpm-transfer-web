import React from 'react'
import { useHistory } from 'react-router-dom'
import MainButton from '../../../../components/Form/MainButton'
import styles from './success.module.scss'

const Success = ({
  email
}) => {
  const router = useHistory();

  return (
    <div className={styles.success}>
      <div className={styles.heading}>
        <h1>Atur ulang kata sandi telah terkirim!</h1>
      </div>
      <div className={styles.banner}>
        <img src="/assets/media/others/verified.png" alt="" />
      </div>
      <div className={styles.fieldInfo}>
        <h5>Email Anda</h5>
        <div className={styles.fieldCard}>
          <span>{email}</span>
        </div>
      </div>
      <div className={styles.note}>
        <p>Silakan periksa Email yang Anda daftarkan untuk melihat instruksi lebh lanjut</p>
      </div>
      <MainButton
        type="button"
        onClick={() => router.push("/login")}
      >
        Kembali
      </MainButton>
    </div>
  )
}

export default Success
