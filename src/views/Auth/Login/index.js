import React, { useEffect, useState } from 'react'
import TextField from '../../../components/Form/TextField'
import AuthLayout from '../../../layouts/AuthLayout'
import styles from './login.module.scss'

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import authService from '../../../axios/services/authService';
import { useToasts } from 'react-toast-notifications';
import MainButton from '../../../components/Form/MainButton';
import { authActions } from '../../../redux/actions/authActions';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import Modal from '../../../components/Modal';
import { CloseCircle } from '../../../assets/icons';

const schema = yup.object().shape({
  code: yup.string().required('Kode perusahaan tidak boleh kosong'),
  email: yup.string()
    .required('Email tidak boleh kosong')
    .email('Format email salah!, Contoh: email@dipay.com'),
  password: yup.string().required('Password tidak boleh kosong'),
})

const Login = () => {
  const dispatch = useDispatch()
  const { addToast } = useToasts();
  const [showForgotId, setShowForgotId] = useState(false);
  const [loading, setLoading] = useState(false)

  const { register, handleSubmit, errors, setValue, unregister, watch, formState: { isValid } } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      code: '',
    }
  });

  const { code } = watch(["code"])

  useEffect(() => {
    register('code')
    return () => {
      unregister('code')
    }
  }, [register, unregister])

  const onSubmit = (values) => {
    setLoading(true)
    authService.login(values)
      .then(({ data: { user, company, accessToken, refreshToken, signature } }) => {
        dispatch(authActions.loginRegister(user, company, accessToken, refreshToken, false, signature));
      })
      .catch((e) => {
        setLoading(false)
        const message = e?.response?.data?.message ?? `${e}`
        addToast(message, { appearance: 'danger' });
      })
  }

  return (
    <>
      <AuthLayout>
        <div className={styles.login}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className={styles.formGroup}>
              <TextField
                label="ID Perusahaan"
                placeholder="Masukkan ID perusahaan"
                error={errors.code?.message}
                helperText={errors.code?.message}
                value={code}
                onChange={e => setValue('code', e.target.value.toUpperCase(), { shouldValidate: true })}
              />
              <TextField
                ref={register}
                name="email"
                label="Email"
                placeholder="Masukan email"
                error={errors.email?.message}
                helperText={errors.email?.message}
              />
              <TextField
                password
                ref={register}
                name="password"
                label="Kata Sandi"
                placeholder="Kata sandi (minimal 8 karakter)"
                error={errors.password?.message}
                helperText={errors.password?.message}
              />
            </div>
            <MainButton
              type="submit"
              disabled={!isValid}
              loading={loading}
            >
              Masuk
            </MainButton>
          </form>
          <div className={styles.bottom}>
            <h6>
              Belum memiliki akun Dipay Disbursement? <Link to="/register">Daftar di sini</Link>
            </h6>
            <h6>
              <Link to="/forgot-password">
                Lupa Kata Sandi?
              </Link>
              {" "}
              atau
              {" "}
              <button
                type="button"
                onClick={() => setShowForgotId(true)}
              >
                Lupa ID Perusahaan?
              </button>
            </h6>
          </div>
        </div>
      </AuthLayout>
      <Modal in={showForgotId} onClose={() => setShowForgotId(false)}>
        <div className={styles.modalCard}>
          <div className={styles.header}>
            <h1>Lupa ID Perusahaan?</h1>
            <button onClick={() => setShowForgotId(false)}>
              <CloseCircle />
            </button>
          </div>
          <div className={styles.infoWrapper}>
            <p>
              Jika Anda lupa ID Perusahaan, silakan hubungi kami di
              {" "}
              <a href="mailto:enterprise@dipay.id">enterprise@dipay.id</a>
            </p>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default Login
