import React, { useEffect, useState } from 'react'
import TextField from '../../../../components/Form/TextField'
import MainButton from '../../../../components/Form/MainButton'
import styles from './companyDetail.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import ImageField from '../../../../components/Form/ImageFieldNew';
import registerService from '../../../../axios/services/registerService'
import { useDispatch } from "react-redux";
import { authActions } from '../../../../redux/actions/authActions'
import { useToasts } from 'react-toast-notifications'
import OnboardLayout from '../../../../layouts/OnBoardLayout';
import SelectField from '../../../../components/Form/SelectField';
import useAsync from '../../../../components/hooks/useAsync';
import optionsService from '../../../../axios/services/optionsService';
import TooltipTemplate from '../../../../components/templates/TooltipTemplate';
import AgreementCheckbox from '../../../../components/Form/AgreementCheckbox';
import Modal from '../../../../components/ModalNew';
import TncTemplate from '../../../../components/templates/TncTemplate';

const schema = yup.object().shape({
  logo: yup.mixed(),
  name: yup.string().required('Nama perusahaan tidak boleh kosong'),
  code: yup.string()
    .required('ID perusahaan tidak boleh kosong')
    .matches(/^(\S+$)/g, "Tidak boleh mengandung spasi")
    .matches(/^[a-zA-Z0-9]+$/g, "Tidak boleh mengandung simbol")
    .min(2, 'Minimal 2 karakter')
    .max(15, 'Maksimal 15 karakter'),
  transactionVolume: yup.string().required('Volume Transaksi tidak boleh kosong'),
  formOfBusinessEntity: yup.string().required('Bentuk Badan Usaha tidak boleh kosong'),
  industry: yup.string().required('Industri tidak boleh kosong'),
  department: yup.string().required('Departemen perusahaan tidak boleh kosong'),
  title: yup.string().required('Jabatan perusahaan tidak boleh kosong')
})

const CompanyDetail = ({ data, signature }) => {
  const { register, unregister, watch, handleSubmit, errors, formState: { isValid }, setValue } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema)
  });
  const dispatch = useDispatch();
  const { addToast } = useToasts();
  const [agree, setAgree] = useState(false);
  const [loading, setLoading] = useState(false);
  const [showTnc, setShowTnc] = useState(false);

  const {
    logo,
    code,
    transactionVolume,
    formOfBusinessEntity,
    industry,
    department,
    title
  } = watch([
    "logo",
    "code",
    "transactionVolume",
    "formOfBusinessEntity",
    "industry",
    "department",
    "title"
  ])

  const onSubmit = (values) => {
    const allData = {
      ...values,
      ...data,
      role: 'SUPERVISOR',
      logo
    }

    setLoading(true)
    registerService.register(allData, signature)
      .then(({ data: { user, company, accessToken, refreshToken } }) => {
        dispatch(authActions.loginRegister(user, company, accessToken, refreshToken, true));
      })
      .catch((e) => {
        if (e.response?.body?.message === "Connection Timed Out") {
          addToast("File Gambar terlalu besar, Maksimal 2MB!", { appearance: 'danger' })
        }
        if (e.response?.data?.message) {
          addToast(e.response.data.message, { appearance: 'danger' })
        }
      })
      .finally(() => setLoading(false))
  }

  const {
    trxVolumePending,
    value: { data: { trxVolumes = [] } = {} } = {}
  } = useAsync(optionsService.monthlyTransactionVolumeList, {});

  const {
    businessIndustryPending,
    value: { data: { industries = [] } = {} } = {}
  } = useAsync(optionsService.businessIndustryList, {});

  // const {
  //   businessEntityPending,
  //   value: { data: { businessEntities = [] } = {} } = {},
  // } = useAsync(optionsService.businessEntityList, {});

  const {
    jobDepartmentPending,
    value: { data: { departments = [] } = {} } = {}
  } = useAsync(optionsService.jobDepartmentList, {});

  const {
    jobPositionPending,
    value: { data: { positions = [] } = {} } = {}
  } = useAsync(optionsService.jobPositionList, {});

  useEffect(() => {
    register("logo");
    register("code");
    register("transactionVolume");
    register("formOfBusinessEntity");
    register("industry");
    register("department");
    register("title");

    return () => {
      unregister("logo");
      unregister("code");
      unregister("transactionVolume");
      unregister("formOfBusinessEntity");
      unregister("industry");
      unregister("department");
      unregister("title");
    }
  }, [register, unregister])

  return (
    <>
      <OnboardLayout>
        <div className={styles.companyDetail}>
          <div className={styles.heading}>
            <h1>Hampir Selesai!</h1>
            <p>Silakan masukkan detail perusahaan Anda</p>
          </div>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className={styles.logo}>
              <ImageField
                name="logo"
                maxSize={512000}
                error={errors.logo}
                onChange={(v) => setValue("logo", v, { shouldValidate: true })}
                helperText={errors.logo ? errors.logo?.message : "Maksimal ukuran 512KB. Direkomendasikan logo 1:1 (persegi)"}
              />
            </div>
            <div className={styles.formWrapper}>
              <TextField
                ref={register}
                name="name"
                label="Nama Perusahaan"
                placeholder="Masukkan nama perusahaan"
                error={errors.name}
                helperText={errors.name?.message}
              />
              <div className={styles.formGroup}>
                <TextField
                  name="code"
                  label="ID Perusahaan"
                  placeholder="Maksimal 15 karakter tanpa simbol/spasi"
                  error={errors.code}
                  helperText={errors.code?.message}
                  className="mb-6"
                  maxLength={15}
                  value={code}
                  onChange={e => setValue('code', e.target.value.toUpperCase(), { shouldValidate: true })}
                />
                <TooltipTemplate
                  label="Apa itu ID perusahaan?"
                  heading="Mengenai ID Perusahaan"
                  content="Pastikan ID Perusahaan Dipay Disbursement yang digunakan mudah diingat dan berhubungan dengan usaha/diri Anda karena akan digunakan untuk masuk ke akun Dipay Disbursement Anda."
                />
              </div>
              <SelectField
                options={trxVolumes}
                label="Volume Transaksi Per Bulan"
                placeholder="Pilih volume transaksi per bulan"
                name="transactionVolume"
                value={transactionVolume}
                loading={trxVolumePending}
                error={errors.transactionVolume}
                helperText={errors.transactionVolume?.message}
                onChange={(v) => {
                  setValue("transactionVolume", v?.value, { shouldValidate: true })
                }}
              />
              <SelectField
                // options={businessEntities}
                options={[
                  {
                    value: "PT",
                    label: "PT"
                  }
                ]}
                label="Bentuk Badan Usaha"
                placeholder="Pilih bentuk badan usaha"
                name="formOfBusinessEntity"
                value={formOfBusinessEntity}
                // loading={businessEntityPending}
                error={errors.formOfBusinessEntity}
                helperText={errors.formOfBusinessEntity?.message}
                onChange={(v) => {
                  setValue("formOfBusinessEntity", v?.value, { shouldValidate: true })
                }}
              />
              <SelectField
                options={industries}
                label="Industri"
                placeholder="Pilih industri"
                name="industry"
                searchable
                value={industry}
                loading={businessIndustryPending}
                error={errors.industry}
                helperText={errors.industry?.message}
                onChange={(v) => {
                  setValue("industry", v?.value, { shouldValidate: true })
                }}
              />
              <SelectField
                options={departments}
                label="Departemen Saya"
                placeholder="Pilih Departemen"
                name="department"
                value={department}
                loading={jobDepartmentPending}
                error={errors.department}
                helperText={errors.department?.message}
                onChange={(v) => {
                  setValue("department", v?.value, { shouldValidate: true })
                }}
              />
              <SelectField
                options={positions}
                label="Jabatan Saya"
                placeholder="Pilih jabatan"
                name="title"
                value={title}
                loading={jobPositionPending}
                error={errors.title}
                helperText={errors.title?.message}
                onChange={(v) => {
                  setValue("title", v?.value, { shouldValidate: true })
                }}
              />
            </div>

            <div className={styles.agreement}>
              <AgreementCheckbox
                onAgree={() => setAgree(!agree)}
                isAgree={agree}
                disabled={!isValid}
                label={
                  <>
                    Dengan mendaftar, saya menyetujui
                    {" "}
                    <button type="button" style={{ display: "inline-block" }} onClick={() => setShowTnc(true)}><span className="text-primary">
                      Syarat dan Ketentuan
                    </span></button>
                    {" "}
                    penggunaan Dipay Disbursement
                  </>
                }
              />
            </div>

            <MainButton
              disabled={!agree}
              loading={loading}
            >
              Lanjutkan
            </MainButton>
          </form>
        </div >
      </OnboardLayout>
      <Modal in={showTnc} onClose={() => setShowTnc(false)}>
        <TncTemplate />
      </Modal>
    </>
  )
}

export default CompanyDetail
