
import React, { useCallback, useEffect, useState } from 'react'
import registerService from '../../../../axios/services/registerService'
import Button from '../../../../components/Button'
import OTPInput from '../../../../components/Form/OTPInput'
import MainButton from '../../../../components/Form/MainButton'
import OnboardLayout from '../../../../layouts/OnBoardLayout'
import timeConvert from '../../../../utils/helpers/timeConverter'
import styles from './emailVerification.module.scss'
import { Warning2 } from '../../../../assets/icons'
import { decryptCode } from '../../../../utils/helpers/decryptCode'

const EmailVerification = ({ setStep, data, signature, setSignature }) => {
  const [value, setValue] = useState('');
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  const [resendTimer, setResendTimer] = useState(120);
  const [expiredCode, setExpiredCode] = useState(300)
  const [resend, setResend] = useState(true)

  const validateOtp = useCallback(() => {
    setLoading(true)
    registerService.validateOtp({
      otp: value
    }, signature)
      .then(({ data: { signature, code } }) => {
        if (decryptCode(code) === '200') {
          setStep(3);
          setSignature(signature)
        }
      })
      .catch((err) => {
        if (err.response) {
          if (err.response.body) {
            setError(err.response.body.message);
          }
          if (err.response.data) {
            for (const fields of err.response.data.message) {
              for (const message of Object.keys(fields.constraints)) {
                setError(fields.constraints[message]);
              }
            }
          }
        } else {
          setError(err);
        }
      })
      .finally(() => setLoading(false))
  }, [setSignature, setStep, value, signature, setLoading])

  const resendOTP = () => {
    setLoading(true)
    registerService.requestOtp({
      email: data.email
    }).then(({ data: { signature } }) => {
      setSignature(signature)
      setResend(true);
      setResendTimer(120);
      setExpiredCode(300);
    })
      .catch((err) => {
        if (err.response) {
          if (err.response.body) {
            setError(err.response.body.message);
          }
          if (err.response.data) {
            for (const fields of err.response.data.message) {
              for (const message of Object.keys(fields.constraints)) {
                setError(fields.constraints[message]);
              }
            }
          }
        } else {
          setError(err);
        }
      })
      .finally(() => setLoading(false))
  }

  useEffect(() => {
    const tryAgain = setInterval(() => {
      if (resendTimer) setResendTimer(prev => prev - 1)
      else {
        clearInterval(tryAgain)
        setResend(false)
      }
    }, 1000);
    return () => {
      clearInterval(tryAgain)
    }
  }, [resendTimer, setResendTimer, setResend])

  useEffect(() => {
    const tryAgain = setInterval(() => {
      if (expiredCode) setExpiredCode(prev => prev - 1)
      else {
        clearInterval(tryAgain)
      }
    }, 1000);
    return () => {
      clearInterval(tryAgain)
    }
  }, [expiredCode, setExpiredCode, setResend])

  return (
    <OnboardLayout>
      <div className={styles.emailVerification}>
        <div className={styles.headingText}>
          <h5>Verifikasi Email telah dikirim!</h5>
          <p>Dipay Disbursement telah mengirimkan kode verifikasi email ke</p>
          <h6>{data.email}</h6>
        </div>
        <div className={styles.image}>
          <img
            src="/assets/media/others/otp-email.png"
            alt=""
          />
        </div>
        {!expiredCode &&
          <div className={styles.resendAlert}>
            <Warning2 />
            <div className={styles.message}>
              <p>Waktu verifikasi email Anda telah habis, silakan kirim ulang kode verifikasi email.</p>
            </div>
          </div>
        }
        <div className={styles.input}>
          <OTPInput
            length={6}
            numberOnly
            value={value}
            onChange={v => {
              if (error) setError('')
              setValue(v)
            }}
            error={error}
          />
        </div>
        <div className={styles.note}>
          <span>
            Silakan cek inbox Anda dan masukkan kode verifikasi email
          </span>
        </div>
        <div className={styles.buttonWrapper}>
          <MainButton
            onClick={validateOtp}
            disabled={!(value.length === 6)}
            loading={loading}
          >
            Selanjutnya
          </MainButton>
        </div>
        {resendTimer ?
          <div className={styles.resend}>
            <span>Kirim ulang dalam <b>{resend && ` (${timeConvert(resendTimer)})`}</b></span>
          </div>
          :
          <div className={styles.timer}>
            <span>Belum menerima email?</span>
            <Button
              onClick={resendOTP}
            >
              Kirim Ulang
            </Button>
          </div>
        }
      </div>
    </OnboardLayout>
  )
}

export default EmailVerification
