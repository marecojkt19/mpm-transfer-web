import React, { useMemo, useState } from 'react'
import { Helmet } from 'react-helmet'
import CompanyDetail from './CompanyDetail'
import CreateNewAccount from './CreateNewAccount'
import EmailVerification from './EmailVerification'

const Onboarding = () => {
  const [data, setData] = useState({})
  const [step, setStep] = useState(1)
  const [signature, setSignature] = useState();

  const renderView = useMemo(() => {
    if (step === 1) return (
      <CreateNewAccount
        setStep={setStep}
        setData={setData}
        setSignature={setSignature}
      />
    )
    else if (step === 2) return (
      <EmailVerification
        data={data}
        setStep={setStep}
        setSignature={setSignature}
        signature={signature}
      />
    )
    else if (step === 3) return (
      <CompanyDetail
        setStep={setStep}
        signature={signature}
        data={data}
      />
    )
  }, [step, data, signature])

  return (
    <>
      <Helmet title="Daftar" />
      {renderView}
    </>
  )
}

export default Onboarding
