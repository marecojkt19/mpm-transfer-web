import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import MainButton from '../../../../components/Form/MainButton'
import OnboardLayout from '../../../../layouts/OnBoardLayout';
import { authActions } from '../../../../redux/actions/authActions';
import styles from './success.module.scss'

const RegistrationSuccess = () => {
  const {
    company
  } = useSelector(state => state.auth);
  const dispatch = useDispatch();

  return (
    <OnboardLayout>
      <div className={styles.success}>
        <div className={styles.heading}>
          <h1>Akun Anda berhasil dibuat</h1>
        </div>
        <div className={styles.banner}>
          <img src="/assets/media/others/verified.png" alt="" />
        </div>
        <div className={styles.fieldInfo}>
          <h5>ID Perusahaan Anda</h5>
          <div className={styles.fieldCard}>
            <span>{company?.code}</span>
          </div>
        </div>
        <div className={styles.note}>
          <p>
            ID perusahaan diperlukan saat masuk ke akun, kami juga mengirimkan kode ini ke alamat email Anda yang terdaftar.
          </p>
        </div>
        <MainButton
          onClick={() => dispatch(authActions.setIsNewRegistered(false))}
        >
          Masuk ke Beranda
        </MainButton>
      </div>
    </OnboardLayout>
  )
}

export default RegistrationSuccess
