import React, { useState } from 'react'
import TextField from '../../../../components/Form/TextField'
import AuthLayout from '../../../../layouts/AuthLayout'
import styles from './newAccount.module.scss'
import { useToasts } from 'react-toast-notifications';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import registerService from '../../../../axios/services/registerService';
import MainButton from '../../../../components/Form/MainButton';
import { Link } from 'react-router-dom';

const schema = yup.object().shape({
  fullName: yup.string().required('Nama lengkap tidak boleh kosong'),
  email: yup.string()
    .required('Email tidak boleh kosong')
    .email('Format email salah!, Contoh: email@dipay.com'),
  password: yup
    .string()
    .required('Kata sandi tidak boleh kosong')
    .matches(
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?([^\w\s]|[_])).{8,}$/,
      'Pastikan kata sandi terdiri dari 8 huruf dengan gabungan simbol, angka, dan kapital!'
    ),
  passwordConfirmation: yup
    .string()
    .required('Kata sandi tidak boleh kosong')
    .test(
      'password',
      "Konfirmasi kata sandi tidak sesuai",
      (value, context) => value === context.parent.password)
})

const CreateNewAccount = ({
  setData,
  setStep,
  setSignature
}) => {
  const [loading, setLoading] = useState(false);
  const { register, handleSubmit, errors, formState: { isValid } } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema)
  });

  const { addToast } = useToasts();

  const onSubmit = (values) => {
    setLoading(true)
    registerService.requestOtp({
      email: values.email
    })
      .then(({ data: { signature } }) => {
        setData(prev => ({ ...prev, ...values }))
        setStep(2)
        setSignature(signature)
      })
      .catch((err) => {
        if (err) {
          if (err.response) {
            if (err.response.body) {
              addToast(err.response.body.message, { appearance: 'danger' });
            }
            if (err.response.data) {
              if (typeof err.response.data.message === "string") {
                addToast(err.response.data.message, { appearance: 'danger' });
              } else {
                for (const fields of err.response.data.message) {
                  for (const message of Object.keys(fields.constraints)) {
                    addToast(fields.constraints[message], { appearance: 'danger' });
                  }
                }
              }
            }
          } else {
            addToast(err, { appearance: 'danger' });
          }
        }
      })
      .finally(() => setLoading(false))
  }

  return (
    <AuthLayout>
      <div className={styles.newAccount}>
        <div className={styles.heading}>
          <h5>Buat Akun Baru</h5>
        </div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className={styles.formGroup}>
            <TextField
              ref={register}
              name="fullName"
              label="Nama"
              placeholder="Nama lengkap Anda sesuai kartu identitas"
              error={errors.fullName?.message}
              helperText={errors.fullName?.message}
            />
            <TextField
              ref={register}
              name="email"
              label="Email"
              placeholder="Masukkan email"
              error={errors.email?.message}
              helperText={errors.email?.message}
            />
            <TextField
              ref={register}
              name="password"
              label="Kata Sandi"
              placeholder="Kata sandi (minimal 8 karakter)"
              error={errors.password?.message}
              helperText={errors.password?.message}
              password
            />
            <TextField
              ref={register}
              name="passwordConfirmation"
              label="Konfirmasi Kata Sandi"
              placeholder="Kata sandi (minimal 8 karakter)"
              error={errors.passwordConfirmation?.message}
              helperText={errors.passwordConfirmation?.message}
              password
            />
          </div>
          <MainButton
            type="submit"
            disabled={!isValid}
            loading={loading}
          >
            Daftar Sekarang
          </MainButton>
        </form>
        <div className={styles.bottom}>
          <h6>
            Sudah memiliki akun Dipay Disbursement? <Link to="/login">Masuk Di sini</Link>
          </h6>
        </div>
      </div>
    </AuthLayout>
  )
}

export default CreateNewAccount
