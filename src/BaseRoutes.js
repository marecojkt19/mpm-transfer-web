import React, { useCallback, useEffect, useState } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import MainRoutes from "./MainRoutes";
import AuthRoutes from "./AuthRoutes";
import Logout from "./views/Auth/Logout";
import { createTransactionAction } from "./redux/actions/createTransactionAction";
import { useToasts } from 'react-toast-notifications';
import { OrderStatusTypes } from "./utils/enums/orderStatusTypes";
import { authActions } from "./redux/actions/authActions";
import Maintenance from "./views/Maintenance";
// import axios from "axios";

export default function BaseRoutes() {
  const { authToken, company } = useSelector(state => state.auth);
  const { socket } = useSelector(state => state.common);
  const { addToast } = useToasts();
  const dispatch = useDispatch();
  const [isMaintenance] = useState(false);
  const { user } = useSelector(state => state.auth);

  const notificationToast = useCallback((status) => {
    if (status && (company?.name === status?.company?.name)) {
      let statusOrderStr;
      if (status?.order?.status === OrderStatusTypes.PENDING) {
        statusOrderStr = OrderStatusTypes.getStr(OrderStatusTypes.PENDING)
      } else if (status?.order?.status === OrderStatusTypes.PROCESSING) {
        statusOrderStr = OrderStatusTypes.getStr(OrderStatusTypes.PROCESSING)
      } else if (status?.order?.status === OrderStatusTypes.COMPLETED) {
        statusOrderStr = OrderStatusTypes.getStr(OrderStatusTypes.COMPLETED)
      }

      addToast(`1 Transaksi ${statusOrderStr}`, { appearance: 'success', autoDismiss: true })
    }
  }, [addToast, company])

  useEffect(() => {
    socket.on('GENERATE_XLSX_ORDER', (xlsx) => {
      dispatch(createTransactionAction.loaded(xlsx))
    })

    socket.on('UPDATE_STATUS_ORDER', (status) => {
      notificationToast(status)
    })

    return () => {
      socket.off('GENERATE_XLSX_ORDER');
      socket.off('UPDATE_STATUS_ORDER');
    };

  }, [socket, dispatch, notificationToast])

  useEffect(() => {
    if (user?._id) {
      socket.on(`UPLOAD_TRX_EXCEL_COMPLETE_${user?._id}`, ({ transactionId }) => {
        dispatch(createTransactionAction.loaded(transactionId));
        dispatch(createTransactionAction.setErrorUpload(false));
      })
    }
  }, [socket, dispatch, user?._id])

  useEffect(() => {
    socket.on('UPDATE_BALANCE', ({ companyCode }) => {
      if (companyCode === company?.code) {
        dispatch(authActions.requestUser());
      }
    })

    return () => {
      socket.off('UPDATE_BALANCE');
    };

  }, [socket, dispatch, notificationToast, company])

  useEffect(() => {
    socket.on('VERIFICATION_COMPANY', ({ companyCode }) => {
      if (companyCode === company?.code) {
        dispatch(authActions.requestUser());
      }
    })

    return () => {
      socket.off('VERIFICATION_COMPANY');
    };

  }, [socket, dispatch, notificationToast, company])

  useEffect(() => {
    socket.on('UPDATE_STATUS_ORDER', ({ companyCode }) => {
      if (companyCode === company?.code) {
        dispatch(authActions.requestUser());
      }
    })

    return () => {
      socket.off('UPDATE_STATUS_ORDER');
    };

  }, [socket, dispatch, notificationToast, company])

  useEffect(() => {
    if (user?._id) {
      socket.on(`UPLOAD_TRX_EXCEL_PROGGRESS_${user?._id}`, ({ precentageProggress, transactionId, companyCode }) => {
        dispatch(createTransactionAction.setErrorUpload(false));
        dispatch(createTransactionAction.createTransactionProgress({
          precentageProggress,
          transactionId,
          companyCode
        }))
      })
    }
  }, [socket, dispatch, user])

  useEffect(() => {
    if (user?._id) {
      socket.on(`ESTIMATE_TIME_UPLOAD_${user?._id}`, (data) => {
        dispatch(createTransactionAction.setEstimateTime(`${timeConvert(data.time ?? 0)}`));
      })
    }
  }, [socket, dispatch, user])

  useEffect(() => {
    socket.on('UPDATE_NOTIF_COMPANY', () => {
      dispatch(authActions.requestUser());
    })

    return () => {
      socket.off('UPDATE_NOTIF_COMPANY');
    };

  }, [socket, dispatch])

  // useEffect(() => {
  //   axios.get("https://app.dipay.id/maintenance-check")
  //     .then(({ maintenance }) => setIsMaintenance(maintenance))
  //     .catch(() => setIsMaintenance(false))
  // }, [])

  return (
    <>
      <Switch>
        <Route exact path="/logout" component={Logout} />
        <Route>
          {
            !isMaintenance ?
              authToken
                ?
                <MainRoutes />
                :
                <AuthRoutes />
              :
              <Switch>
                <Route path="/maintenance" component={Maintenance} />
                <Redirect to="/maintenance" />
              </Switch>
          }
        </Route>
      </Switch>
    </>
  );
}

function timeConvert(n) {
  let num = n;
  let hours = (num / 60);
  let rhours = Math.floor(hours);
  let minutes = (hours - rhours) * 60;
  let rminutes = Math.round(minutes);
  if (rhours && rminutes) return rhours + " Jam dan " + rminutes + " Menit";
  return rminutes + " menit";
}
