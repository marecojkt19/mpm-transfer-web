import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Login from "./views/Auth/Login";
import Onboarding from "./views/Auth/Onboarding";
import ResetPassword from "./views/Auth/ResetPassword";
import NewPassword from "./views/Auth/ResetPassword/NewPassword";

export default function AuthRoutes() {
  return (
    <Switch>
      <Route path="/register" component={Onboarding} />
      <Route path="/login" component={Login} />
      <Route path="/forgot-password" component={ResetPassword} />
      <Route path="/user/new-password" component={NewPassword} />
      <Redirect to="/login" />
    </Switch>
  );
}
