echo "-------prepare--------"
if [ $1 == "prod" ]; then
    rm -rf .firebase
    cp firebase.prod.json firebase.json
    firebase use --add $2 --token $FIREBASE_TOKEN
elif [ $1 == "stage" ]; then
    rm -rf .firebase
    cp firebase.stage.json firebase.json
    firebase use --add $2 --token $FIREBASE_TOKEN
else
    rm -rf .firebase
    cp firebase.dev.json firebase.json
    firebase use --add $2 --token $FIREBASE_TOKEN
fi
firebase deploy --non-interactive --only hosting:$3
echo "-------success--------"
